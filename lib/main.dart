// ignore_for_file: always_use_package_imports

import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
// import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'components/connectivity.dart';
import 'imports.dart';
import 'modules/auth/pages/home.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //final WidgetsBinding widgetsBinding =
  // WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky);
  // SystemChrome.setSystemUIOverlayStyle(
  //   SystemUiOverlayStyle(
  //     statusBarColor: Colors.transparent,
  //   ),
  //);
  // FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);

  await Firebase.initializeApp();
  timeago.setLocaleMessages('ar', timeago.ArMessages());
  timeago.setLocaleMessages('fr', timeago.FrMessages());
  timeago.setLocaleMessages('ru', timeago.RuMessages());
  timeago.setLocaleMessages('zh', timeago.ZhMessages());
  timeago.setLocaleMessages('de', timeago.DeMessages());
  timeago.setLocaleMessages('ko', timeago.KoMessages());
  timeago.setLocaleMessages('ja', timeago.JaMessages());
  timeago.setLocaleMessages('it', timeago.ItMessages());
  timeago.setLocaleMessages('id', timeago.IdMessages());
  timeago.setLocaleMessages('tr', timeago.TrMessages());
  timeago.setLocaleMessages('hi', timeago.HiMessages());
  timeago.setLocaleMessages('fa', timeago.FaMessages());
  timeago.setLocaleMessages('es', timeago.EsMessages());

  // await JustAudioBackground.init(
  //   androidNotificationChannelId: 'com.ryanheise.bg_demo.channel.audio',
  //   androidNotificationChannelName: 'Audio playback',
  //   androidNotificationOngoing: true,
  // );

  // LocaleSettings.useDeviceLocale();
  //Load App Configs
  await AppConfigs.init();
  // Initialize app preferences and settings
  await AppPreferences.init();
  // Initialize Authentication, check if user logged In
  await AuthProvider.init();

  runApp(TranslationProvider(child: App()));
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  //mkmkmk uncomment
  //final ads = Get.put(AdsHelper());

  @override
  void dispose() {
    //mkmkmk uncomment
    // ads.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => GetMaterialApp(
        debugShowCheckedModeBanner: false,
        title: t.AppName,
        builder: (_, w) => ConnectivityWidget(
          builder: (_, __) => BotToastInit()(_, w),
        ),
        navigatorObservers: [
          BotToastNavigatorObserver(),
          appPrefs.routeObserver,
        ],
        themeMode: appPrefs.isDarkMode() ? ThemeMode.dark : ThemeMode.light,
        theme: AppStyles.lightTheme,
        darkTheme: AppStyles.darkTheme,
        home: HomePage(),
        localizationsDelegates: const [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: LocaleSettings.supportedLocales,
      ),
    );
  }
}
