// ignore_for_file: always_use_package_imports

import 'package:shared_preferences/shared_preferences.dart';

import '../imports.dart';

AppPreferences get appPrefs => Get.find();

class AppPreferences {
  final routeObserver = RouteObserver<PageRoute>();
  AppPage page = AppPage.others();

  final SharedPreferences prefs;
  AppPreferences._(this.prefs);
  static Future<void> init() async {
    final p = AppPreferences._(await SharedPreferences.getInstance());
    final appLocale =
        StringAppLocaleExtensions(p.prefs.getString(APP_LANGUAGE) ?? 'en')
            .toAppLocale()!;
    LocaleSettings.setLocale(appLocale);
    Get.updateLocale(appLocale.flutterLocale);

    p.isDarkMode(p.prefs.getBool(DARK_MODE_KEY) ?? true);
    Get.put(p);
  }

  static const APP_LANGUAGE = 'APP_LANGUAGE';

  static const DARK_MODE_KEY = 'DARK_MODE_KEY';
  final isDarkMode = false.obs;

  void changeAppLanguage(String lang) {
    prefs.setString(APP_LANGUAGE, lang);
  }

  void changeThemeMode() {
    isDarkMode.toggle();
    prefs.setBool(DARK_MODE_KEY, isDarkMode());
  }

  final drawerState = DrawerState.GROUPS.obs;
}

enum DrawerState {
  GROUPS,
  Notifications,
}
