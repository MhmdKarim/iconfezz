// ignore_for_file: always_use_package_imports

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:get/get.dart';

import '../components/people_list.dart';
import '../modules/admin/admin.dart';
import '../modules/auth/pages/email/index.dart';
import '../modules/auth/pages/phone/index.dart';
import '../modules/auth/pages/profile/editor.dart';
import '../modules/auth/pages/profile/index.dart';
import '../modules/auth/pages/register/index.dart';
import '../modules/chat/models/group.dart';
import '../modules/chat/pages/chats/index.dart';
import '../modules/chat/pages/chats/users_search.dart';
import '../modules/chat/pages/conversation/group.dart';
import '../modules/chat/pages/conversation/private.dart';
import '../modules/chat/pages/groups/editor.dart';
import '../modules/chat/pages/groups/group_info.dart';
import '../modules/chat/pages/groups/members.dart';
import '../modules/chat/pages/groups/search.dart';
import '../modules/feeds/models/comment.dart';
import '../modules/feeds/models/post.dart';
import '../modules/feeds/pages/comments/index.dart';
import '../modules/feeds/pages/comments/reply/index.dart';
import '../modules/feeds/pages/posts/editor.dart';
import '../modules/feeds/pages/posts/post.dart';
import '../modules/feeds/pages/posts/report.dart';
import '../modules/settings/pages/about.dart';
import '../modules/settings/pages/app_language.dart';
import '../modules/settings/pages/coming_soon.dart';
import '../modules/settings/pages/community_guidelines.dart';
import '../modules/settings/pages/faq.dart';
import '../modules/settings/pages/privacy.dart';
import '../modules/settings/pages/report_problem.dart';
import '../modules/settings/pages/terms.dart';
import '../modules/settings/settings.dart';

//import 'ads/index.dart';

part 'router.freezed.dart';

mixin AppNavigator {
  static var _page = AppPage.others();
  static AppPage get page => _page;

  //Auth Module
  static Future<void>? toPhoneLogin() => Get.to(
        () => GetPlatform.isMacOS && !GetPlatform.isWeb
            ? EmailLoginPage()
            : PhoneLoginPage(),
      );
  static Future<void>? toEmailPage() => Get.to(() => EmailLoginPage());

  static Future<void>? toEditProfile() => Get.to(() => EditProfilePage());
  static Future<void>? toProfile([String? userID]) =>
      Get.to(() => ProfilePage(userID: userID), preventDuplicates: false);
  static Future<void>? toRegister() => Get.to(() => RegisterPage());

  //Feed Module
  static Future<void> toSinglePost(String id) async =>
      Get.to(() => PostPage(id));
  static Future<void> toComments(Post post) async {
    _page = AppPage.commenting(post.id);
    await Get.to(() => CommentsPage(post));
    _page = AppPage.others();
  }

  static Future<void>? toPostEditor([Post? toEdit]) =>
      Get.to(() => PostEditorPage(toEditPost: toEdit));

  static Future<void>? toReplies(Comment comment) =>
      Get.to(() => ReplyPage(comment));
  static Future<void>? toPeopleList(List<String> iDs, String title) =>
      Get.to(() => PeopleListPage(iDs, title));
  static void toReport(Post? post) => Get.to(() => ReportPage(post));

  //Chat Module
  static Future<void> toChats() async {
    //mkmkmk uncomment
    //Get.find<AdsHelper>().loadFullAds();
    await Get.to(() => ChatsPage());
    //mkmkmk uncomment
    // Get.find<AdsHelper>().showFullAds();
  }

  static Future<void> toPrivateChat(String userId) async {
    _page = AppPage.chatting(userId);
    await Get.to(() => PrivateChatPage(userId));
    _page = AppPage.others();
  }

  static void toUsersSearchPage() => Get.to(() => UsersSearchPage());

  //Groups Module
  static Future<void> toGroupChat(String id) async {
    //mkmkmk uncomment
    // Get.find<AdsHelper>().loadFullAds();
    _page = AppPage.groupChatting(id);
    await Get.to(() => GroupChatPage(id));
    _page = AppPage.others();
    //mkmkmk uncomment
    //Get.find<AdsHelper>().showFullAds();
  }

  static Future<void> toGroupEditor([Group? toEdit]) async =>
      Get.to(() => GroupEditor(toEditGroup: toEdit));
  static Future<void> toGroupInfo(Group group) async =>
      Get.to(() => GroupInfoPage(group));
  static Future<void> toGroupMembers(Group group) async =>
      Get.to(() => GroupMembersPage(group));
  static Future<void> toGroupsSearch() async => Get.to(() => GroupsSearch());

  //
  static Future<void>? toSettings() => Get.to(() => SettingsPage());
  static Future<void>? toAbout() => Get.to(() => AboutPage());
  static Future<void>? toCommunityGuidelines() =>
      Get.to(() => CommunityGuidelinesPage());
  static Future<void>? toFAQ() => Get.to(() => FAQPage());
  static Future<void>? toComingSoon() => Get.to(() => ComingSoonPage());

  static Future<void>? toAdmin() => Get.to(() => AdminPage());
  static Future<void>? toPrivacy() => Get.to(() => PrivacyPage());
  static Future<void>? toTerms() => Get.to(() => TermsPage());
  static Future<void>? toReportProblem() => Get.to(() => ReportProblemPage());
  static Future<void>? toAppLanguage() => Get.to(() => AppLanguagePage());
}

@freezed
class AppPage with _$AppPage {
  const factory AppPage.chatting(String userId) = _Chatting;
  const factory AppPage.groupChatting(String groupId) = _GroupChatting;
  const factory AppPage.commenting(String postID) = _Commenting;
  const factory AppPage.others() = _Others;
}
