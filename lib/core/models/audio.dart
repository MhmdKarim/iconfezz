// ignore_for_file: always_use_package_imports

import 'dart:io';

import 'package:equatable/equatable.dart';

//import 'package:flutter_image_compress/flutter_image_compress.dart';

import 'file.dart';

class AudioModel extends FileModel with EquatableMixin {
  const AudioModel(
    String path, {
    int size = 0,
  }) : super(
          path: path,
          size: size,
        );

  @override
  bool get isSound => true;

  static Future<AudioModel?> create(String path) async {
    if (path.isEmpty) return null;
    final audio = File(path);
    final size = await audio.length();
    return AudioModel(path, size: size);
  }

  factory AudioModel.fromMap(Map<String, dynamic> map) {
    return AudioModel(
      map['path'] as String? ?? '',
      size: map['size'] as int? ?? 0,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'path': path,
      'size': size,
    };
  }

  @override
  bool get stringify => true;
  @override
  List<Object?> get props {
    return [
      path,
      size,
    ];
  }

  AudioModel copyWith({
    String? path,
    int? size,
  }) {
    return AudioModel(
      path ?? this.path,
      size: size ?? this.size,
    );
  }
}
