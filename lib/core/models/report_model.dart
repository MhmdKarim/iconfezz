// // ignore_for_file: always_use_package_imports

// import 'package:equatable/equatable.dart';

// class ReportModel extends Equatable {
//   final String reporterId;
//   final String? reportText;

//   const ReportModel({
//     required this.reporterId,
//     this.reportText,
//   });

//   Map<String, String> toMap() {
//     return {
//       'reporterId': reporterId,
//       'reportText': reportText ?? '',
//     };
//   }

//   factory ReportModel.fromMap(Map<String, String> map) {
//     return ReportModel(
//       reporterId: map['reporterId'] ?? '',
//       reportText: map['reportText'] ?? '',
//     );
//   }

//   @override
//   List<Object?> get props {
//     return [
//       reporterId,
//       reportText,
//     ];
//   }

//   ReportModel copyWith({
//     String? reporterId,
//     String? reportText,
//   }) {
//     return ReportModel(
//       reporterId: reporterId ?? this.reporterId,
//       reportText: reportText ?? this.reportText,
//     );
//   }
// }
