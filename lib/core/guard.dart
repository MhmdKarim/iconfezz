// ignore_for_file: always_use_package_imports

import 'logger.dart';

T? guard<T>(T Function() callback, [T? defaultValue, bool log = true]) {
  T? result;
  try {
    result = callback();
  } catch (e) {
    if (log) logError(e);
  }
  return result ?? defaultValue;
}

Future<T?> asyncGuard<T>(
  Future<T> Function() callback, [
  T? defaultValue,
  bool log = true,
]) async {
  T? result;
  try {
    result = await callback();
  } catch (e) {
    if (log) logError(e);
  }
  return result ?? defaultValue;
}
