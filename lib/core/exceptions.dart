// ignore_for_file: always_use_package_imports

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';

import '../imports.dart';

class NotFoundException implements Exception {
  final dynamic message;
  NotFoundException([this.message]) : super();
}

class AppAuthException implements Exception {
  final String? message;

  AppAuthException(this.message);
  factory AppAuthException.handleError(dynamic e, [StackTrace? s]) {
    String? msg;
    if (e is FirebaseAuthException) {
      msg = e.message;
    } else if (e is PlatformException) {
      if (e.code == 'ERROR_INVALID_VERIFICATION_CODE') {
        msg = t.OopsCode;
      } else {
        msg = e.message;
      }
    } else {
      msg = '${e.message ?? ''}';
    }
    if (msg!.isEmpty) {
      msg = t.OopsNetwork;
    } else if (msg.contains('password is invalid')) {
      msg = t.OopsEmailorPassword;
    } else if (msg.contains('no user record')) {
      msg = t.OopsNotRegister;
    } else if (msg.contains('email address is already')) {
      msg = t.OopsEmailUsed;
    } else if (msg.contains('The format of the phone number')) {
      msg = t.OopsPhoneFormat;
    }
    logError(msg, e, s);
    return AppAuthException(msg);
  }

  @override
  String toString() => message!;
}
