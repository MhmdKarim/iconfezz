// ignore_for_file: depend_on_referenced_packages, always_use_package_imports

import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart' as p;

import '../components/picker.dart';
import '../imports.dart';
import 'models/audio.dart';

class AppUploader with EquatableMixin {
  static final uploaders = <AppUploader>[];
  static AppUploader? find(String path) => uploaders.firstWhereOrNull(
        (e) => e.picked?.path == path,
      );
  FileModel? picked;
  FileModel? uploaded;
  String path([String? or]) => state.maybeWhen(
        orElse: () => picked?.path ?? or ?? '',
      );
  String audioPath([String? or]) => state.maybeWhen(
        orElse: () => uploaded?.path ?? or ?? '',
      );
  final _state = UploadState.initial().obs;
  UploadState get state => _state();

  bool get isPicked =>
      state.maybeMap(orElse: () => picked?.path.isNotEmpty == true);
  bool get isUploading =>
      state.maybeMap(uploading: (_) => true, orElse: () => true);

  Future<void> pick(BuildContext context, [bool? showVideoPicker]) async {
    final res = await pickFile(context, showVideoPicker);
    if (res == null) return;
    picked = res;
    _state(UploadState.picked(res));
    if (find(res.path) != null) return;
    uploaders.add(this);
  }

  Future<void> upload(
    Reference ref, {
    String filename = '',
    ValueChanged<FileModel>? onSuccess,
    ValueChanged<String>? onFailed,
  }) async {
    try {
      if (!isPicked) throw Exception('No Photo Picked'); //mkmkmk
      final url = await uploadFile(
        path(),
        ref,
        (v) => _state(UploadState.uploading(v)),
        filename,
      );

      uploaded = await picked!.when<Future<FileModel?>>(
        image: (v) async {
          final img = await ImageModel.create(v.path);
          return img?.copyWith(path: url);
        },
        orElse: (_) async => null,
      );
      if (uploaded != null) {
        _state(UploadState.success(uploaded!));
        onSuccess?.call(uploaded!);
        clear();
      }
    } catch (e, s) {
      logError(e, '', s);
      _state(UploadState.failed('$e'));
      onFailed?.call('$e');
    }
  }

  Future<void> uploadAudio(
    String path,
    Reference ref, {
    String filename = '',
    ValueChanged<FileModel>? onSuccess,
    ValueChanged<String>? onFailed,
  }) async {
    try {
      if (!isUploading) throw Exception(t.NoAudioRecorded);
      final url = await uploadFile(
        path,
        ref,
        (v) => _state(UploadState.uploading(v)),
        filename,
      );

      uploaded = await uploaded!.when<Future<FileModel?>>(
        audioFile: (v) async {
          final aud = await AudioModel.create(v.path);
          return aud?.copyWith(path: url);
        },
        orElse: (_) async => null,
      );
      if (uploaded != null) {
        _state(UploadState.success(uploaded!));
        onSuccess?.call(uploaded!);
        clear();
      }
    } catch (e, s) {
      logError(e, '', s);
      _state(UploadState.failed('$e'));
      onFailed?.call('$e');
    }
  }

  Future<String> uploadFile(
    String path,
    Reference ref,
    ValueChanged<double> onProgress, [
    String? filename,
  ]) async {
    final file = File(path);
    if (!file.existsSync()) {
      throw Exception(t.CannotFindFile);
    }

    final basename =
        '${authProvider.user?.username ?? ''}-${filename ?? ''}-${DateTime.now().millisecondsSinceEpoch}${p.extension(path)}';
    Stream<TaskSnapshot> snap;
    if (path.endsWith(".m4a")) {
      snap = ref
          .child(basename)
          .putFile(
            file,
            SettableMetadata(
              contentType: 'audio/m4a',
              customMetadata: <String, String>{
                'file': 'audio',
              },
            ),
          )
          .snapshotEvents;
    } else {
      snap = ref.child(basename).putFile(file).snapshotEvents;
    }
    await for (final e in snap) {
      onProgress((e.bytesTransferred / e.totalBytes).toPrecision(2));
      if (e.state == TaskState.success) {
        uploaded = await AudioModel.create(path);
        return e.ref.getDownloadURL();
      }
    }

    throw Exception(t.OopsWrong);
  }

  void reset() {
    _state(UploadState.initial());
    clear();
  }

  void clear() {
    AppUploader.uploaders.remove(this);
    picked = null;
  }

  void setAsUploaded(FileModel? file) {
    if (file == null) return;
    uploaded = file;
    _state(UploadState.success(file));
  }

  void setAsPicked(FileModel? file) {
    if (file == null) return;
    picked = file;
    _state(UploadState.picked(file));
    if (find(picked!.path) != null) return;
    uploaders.add(this);
  }

  @override
  List<Object> get props => [path];
  @override
  bool get stringify => true;
}
