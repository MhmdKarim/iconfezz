// ignore_for_file: always_use_package_imports

import 'package:flutter/material.dart';

import 'preferences.dart';

mixin AppStyles {
  static ThemeData get theme => appPrefs.isDarkMode() ? darkTheme : lightTheme;
  // static final lightPrimaryColor = Color(0xff1ab39e);
  static final lightPrimaryColor = Color(0xff249f9c);

  static final lightAccentColor = Color(0xff00ebff);
  static ThemeData get lightTheme => ThemeData.light().copyWith(
        primaryColor: lightPrimaryColor,
        primaryColorLight: lightPrimaryColor,
        primaryColorDark: darkPrimaryColor,
        textSelectionTheme: TextSelectionThemeData(
          cursorColor: lightPrimaryColor,
          selectionColor: lightPrimaryColor,
          selectionHandleColor: lightPrimaryColor,
        ),
        colorScheme:
            ColorScheme.fromSwatch().copyWith(secondary: lightAccentColor),
      );
  static final darkPrimaryColor = Color(0xff249f9c);
  // static final darkPrimaryColor = Color(0xffFDE800);
  // static final darkPrimaryColor = Color(0xff013245);
  static final darkAccentColor = Color(0xff00ebff);
  static ThemeData get darkTheme => ThemeData.dark().copyWith(
        primaryColor: darkPrimaryColor,
        primaryColorDark: darkPrimaryColor,
        primaryColorLight: lightPrimaryColor,
        textSelectionTheme: TextSelectionThemeData(
          cursorColor: lightPrimaryColor,
          selectionColor: lightPrimaryColor,
          selectionHandleColor: lightPrimaryColor,
        ),
        colorScheme:
            ColorScheme.fromSwatch().copyWith(secondary: darkAccentColor),
      );

  static LinearGradient get primaryGradient => LinearGradient(
        colors: [lightPrimaryColor, theme.colorScheme.secondary],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
      );
}

extension ThemeDataEx on ThemeData {
  Color get inversePrimaryColor =>
      brightness == Brightness.dark ? primaryColorLight : primaryColorDark;
}
