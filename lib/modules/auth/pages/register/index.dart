// ignore_for_file: always_use_package_imports

import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import '../../../../components/form_field.dart';
import '../../../../imports.dart';
import '../../data/register.dart';
import 'widgets/gender.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  // Form Fields
  final formKey = GlobalKey<FormState>();
  final usernameController = TextEditingController();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final bioController = TextEditingController(text: '');
  final emailController = TextEditingController(text: '');
  final cityController = TextEditingController(text: '');
  final dobController = TextEditingController(text: '');
  String gender = t.Male;

  final usernameErrorText = Rxn<String>();
  final accepted = false.obs;
  final isLoading = false.obs;

  static String? birthDateValidator(String? value) {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('yyyy');
    final String formatted = formatter.format(now);
    final String str1 = value!;
    final List<String> str2 = str1.split('/');
    final String month = str2.isNotEmpty ? str2[0] : '';
    final String day = str2.length > 1 ? str2[1] : '';
    final String year = str2.length > 2 ? str2[2] : '';
    if (value.isEmpty) {
      return 'BirthDate is Empty';
    } else if (int.parse(month) > 13) {
      return 'Month is invalid';
    } else if (int.parse(day) > 32) {
      return 'Day is invalid';
    } else if (int.parse(year) > int.parse(formatted)) {
      return 'Year is invalid';
    } else if (int.parse(year) < 1920) {
      return 'Year is invalid';
    }
    return '';
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: Appbar(
        titleStr: t.Register,
      ),
      body: Form(
        key: formKey,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              //Spacer(),
              SizedBox(height: 20),
              Center(
                child: Image.asset(
                  Assets.appIcon.path,
                  height: 150,
                ),
              ),
              SizedBox(height: 20),
              Obx(
                () => AppTextFormField(
                  label: t.Username,
                  icon: Icons.alternate_email,
                  controller: usernameController,
                  keyboardType: TextInputType.name,
                  formatters: [
                    TextInputFormatter.withFunction((o, n) {
                      final match =
                          RegExp(r"^[a-zA-Z0-9_\-]+$").hasMatch(n.text);
                      return !match
                          ? o
                          : n.copyWith(text: n.text.toLowerCase());
                    })
                  ],
                  // maxLength: 20,
                  validator: (s) => s!.length < 6 ? t.AddValidUsername : null,
                  errorText: usernameErrorText(),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 6, vertical: 10),
                child: Row(
                  children: [
                    Expanded(
                      child: AppTextFormField(
                        label: t.FirstName,
                        controller: firstNameController,
                        maxLength: 30,
                        //labelSize: 12,
                        validator: (s) =>
                            s!.isEmpty ? t.AddValidFirstName : null,
                      ),
                    ),
                    Expanded(
                      child: AppTextFormField(
                        label: t.LastName,
                        controller: lastNameController,
                        maxLength: 30,
                        validator: (s) =>
                            s!.isEmpty ? t.AddValidLastName : null,
                      ),
                    ),
                  ],
                ),
              ),

              AppTextFormField(
                label: '${t.bio} (${t.Optional})',
                //icon: Icons.person,
                controller: bioController,
                keyboardType: TextInputType.text,
                maxLines: 5,
                maxLength: 200,
              ),
              SizedBox(height: 20),
              AppTextFormField(
                label: '${t.Email} (${t.Optional})',
                icon: Icons.email,
                controller: emailController,
                keyboardType: TextInputType.emailAddress,
              ),

              SizedBox(height: 20),
              AppTextFormField(
                label: '${t.city} (${t.Optional})',
                icon: Icons.location_city,
                controller: cityController,
                keyboardType: TextInputType.text,
              ),
              SizedBox(height: 20),
              AppTextFormField(
                label: '${t.dob} (${t.Optional})',
                icon: Icons.calendar_view_month,
                controller: dobController,
                //validator: birthDateValidator,
                keyboardType: TextInputType.number,
                formatters: [
                  LengthLimitingTextInputFormatter(10),
                  FilteringTextInputFormatter.singleLineFormatter,
                  BirthTextInputFormatter(),
                ],
              ),
              SizedBox(height: 20),
              Align(
                alignment: Alignment(-0.8, 0),
                child: AppTextRegular(t.SelectGender),
              ),
              GenderWidget(onSelect: (v) => gender = v!),
              SizedBox(height: 8),
              SizedBox(
                width: Get.width,
                child: ListTile(
                  leading: Obx(
                    () => Checkbox(
                      activeColor: theme.primaryColor,
                      value: accepted(),
                      onChanged: (v) => accepted(v),
                    ),
                  ),
                  title: AppTextRegular(
                    t.AcceptTerms,
                    decoration: TextDecoration.underline,
                  ),
                  onTap: () => AppNavigator.toTerms(),
                ),
              ),
              SizedBox(height: 10),
              Obx(
                () => AppButton(
                  t.Save,
                  //width: MediaQuery.of(context).size.width / 2,
                  isLoading: isLoading(),
                  onTap: accepted() ? onSave : null,
                ),
              ),
              SizedBox(height: 50),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> onSave() async {
    if (!formKey.currentState!.validate()) return;
    isLoading(true);
    try {
      final username = usernameController.text;
      usernameErrorText(null);
      if (await RegisterRepository.checkIfUsernameTaken(username)) {
        usernameErrorText(t.UsernameTaken);
        throw Exception(t.UsernameTaken);
      }

      await RegisterRepository.createNewUser(
        username: username,
        firstName: firstNameController.text,
        lastName: lastNameController.text,
        email: emailController.text,
        gender: gender,
        bio: bioController.text,
        city: cityController.text,
        dob: dobController.text,
      );
      await authProvider.login();
    } catch (e) {
      BotToast.showText(text: AppAuthException.handleError(e).message!);
    }
    isLoading(false);
  }
}

class BirthTextInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    if (oldValue.text.length >= newValue.text.length) {
      return newValue;
    }
    final dateText = _addSeparator(newValue.text, '/');
    return newValue.copyWith(
      text: dateText,
      selection: updateCursorPosition(dateText),
    );
  }

  String _addSeparator(String value, String separator) {
    // ignore: parameter_assignments
    value = value.replaceAll('/', '');
    var newString = '';
    for (int i = 0; i < value.length; i++) {
      newString += value[i];
      if (i == 1) {
        newString += separator;
      }
      if (i == 3) {
        newString += separator;
      }
    }
    return newString;
  }

  TextSelection updateCursorPosition(String text) {
    return TextSelection.fromPosition(TextPosition(offset: text.length));
  }
}
