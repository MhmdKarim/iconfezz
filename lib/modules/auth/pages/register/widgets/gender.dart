// ignore_for_file: always_use_package_imports

import '../../../../../imports.dart';

class GenderWidget extends StatefulWidget {
  final ValueChanged<String?>? onSelect;

  const GenderWidget({
    Key? key,
    this.onSelect,
  }) : super(key: key);

  @override
  _GenderWidgetState createState() => _GenderWidgetState();
}

class _GenderWidgetState extends State<GenderWidget> {
  String? gender = t.Male;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Row(
      children: [
        for (final g in [t.Male, t.Female])
          Flexible(
            child: RadioListTile<String>(
              title: AppTextRegular(g),
              value: g,
              groupValue: gender,
              onChanged: onSelect,
              selectedTileColor: theme.primaryColor,
              activeColor: theme.primaryColor,
            ),
          ),
      ],
    );
  }

  void onSelect(String? v) {
    gender = v;
    widget.onSelect!(v);
    setState(() {});
  }
}
