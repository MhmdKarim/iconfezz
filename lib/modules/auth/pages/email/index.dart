// ignore_for_file: always_use_package_imports

import '../../../../components/appbar_icon.dart';
import '../../../../components/form_field.dart';
import '../../../../imports.dart';
import '../../data/register.dart';

class EmailLoginPage extends StatefulWidget {
  @override
  _EmailLoginPageState createState() => _EmailLoginPageState();
}

class _EmailLoginPageState extends State<EmailLoginPage> {
  final formKey = GlobalKey<FormState>();

  late String email;
  late String password;
  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Appbar(
        titleStr: t.Email,
        actions: const [
          AppBarIcon(Icons.email),
        ],
      ),
      body: Form(
        key: formKey,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(height: 20),
              Center(
                child: Image.asset(
                  Assets.appIcon.path,
                  height: 150,
                ),
              ),
              SizedBox(height: 20),
              AppTextFormField(
                label: t.Email,
                icon: Icons.email,
                keyboardType: TextInputType.emailAddress,
                validator: (s) =>
                    GetUtils.isEmail(s ?? '') ? null : t.InvalidEmail,
                onSave: (v) => email = v ?? '',
              ),
              AppTextFormField(
                label: t.Password,
                icon: Icons.lock,
                onSave: (v) => password = v ?? '',
                isObscure: true,
              ),
              SizedBox(height: 20),
              AppButton(
                t.Save,
                isLoading: isLoading,
                onTap: onSave,
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> onSave() async {
    if (!formKey.currentState!.validate()) return;
    formKey.currentState!.save();
    setState(() => isLoading = true);
    try {
      await RegisterRepository.login(email, password);
      await authProvider.login();
    } catch (e) {
      BotToast.showText(text: AppAuthException.handleError(e).message!);
    }
    setState(() => isLoading = false);
  }
}
