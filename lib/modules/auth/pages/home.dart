// ignore_for_file: always_use_package_imports

//import 'package:flutter_native_splash/flutter_native_splash.dart';

import '../../../components/bottom_bar.dart';

import '../../../imports.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // FlutterNativeSplash.remove();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => authProvider.isLoggedIn() ? BottomBar() : _WelcomePage(),
    );
  }
}

class _WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Center(
          child: Column(
            children: [
              Spacer(),
              Image.asset(
                Assets.appIcon.path,
                height: 150,
              ),
              SizedBox(height: 20),
              AppTextTitle(
                t.AppName,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              AppTextSubTitle(
                t.AboutApp,
                textAlign: TextAlign.center,
                color: context.isDarkMode
                    ? Colors.white
                    : context.theme.primaryColor,
              ),
              Spacer(),
              AppButton(
                t.AppLanguage,
                onTap: AppNavigator.toAppLanguage,
              ),
              SizedBox(
                height: 20,
              ),
              AppButton(
                t.START,
                onTap: AppNavigator.toPhoneLogin,
              ),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
