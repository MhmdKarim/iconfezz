// ignore_for_file: always_use_package_imports

import 'package:countries/countries.dart';
import 'package:firebase_auth/firebase_auth.dart' hide User;
import 'package:flutter/foundation.dart';

import '../../../../components/appbar_icon.dart';
import '../../../../imports.dart';
import '../../data/phone.dart';
import 'verification.dart';
import 'widgets/phone_form.dart';

class PhoneLoginPage extends StatefulWidget {
  @override
  _PhoneLoginPageState createState() => _PhoneLoginPageState();
}

class _PhoneLoginPageState extends State<PhoneLoginPage> {
  final formKey = GlobalKey<FormState>();

  // Phone Number Controllers
  final codeController = TextEditingController();
  final phoneController = TextEditingController();
  String get phoneNumber => '+${codeController.text}${phoneController.text}';

  final country = CountriesRepo.getCountryByIsoCode('US').obs;

  final isLoading = false.obs;
  final error = ''.obs;

  @override
  void initState() {
    codeController.text = country().phoneCode;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Appbar(
        titleStr: t.PhoneNumber,
        actions: const [
          AppBarIcon(Icons.phone),
        ],
      ),
      body: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: FocusScope.of(context).unfocus,
        child: SingleChildScrollView(
          child: Column(
            children: [
              // AuthTopHeader(),
              SizedBox(height: 30),
              Image.asset(
                Assets.appIcon.path,
                height: 150,
              ),
              SizedBox(height: 30),
              PhoneFormWidget(
                codeController: codeController,
                phoneController: phoneController,
                country: country,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Obx(
                  () => AppTextRegularBold(
                    error(),
                    color: Colors.red,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              SizedBox(
                height: context.height / 4,
                child: Center(
                  child: Obx(
                    () => AppButton(
                      t.Next,
                      isLoading: isLoading(),
                      onTap: getValidationCode,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> getValidationCode() async {
    if (!phoneNumber.isPhoneNumber) {
      BotToast.showText(text: t.InvalidPhone);
      return;
    }
    isLoading(true);
    error('');
    appPrefs.prefs.setString('country', country().name);
    if (kIsWeb) {
      final confResult = await FirebaseAuth.instance.signInWithPhoneNumber(
        phoneNumber,
        RecaptchaVerifier(
          onSuccess: () {
            Get.to(
              () => OTPVerificationPage(phoneNumber: phoneNumber, verID: ''),
            );
          },
        ),
      );
      Get.put<ConfirmationResult>(confResult);
    } else {
      PhoneRepository.verifyPhone(
        phoneNumber,
        onCodeSent: (id) => Get.to(
          () => OTPVerificationPage(phoneNumber: phoneNumber, verID: id),
        ),
        onFailed: (e) {
          error(e.message);
          isLoading(false);
        },
      );
    }
    6.delay(() => isLoading(false));
  }
}
