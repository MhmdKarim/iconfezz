// ignore_for_file: always_use_package_imports

import 'package:countries/countries.dart';

import '../../../../../components/form_field.dart';
import '../../../../../imports.dart';

class PhoneFormWidget extends StatefulWidget {
  final TextEditingController codeController;
  final TextEditingController phoneController;
  final Rx<Country> country;
  const PhoneFormWidget({
    Key? key,
    required this.codeController,
    required this.phoneController,
    required this.country,
  }) : super(key: key);

  @override
  _PhoneFormWidgetState createState() => _PhoneFormWidgetState();
}

class _PhoneFormWidgetState extends State<PhoneFormWidget> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      //constraints: BoxConstraints(maxWidth: 400),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          //Phone Code
          SizedBox(
            width: 100,
            child: Theme(
              data: Theme.of(context).copyWith(
                textSelectionTheme: TextSelectionThemeData(
                  selectionColor: theme.primaryColor,
                  selectionHandleColor: theme.primaryColor,
                  cursorColor: theme.primaryColor,
                ),
              ),
              child: TextFormField(
                controller: widget.codeController,
                keyboardType: TextInputType.number,
                maxLength: 3,
                cursorColor: theme.primaryColor,
                cursorHeight: 25,
                //enableInteractiveSelection: false,
                style: theme.textTheme.subtitle1,
                decoration: InputDecoration(
                  counterStyle: TextStyle(fontSize: 0),
                  filled: true,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(color: theme.primaryColor),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(color: theme.primaryColor),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(color: theme.primaryColor),
                  ),
                  counter: SizedBox.shrink(),
                  contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                  // labelText: 'Code',
                  // labelStyle: TextStyle(color: theme.primaryColor),
                  prefix: Row(
                    mainAxisSize: MainAxisSize.min,
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 30,
                        child: GestureDetector(
                          onTap: () => showCountriesBottomSheet(
                            context,
                            onValuePicked: (v) {
                              widget.codeController.text = v.phoneCode;
                              widget.country(v);
                            },
                          ),
                          child: Obx(
                            () => CountryFlagWidget(widget.country()),
                          ),
                        ),
                      ),
                      SizedBox(width: 5),
                      AppTextRegular('+')
                    ],
                  ),
                ),
                onChanged: (str) {
                  if (str.length == 3) {
                    FocusScope.of(context).nextFocus();
                  }
                  widget.country(
                    CountriesRepo.getCountryByPhoneCode(
                      str,
                      Country(),
                    ),
                  );
                },
              ),
            ),
          ),

          SizedBox(width: 12),
          //Phone Number Main TextField
          Expanded(
            child: AppTextFormField(
              controller: widget.phoneController,
              keyboardType: TextInputType.number,
              label: t.PhoneNumber,
              // decoration: InputDecoration(
              //   counter: SizedBox.shrink(),
              //   hintText: t.PhoneNumber,
              // ),
            ),
          ),
        ],
      ),
    );
  }
}
