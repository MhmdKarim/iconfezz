// ignore_for_file: always_use_package_imports

import 'package:auto_size_text/auto_size_text.dart';
import 'package:intl/intl.dart';

import '../../../../imports.dart';
import '../../../feeds/data/posts.dart';
import '../../../feeds/pages/posts/widgets/user_posts.dart';
import '../../data/user_repository.dart';
import 'widgets/appbar.dart';
import 'widgets/header.dart';

class ProfilePage extends StatefulWidget {
  final String? userID;

  const ProfilePage({
    Key? key,
    this.userID,
  }) : super(key: key);
  @override
  _MyProfilePageState createState() => _MyProfilePageState();
}

class _MyProfilePageState extends State<ProfilePage> {
  Rxn<User?> rxUser = Rxn<User?>();
  User get user => rxUser()!;
  @override
  void initState() {
    if (widget.userID?.isNotEmpty == true) {
      UserRepository.fetchUser(widget.userID).then(rxUser);
    } else {
      rxUser = authProvider.rxUser;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Obx(() {
        final theme = Theme.of(context);
        final user = rxUser();
        if (user == null) return Scaffold();
        return DefaultTabController(
          length: user.isMe ? 3 : 2,
          child: Scaffold(
            appBar: Appbar(
              titleStr: t.Profile,
              actions: [
                Material(
                  //  color: Theme.of(context).primaryColor,
                  child: ProfileAppBarTrailing(rxUser),
                ),
              ],
            ),
            body: NestedScrollView(
              headerSliverBuilder: (_, v) => [
                SliverAppBar(
                  expandedHeight: 400, //user.isMe ? 350 : 400,
                  pinned: true,
                  floating: true,
                  flexibleSpace: ProfileHeader(rxUser),
                  leading: SizedBox(),
                  shadowColor: theme.primaryColor,
                  backgroundColor: theme.primaryColor,
                  foregroundColor: theme.primaryColor,
                  bottom: TabBar(
                    // automaticIndicatorColorAdjustment: true,
                    labelColor: theme.primaryColor,
                    indicatorColor: theme.primaryColor,
                    unselectedLabelColor: Colors.grey,
                    overlayColor: MaterialStateProperty.all(theme.primaryColor),
                    tabs: [
                      _Item(
                        title: t.POSTS,
                        info: '${user.posts.length}',
                      ),
                      if (user.isMe)
                        _Item(
                          title: t.SAVED,
                          info: '${user.savedPosts.length}',
                        ),
                      _Item(
                        title: t.SHARED,
                        info: '${user.sharedPosts.length}',
                      ),
                    ],
                  ),
                ),
              ],
              body: TabBarView(
                children: [
                  if (user.posts.isNotEmpty)
                    UserPosts(
                      user.id,
                      PostsRepository.getUserPostsStream(user, 10),
                      // key: PageStorageKey('0'),
                    )
                  else
                    SizedBox(),
                  if (user.isMe)
                    if (user.savedPosts.isNotEmpty)
                      UserPosts(
                        user.id,
                        PostsRepository.getSavedPostsStream(user, 10),
                        // key: PageStorageKey('1'),
                      )
                    else
                      SizedBox(),
                  if (user.sharedPosts.isNotEmpty)
                    UserPosts(
                      user.id,
                      PostsRepository.getSharedPostsStream(user, 10),
                      // key: PageStorageKey('1'),
                    )
                  else
                    SizedBox(),
                ],
              ),
            ),
          ),
        );
      });
}

class _Item extends StatelessWidget {
  final String title;
  final String info;
  // final VoidCallback? onTap;

  const _Item({
    Key? key,
    required this.title,
    required this.info,
    // this.onTap,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // onTap: onTap,
      child: Column(
        children: <Widget>[
          AutoSizeText(
            title,
            minFontSize: 10,
            maxFontSize: 11,
            maxLines: 1,
            style: TextStyles.textStyleRegularBold(
              context,
            ),
          ),
          //SizedBox(height: 2),
          AutoSizeText(
            NumberFormat.compact().format(int.parse(info)),
            maxLines: 1,
            minFontSize: 9,
            maxFontSize: 11,
            style: TextStyles.textStyleRegularBold(
              context,
            ),
          ),
          // AppTextSubTitleBold(
          //   NumberFormat.compact().format(int.parse(info)),
          //   color: Colors.grey,
          // ),
        ],
      ),
    );
  }
}
