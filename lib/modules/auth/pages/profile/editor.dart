// ignore_for_file: always_use_package_imports

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../../components/appbar_icon.dart';
import '../../../../components/form_field.dart';
import '../../../../imports.dart';
import '../../data/user_repository.dart';

class EditProfilePage extends StatefulWidget {
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  final formKey = GlobalKey<FormState>();

  final photoUploader = AppUploader();
  final coverUploader = AppUploader();
  final isLoading = false.obs;

  User? get user => edited ?? authProvider.user;

  User? edited;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: Appbar(
        titleStr: t.EditProfile,
        actions: const [
          AppBarIcon(Icons.edit),
        ],
      ),
      body: SingleChildScrollView(
        child: SizedBox(
          height: 1000,
          child: Stack(
            children: <Widget>[
              // Align(
              //   alignment: Alignment.topCenter,
              //   child: Obx(
              //     () => Stack(
              //       children: [
              //         GestureDetector(
              //           onTap: () =>
              //               pushImagesPage(ImageModel(user!.coverPhotoURL)),
              //           child: AppImage(
              //             ImageModel(
              //               coverUploader.path(user!.coverPhotoURL),
              //               height: 200,
              //               width: double.maxFinite,
              //             ),
              //             errorAsset: Assets.images.blurred.path,
              //             fit: BoxFit.cover,
              //           ),
              //         ),
              //         Positioned(
              //           top: 0,
              //           left: 0,
              //           right: 0,
              //           child: Row(
              //             mainAxisAlignment: MainAxisAlignment.end,
              //             children: [
              //               IconButton(
              //                 icon: Icon(
              //                   Icons.edit,
              //                   color: theme.primaryColor,
              //                 ),
              //                 onPressed: () => coverUploader.pick(context),
              //               ),
              //             ],
              //           ),
              //         ),
              //         // if (coverUploader.isUploading)
              //         //   Center(child: CircularProgressIndicator()),
              //       ],
              //     ),
              //   ),
              // ),

              Positioned(
                top: 100,
                left: 0,
                right: 0,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    children: [
                      Material(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        // elevation: 0.0,
                        // color: theme.primaryColorDark,
                        child: Form(
                          key: formKey,
                          child: Column(
                            children: <Widget>[
                              SizedBox(height: 80),
                              Row(
                                children: [
                                  Expanded(
                                    child: AppTextFormField(
                                      label: t.FirstName,
                                      initialValue: user!.firstName,
                                      maxLength: 20,
                                      labelSize: 12,
                                      validator: (s) => s!.isEmpty
                                          ? t.AddValidFirstName
                                          : null,
                                      onSave: (v) {
                                        if (v!.trim().isEmpty ||
                                            v == user!.firstName) {
                                          return;
                                        }
                                        edited = user!.copyWith(firstName: v);
                                      },
                                    ),
                                  ),
                                  Expanded(
                                    child: AppTextFormField(
                                      label: t.LastName,
                                      initialValue: user!.lastName,
                                      maxLength: 20,
                                      validator: (s) => s!.isEmpty
                                          ? t.AddValidLastName
                                          : null,
                                      onSave: (v) {
                                        if (v!.trim().isEmpty ||
                                            v == user!.lastName) {
                                          return;
                                        }
                                        edited = user!.copyWith(lastName: v);
                                      },
                                    ),
                                  ),
                                ],
                              ),
                              AppTextFormField(
                                initialValue: user!.bio,
                                label: t.bio,
                                maxLength: 200,
                                maxLines: 5,
                                onSave: (v) {
                                  if (v!.trim().isEmpty || v == user!.bio) {
                                    return;
                                  }
                                  edited = user!.copyWith(bio: v);
                                },
                              ),
                              AppTextFormField(
                                initialValue: user!.status,
                                label: t.Status,
                                icon: FontAwesomeIcons.circleHalfStroke,
                                maxLength: 30,
                                onSave: (v) {
                                  if (v!.trim().isEmpty || v == user!.status) {
                                    return;
                                  }
                                  edited = user!.copyWith(status: v);
                                },
                              ),
                              SizedBox(height: 10),
                              SizedBox(height: 10),
                              AppTextFormField(
                                initialValue: user!.email,
                                label: t.Email,
                                icon: Icons.email,
                                maxLength: 30,
                                onSave: (v) {
                                  if (v!.trim().isEmpty || v == user!.email) {
                                    return;
                                  }
                                  edited = user!.copyWith(email: v);
                                },
                              ),
                              AppTextFormField(
                                initialValue: user!.city,
                                label: t.city,
                                icon: Icons.location_city,
                                maxLength: 30,
                                onSave: (v) {
                                  if (v!.trim().isEmpty || v == user!.city) {
                                    return;
                                  }
                                  edited = user!.copyWith(city: v);
                                },
                              ),
                              SizedBox(height: 10),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 30),
                      Obx(
                        () => AppButton(
                          t.Save,
                          isLoading: isLoading(),
                          onTap: onSave,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                top: 30,
                left: 0,
                right: 0,
                child: GestureDetector(
                  onTap: () => pushImagesPage(ImageModel(user!.photoURL)),
                  child: Center(
                    child: Obx(
                      () => AvatarWidget(
                        photoUploader.path(user!.photoURL),
                        badgeColor: theme.primaryColor,
                        isLoading: photoUploader.isUploading,
                        radius: 120,
                        showBadge: true,
                        badgeContent: ClipOval(
                          child: Material(
                            color: theme.primaryColor, // Button color
                            child: InkWell(
                              onTap: () => photoUploader.pick(context),
                              child: SizedBox(
                                width: 40,
                                height: 40,
                                child: Icon(
                                  Icons.edit,
                                  color: theme.iconTheme.color,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              // Positioned(
              //   top: 0,
              //   left: 0,
              //   right: 0,
              //   child: Appbar(
              //     backgroundColor: Colors.transparent,
              //     actions: <Widget>[
              //       IconButton(
              //         icon: Icon(Icons.edit),
              //         onPressed: () => coverUploader.pick(context),
              //       ),
              //     ],
              //   ),
              // )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> onSave() async {
    formKey.currentState!.save();
    FocusScope.of(context).unfocus();
    isLoading(true);
    try {
      // Update Photo
      if (photoUploader.isPicked) {
        await photoUploader.upload(
          StorageHelper.profilesPicRef,
          onSuccess: (f) => edited = user!.copyWith(photoURL: f.path),
          onFailed: (e) => BotToast.showText(text: e),
        );
      }
      // Update Cover
      if (coverUploader.isPicked) {
        await coverUploader.upload(
          StorageHelper.profilesCoverRef,
          onSuccess: (f) => edited = user!.copyWith(coverPhotoURL: f.path),
          onFailed: (e) => BotToast.showText(text: e),
        );
      }

      if (edited != null) {
        authProvider.rxUser(edited);
        await UserRepository.editProfile(edited!);
      }
      if (!mounted) return; //mkmkmk added
      Navigator.pop(context);
    } catch (e) {
      logError(e);
    }
    isLoading(false);
  }
}
