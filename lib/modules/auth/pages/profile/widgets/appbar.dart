// ignore_for_file: always_use_package_imports

import '../../../../../components/confirm_dialog.dart';
import '../../../../../imports.dart';
import '../../../data/user_repository.dart';

class ProfileAppBarTrailing extends StatelessWidget {
  final Rx<User?> rxUser;

  const ProfileAppBarTrailing(
    this.rxUser, {
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) => Obx(
        () {
          final user = rxUser()!;
          return PopupMenuButton<int>(
            icon: Icon(
              Icons.more_vert,
              color: Theme.of(context).primaryColor,
            ),
            // color: Theme.of(context).primaryColor,
            itemBuilder: (_) => [
              if (!user.isMe)
                PopupMenuItem(
                  value: 0,
                  child: AppTextRegular(
                    user.isBlocked() ? t.Unblock : t.Block,
                  ),
                ),
              if (!user.isMe && authProvider.user!.isAdmin)
                PopupMenuItem(
                  value: 1,
                  child: AppTextRegular(
                    user.isBanned ? t.Unban : t.Ban,
                  ),
                ),
              if (user.isMe)
                PopupMenuItem(
                  value: 2,
                  child: AppTextRegular('Blocked Users'),
                ),
              // if (user.isMe)
              //   PopupMenuItem(
              //     value: 3,
              //     child: AppTextRegular(t.Logout),
              //   ),
            ],
            onSelected: (v) {
              if (v == 0) {
                showConfirmDialog(
                  context,
                  title:
                      '${t.AreYouSure} ${user.isBlocked() ? t.Unblock : t.Block} ${user.fullName}',
                  onConfirm: () {
                    user.toggleBlocking();
                    rxUser.refresh();
                    UserRepository.toggleBlockUser(user);
                    Navigator.pop(context);
                  },
                );
              } else if (v == 1) {
                showConfirmDialog(
                  context,
                  title:
                      '${t.AreYouSure} ${user.isBanned ? t.Unban : t.Ban} ${user.fullName}',
                  onConfirm: () {
                    rxUser(user.copyWith(isBanned: !user.isBanned));
                    UserRepository.banUser(user.id);
                    Navigator.pop(context);
                  },
                );
              } else if (v == 2) {
                AppNavigator.toPeopleList(
                  rxUser()!.usersBlockedByMe,
                  'Blocked Users',
                );
              }
              //else if (v == 3) {
              //   authProvider.logout();
              // }
            },
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
          );
        },
      );
}
