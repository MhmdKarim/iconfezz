// ignore_for_file: always_use_package_imports

import 'package:timeago/timeago.dart';

import '../../../../../imports.dart';
import '../../../../chat/pages/chats/widgets/chat_icon.dart';
import '../../../data/user_repository.dart';

class ProfileHeader extends StatelessWidget implements PreferredSizeWidget {
  final Rx<User?> rxUser;

  const ProfileHeader(
    this.rxUser, {
    Key? key,
  }) : super(key: key);

  User get user => rxUser()!;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final width = MediaQuery.of(context).size.width;
    return Container(
      height: 400, //user.isMe ? 350 : 400,
      color: theme.scaffoldBackgroundColor,
      child: Stack(
        children: <Widget>[
          // Positioned(
          //   bottom: user.isMe ? 200 : 250,
          //   left: 0,
          //   right: 0,
          //   child: GestureDetector(
          //     onTap: () => pushImagesPage(ImageModel(user.coverPhotoURL)),
          //     child: AppImage(
          //       ImageModel(user.coverPhotoURL),
          //       fit: BoxFit.cover,
          //       // errorAsset: Assets.images.cover.path,
          //       placeholder: Container(color: theme.canvasColor, height: 200),
          //     ),
          //   ),
          // ),
          Positioned(
            bottom: 110, //user.isMe ? 50 : 110,
            left: .6,
            right: .6,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  SizedBox(height: 80),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      AppTextTitleBold(
                        user.fullName,
                      ),

                      // Text(
                      //   '@${user.username}',
                      //   style: GoogleFonts.abel().copyWith(
                      //       fontSize: 24, fontWeight: FontWeight.bold),
                      //   maxLines: 1,
                      // ),
                    ],
                  ),
                  SizedBox(height: 8),
                  AppTextRegular(
                    user.status,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 8),
                  AppTextRegular(
                    '${t.MemberSince}  ${format(user.createdAt, locale: LocaleSettings.currentLocale.languageTag)}',
                  ),
                  SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      AppButton(
                        '${t.FOLLOWERS}  ${user.followers.length}',
                        onTap: () => AppNavigator.toPeopleList(
                          user.followers,
                          t.FOLLOWERS,
                        ),
                        width: width * 0.40,
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                        backgroundColor: theme.scaffoldBackgroundColor,
                        borderColor: Colors.grey,
                      ),
                      AppButton(
                        '${t.Following}   ${user.following.length}',
                        onTap: () => AppNavigator.toPeopleList(
                          user.following,
                          t.FOLLOWINGS,
                        ),
                        width: width * 0.40,
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                        backgroundColor: theme.scaffoldBackgroundColor,
                        borderColor: Colors.grey,
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
          // Positioned(
          //   left: 0,
          //   right: 0,
          //   bottom: user.isMe ? 350 : 410,
          //   child: ProfileAppBar(rxUser),
          // ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 275, //user.isMe ? 225 : 275,
            child: Material(
              shape: CircleBorder(),
              child: GestureDetector(
                onTap: () => pushImagesPage(ImageModel(user.photoURL)),
                child: AvatarWidget(
                  user.photoURL,
                  radius: 100,
                ),
              ),
            ),
          ),

          //Follow And Message Buttons
          if (!user.isMe)
            Positioned(
              right: 0,
              left: 0,
              bottom: 50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  AppButton(
                    user.isFollowing ? t.Following : t.Follow,
                    width: width * 0.40,
                    color: Colors.white,
                    backgroundColor:
                        user.isFollowing ? Colors.grey : theme.primaryColorDark,
                    onTap: () {
                      user.toggleFollowing();
                      rxUser.refresh();
                      UserRepository.toggleFollowUser(user);
                    },
                  ),
                  AppButton(
                    t.Message,
                    width: width * 0.4,
                    color: Colors.white,
                    onTap: () => AppNavigator.toPrivateChat(user.id),
                  ),
                ],
              ),
            ),
          if (user.isMe)
            Positioned(
              right: 0,
              left: 0,
              bottom: 50,
              child: Center(
                //padding: const EdgeInsets.symmetric(horizontal: 15),
                child: ChatIcon(),
              ),
            )
        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(300);
}
