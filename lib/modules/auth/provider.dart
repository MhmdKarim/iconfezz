// ignore_for_file: always_use_package_imports

import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart' hide User;

import '../../imports.dart';
import '../notifications/data/notifications.dart';
import 'data/user_repository.dart';

AuthProvider get authProvider => Get.find();

class AuthProvider {
  AuthProvider._();
  static final auth = FirebaseAuth.instance;
  String get uid => auth.currentUser?.uid ?? '';

  final rxUser = Rxn<User>();
  User? get user => rxUser.value;
  final isLoggedIn = false.obs;

  static Future<AuthProvider> init() async {
    final c = AuthProvider._();
    Get.put(c);
    await c._fetchUser();
    if (auth.currentUser != null) {
      if (c.user == null) {
        0.5.delay().then((_) => AppNavigator.toRegister());
      } else {
        NotificationRepo.registerNotification();
      }
    }
    return c;
  }

  Future<void> _fetchUser() async {
    if (auth.currentUser == null) return;
    final user = await UserRepository.fetchUser();
    if (user != null) {
      isLoggedIn(true);
      updateUser(user);
      UserRepository.updateActiveAt(true);
    }
  }

  Future<void> updateUser(User user) async {
    if (user.isBanned) {
      rxUser(null);
      await UserRepository.updateActiveAt(false);
      await auth.signOut();
    } else {
      rxUser(user);
    }
  }

  Future<void> login() async {
    try {
      await _fetchUser();
      Get.until((r) => r.isFirst);
      if (user == null) {
        AppNavigator.toRegister();
      } else {
        NotificationRepo.registerNotification();
      }
    } catch (e) {
      logError(e);
    }
  }

  Future<void> logout() async {
    try {
      Get.until((r) => r.isFirst);
      isLoggedIn(false);
      rxUser.value = null;
      await UserRepository.updateActiveAt(false);
      await NotificationRepo.clearToken();
      await auth.signOut();
    } catch (e, s) {
      logError('', e, s);
    }
  }
}
