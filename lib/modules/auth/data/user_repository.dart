// ignore_for_file: always_use_package_imports

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' hide User;

import '../../../imports.dart';
import '../../feeds/models/post.dart';
import '../../notifications/data/notifications.dart';
import '../../notifications/models/notification.dart';

mixin UserRepository {
  static final auth = FirebaseAuth.instance;
  static String? get _uid => auth.currentUser?.uid;
  static List<String> get following => authProvider.user!.following;
  static DocumentSnapshot? lastPostDoc;
  static final usersCol =
      FirebaseFirestore.instance.collection('users').withConverter<User>(
            fromFirestore: (e, _) => User.fromMap(e.data()!),
            toFirestore: (m, _) => m.toMap(),
          );
  static DocumentReference<User> userDoc([String? id]) =>
      usersCol.doc(id ?? _uid);

  static Future<List<User>> fetchPeopleToFollow(int page) async {
    following.add(_uid!);
    //var col = usersCol.limit(20).orderBy('createdAt', descending: true);
    var col = usersCol.where('id', whereNotIn: following).limit(20);
    //.orderBy('createdAt', descending: true);

    if (page != 0 && lastPostDoc != null) {
      col = col.startAfterDocument(lastPostDoc!);
    }
    final docs = (await col.get()).docs;
    if (docs.isNotEmpty) lastPostDoc = docs.last;
    return docs.map((s) => s.data()).toList();
  }

  static Future<User?> fetchUser([String? id]) async {
    final doc = await userDoc(id ?? _uid).get();
    return doc.data();
  }

  static Future<String?> fetchUserImage([String? id]) async {
    final doc = await userDoc(id ?? _uid).get();
    final String image = doc.data()!.photoURL;
    return image;
  }

  static Future<void> deleteUser([String? id]) async {
    await userDoc(id ?? _uid).delete();
    //return doc.data();
  }

  static Stream<User>? userStream([String? id]) {
    final uid = id ?? _uid ?? '';
    if (uid.isEmpty) return null;
    return userDoc(uid)
        .snapshots()
        .map((s) => s.data()!)
        .handleError((e) => logError(e));
  }

  static Future<void> editProfile(User ed) async {
    final c = authProvider.user!;
    try {
      FirebaseFunctions.instance.httpsCallable('editProfile').call({
        'id': c.id,
        if (c.firstName != ed.firstName) 'firstName': ed.firstName,
        if (c.lastName != ed.lastName) 'lastName': ed.lastName,
        if (c.fullName != ed.fullName) 'fullName': ed.fullName,
        if (c.email != ed.email) 'email': ed.email,
        if (c.status != ed.status) 'status': ed.status,
        if (c.bio != ed.bio) 'bio': ed.bio,
        if (c.city != ed.city) 'city': ed.city,
        if (c.photoURL != ed.photoURL) 'photoURL': ed.photoURL,
        if (c.coverPhotoURL != ed.coverPhotoURL)
          'coverPhotoURL': ed.coverPhotoURL,
      });
      authProvider.rxUser(ed);
      saveMyInfo();
    } catch (e) {
      logError(e);
    }
  }

  static Future<void> saveMyInfo() async {
    if (_uid == null) return;
    await userDoc().set(
      authProvider.user!,
      SetOptions(merge: true),
    );
  }

  static Future<void> updateActiveAt(bool isActive) async {
    userDoc().update(
      {
        "isActive": isActive,
        "activeAt": FieldValue.serverTimestamp(),
      },
    ).catchError((e) => logError(e));
  }

  static Future<void> onLineStatus(bool onlineStatus) async {
    userDoc().update(
      {
        "onlineStatus": onlineStatus,
      },
    ).catchError((e) => logError(e));
    // print(onlineStatus);
  }

  static Future<void> chatNotify(bool chatNotify) async {
    userDoc().update(
      {
        "chatNotify": chatNotify,
      },
    ).catchError((e) => logError(e));
  }

  static Future<void> commentLikesNotify(bool commentLikesNotify) async {
    userDoc().update(
      {
        "commentLikesNotify": commentLikesNotify,
      },
    ).catchError((e) => logError(e));
  }

  static Future<void> postLikesNotify(bool postLikesNotify) async {
    userDoc().update(
      {
        "postLikesNotify": postLikesNotify,
      },
    ).catchError((e) => logError(e));
  }

  static Future<void> replyLikesNotify(bool replyLikesNotify) async {
    userDoc().update(
      {
        "replyLikesNotify": replyLikesNotify,
      },
    ).catchError((e) => logError(e));
  }

  static Future<void> toggleFollowUser(User user) async {
    await FirebaseFirestore.instance.runTransaction((tr) async {
      if (user.isFollowing) {
        tr.update(userDoc(_uid), {
          'following': FieldValue.arrayUnion([user.id])
        });
        tr.update(userDoc(user.id), {
          'followers': FieldValue.arrayUnion([_uid])
        });
      } else {
        tr.update(userDoc(_uid), {
          'following': FieldValue.arrayRemove([user.id])
        });
        tr.update(userDoc(user.id), {
          'followers': FieldValue.arrayRemove([_uid])
        });
      }
    });
    if (user.isFollowing) {
      NotificationRepo.sendNotificaion(
        NotificationModel.create(
          toId: user.id,
          type: NotificationType.Follow,
        ),
      );
    }
  }

  static Future<void> toggleBlockUser(User user) async {
    await FirebaseFirestore.instance.runTransaction((tr) async {
      if (user.isBlockedBy) {
        tr.update(userDoc(_uid), {
          'usersBlockedByMe': FieldValue.arrayUnion([user.id])
        });
        tr.update(userDoc(user.id), {
          'blockedBy': FieldValue.arrayUnion([_uid])
        });
      } else {
        tr.update(userDoc(_uid), {
          'usersBlockedByMe': FieldValue.arrayRemove([user.id])
        });
        tr.update(userDoc(user.id), {
          'blockedBy': FieldValue.arrayRemove([_uid])
        });
      }
    });
  }

//old
  // static Future<void> toggleBlock(User user) async {
  //   user.toggleBlocking();
  //   userDoc(user.id).update({
  //     'blockedBy': user.isBlocked()
  //         ? FieldValue.arrayUnion([_uid])
  //         : FieldValue.arrayRemove([_uid])
  //   });
  //   userDoc(_uid).update({
  //     'usersBlockedByMe': user.isBlocked()
  //         ? FieldValue.arrayUnion([user.id])
  //         : FieldValue.arrayRemove([user.id])
  //   });
  // }

  static Future<void> banUser(String userID) async {
    FirebaseFunctions.instance
        .httpsCallable('banUser')
        .call({'userID': userID});
  }

  static Future<void> toggleSavedPosts(Post post) async {
    await FirebaseFirestore.instance.runTransaction((tr) async {
      if (post.isSavedByMe) {
        tr.update(userDoc(_uid), {
          'savedPosts': FieldValue.arrayUnion([post.id])
        });
      } else {
        tr.update(userDoc(_uid), {
          'savedPosts': FieldValue.arrayRemove([post.id])
        });
      }
    });
  }

  static Future<void> toggleSharePosts(Post post) async {
    await FirebaseFirestore.instance.runTransaction((tr) async {
      final bool isSharedByMe =
          authProvider.user!.sharedPosts.contains(post.id);
      if (isSharedByMe) {
        tr.update(userDoc(_uid), {
          'sharedPosts': FieldValue.arrayUnion([post.id])
        });
      } else {
        tr.update(userDoc(_uid), {
          'sharedPosts': FieldValue.arrayRemove([post.id])
        });
      }
    });
  }
}
