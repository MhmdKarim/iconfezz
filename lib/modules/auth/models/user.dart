// ignore_for_file: always_use_package_imports

import 'package:equatable/equatable.dart';

import '../../../core/utils.dart';
import '../provider.dart';

class User extends Equatable {
  final String id;
  final String username;
  final String firstName;
  final String lastName;
  final String status;
  final String email;
  final String phone;
  final String photoURL;
  final String coverPhotoURL;
  final String country;
  final String gender;
  final bool isBanned;
  final bool isAdmin;
  final bool isActive;
  final DateTime activeAt;
  final DateTime createdAt;
  final DateTime updatedAt;
  final List<String> reportedBy;
  final List<String> usersBlockedByMe;
  final List<String> posts;
  final List<String> followers;
  final List<String> following;
  final List<String> blockedBy;
  final bool chatNotify;
  final bool groupsNotify;
  final bool onlineStatus;
  final bool postLikesNotify;
  final bool commentLikesNotify;
  final bool replyLikesNotify;
  final String bio;
  final String city;
  final String dob;

  final List<String> intersts;
  final List<String> languages;

  final List<String> likedPosts;
  final List<String> likedComments;
  final List<String> likedReplies;

  final List<String> savedPosts;
  final List<String> sharedPosts;

  const User({
    required this.id,
    required this.username,
    required this.firstName,
    required this.lastName,
    required this.status,
    required this.email,
    required this.phone,
    required this.photoURL,
    required this.coverPhotoURL,
    required this.country,
    required this.gender,
    required this.isBanned,
    required this.isAdmin,
    required this.isActive,
    required this.activeAt,
    required this.createdAt,
    required this.updatedAt,
    required this.reportedBy,
    required this.usersBlockedByMe,
    required this.posts,
    required this.followers,
    required this.following,
    required this.blockedBy,
    required this.chatNotify,
    required this.groupsNotify,
    required this.onlineStatus,
    required this.postLikesNotify,
    required this.commentLikesNotify,
    required this.replyLikesNotify,
    required this.bio,
    required this.city,
    required this.dob,
    required this.intersts,
    required this.languages,
    required this.likedPosts,
    required this.likedComments,
    required this.likedReplies,
    required this.savedPosts,
    required this.sharedPosts,
  });

  bool get isOnline =>
      isActive && DateTime.now().difference(activeAt).inMinutes < 1;

  String get _uid => authProvider.uid;
  bool get isMe => id == _uid;
  bool get isFollowing => followers.contains(_uid);
  bool get isBlockedBy => blockedBy.contains(_uid);

  void toggleFollowing() {
    final currentUser = authProvider.user;
    if (isFollowing) {
      followers.remove(_uid);
      currentUser!.following.remove(id);
    } else {
      followers.add(_uid);
      currentUser!.following.add(id);
    }
    authProvider.rxUser.refresh();
  }

  void toggleBlocking() {
    final currentUser = authProvider.user;
    if (isBlockedBy) {
      blockedBy.remove(_uid);
      currentUser!.usersBlockedByMe.remove(id);
    } else {
      blockedBy.add(_uid);
      currentUser!.usersBlockedByMe.add(id);
    }
    authProvider.rxUser.refresh();
  }

  void toggleSave(String postId) {
    final currentUser = authProvider.user;
    final bool isSaved = currentUser!.savedPosts.contains(postId);
    if (isSaved) {
      savedPosts.remove(postId);
    } else {
      savedPosts.add(postId);
    }
    authProvider.rxUser.refresh();
  }

  void toggleShare(String postId) {
    final currentUser = authProvider.user;
    final bool isShared = currentUser!.sharedPosts.contains(postId);
    if (isShared) {
      sharedPosts.remove(postId);
    } else {
      sharedPosts.add(postId);
    }
    authProvider.rxUser.refresh();
  }

  bool isBlocked([String? uid]) => blockedBy.contains(uid ?? _uid);

  // void toggleBlock() {
  //   isBlocked() ? blockedBy.remove(_uid) : blockedBy.add(_uid);
  // }

  String get fullName => '$firstName $lastName';

  factory User.createNew({
    required String uid,
    required String phone,
    required String username,
    String? firstName,
    String? lastName,
    String? country,
    String? photoURL,
    String? coverPhoto,
    String? email,
    String? status,
    String? gender,
    String? bio,
    String? city,
    String? dob,
    // ignore: avoid_unused_constructor_parameters
    List<String>? intersts, //mkmkmk ??????
    // ignore: avoid_unused_constructor_parameters
    List<String>? languages, //mkmkmk ??????
  }) {
    return User(
      id: uid,
      username: username,
      firstName: firstName ?? '',
      lastName: lastName ?? '',
      email: email ?? '',
      photoURL: photoURL ?? '',
      status: status ?? '',
      coverPhotoURL: coverPhoto ?? '',
      phone: phone,
      country: country ?? '',
      gender: gender ?? '',
      isBanned: false,
      isAdmin: false,
      isActive: true,
      activeAt: DateTime.now(),
      createdAt: DateTime.now(),
      updatedAt: DateTime.now(),
      posts: const <String>[],
      followers: const <String>[],
      following: const <String>[],
      blockedBy: const <String>[],
      reportedBy: const <String>[],
      usersBlockedByMe: const <String>[],
      onlineStatus: true,
      chatNotify: true,
      groupsNotify: true,
      postLikesNotify: true,
      commentLikesNotify: true,
      replyLikesNotify: true,
      bio: bio ?? '',
      city: city ?? '',
      dob: dob ?? '',
      intersts: const <String>[],
      languages: const <String>[],
      likedPosts: const <String>[],
      likedComments: const <String>[],
      likedReplies: const <String>[],
      savedPosts: const <String>[],
      sharedPosts: const <String>[],
    );
  }

  List<String> get searchIndexes {
    final indices = <String>[];
    for (final s in [username, firstName, lastName]) {
      for (var i = 1; i < s.length; i++) {
        indices.add(s.substring(0, i).toLowerCase());
      }
    }
    return indices;
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'username': username,
      'firstName': firstName,
      'lastName': lastName,
      'fullName': fullName,
      'status': status,
      'email': email,
      'phone': phone,
      'photoURL': photoURL,
      'coverPhotoURL': coverPhotoURL,
      'onlineStatus': onlineStatus,
      'country': country,
      'gender': gender,
      'isBanned': isBanned,
      'isAdmin': isAdmin,
      'isActive': isActive,
      'activeAt': activeAt,
      'createdAt': createdAt,
      'reporters': reportedBy,
      'usersBlockedByMe': usersBlockedByMe,
      'chatNotify': chatNotify,
      'groupsNotify': groupsNotify,
      'posts': posts,
      'followers': followers,
      'following': following,
      'blockedBy': blockedBy,
      'searchIndexes': searchIndexes,
      'postLikesNotify': postLikesNotify,
      'commentLikesNotify': commentLikesNotify,
      'replyLikesNotify': replyLikesNotify,
      'bio': bio,
      'city': city,
      'dob': dob,
      'intersts': intersts,
      'languages': languages,
      'likedPosts': likedPosts,
      'likedComments': likedComments,
      'likedReplies': likedReplies,
      'savedPosts': savedPosts,
      'sharedPosts': sharedPosts,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      id: map['id'] as String? ?? '',
      username: (map['username'] ?? map['name']) as String? ?? '',
      firstName: map['firstName'] as String? ?? '',
      lastName: map['lastName'] as String? ?? '',
      status: map['status'] as String? ?? '',
      email: map['email'] as String? ?? '',
      phone: (map['phone'] ?? map['phoneNumber']) as String? ?? '',
      photoURL: (map['photoURL'] ?? map['photoURL']) as String? ?? '',
      coverPhotoURL: map['coverPhotoURL'] as String? ?? '',
      onlineStatus: map['onlineStatus'] as bool? ?? false,
      country: map['country'] as String? ?? '',
      gender: map['gender'] as String? ?? '',
      isBanned: map['isBanned'] as bool? ?? false,
      isAdmin: map['isAdmin'] as bool? ?? false,
      isActive: map['isActive'] as bool? ?? false,
      activeAt: timeFromJson(map['lastTimeSeen'] ?? map['activeAt']),
      createdAt: timeFromJson(map['createdAt']),
      updatedAt: timeFromJson(map['updatedAt']),
      reportedBy: List<String>.from(map['reportedBy'] as List? ?? const []),
      usersBlockedByMe:
          List<String>.from(map['usersBlockedByMe'] as List? ?? const []),
      chatNotify: map['chatNotify'] as bool? ?? false,
      groupsNotify: map['groupsNotify'] as bool? ?? false,
      posts: List<String>.from(map['posts'] as List? ?? []),
      followers: List<String>.from(map['followers'] as List? ?? []),
      following: List<String>.from(map['following'] as List? ?? []),
      blockedBy: List<String>.from(map['blockedBy'] as List? ?? []),
      postLikesNotify: map['postLikesNotify'] as bool? ?? false,
      commentLikesNotify: map['commentLikesNotify'] as bool? ?? false,
      replyLikesNotify: map['replyLikesNotify'] as bool? ?? false,
      bio: map['bio'] as String? ?? '',
      city: map['city'] as String? ?? '',
      dob: map['dob'] as String? ?? '',
      intersts: List<String>.from(map['intersts'] as List? ?? []),
      languages: List<String>.from(map['languages'] as List? ?? []),
      likedPosts: List<String>.from(map['likedPosts'] as List? ?? []),
      likedComments: List<String>.from(map['likedComments'] as List? ?? []),
      likedReplies: List<String>.from(map['likedReplies'] as List? ?? []),
      savedPosts: List<String>.from(map['savedPosts'] as List? ?? []),
      sharedPosts: List<String>.from(map['sharedPosts'] as List? ?? []),
    );
  }

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      id,
      username,
      firstName,
      lastName,
      status,
      email,
      phone,
      photoURL,
      coverPhotoURL,
      country,
      gender,
      isBanned,
      isAdmin,
      isActive,
      activeAt,
      createdAt,
      updatedAt,
      reportedBy,
      usersBlockedByMe,
      posts,
      followers,
      following,
      blockedBy,
      chatNotify,
      groupsNotify,
      onlineStatus,
      postLikesNotify,
      commentLikesNotify,
      replyLikesNotify,
      bio,
      city,
      dob,
      intersts,
      languages,
      likedPosts,
      likedComments,
      likedReplies,
      savedPosts,
      sharedPosts
    ];
  }

  User copyWith({
    String? id,
    String? username,
    String? firstName,
    String? lastName,
    String? status,
    String? email,
    String? phone,
    String? photoURL,
    String? coverPhotoURL,
    String? country,
    String? gender,
    bool? isBanned,
    bool? isAdmin,
    bool? isActive,
    DateTime? activeAt,
    DateTime? createdAt,
    DateTime? updatedAt,
    List<String>? reportedBy,
    List<String>? usersBlockedByMe,
    List<String>? posts,
    List<String>? followers,
    List<String>? following,
    List<String>? blockedBy,
    bool? chatNotify,
    bool? groupsNotify,
    bool? onlineStatus,
    bool? postLikesNotify,
    bool? commentLikesNotify,
    bool? replyLikesNotify,
    String? bio,
    String? city,
    String? dob,
    List<String>? intersts,
    List<String>? languages,
    List<String>? likedPosts,
    List<String>? likedComments,
    List<String>? likedReplies,
    List<String>? savedPosts,
    List<String>? sharedPosts,
  }) {
    return User(
      id: id ?? this.id,
      username: username ?? this.username,
      firstName: firstName ?? this.firstName,
      lastName: lastName ?? this.lastName,
      status: status ?? this.status,
      email: email ?? this.email,
      phone: phone ?? this.phone,
      photoURL: photoURL ?? this.photoURL,
      coverPhotoURL: coverPhotoURL ?? this.coverPhotoURL,
      country: country ?? this.country,
      gender: gender ?? this.gender,
      isBanned: isBanned ?? this.isBanned,
      isAdmin: isAdmin ?? this.isAdmin,
      isActive: isActive ?? this.isActive,
      activeAt: activeAt ?? this.activeAt,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
      reportedBy: reportedBy ?? this.reportedBy,
      usersBlockedByMe: usersBlockedByMe ?? this.usersBlockedByMe,
      posts: posts ?? this.posts,
      followers: followers ?? this.followers,
      following: following ?? this.following,
      blockedBy: blockedBy ?? this.blockedBy,
      chatNotify: chatNotify ?? this.chatNotify,
      groupsNotify: groupsNotify ?? this.groupsNotify,
      onlineStatus: onlineStatus ?? this.onlineStatus,
      postLikesNotify: postLikesNotify ?? this.postLikesNotify,
      commentLikesNotify: commentLikesNotify ?? this.commentLikesNotify,
      replyLikesNotify: replyLikesNotify ?? this.replyLikesNotify,
      bio: bio ?? this.bio,
      city: city ?? this.city,
      dob: dob ?? this.dob,
      intersts: intersts ?? this.intersts,
      languages: languages ?? this.languages,
      likedPosts: likedPosts ?? this.likedPosts,
      likedComments: likedComments ?? this.likedComments,
      likedReplies: likedReplies ?? this.likedReplies,
      savedPosts: savedPosts ?? this.savedPosts,
      sharedPosts: sharedPosts ?? this.sharedPosts,
    );
  }
}
