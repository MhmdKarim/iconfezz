// ignore_for_file: always_use_package_imports

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:timeago/timeago.dart';

import '../../../../imports.dart';
import '../../data/notifications.dart';
import '../../models/notification.dart';

class NotificationItem extends StatelessWidget {
  final NotificationModel not;

  const NotificationItem(
    this.not, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Padding(
      padding: const EdgeInsets.only(top: 10, right: 8, left: 8),
      child: Material(
        color: theme.brightness == Brightness.dark
            ? theme.primaryColor.withAlpha(50)
            : theme.primaryColor.withAlpha(50),
        borderRadius: BorderRadius.circular(8),
        child: GestureDetector(
          onTap: () => NotificationRepo.onNotificationTap(not),
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: <Widget>[
                    Icon(
                      icon,
                      size: 30,
                      color: theme.primaryColor,
                    ),
                    SizedBox(width: 12),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: MediaQuery.of(context).size.width - 80,
                          child: AppTextSubTitle(
                            not.text,
                            maxLines: 5,
                          ),
                        ),
                        SizedBox(height: 8),
                        AppTextSmall(
                          format(
                            not.createdAt!,
                            locale: LocaleSettings.currentLocale.languageTag,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  IconData get icon {
    if (not.isFollow) {
      return FontAwesomeIcons.userPlus;
    } else if (not.isPostReaction) {
      return Icons.thumb_up;
    } else if (not.isComment) {
      return FontAwesomeIcons.comment;
    } else {
      return FontAwesomeIcons.comment;
    }
  }
}
