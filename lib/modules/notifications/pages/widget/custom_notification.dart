// ignore_for_file: always_use_package_imports

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../../imports.dart';

class CustomNotificationWidget extends StatefulWidget {
  final String title;
  final String body;
  final void Function(BuildContext)? onTap;

  const CustomNotificationWidget({
    Key? key,
    required this.title,
    required this.body,
    this.onTap,
  }) : super(key: key);

  @override
  _CustomNotificationWidgetState createState() =>
      _CustomNotificationWidgetState();
}

class _CustomNotificationWidgetState extends State<CustomNotificationWidget> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return GestureDetector(
      onTap: () => widget.onTap!(context),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 16),
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
          color: theme.backgroundColor,
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              children: <Widget>[
                Icon(
                  FontAwesomeIcons.solidMessage,
                  color: Colors.white,
                ),
                SizedBox(width: 12),
                Expanded(
                  child: AppTextRegular(
                    widget.title,
                    overflow: TextOverflow.ellipsis,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: AppTextRegular(
                widget.body,
                overflow: TextOverflow.ellipsis,
                color: Colors.white,
              ),
            )
          ],
        ),
      ),
    );
  }
}
