// ignore_for_file: always_use_package_imports

import 'package:cloud_firestore/cloud_firestore.dart';

import '../../../imports.dart';
import '../../auth/data/user_repository.dart';

mixin ChatUsersRepository {
  static Stream<List<User>> onlineUsers() => UserRepository.usersCol
      .where('isActive', isEqualTo: true)
      .where('onlineStatus', isEqualTo: true)
      .limit(30)
      .snapshots()
      .map((e) => [for (final d in e.docs) d.data()])
      .map(
        (e) => [
          for (final u in e)
            if (!u.isMe && !u.isBanned && u.isOnline) u
        ],
      )
      .handleError((e) => logError(e));

  static DocumentSnapshot? lastMemDoc;
  static Future<List<User>> fetchAllUsers(int page) async {
    var query =
        UserRepository.usersCol.orderBy('activeAt', descending: true).limit(20);

    if (lastMemDoc != null && page != 0) {
      query = query.startAfterDocument(lastMemDoc!);
    }
    final docs = (await query.get()).docs;
    if (docs.isEmpty) return [];
    lastMemDoc = docs.last;
    return [for (final d in docs) d.data()];
  }

  static Future<List<User>> usersSearch(String query) async {
    final docs = await UserRepository.usersCol
        .where('searchIndexes', arrayContains: query.toLowerCase())
        .limit(10)
        .get();

    return [for (final g in docs.docs) g.data()];
  }
}
