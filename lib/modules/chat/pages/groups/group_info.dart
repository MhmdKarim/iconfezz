// ignore_for_file: always_use_package_imports

import '../../../../imports.dart';
import '../../data/groups.dart';
import '../../models/group.dart';

class GroupInfoPage extends StatefulWidget {
  final Group group;
  const GroupInfoPage(
    this.group, {
    Key? key,
  }) : super(key: key);

  @override
  _GroupInfoPageState createState() => _GroupInfoPageState();
}

class _GroupInfoPageState extends State<GroupInfoPage> {
  Group get group => widget.group;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Appbar(
        titleStr: group.name,
        actions: [
          if (group.isAdmin() || authProvider.user!.isAdmin)
            IconButton(
              icon: Icon(Icons.edit),
              onPressed: () async {
                await AppNavigator.toGroupEditor(group);
                setState(() {});
              },
            ),
        ],
      ),
      body: SingleChildScrollView(
        child: SizedBox(
          height: context.height - 80,
          child: Column(
            children: <Widget>[
              SizedBox(height: 20),
              AvatarWidget(
                group.photoURL,
                radius: 120,
              ),
              SizedBox(height: 20),
              AppTextTitle(
                group.name,
              ),
              SizedBox(height: 20),
              //Members
              ListTile(
                title: AppTextRegular(t.Members),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    AppTextRegular('${group.members.length}'),
                    SizedBox(width: 12),
                    Icon(Icons.keyboard_arrow_right)
                  ],
                ),
                onTap: () => AppNavigator.toGroupMembers(widget.group),
              ),

              //Notifications
              if (group.isMember())
                SwitchListTile.adaptive(
                  value: group.isMuted(),
                  title: AppTextRegular(t.Notifications),
                  subtitle: AppTextRegular(group.isMuted() ? t.Off : t.On),
                  onChanged: (_) {
                    setState(() => group.toggleMute());
                    GroupsRepository.muteOrUnmuteGroup(group);
                  },
                ),

              Spacer(flex: 4),
              if (group.isAdmin())
                AppButton(
                  group.isMember() ? t.LeaveGroup : t.Join,
                  onTap: () {
                    group.toggleJoin();
                    GroupsRepository.joinOrLeaveGroup(group);
                    Navigator.of(context).popUntil((r) => r.isFirst);
                    if (group.isMember()) {
                      AppNavigator.toGroupChat(group.id);
                    }
                  },
                ),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
