// ignore_for_file: always_use_package_imports

import 'package:badges/badges.dart';

import '../../../../../imports.dart';
import '../../../data/group_msgs.dart';
import '../../../models/group.dart';

class GroupItem extends StatelessWidget {
  final Group group;
  const GroupItem(
    this.group, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.symmetric(horizontal: 4),
      leading: AvatarWidget(
        group.photoURL,
        radius: 40,
      ),
      title: AppTextSubTitle(
        group.name,
      ),
      trailing: StreamBuilder<List<Message>>(
        stream: GroupMessagesRepository.msgsStream(group.id, 10),
        builder: (_, snapshot) {
          final msgs = [
            for (final m in snapshot.data ?? <Message>[])
              if (!m.isSeenByMe) m
          ];
          return Badge(
            showBadge: msgs.isNotEmpty,
            badgeContent: AppTextRegular(
              msgs.length > 9 ? '9+' : ' ${msgs.length} ',
              color: Colors.white,
            ),
          );
        },
      ),
      onTap: () => AppNavigator.toGroupChat(group.id),
    );
  }
}
