// import 'package:flutter_spinkit/flutter_spinkit.dart';
// import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

// import '../../../../components/search_bar.dart';
// import '../../../../imports.dart';
// import '../../data/groups.dart';
// import '../../models/group.dart';

// class GroupsSearch extends StatefulWidget {
//   @override
//   _GroupsSearchState createState() => _GroupsSearchState();
// }

// class _GroupsSearchState extends State<GroupsSearch> {
//   final searchBarController = SearchBarController<Group?>();
//   final pagingController = PagingController<int, Group>(firstPageKey: 0);

//   List<Group>? groups;
//   @override
//   void initState() {
//     pagingController.addPageRequestListener((page) async {
//       try {
//         final res = await GroupsRepository.fetchAllGroups(page);
//         if (res.length < 20) {
//           pagingController.appendLastPage(res);
//         } else {
//           pagingController.appendPage(res, page + 1);
//         }
//       } catch (e) {
//         logError(e);
//         pagingController.error = e;
//       }
//     });
//     super.initState();
//   }

//   @override
//   void dispose() {
//     super.dispose();
//     pagingController.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     final theme = Theme.of(context);
//     return WillPopScope(
//       onWillPop: () async {
//         searchBarController.clear();
//         await 0.1.delay();
//         return true;
//       },
//       child: Scaffold(
//         body: SafeArea(
//           child: Padding(
//             padding: const EdgeInsets.symmetric(horizontal: 8),
//             child: SearchBar<Group?>(
//               searchBarController: searchBarController,
//               icon: BackButton(),
//               onSearch: (s) => GroupsRepository.groupsSearch(s ?? ''),
//               // textStyle: TextStyle(color: Colors.white),
//               minimumChars: 2,
//               // searchBarStyle: SearchBarStyle(
//               //   backgroundColor: theme.primaryColor,
//               // ),
//               cancellationWidget: Icon(Icons.close),
//               shrinkWrap: true,
//               onItemFound: (group, _) => _SearchResultItem(group!),
//               placeHolder: PagedListView<int, Group>(
//                 pagingController: pagingController,
//                 builderDelegate: PagedChildBuilderDelegate<Group>(
//                   itemBuilder: (_, group, __) => _SearchResultItem(group),
//                   newPageProgressIndicatorBuilder: (_) =>
//                       SpinKitCircle(color: theme.primaryColor),
//                   firstPageProgressIndicatorBuilder: (_) =>
//                       SpinKitCircle(color: Colors.green),
//                 ),
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

// class _SearchResultItem extends StatelessWidget {
//   final Group group;

//   const _SearchResultItem(
//     this.group, {
//     Key? key,
//   }) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.only(top: 8),
//       child: ListTile(
//         onTap: () => AppNavigator.toGroupInfo(group),
//         leading: AvatarWidget(
//           group.photoURL,
//           radius: 50,
//         ),
//         title: AppTextSubTitle(
//           group.name,
//         ),
//         trailing: group.isAdmin()
//             ? OutlinedButton(onPressed: () {}, child: AppTextRegular(t.Admin))
//             : group.isMember()
//                 ? OutlinedButton(
//                     onPressed: () {},
//                     child: AppTextRegular(t.Joined),
//                   )
//                 : null,
//       ),
//     );
//   }
// }
