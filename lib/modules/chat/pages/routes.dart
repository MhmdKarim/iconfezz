// ignore_for_file: always_use_package_imports

import '../../../imports.dart';
import '../models/group.dart';
import 'chats/index.dart';
import 'chats/users_search.dart';
import 'conversation/group.dart';
import 'conversation/private.dart';
import 'groups/editor.dart';
import 'groups/group_info.dart';
import 'groups/members.dart';
import 'groups/search.dart';

mixin ChatRoutes {
  static Future<void> toChats() async {
    // Get.find<AdsHelper>().loadFullAds();
    await Get.to(() => ChatsPage());
    // Get.find<AdsHelper>().showFullAds();
  }

  static Future<void> toPrivateChat(String userId) async {
    appPrefs.page = AppPage.chatting(userId);
    await Get.to(() => PrivateChatPage(userId));
    appPrefs.page = AppPage.others();
  }

  static void toUsersSearchPage() => Get.to(() => UsersSearchPage());

  //Groups Module
  static Future<void> toGroupChat(String id) async {
    // Get.find<AdsHelper>().loadFullAds();
    appPrefs.page = AppPage.groupChatting(id);
    await Get.to(() => GroupChatPage(id));
    appPrefs.page = AppPage.others();
    // Get.find<AdsHelper>().showFullAds();
  }

  static Future<void> toGroupEditor([Group? toEdit]) async =>
      Get.to(() => GroupEditor(toEditGroup: toEdit));
  static Future<void> toGroupInfo(Group group) async =>
      Get.to(() => GroupInfoPage(group));
  static Future<void> toGroupMembers(Group group) async =>
      Get.to(() => GroupMembersPage(group));
  static Future<void> toGroupsSearch() async => Get.to(() => GroupsSearch());
}
