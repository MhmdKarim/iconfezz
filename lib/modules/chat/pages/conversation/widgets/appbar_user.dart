// ignore_for_file: always_use_package_imports

import 'package:timeago/timeago.dart';

import '../../../../../components/confirm_dialog.dart';
import '../../../../../imports.dart';
import '../../../data/chats.dart';
import 'typing.dart';

class UserAppBarTile extends StatelessWidget implements PreferredSizeWidget {
  final User user;
  final bool isTyping;

  UserAppBarTile(
    this.user, {
    Key? key,
    this.isTyping = false,
  }) : super(key: key);

  User? get currentUser => authProvider.user;
  final Rxn<User?> rxUser = Rxn<User?>();
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return AppBar(
      backgroundColor: theme.canvasColor,
      elevation: 0,
      leading: BackButton(
        color: theme.primaryColor,
      ),
      title: ListTile(
        contentPadding: EdgeInsets.all(0),
        leading: Hero(
          tag: user.id,
          child: Padding(
            padding: const EdgeInsets.all(4),
            child: Theme(
              data: ThemeData.dark(),
              child: AvatarWidget(
                user.photoURL,
                radius: 45,
                showBadge: user.isOnline && user.onlineStatus,
              ),
            ),
          ),
        ),
        title: AppTextRegularBold(
          user.fullName,
        ),
        subtitle: isTyping
            ? TypingWidget()
            : AppTextSmall(
                user.isOnline && user.onlineStatus
                    ? t.Online
                    : user.onlineStatus
                        ? format(
                            user.activeAt,
                            locale: LocaleSettings.currentLocale.languageTag,
                          )
                        : '',
              ),
        onTap: () => AppNavigator.toProfile(user.id),
      ),
      actions: <Widget>[
        PopupMenuButton<int>(
          icon: Icon(
            Icons.more_vert,
            color: Theme.of(context).primaryColor,
          ),
          itemBuilder: (_) => [
            // PopupMenuItem(
            //   value: 0,
            //   child: AppTextRegular(
            //     user.isBlocked() ? t.Unblock : t.Block,
            //   ),
            // ),
            PopupMenuItem(
              value: 1,
              child: AppTextRegular(t.RemoveConversation),
            ),
          ],
          onSelected: (v) {
            if (v == 0) {
              // showConfirmDialog(
              //   context,
              //   title:
              //       '${t.AreYouSure} ${user.isBlocked() ? t.Unblock : t.Block} ${user.username}',
              //   onConfirm: () {
              //     user.toggleBlocking();

              //     UserRepository.toggleBlockUser(user);
              //     rxUser.refresh();
              //     Navigator.pop(context);
              //   },
              // );
            } else {
              showConfirmDialog(
                context,
                title: t.ConfirmChatDeletion,
                onConfirm: () {
                  ChatsRepository.removeChat(user.id);
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
              );
            }
          },
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
