// ignore_for_file: always_use_package_imports

import '../../../../../imports.dart';
import '../../../models/group.dart';
import 'typing.dart';

class GroupAppBar extends StatelessWidget with PreferredSizeWidget {
  final Group group;

  const GroupAppBar(
    this.group, {
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: ListTile(
        contentPadding: EdgeInsets.all(0),
        leading: AvatarWidget(
          group.photoURL,
          radius: 40,
        ),
        title: AppTextRegularBold(
          group.name,
        ),
        subtitle: group.isTyping
            ? TypingWidget()
            : AppTextTitleBold(
                '${group.members.length} ${t.Members}',
              ),
        trailing: !group.isMember() ? null : Icon(Icons.info),
        onTap: !group.isMember() ? null : () => AppNavigator.toGroupInfo(group),
      ),
    );
  }

  @override
  Size get preferredSize => Size(double.infinity, 50);
}
