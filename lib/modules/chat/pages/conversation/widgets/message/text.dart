// ignore_for_file: always_use_package_imports

import 'package:flutter_linkify/flutter_linkify.dart';

import '../../../../../../imports.dart';

class TextMsgItem extends StatelessWidget {
  final Message msg;

  const TextMsgItem(
    this.msg, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      constraints: BoxConstraints(maxWidth: context.width - 100),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: theme.primaryColor,
      ),
      child: Linkify(
        onOpen: (l) => launchURL(l.url),
        text: msg.content,
        style: appFont(
          textStyle: theme.textTheme.subtitle1,
        ),
      ),
    );
  }
}
