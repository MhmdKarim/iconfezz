// ignore_for_file: always_use_package_imports

import 'package:badges/badges.dart';

import '../../../../../imports.dart';

class ChatIcon extends StatelessWidget {
  const ChatIcon({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Badge(
      position: BadgePosition.topEnd(end: 5, top: 5),
      child: AppButton(
        t.Chats,
        width: 200,
        // IconButton(
        //   icon: Icon(
        //     Icons.email,
        //     size: 32,
        //     color: Theme.of(context).primaryColor,
        //   ),
        onTap: AppNavigator.toChats,
      ),
    );
  }
}
