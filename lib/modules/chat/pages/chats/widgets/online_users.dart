// ignore_for_file: always_use_package_imports

import '../../../../../imports.dart';
import '../../../data/users.dart';

class OnlineUsersWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //final theme = Theme.of(context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(12),
          child: AppTextTitleBold(
            t.OnlineUsers,
            color: Colors.grey,
          ),
        ),
        SizedBox(
          height: 105.0,
          child: StreamBuilder<List<User>>(
            stream: ChatUsersRepository.onlineUsers(),
            builder: (_, snap) {
              final users = snap.data ?? [];
              return ListView.builder(
                itemCount: users.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (_, i) => _OnlineUserItem(users[i]),
              );
            },
          ),
        ),
      ],
    );
  }
}

class _OnlineUserItem extends StatelessWidget {
  final User user;

  const _OnlineUserItem(
    this.user, {
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => AppNavigator.toPrivateChat(user.id),
      child: Padding(
        padding: const EdgeInsets.only(right: 16),
        child: Column(
          children: <Widget>[
            AvatarWidget(
              user.photoURL,
              showBadge: user.isOnline && user.onlineStatus,
            ),
            SizedBox(height: 4),
            AppTextRegularBold(user.fullName, fontWeight: FontWeight.w600),
          ],
        ),
      ),
    );
  }
}
