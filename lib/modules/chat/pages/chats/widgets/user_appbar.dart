// ignore_for_file: always_use_package_imports

import 'package:timeago/timeago.dart';

import '../../../../../imports.dart';

class UserAppBarTile extends StatelessWidget {
  final User user;

  const UserAppBarTile(
    this.user, {
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    //final theme = Theme.of(context);

    return ListTile(
      contentPadding: EdgeInsets.all(0),
      leading: Hero(
        tag: user.id,
        child: Padding(
          padding: const EdgeInsets.all(4),
          child: Theme(
            data: ThemeData.dark(),
            child: AvatarWidget(
              user.photoURL,
              radius: 45,
            ),
          ),
        ),
      ),
      title: AppTextRegularBold(
        user.username,
      ),
      subtitle: AppTextSmall(
        user.isOnline
            ? t.Online
            : user.onlineStatus
                ? format(
                    user.activeAt,
                    locale: LocaleSettings.currentLocale.languageTag,
                  )
                : '',
      ),
      onTap: () => AppNavigator.toProfile(user.id),
    );
  }
}
