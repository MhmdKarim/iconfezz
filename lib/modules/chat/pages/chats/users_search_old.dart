// import 'package:flutter_spinkit/flutter_spinkit.dart';
// import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

// import '../../../../components/search_bar.dart';
// import '../../../../imports.dart';
// import '../../data/users.dart';

// class UsersSearchPage extends StatefulWidget {
//   @override
//   _UsersSearchPageState createState() => _UsersSearchPageState();
// }

// class _UsersSearchPageState extends State<UsersSearchPage> {
//   final searchBarController = SearchBarController<User?>();

//   final pagingController = PagingController<int, User>(firstPageKey: 0);

//   List<User>? users;
//   @override
//   void initState() {
//     pagingController.addPageRequestListener((page) async {
//       try {
//         final res = await ChatUsersRepository.fetchAllUsers(page);
//         if (res.length < 20) {
//           pagingController.appendLastPage(res);
//         } else {
//           pagingController.appendPage(res, page + 1);
//         }
//       } catch (e) {
//         logError(e);
//         pagingController.error = e;
//       }
//     });
//     super.initState();
//   }

//   @override
//   void dispose() {
//     pagingController.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     final theme = Theme.of(context);

//     return WillPopScope(
//       onWillPop: () async {
//         searchBarController.clear();
//         await 0.1.delay();
//         return true;
//       },
//       child: Scaffold(
//         body: SafeArea(
//           child: Padding(
//             padding: const EdgeInsets.symmetric(horizontal: 8),
//             child: SearchBar<User?>(
//               searchBarController: searchBarController,
//               icon: BackButton(
//                 color: theme.primaryColor,
//               ),
//               onSearch: (s) => ChatUsersRepository.usersSearch(s ?? ''),
//               minimumChars: 2,
//               cancellationWidget: Icon(Icons.close),
//               shrinkWrap: true,
//               onItemFound: (item, _) => _SearchResultItem(item!),
//               placeHolder: PagedListView<int, User>(
//                 pagingController: pagingController,
//                 builderDelegate: PagedChildBuilderDelegate(
//                   itemBuilder: (_, item, __) => _SearchResultItem(item),
//                   newPageProgressIndicatorBuilder: (_) =>
//                       SpinKitCircle(color: theme.primaryColor),
//                   firstPageProgressIndicatorBuilder: (_) =>
//                       SpinKitCircle(color: theme.primaryColor),
//                 ),
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

// class _SearchResultItem extends StatelessWidget {
//   final User user;

//   const _SearchResultItem(
//     this.user, {
//     Key? key,
//   }) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     if (user.isMe) return SizedBox();
//     return ListTile(
//       onTap: () => AppNavigator.toProfile(user.id),
//       contentPadding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
//       leading: AvatarWidget(
//         user.photoURL,
//         radius: 50,
//       ),
//       title: AppTextSubTitle(
//         user.fullName,
//       ),
//       subtitle: AppTextRegular(user.username),
//     );
//   }
// }
