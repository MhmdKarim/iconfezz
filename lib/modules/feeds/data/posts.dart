// ignore_for_file: always_use_package_imports

import 'package:cloud_firestore/cloud_firestore.dart';

import '../../../imports.dart';
import '../../auth/data/user_repository.dart';
import '../../notifications/data/notifications.dart';
import '../../notifications/models/notification.dart';
import '../models/post.dart';
import 'comments.dart';

mixin PostsRepository {
  static String? get _uid => authProvider.uid;
  static List<String>? get _userFollowing => authProvider.user!.following;
  static final _firestore = FirebaseFirestore.instance;

  static final postsCol = _firestore.collection('posts').withConverter<Post>(
        fromFirestore: (m, _) => Post.fromMap(m.data()!),
        toFirestore: (m, _) => m.toMap(),
      );
  static DocumentReference<Post> postDoc(String? postID) =>
      postsCol.doc(postID);

  static final reportedPostsCol =
      _firestore.collection('reported_posts').withConverter<Post>(
            fromFirestore: (m, _) => Post.fromMap(m.data()!),
            toFirestore: (m, _) => m.toMap(),
          );
  static DocumentReference<Post> reportedDoc(String id) =>
      reportedPostsCol.doc(id);

  //SECTION ------------------------------Posts
  static DocumentSnapshot? lastPostDoc;

  static Future<List<Post>> fetchFollowingPosts(int page) async {
    var col = postsCol
        .limit(20)
        .where(
          'authorID',
          whereIn: _userFollowing!.isNotEmpty ? _userFollowing : [''],
        ) //mkmkmk
        .orderBy('createdAt', descending: true);

    if (page != 0 && lastPostDoc != null) {
      col = col.startAfterDocument(lastPostDoc!);
    }
    final docs = (await col.get()).docs;
    if (docs.isNotEmpty) lastPostDoc = docs.last;
    return docs.map((s) => s.data()).toList();
  }

  static Future<List<Post>> fetchFollowingSharedPosts(int page) async {
    var col = postsCol
        .limit(20)
        .where(
          'authorID',
          whereIn: _userFollowing!.isNotEmpty ? _userFollowing : [''],
        ) //mkmkmk
        .orderBy('createdAt', descending: true);

    if (page != 0 && lastPostDoc != null) {
      col = col.startAfterDocument(lastPostDoc!);
    }
    final docs = (await col.get()).docs;
    if (docs.isNotEmpty) lastPostDoc = docs.last;
    return docs.map((s) => s.data()).toList();
  }

  static Stream<Post> singlePostStream(String? postID) {
    return postDoc(postID).snapshots().map(
          (s) => s.data()!,
        );
  }

  static Future<Post> fetchPost(String? id) =>
      postDoc(id).get().then((v) => v.data()!);

  static Stream<List<Post>> getUserPostsStream(User user, int offset) {
    return _firestore
        .collection('posts')
        .where('id', whereIn: user.posts)
        .limit(offset)
        .orderBy('createdAt', descending: true)
        .snapshots()
        .map(
          (s) => [
            for (final d in s.docs)
              Post.fromMap(
                d.data(),
              ),
          ],
        )
        .handleError((e) => logError(e));
  }

  // static Stream<List<Post>> getUserPostsStream(String userId, int offset) {
  //   return _firestore
  //       .collection('posts')
  //       .where('authorID', isEqualTo: userId)
  //       .limit(offset)
  //       .orderBy('createdAt', descending: true)
  //       .snapshots()
  //       .map(
  //         (s) => [
  //           for (final d in s.docs)
  //             Post.fromMap(
  //               d.data(),
  //             ),
  //         ],
  //       )
  //       .handleError((e) => logError(e));
  // }

  static Future<void> addPost(Post post) async {
    await postDoc(post.id).set(post);
    await _firestore.doc('users/${post.authorID}').update(
      {
        'posts': FieldValue.arrayUnion([post.id])
      },
    );
  }

  static Future<void> updatePost(Post post) async {
    await postDoc(post.id).update({
      'content': post.content,
      'updatedAt': FieldValue.serverTimestamp(),
    });
  }

  static Future<void> removePost(String postID, List<String> comments) async {
    await postDoc(postID).delete();
    for (final id in comments) {
      await CommentsRepository.commentDoc(id).delete();
    }
    await _firestore.doc('users/$_uid').update(
      {
        'posts': FieldValue.arrayRemove([postID])
      },
    );
  }

//SECTION ------------------------------Post Reactions

  static Future<void> togglePostReaction(Post post) async {
    await postDoc(post.id).update({
      'usersLikes': post.liked
          ? FieldValue.arrayUnion([_uid])
          : FieldValue.arrayRemove([_uid]),
      'likesCount':
          post.liked ? FieldValue.increment(1) : FieldValue.increment(-1),
    });
    final User? user =
        await UserRepository.fetchUser(post.authorID); //mkmkmk notif
    if (post.liked && !post.isMine && user!.postLikesNotify) {
      NotificationRepo.sendNotificaion(
        NotificationModel.create(
          toId: post.authorID,
          postID: post.id,
          type: NotificationType.PostReaction,
        ),
      );
    }
  }

//SECTION ------------------------------Post Report

  static Stream<List<Post>> reportedPostsStream() =>
      reportedPostsCol.orderBy('id', descending: true).snapshots().map(
            (s) => [for (final d in s.docs) d.data()],
          );

  static Future<void> reportPost(Post post) async {
    post.reportedBy.add(_uid);
    await reportedDoc(post.id).set(post, SetOptions(merge: true));
    await postDoc(post.id).update({
      'reportedBy': FieldValue.arrayUnion([_uid])
    });
  }

  // static Future<void> unReportPost(String id) async {
  //   _firestore.runTransaction(
  //     (tr) async => tr
  //       ..update(postDoc(id), {
  //         'reportedBy': FieldValue.arrayRemove([_uid])
  //       })
  //       ..delete(reportedDoc(id)),
  //   );
  // }

//SECTION ------------------------------Post Save

  static Future<void> toggleSavePost(Post post) async {
    await FirebaseFirestore.instance.runTransaction((tr) async {
      if (post.isSavedByMe) {
        tr.update(postDoc(post.id), {
          'savedBy': FieldValue.arrayUnion([_uid])
        });
      } else {
        tr.update(postDoc(post.id), {
          'savedBy': FieldValue.arrayRemove([_uid])
        });
      }
    });
  }

  static Stream<List<Post>> getSavedPostsStream(User user, int offset) {
    return _firestore
        .collection('posts')
        .where('id', whereIn: user.savedPosts)
        .limit(offset)
        //.orderBy('createdAt', descending: true)
        .snapshots()
        .map(
          (s) => [
            for (final d in s.docs)
              Post.fromMap(
                d.data(),
              ),
          ],
        )
        .handleError((e) => logError(e));
  }

//SECTION ------------------------------Post Share

  static Future<void> toggleSharePost(Post post) async {
    await FirebaseFirestore.instance.runTransaction((tr) async {
      if (post.isSharedByMe) {
        tr.update(postDoc(post.id), {
          'sharedBy': FieldValue.arrayUnion([_uid])
        });
      } else {
        tr.update(postDoc(post.id), {
          'sharedBy': FieldValue.arrayRemove([_uid])
        });
      }
    });
  }

  static Stream<List<Post>> getSharedPostsStream(User user, int offset) {
    return _firestore
        .collection('posts')
        .where('id', whereIn: user.sharedPosts)
        .limit(offset)
        //.orderBy('createdAt', descending: true)
        .snapshots()
        .map(
          (s) => [
            for (final d in s.docs)
              Post.fromMap(
                d.data(),
              ),
          ],
        )
        .handleError((e) => logError(e));
  }

  // static Stream<List<Post>> getFollowingSharedPostsStream(
  //   User user,
  //   int offset,
  // ) {
  //   return _firestore
  //       .collection('posts')
  //       .where('id', whereIn: user.sharedPosts)
  //       .limit(offset)
  //       .orderBy('createdAt', descending: true)
  //       .snapshots()
  //       .map(
  //         (s) => [
  //           for (final d in s.docs)
  //             Post.fromMap(
  //               d.data(),
  //             ),
  //         ],
  //       )
  //       .handleError((e) => logError(e));
  // }
}
