// ignore_for_file: always_use_package_imports

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/services.dart';
import 'package:focused_menu/focused_menu.dart';
import 'package:focused_menu/modals.dart';
import 'package:timeago/timeago.dart';

import '../../../../../../imports.dart';
import '../../../../../auth/data/user_repository.dart';
import '../../../../data/comments.dart';
import '../../../../models/comment.dart';

class ReplyWidget extends StatefulWidget {
  final Comment comment;
  final String commentParentId;
  final bool visible;
  const ReplyWidget({
    Key? key,
    required this.comment,
    required this.commentParentId,
    required this.visible,
  }) : super(key: key);

  @override
  _ReplyWidgetState createState() => _ReplyWidgetState();
}

class _ReplyWidgetState extends State<ReplyWidget> {
  bool get isAdmin => authProvider.user!.isAdmin;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return FocusedMenuHolder(
      menuItems: _menuItems(context),
      onPressed: () {},
      child: FutureBuilder<String?>(
        future: UserRepository.fetchUserImage(widget.comment.authorID),
        builder: (context, snapshot) {
          final image = snapshot.data;
          return ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 5),
            leading: Column(
              children: [
                AvatarWidget(
                  image ?? '',
                  radius: 40,
                  onTap: widget.comment.isMine
                      ? null
                      : () => AppNavigator.toProfile(widget.comment.authorID),
                ),
              ],
            ),
            title: Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                border: Border.all(color: theme.iconTheme.color!, width: 0.5),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: widget.comment.isMine
                        ? null
                        : () => AppNavigator.toProfile(widget.comment.authorID),
                    child: AppTextRegular(
                      '${widget.comment.isMine ? t.Me : widget.comment.authorName} ',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 8),
                  AppTextRegular(
                    widget.comment.content!,
                  ),
                ],
              ),
            ),
            subtitle: Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Row(
                children: <Widget>[
                  AppTextRegular(
                    format(
                      widget.comment.createdAt!,
                      locale: LocaleSettings.currentLocale.languageTag,
                    ),
                  ),
                  // SizedBox(width: 20),
                  // Visibility(
                  //   visible: widget.visible,
                  //   child: AppTextRegular(
                  //     widget.comment.usersLikeIds.isEmpty
                  //         ? ''
                  //         : '${widget.comment.usersLikeIds.length} ${t.Likes}',
                  //   ),
                  // ),
                  // SizedBox(width: 8),
                  // Visibility(
                  //   visible: widget.visible,
                  //   child: GestureDetector(
                  //     onTap: () =>
                  //         CommentsRepository.toggleLikeComment(widget.comment),
                  //     child: Icon(
                  //       widget.comment.isLikedComment
                  //           ? Icons.thumb_up
                  //           : Icons.thumb_up_alt_outlined,
                  //       color: theme.primaryColor,
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  List<FocusedMenuItem> _menuItems(BuildContext context) => [
        FocusedMenuItem(
          backgroundColor: context.theme.scaffoldBackgroundColor,
          title: AppTextRegular(t.Copy),
          trailingIcon: Icon(Icons.copy),
          onPressed: () {
            Clipboard.setData(ClipboardData(text: widget.comment.content));
            BotToast.showText(text: t.Copied);
          },
        ),
        if (widget.comment.isMine || isAdmin)
          FocusedMenuItem(
            backgroundColor: context.theme.scaffoldBackgroundColor,
            title: AppTextRegular(t.Delete, color: Colors.redAccent),
            trailingIcon: Icon(
              Icons.delete,
              color: Colors.redAccent,
            ),
            onPressed: () {
              AwesomeDialog(
                context: context,
                dialogType: DialogType.NO_HEADER,
                body: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: AppTextRegular(t.CommentRemoveConfirm),
                ),
                btnCancelOnPress: () => null,
                btnOkOnPress: () {
                  CommentsRepository.removeCommentReply(
                    widget.comment.parentID,
                    widget.comment,
                  );
                },
              ).show();
            },
          ),
      ];
}
