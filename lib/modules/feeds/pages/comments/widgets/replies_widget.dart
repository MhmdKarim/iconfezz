// ignore_for_file: always_use_package_imports

import 'package:timeago/timeago.dart';

import '../../../../../imports.dart';
import '../../../../auth/data/user_repository.dart';
import '../../../models/comment.dart';

class RepliesWidget extends StatelessWidget {
  final List<Comment> replies;

  const RepliesWidget({
    Key? key,
    required this.replies,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          for (final reply in replies.toList())
            replyWidget(reply, context, theme),
        ],
      ),
    );
  }

  Widget replyWidget(Comment reply, BuildContext context, ThemeData theme) {
    return FutureBuilder<String?>(
      future: UserRepository.fetchUserImage(reply.authorID),
      builder: (context, snapshot) {
        final image = snapshot.data;
        return Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(width: 60),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: AvatarWidget(
                  image ?? '',
                  radius: 30,
                  onTap: reply.isMine
                      ? null
                      : () => AppNavigator.toProfile(reply.authorID),
                ),
              ),
              SizedBox(width: 10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width - 120,
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      //color: Colors.grey,
                      border: Border.all(
                        color: theme.iconTheme.color!,
                        width: 0.5,
                      ),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: reply.isMine
                              ? null
                              : () => AppNavigator.toProfile(reply.authorID),
                          child: AppTextRegular(
                            reply.isMine ? t.Me : reply.authorName,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 8),
                        AppTextRegular(reply.content!),
                      ],
                    ),
                  ),
                  SizedBox(height: 5),
                  AppTextRegular(
                    format(
                      reply.createdAt!,
                      locale: LocaleSettings.currentLocale.languageTag,
                    ),
                    color: Colors.grey,
                  ),
                  SizedBox(height: 5),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
