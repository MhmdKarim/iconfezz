// ignore_for_file: always_use_package_imports

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/services.dart';
import 'package:focused_menu/focused_menu.dart';
import 'package:focused_menu/modals.dart';
import 'package:timeago/timeago.dart';

import '../../../../../components/confirm_dialog.dart';
import '../../../../../imports.dart';
import '../../../../auth/data/user_repository.dart';
import '../../../data/comments.dart';
import '../../../models/comment.dart';
import 'input.dart';
import 'replies_widget.dart';

class CommentWidget extends StatefulWidget {
  final Comment comment;
  final ValueChanged<String>? onEdit;
  const CommentWidget(
    this.comment, {
    Key? key,
    this.onEdit,
  }) : super(key: key);

  @override
  _CommentWidgetState createState() => _CommentWidgetState();
}

class _CommentWidgetState extends State<CommentWidget> {
  // Comment get comment => widget.comment;
  bool get isAdmin => authProvider.user!.isAdmin;
  String? imageUrl;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return FutureBuilder<String?>(
      future: UserRepository.fetchUserImage(widget.comment.authorID),
      builder: (context, snapshot) {
        final image = snapshot.data;
        return SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: ListTile(
                  //horizontalTitleGap: 10,
                  contentPadding: EdgeInsets.symmetric(horizontal: 5),
                  leading: Column(
                    children: [
                      AvatarWidget(
                        image ?? '',
                        radius: 40,
                        onTap: widget.comment.isMine
                            ? null
                            : () =>
                                AppNavigator.toProfile(widget.comment.authorID),
                      ),
                    ],
                  ),
                  title: FocusedMenuHolder(
                    menuItems: _menuItems(),
                    animateMenuItems: false,
                    onPressed: () {},
                    child: Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        // color: Colors.grey,
                        border: Border.all(
                          color: theme.iconTheme.color!,
                          width: 0.5,
                        ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          InkWell(
                            onTap: widget.comment.isMine
                                ? null
                                : () => AppNavigator.toProfile(
                                      widget.comment.authorID,
                                    ),
                            child: AppTextRegular(
                              '${widget.comment.isMine ? t.Me : widget.comment.authorName} ',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          AppTextRegular(
                            widget.comment.content!,
                          ),
                        ],
                      ),
                    ),
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top: 5),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        AppTextRegular(
                          format(
                            widget.comment.createdAt!,
                            locale: LocaleSettings.currentLocale.languageTag,
                          ),
                        ),
                        // SizedBox(width: 30),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            GestureDetector(
                              onTap: () => AppNavigator.toPeopleList(
                                widget.comment.usersLikeIds,
                                t.PeopleWhoReacted,
                              ),
                              child: AppTextRegular(
                                widget.comment.usersLikeIds.isEmpty
                                    ? ''
                                    : '${widget.comment.usersLikeIds.length} ${t.Likes}',
                              ),
                            ),
                            SizedBox(width: 8),
                            GestureDetector(
                              onTap: () => CommentsRepository.toggleLikeComment(
                                widget.comment,
                              ),
                              child: Icon(
                                widget.comment.isLikedComment
                                    ? Icons.thumb_up
                                    : Icons.thumb_up_alt_outlined,
                                color: theme.primaryColor,
                              ),
                            ),
                          ],
                        ),

                        // const SizedBox(width: 30),
                        GestureDetector(
                          onTap: () => AppNavigator.toReplies(widget.comment),
                          child: AppTextRegularBold(
                            t.Reply,
                            color: theme.primaryColor,
                          ),
                        ),
                        // const SizedBox(width: 30),
                        Visibility(
                          visible: widget.comment.replyComments.isNotEmpty,
                          child: GestureDetector(
                            onTap: () => setState(() {
                              widget.comment.showReplies =
                                  !widget.comment.showReplies!;
                              // widget.comment.toggleShowReplies();
                            }),
                            child: AppTextRegularBold(
                              widget.comment.showReplies == false
                                  ? 'Show Replies'
                                  : 'Hide Replies', //mkmkmk
                              color: theme.primaryColor,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.comment.showReplies!,
                child: RepliesWidget(
                  replies: widget.comment.replyComments,
                ),
              )
            ],
          ),
        );
      },
    );
  }

  List<FocusedMenuItem> _menuItems() => [
        if (widget.comment.isMine || isAdmin)
          FocusedMenuItem(
            backgroundColor: context.theme.scaffoldBackgroundColor,
            title: AppTextRegular(t.Edit),
            trailingIcon: Icon(Icons.edit),
            onPressed: () {
              AwesomeDialog(
                padding: EdgeInsets.symmetric(horizontal: 10),
                dialogType: DialogType.NO_HEADER,
                dialogBorderRadius: BorderRadius.circular(15),
                context: context,
                body: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    AppTextRegularBold('Edit Comment'), //mkmkmk
                    SizedBox(
                      height: 10,
                    ),
                    CommentInput(
                      showAvatar: false,
                      initContent: widget.comment.content,
                      onSubmit: (c) {
                        widget.onEdit?.call(c);
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              ).show();
            },
          ),
        FocusedMenuItem(
          backgroundColor: context.theme.scaffoldBackgroundColor,
          title: AppTextRegular(t.Copy),
          trailingIcon: Icon(Icons.copy),
          onPressed: () {
            Clipboard.setData(ClipboardData(text: widget.comment.content));
            BotToast.showText(text: t.Copied);
          },
        ),
        FocusedMenuItem(
          backgroundColor: context.theme.scaffoldBackgroundColor,
          title: AppTextRegular(t.Reply),
          trailingIcon: Icon(Icons.reply),
          onPressed: () => AppNavigator.toReplies(widget.comment),
        ),
        if (widget.comment.isMine || isAdmin)
          FocusedMenuItem(
            backgroundColor: context.theme.scaffoldBackgroundColor,
            title: AppTextRegular(t.Delete, color: Colors.redAccent),
            trailingIcon: Icon(
              Icons.delete,
              color: Colors.redAccent,
            ),
            onPressed: () {
              showConfirmDialog(
                context,
                title: t.CommentRemoveConfirm,
                onConfirm: () {
                  CommentsRepository.removeComment(widget.comment);
                  Navigator.of(context).pop();
                },
              );
              // AwesomeDialog(
              //   context: context,
              //   dialogType: DialogType.NO_HEADER,
              //   body: AppTextRegular(t.CommentRemoveConfirm),
              //   btnCancelOnPress: () => null,
              //   btnOkOnPress: () =>
              //       CommentsRepository.removeComment(widget.comment),
              // ).show();
            },
          ),
      ];
}
