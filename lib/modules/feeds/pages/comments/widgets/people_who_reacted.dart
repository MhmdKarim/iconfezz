// ignore_for_file: always_use_package_imports

import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../../components/user_list.dart';
import '../../../../../imports.dart';

class PeopleWhoReacted extends StatefulWidget {
  final List<String?> usersId;
  final String? title;

  const PeopleWhoReacted({Key? key, required this.usersId, this.title})
      : super(key: key);

  @override
  _UsersListState createState() => _UsersListState();
}

class _UsersListState extends State<PeopleWhoReacted>
    with AutomaticKeepAliveClientMixin {
  final _refreshController = RefreshController();
  Rxn<User?> rxUser = Rxn<User?>();
  int offset = 10;
  @override
  void initState() {
    // if (widget.userID?.isNotEmpty == true) {
    //   UserRepository.fetchUser(widget.userID).then(rxUser);
    // } else {
    //   rxUser = authProvider.rxUser;
    // }
    super.initState();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final ids = widget.usersId.take(offset).toList();
    return Scaffold(
      appBar: Appbar(
        titleStr: t.PeopleWhoReacted,
      ),
      body: SmartRefresher(
        controller: _refreshController,
        enablePullUp: offset <= ids.length,
        enablePullDown: false,
        onLoading: () async {
          setState(() => offset += 10);
          _refreshController.loadComplete();
        },
        child: ListView.builder(
          itemCount: ids.length,
          itemBuilder: (_, i) => UserItemWidget(ids[i]),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
