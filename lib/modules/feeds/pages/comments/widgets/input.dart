// ignore_for_file: always_use_package_imports

import '../../../../../imports.dart';

class CommentInput extends StatefulWidget {
  final Function(String)? onSubmit;
  final EdgeInsets? contentPadding;
  final String? initContent;
  final bool showAvatar;
  final bool? isComment;

  const CommentInput({
    Key? key,
    this.onSubmit,
    this.contentPadding,
    this.initContent,
    this.showAvatar = true,
    this.isComment,
  }) : super(key: key);

  @override
  _CommentInputState createState() => _CommentInputState();
}

class _CommentInputState extends State<CommentInput> {
  final controller = TextEditingController();
  final currentUser = authProvider.user;
  @override
  void initState() {
    controller.text = widget.initContent ?? '';
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Padding(
      padding: widget.contentPadding ??
          const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: currentUser!.isBanned
          ? AppTextRegular(t.NotAllowedToComment)
          : SizedBox(
              height: 75,
              width: MediaQuery.of(context).size.width,
              child: Theme(
                data: Theme.of(context).copyWith(
                  textSelectionTheme: TextSelectionThemeData(
                    selectionColor: theme.primaryColor,
                    selectionHandleColor: theme.primaryColor,
                    cursorColor: theme.primaryColor,
                  ),
                ),
                child: TextField(
                  controller: controller,
                  keyboardType: TextInputType.multiline,
                  cursorColor: theme.primaryColor,
                  cursorHeight: 25,
                  textInputAction: TextInputAction.send,
                  //  enableInteractiveSelection: false,
                  decoration: InputDecoration(
                    fillColor: Colors.grey,
                    filled: false,
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide(color: theme.primaryColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide(color: theme.primaryColor),
                    ),
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.all(12),
                    //hintText: widget.isComment! ? t.TypeComment : t.TypeReply,
                    suffixIcon: GestureDetector(
                      onTap: _onSend,
                      child: Icon(Icons.send, color: theme.primaryColor),
                    ),
                  ),
                  onEditingComplete: _onSend,
                ),
              ),
            ),
    );
  }

  Future<void> _onSend() async {
    final content = controller.text.trim();
    if (content.isEmpty) BotToast.showText(text: t.AddSomeContent);
    widget.onSubmit!(content);
    await Future.delayed(Duration(milliseconds: 50));
    controller.text = '';
    controller.clear();
  }
}
