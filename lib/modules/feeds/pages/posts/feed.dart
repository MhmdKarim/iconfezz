// ignore_for_file: always_use_package_imports

import 'dart:async';

import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../../../components/appbar_icon.dart';
import '../../../../imports.dart';
import '../../../auth/data/user_repository.dart';
import '../../../notifications/provider.dart';
import '../../data/posts.dart';
import '../../models/post.dart';
import 'widgets/people_you_may_know.dart';
import 'widgets/post_add.dart';
import 'widgets/post_item.dart';
import 'widgets/shimmer.dart';

class FeedPage extends StatefulWidget {
  @override
  _PostsScreenState createState() => _PostsScreenState();
}

class _PostsScreenState extends State<FeedPage> with WidgetsBindingObserver {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final scrolleControllerPosts = ScrollController();
  String get uid => authProvider.user!.id;

  final pagingPosts = PagingController<int, Post>(firstPageKey: 0);
  final pagingUsers = PagingController<int, User>(firstPageKey: 0);
  StreamSubscription<User>? userSub;
  bool? isPostsAvailable;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    pagingPosts.addPageRequestListener((page) async {
      try {
        final res = await PostsRepository.fetchFollowingPosts(page);
        if (res.length < 20) {
          pagingPosts.appendLastPage(res);
        } else {
          pagingPosts.appendPage(res, page + 1);
        }
      } catch (error) {
        pagingPosts.error = error;
        error.printError();
      }
    });

    pagingUsers.addPageRequestListener((page) async {
      try {
        final res = await UserRepository.fetchPeopleToFollow(page);

        if (res.length < 20) {
          pagingUsers.appendLastPage(res);
        } else {
          pagingUsers.appendPage(res, page + 1);
        }
      } catch (error) {
        pagingUsers.error = error;
        // print(error);
      }
    });

    userSub =
        UserRepository.userStream()?.listen((e) => authProvider.rxUser(e));
    Get.put<PagingController>(pagingPosts);
    Get.put<PagingController>(pagingUsers);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    scrolleControllerPosts.dispose();
    notificationProvider.dispose();
    userSub?.cancel();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      UserRepository.updateActiveAt(false);
    } else if (state == AppLifecycleState.resumed) {
      UserRepository.updateActiveAt(true);
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      key: scaffoldKey,
      appBar: Appbar(
        titleStr: t.Home,
        leading: IconButton(
          icon: Icon(Icons.search),
          onPressed: AppNavigator.toUsersSearchPage,
        ),
        actions: const [
          AppBarIcon(Icons.home),
        ],
      ),
      body: RefreshIndicator(
        color: theme.primaryColor,
        onRefresh: () async {
          pagingPosts.refresh();
          await 1.delay();
        },
        child: Row(
          children: [
            Flexible(
              child: PagedListView<int, Post>(
                pagingController: pagingPosts,
                builderDelegate: PagedChildBuilderDelegate(
                  itemBuilder: (_, p, i) => Column(
                    children: <Widget>[
                      if (i == 0)
                        Column(
                          children: [
                            AddPostWidget(),
                            AppTextRegularBold(
                              t.PeopleYouMayFollow,
                            ),
                            SizedBox(height: 8),
                            whoToFollow(
                              theme.primaryColor,
                            ),
                          ],
                        ),
                      //mkmkmk uncomment
                      // if (i % 5 == 0) Get.find<AdsHelper>().banner(),
                      if (p.show) PostWidget(p),
                    ],
                  ),
                  newPageProgressIndicatorBuilder: (_) => ShimmerPost(),
                  firstPageProgressIndicatorBuilder: (_) => ShimmerPost(),
                  noItemsFoundIndicatorBuilder: (_) => Column(
                    children: [
                      AddPostWidget(),
                      //if ()
                      Column(
                        children: [
                          AppTextRegularBold(
                            t.PeopleYouMayFollow,
                          ),
                          SizedBox(height: 8),
                          whoToFollow(
                            theme.primaryColor,
                          ),
                        ],
                      ),
                      SizedBox(height: 50),
                      AppTextRegular(
                        'You are not Following Any User Yet.\n\n\n Pull to refresh',
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget whoToFollow(Color color) {
    return SizedBox(
      height: 150,
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxHeight: 150,
        ),
        child: RefreshIndicator(
          color: color,
          onRefresh: () async {
            pagingUsers.refresh();
            await 1.delay();
          },
          child: PagedListView<int, User>(
            pagingController: pagingUsers,
            scrollDirection: Axis.horizontal,
            builderDelegate: PagedChildBuilderDelegate(
              itemBuilder: (_, u, i) => PeopleMayKnowWidget(
                user: u,
              ),
              // PeopleMayKnowWidget(authProvider.user!),
            ),
          ),
        ),
      ),
    );
  }
}
