// ignore_for_file: always_use_package_imports

import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/services.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:path_provider/path_provider.dart';
import 'package:profanity_filter/profanity_filter.dart';

import '../../../../audio/recorder/audio_player.dart';
import '../../../../audio/recorder/recorder.dart';
import '../../../../components/appbar_icon.dart';
import '../../../../core/models/audio.dart';
import '../../../../imports.dart';
import '../../data/posts.dart';
import '../../models/post.dart';

class PostEditorPage extends StatefulWidget {
  final Post? toEditPost;
  const PostEditorPage({
    Key? key,
    this.toEditPost,
  }) : super(key: key);

  @override
  _PostEditorPageState createState() => _PostEditorPageState();
}

class _PostEditorPageState extends State<PostEditorPage> {
  final textController = TextEditingController();
  bool showPlayer = false;
  bool showDelete = false;
  String? path;
  Post? get toEdit => widget.toEditPost;
  User? get currentUser => authProvider.user;
  late File file;
  final uploader = AppUploader();
  String? finalPath;
  bool audioSelected = false;
  //final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();
  String? fileName;
  String? saveAsFileName;
  List<PlatformFile>? _paths;
  String? directoryPath;
  String? _extension;
  bool isLoading = false;
  bool userAborted = false;
  final FileType _pickingType = FileType.audio;

  @override
  void initState() {
    if (toEdit != null) {
      textController.text = toEdit?.content ?? '';
      // if (toEdit!.hasImage) {
      //   uploader.setAsUploaded(toEdit!.image);
      // }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: Appbar(
        titleStr: t.CreatePost,
        actions: const [
          AppBarIcon(
            Icons.mic,
          )
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
        child: Column(
          children: <Widget>[
            Row(
              children: [
                AvatarWidget(
                  currentUser!.photoURL,
                  radius: 40,
                  showBadge: false,
                ),
                SizedBox(width: 12),
                AppTextSubTitleBold(
                  currentUser!.fullName,
                ),
              ],
            ),
            SizedBox(height: 20),

            // Theme(
            //   data: Theme.of(context).copyWith(
            //     textSelectionTheme: TextSelectionThemeData(
            //       selectionColor: theme.primaryColor,
            //       selectionHandleColor: theme.primaryColor,
            //       cursorColor: theme.primaryColor,
            //     ),
            //   ),
            // child:
            TextField(
              style: TextStyles.textStyleSubTitleBold(context),
              controller: textController,
              maxLines: 20,
              minLines: 1,
              maxLength: 250,
              decoration: InputDecoration.collapsed(
                hintText: t.WhatOnMind,
                hintStyle: TextStyles.textStyleSubTitleBold(
                  context,
                  color: Colors.grey,
                ),
              ),
            ),
            // ),
            SizedBox(height: 50),
            container(
              theme,
              t.Record,
              FutureBuilder<String>(
                future: getPath(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (showPlayer) {
                      return Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: AudioPlayer(
                          path: snapshot.data!,
                          onDelete: () {
                            setState(() {
                              showPlayer = false;
                              audioSelected = false;
                              path = null;
                              finalPath = null;
                            });
                            BotToast.showText(
                              text: 'Recorded file is removed with success.',
                            ); //mkmkmk);

                            // ScaffoldMessenger.of(context).showSnackBar(
                            //   SnackBar(
                            //     backgroundColor: Colors.green,
                            //     content: AppTextRegular(
                            //       'Picked files removed with success.', //mkmkmk

                            //       textAlign: TextAlign.center,
                            //     ),
                            //   ),
                            // );
                          },
                        ),
                      );
                    } else {
                      return AudioRecorder(
                        path: snapshot.data!,
                        onStop: () async {
                          path = snapshot.data;

                          setState(() {
                            finalPath = path;
                            showPlayer = true;
                            audioSelected = true;
                          });
                        },
                      );
                    }
                  } else {
                    return Center(
                      child: CircularProgressIndicator(
                        color: theme.primaryColor,
                      ),
                    );
                  }
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: AppTextTitleBold(t.Or),
            ),
            container(
              theme,
              t.PickFile,
              Row(
                mainAxisAlignment: showDelete
                    ? MainAxisAlignment.spaceAround
                    : MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        _pickFiles();
                      });
                    },
                    style: ElevatedButton.styleFrom(
                      primary: Theme.of(context).primaryColor,
                      // padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                      // textStyle: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    ),
                    child: Icon(
                      Icons.add,
                      color: theme.iconTheme.color,
                    ),
                  ),
                  Visibility(
                    visible: showDelete,
                    child: IconButton(
                      onPressed: () {
                        setState(() {
                          _resetState();
                          _clearCachedFiles();
                          showDelete = false;
                        });
                      },
                      icon: Icon(Icons.delete),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 50),
              child: SizedBox(
                width: MediaQuery.of(context).size.width - 100,
                child: ElevatedButton(
                  onPressed: () {
                    onShare(context);
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Theme.of(context).primaryColor,
                    // padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                    // textStyle: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                  child: AppTextSubTitleBold(
                    t.Share,
                    color: theme.iconTheme.color,
                  ),
                ),
              ),
            ),
            // ElevatedButton(
            //   onPressed: () {
            //     onShare(context);
            //   },
            //   style: ElevatedButton.styleFrom(
            //     primary: Theme.of(context).primaryColor,
            //     // padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
            //     // textStyle: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            //   ),
            //   child: AppTextSubTitleBold(
            //     t.Share,
            //   ),
            // ),
            // // Container(
            //   padding: const EdgeInsets.all(8.0),
            //   constraints: BoxConstraints(
            //     minHeight: 200,
            //     minWidth: context.width,
            //     maxHeight: context.height * 2 / 3,
            //   ),
            //   decoration: BoxDecoration(
            //     color: Colors.grey.withAlpha(120),
            //     border: Border.all(color: Colors.grey),
            //     borderRadius: BorderRadius.circular(10),
            //   ),
            //   child: Obx(
            //     () => uploader.state.maybeWhen(
            //       initial: () => IconButton(
            //         icon: Icon(Icons.add, size: 50),
            //         onPressed: () => uploader.pick(context, true),
            //       ),
            //       orElse: () => Stack(
            //         children: <Widget>[
            //           ClipRRect(
            //             borderRadius: BorderRadius.circular(10),
            //             child: (uploader.picked ?? uploader.uploaded)!.when(
            //               image: (img) => AppImage(
            //                 img.copyWith(width: double.maxFinite),
            //                 fit: BoxFit.fitWidth,
            //               ),
            //             ),
            //           ),
            //           if (uploader.isPicked)
            //             Positioned(
            //               right: -25,
            //               top: -6,
            //               child: MaterialButton(
            //                 color: theme.accentColor,
            //                 shape: CircleBorder(),
            //                 onPressed: uploader.reset,
            //                 child: Icon(Icons.close),
            //               ),
            //             ),
            //         ],
            //       ),
            //       failed: (e) => GestureDetector(
            //         onTap: () => uploader.pick(context),
            //         behavior: HitTestBehavior.opaque,
            //         child: Column(
            //           mainAxisAlignment: MainAxisAlignment.center,
            //           children: <Widget>[
            //             Icon(Icons.warning, size: 40),
            //             SizedBox(height: 4),
            //             Text(t.FailedToUpload)
            //           ],
            //         ),
            //       ),
            //     ),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }

  void _showLoaderDialog(BuildContext context) {
    final theme = Theme.of(context);
    final AlertDialog alert = AlertDialog(
      content: Row(
        children: [
          CircularProgressIndicator(
            color: theme.primaryColor,
          ),
          Container(
            margin: EdgeInsets.only(left: 7),
            child: AppTextRegular(t.Loading),
          ),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<void> onShare(BuildContext context) async {
    final content = textController.text.trim();
    if (ProfanityFilter().hasProfanity(content)) {
      BotToast.showText(
        text: t.ProfanityDetected,
        duration: Duration(seconds: 5),
      );
      return;
    } else if (content.isEmpty) {
      BotToast.showText(text: t.AddSomeContent);
      return;
    } else if (audioSelected == false) {
      BotToast.showText(
        text: t.PleaseAddAudio,
      );
      return;
    } else {
      _showLoaderDialog(context);
      final post = Post.create(
        content: content,
      );

      await uploader.uploadAudio(
        finalPath!,
        //_paths!.first.path.toString(),
        // path!,
        StorageHelper.postAudioRef,
        onSuccess: (v) async {
          await PostsRepository.addPost(
            post.copyWith(
              audioFile: v as AudioModel?,
              // isShared: true,
            ),
          );
          //Get.find<PagingController>().refresh();
        },
        onFailed: (e) => BotToast.showText(text: e),
      );

      if (toEdit != null) {
        if (content == toEdit!.content) return;
        await PostsRepository.updatePost(toEdit!.copyWith(content: content));
        BotToast.showText(text: t.SuccessPostEdited);
      }
      Get.find<PagingController>().refresh();
      if (!mounted) return; //mkmkmk
      Navigator.of(context).pop();
      Navigator.pop(context);
    }
  }

  // Future<void> onShareTap(BuildContext context) async {
  //   final content = textController.text.trim();
  //   if (ProfanityFilter().hasProfanity(content)) {
  //     BotToast.showText(
  //       text: t.ProfanityDetected,
  //       duration: Duration(seconds: 5),
  //     );
  //     return;
  //   }
  //   if (content.isEmpty && uploader.picked == null) {
  //     BotToast.showText(text: t.AddSomeContent);
  //     return;
  //   }

  //   var post = Post.create(
  //     content: content,
  //   );
  //   post = post.copyWith(audioFile: AudioModel(path!));
  //   //post = post.copyWith(image: ImageModel(path!));
  //   await uploader.uploadAudio(
  //     path!,
  //     StorageHelper.postAudioRef,
  //     onSuccess: (v) async {
  //       print('uuuuuuuuuuuuuuu$path');
  //       await PostsRepository.addPost(post.copyWith(
  //         audioFile: v as AudioModel?,
  //         isShared: true,
  //       ));
  //       Get.find<PagingController>().refresh();
  //     },
  //     onFailed: (e) => BotToast.showText(text: e),
  //   );

  //   if (toEdit != null) {
  //     if (content == toEdit!.content) return;
  //     await PostsRepository.updatePost(toEdit!.copyWith(content: content));
  //     BotToast.showText(text: t.SuccessPostEdited);
  //   } else {
  //     if (uploader.uploaded == null) {
  //       post = post.copyWith(isShared: true);
  //     } else {
  //       uploader.picked!.when(
  //         image: (img) {
  //           post = post.copyWith(image: img);
  //           uploader.upload(
  //             StorageHelper.postsImageRef,
  //             onSuccess: (v) async {
  //               await PostsRepository.addPost(post.copyWith(
  //                 image: v as ImageModel?,
  //                 isShared: true,
  //               ));
  //               Get.find<PagingController>().refresh();
  //             },
  //             onFailed: (e) => BotToast.showText(text: e),
  //           );
  //         },
  //       );
  //     }
  //     print('adding post');
  //     await PostsRepository.addPost(post);
  //   }
  //   Get.find<PagingController>().refresh();
  //   Navigator.of(context).pop();
  // }

  Widget container(ThemeData themeData, String title, Widget child) {
    return Container(
      height: 150,
      decoration: BoxDecoration(
        border: Border.all(
          color: themeData.iconTheme.color!, // Set border color
        ), // Set border width
        borderRadius: BorderRadius.all(
          Radius.circular(10.0),
        ), // Set rounded corner radius
        // Make rounded corner of border
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          AppTextTitleBold(title),
          child,
        ],
      ),
    );
  }

  Future<void> deletePost() async {
    try {
      await PostsRepository.removePost(toEdit!.id, toEdit!.commentsIDs);
      Get.find<PagingController>().refresh();
    } catch (e) {
      logError(e);
    }
  }

  Future<String> getPath() async {
    if (path == null) {
      final dir = await getApplicationDocumentsDirectory();
      path = '${dir.path}/${DateTime.now().millisecondsSinceEpoch}.aac';
      //print('kkkkkkkkkkkkkkkkkkkk$path');
      //finalPath = path;
    }
    return path!;
  }

  Future _pickFiles() async {
    _resetState();
    try {
      directoryPath = null;
      _paths = (await FilePicker.platform.pickFiles(
        type: _pickingType,
        // allowMultiple: _multiPick,
        // onFileLoading: (FilePickerStatus status) => print(status),
        allowedExtensions: (_extension?.isNotEmpty ?? false)
            ? _extension?.replaceAll(' ', '').split(',')
            : null,
      ))
          ?.files;
      finalPath = _paths!.first.path.toString();
      showDelete = true;
      audioSelected = true;
    } on PlatformException catch (e) {
      _logException('Unsupported operation$e');
    } catch (e) {
      _logException(e.toString());
    }
    if (!mounted) return;
    setState(() {
      isLoading = false;
      fileName = _paths != null ? _paths!.map((e) => e.name).toString() : '...';
      userAborted = _paths == null;
      //audioSelected = false;
    });
  }

  Future _clearCachedFiles() async {
    _resetState();
    audioSelected = false;
    finalPath = null;
    try {
      final bool? result = await FilePicker.platform.clearTemporaryFiles();
      if (!mounted) return; // mkmkmk
      BotToast.showText(
        text: result!
            ? 'Picked files removed with success.' //mkmkmk
            : 'Failed to clean temporary files',
      );
      // ScaffoldMessenger.of(context).showSnackBar(
      //   SnackBar(
      //     backgroundColor: result! ? Colors.green : Colors.red,
      //     content: AppTextRegular(
      //       result
      //           ? 'Picked files removed with success.' //mkmkmk
      //           : 'Failed to clean temporary files', //mkmkmk
      //       textAlign: TextAlign.center,
      //     ),
      //   ),
      // );
    } on PlatformException catch (e) {
      _logException('Unsupported operation$e');
    } catch (e) {
      _logException(e.toString());
    } finally {
      setState(() => isLoading = false);
    }
  }

  void _logException(String message) {
    // print(message);
    _scaffoldMessengerKey.currentState?.hideCurrentSnackBar();
    _scaffoldMessengerKey.currentState?.showSnackBar(
      SnackBar(
        content: AppTextRegular(message),
      ),
    );
  }

  void _resetState() {
    if (!mounted) {
      return;
    }
    setState(() {
      isLoading = true;
      directoryPath = null;
      fileName = null;
      _paths = null;
      saveAsFileName = null;
      userAborted = false;
      audioSelected = false;
      finalPath = null;
    });
  }
}
