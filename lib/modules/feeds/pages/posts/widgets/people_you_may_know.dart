// ignore_for_file: always_use_package_imports

import '../../../../../imports.dart';

class PeopleMayKnowWidget extends StatelessWidget {
  final User? user;

  const PeopleMayKnowWidget({Key? key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return InkWell(
      onTap: () {
        AppNavigator.toProfile(user!.id);
      },
      child: Container(
        width: 100,
        height: 150,
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.all(6),
        decoration: BoxDecoration(
          border: Border.all(color: theme.primaryColor, width: 0.5),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            AvatarWidget(user!.photoURL),
            AppTextSmallBold(
              user!.fullName,
              color: theme.primaryColor,
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }
}
