// ignore_for_file: always_use_package_imports

import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:share/share.dart';
import 'package:timeago/timeago.dart' as time_ago;

import '../../../../../components/confirm_dialog.dart';
import '../../../../../imports.dart';
import '../../../../auth/data/user_repository.dart';
import '../../../data/posts.dart';
import '../../../models/post.dart';

class UserTile extends StatefulWidget {
  final User user;
  final Post? post;

  const UserTile(
    this.user,
    this.post, {
    Key? key,
  }) : super(key: key);

  @override
  _UserTileState createState() => _UserTileState();
}

class _UserTileState extends State<UserTile> {
  bool get isAdmin => authProvider.user!.isAdmin;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: widget.post!.isMine
            ? null
            : () => AppNavigator.toProfile(widget.post!.authorID),
        child: Row(
          children: <Widget>[
            AvatarWidget(
              widget.user.photoURL,
              radius: 45,
              showBadge: widget.user.onlineStatus && widget.user.isOnline,
            ),
            SizedBox(width: 12),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  AppTextSmallBold(
                    '${widget.user.fullName} ${widget.user.isAdmin ? "(${t.Admin})" : ""}',
                    maxLines: 1,
                  ),
                  AppTextSmall(
                    time_ago.format(
                      widget.post!.createdAt!,
                      locale: LocaleSettings.currentLocale.languageTag,
                    ),
                  ),
                ],
              ),
            ),
            PopupMenuButton<int>(
              icon: Icon(
                Icons.more_vert,
              ),
              itemBuilder: (_) => [
                if (widget.post!.isMine || isAdmin)
                  PopupMenuItem(
                    value: 0,
                    child: GestureDetector(
                      onTap: () {
                        showConfirmDialog(
                          context,
                          title: t.PostRemoveConfirm,
                          onConfirm: () {
                            deletePost();
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                        );
                      },
                      child: Row(
                        children: <Widget>[
                          // Icon(Icons.delete, color: Colors.red, size: 30),
                          // SizedBox(width: 4),
                          AppTextRegular(t.Delete),
                          SizedBox(width: 4),
                        ],
                      ),
                    ),
                  ),
                if (!widget.post!.isMine && !widget.post!.isReportedByMe)
                  PopupMenuItem(
                    value: 1,
                    child: AppTextRegular(t.Report),
                  )
                else
                  PopupMenuItem(
                    value: 2,
                    child: AppTextRegular(
                      'Reported',
                      color: Colors.red,
                    ),
                  ),
                if (!widget.post!.isMine && !isAdmin)
                  PopupMenuItem(
                    value: 3,
                    child: AppTextRegular(
                      widget.post!.isSavedByMe ? t.Unsave : t.Save,
                    ),
                  ),
                PopupMenuItem(
                  value: 4,
                  child: Row(
                    children: [
                      // Icon(
                      //   FontAwesomeIcons.share,
                      //   color: Colors.orange,
                      //   size: 25,
                      // ),
                      // SizedBox(width: 8),
                      AppTextRegular(
                        t.ShareExternally,
                      ),
                      SizedBox(width: 4)
                    ],
                  ),
                ),
              ],
              onSelected: (v) {
                if (v == 0) {
                  showConfirmDialog(
                    context,
                    title: t.AreYouSure,
                    onConfirm: () => deletePost(),
                  );
                } else if (v == 1) {
                  AppNavigator.toReport(widget.post);
                }
                // else if (v == 2) {
                //   PostsRepository.unReportPost(widget.post!.id);
                // }
                else if (v == 3) {
                  toggleSaveBtn();
                } else if (v == 4) {
                  _onShareExternally(context);
                }
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Future<void> showConfirmDialog(
  //   BuildContext context, {
  //   String? title,
  //   String desc = '',
  //   VoidCallback? onConfirm,
  // }) async {
  //   await AwesomeDialog(
  //     context: context,
  //     dialogType: DialogType.NO_HEADER,
  //     title: title ?? t.AreYouSure,
  //     desc: desc,
  //     btnOk: AppButton(t.Ok, onTap: onConfirm),
  //     btnCancelColor: Colors.grey,
  //     btnCancel: AppButton(
  //       t.Cancel,
  //       onTap: () {
  //         Navigator.of(context).pop();
  //         Navigator.of(context).pop();
  //       },
  //     ),
  //   ).show();
  // }

  Future<void> deletePost() async {
    try {
      await PostsRepository.removePost(
        widget.post!.id,
        widget.post!.commentsIDs,
      );
      Get.find<PagingController>().refresh();
    } catch (e) {
      logError(e);
    }
  }

  Future<void> toggleSaveBtn() async {
    try {
      widget.post!.toggleSaveInPosts();
      await PostsRepository.toggleSavePost(widget.post!);
      await UserRepository.toggleSavedPosts(widget.post!);
      widget.user.toggleSave(widget.post!.id);

      Get.find<PagingController>().refresh();
    } catch (e) {
      logError(e);
    }
  }

  Future _onShareExternally(BuildContext context) async {
    await Share.share(
      'This is a link to post on webApp iConfezz.com', //mkmkmk should be link to website and no token in url
      subject: 'iConFezz',
    );
  }
}
