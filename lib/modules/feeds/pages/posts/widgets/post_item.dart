// ignore_for_file: always_use_package_imports, depend_on_referenced_packages

import 'dart:io';

import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:intl/intl.dart' as intl;
//import 'package:intl/intl.dart';
import 'package:just_waveform/just_waveform.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:rxdart/rxdart.dart';

import '../../../../../audio/post_player.dart';
//import '../../../../../components/picker_share.dart';
import '../../../../../imports.dart';
import '../../../../auth/data/user_repository.dart';
import '../../../data/posts.dart';
import '../../../models/post.dart';
import 'user_tile.dart';

class PostWidget extends StatefulWidget {
  final Post post;
  final bool showMore;

  const PostWidget(
    this.post, {
    Key? key,
    this.showMore = true,
  }) : super(key: key);
  @override
  _PostWidgetState createState() => _PostWidgetState();
}

class _PostWidgetState extends State<PostWidget> {
  String get uid => authProvider.user!.id;
  Post get post => widget.post;
  final progressStream = BehaviorSubject<WaveformProgress>();
  @override
  void initState() {
    super.initState();
    _init();
  }

  Future<void> _init() async {
    final audioFile = File(
      p.join((await getTemporaryDirectory()).path, post.audioFile!.path),
    );
    // print(audioFile.path);
    try {
      await audioFile.writeAsBytes(
        (await rootBundle.load(post.audioFile!.path)).buffer.asUint8List(),
      );
      final waveFile = File(
        p.join(
          (await getTemporaryDirectory()).path,
          post.audioFile!.path,
        ),
      );
      JustWaveform.extract(audioInFile: audioFile, waveOutFile: waveFile)
          .listen(progressStream.add, onError: progressStream.addError);
    } catch (e) {
      progressStream.addError(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    //final postSentences = post.content.split('\n');
    final theme = Theme.of(context);

    //final showMore = widget.showMore && postSentences.length > 5;
    return StreamBuilder<User>(
      stream: UserRepository.userStream(post.authorID),
      builder: (_, snap) {
        final user = snap.data ?? post.getUser;
        return Container(
          margin: const EdgeInsets.all(6),
          decoration: BoxDecoration(
            color: user.isAdmin ? theme.primaryColor.withAlpha(100) : null,
            border: Border.all(color: theme.iconTheme.color!, width: 0.5),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            children: <Widget>[
              //mkmkmk
              // if (!post.sharedBy.contains(post.authorID))
              //   Row(
              //     children: [
              //       Padding(
              //         padding:
              //             const EdgeInsets.only(left: 10, right: 10, top: 5),
              //         child: AppTextRegularBold(
              //           '${authProvider.user!.fullName} shared',
              //           color: theme.primaryColor,
              //         ),
              //       ),
              //     ],
              //   )
              // else
              //   SizedBox(),
              UserTile(user, post),

              if (post.content.isNotEmpty)
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: AppTextRegular(
                    post.content,
                    textAlign: TextAlign.center,
                  ),
                ),

              Directionality(
                textDirection: TextDirection.ltr,
                child: PostPlayer(audioUrl: post.audioFile!.path),
              ),
              // PostPlayerWidget(
              //   audioFile: post.audioFile!.path.toString(),
              //   title: post.content,
              //   id: post.id,
              // ),
              //MainScreen(),
              // BackgroundPlayer(),

              Row(
                children: <Widget>[
                  SizedBox(width: 10),
                  GestureDetector(
                    onTap: () =>
                        AppNavigator.toPeopleList(post.usersLikes, t.Likes),
                    child: AppTextSmallBold(
                      '${intl.NumberFormat.compact().format(post.usersLikes.length)} ${t.Likes}',
                    ),
                  ),
                  Spacer(),
                  AppTextSmallBold(
                    '${intl.NumberFormat.compact().format(post.commentsIDs.length)} ${t.Comments}',
                  ),
                  SizedBox(width: 10),
                ],
              ),
              SizedBox(height: 10),
              Container(
                // margin: EdgeInsets.all(10),
                height: 0.5,
                color: theme.iconTheme.color,
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(width: 10),
                  SizedBox(
                    // height: 40,
                    child: InkWell(
                      onTap: toggleReaction,
                      child: Row(
                        children: [
                          Icon(
                            post.liked
                                ? Icons.thumb_up
                                : Icons.thumb_up_alt_outlined,
                            color: theme.primaryColor,
                          ),
                          SizedBox(width: 3),
                          AppTextSmallBold(post.liked ? t.Liked : t.Like),
                        ],
                      ),
                    ),
                  ),
                  Spacer(),
                  if (!user.isMe)
                    InkWell(
                      onTap: () async {
                        toggleShareBtn();
                      },
                      child: Row(
                        children: [
                          Icon(
                            (post.isSharedByMe &&
                                    authProvider.user!.sharedPosts
                                        .contains(widget.post.id))
                                ? FontAwesomeIcons.squareShareNodes
                                : FontAwesomeIcons.shareNodes,
                            color: Colors.blue,
                          ),
                          SizedBox(width: 3),
                          AppTextSmallBold(
                            post.isSharedByMe ? t.UnShare : t.Share,
                          ),
                        ],
                      ),
                    ),
                  Spacer(),
                  InkWell(
                    onTap: () async {
                      await AppNavigator.toComments(post);
                      setState(() {});
                    },
                    child: Row(
                      children: [
                        Icon(
                          post.commentsIDs.isNotEmpty
                              ? Icons.mode_comment
                              : Icons.mode_comment_outlined,
                          color: Colors.pink,
                        ),
                        SizedBox(width: 3),
                        AppTextSmallBold(t.Comment),
                      ],
                    ),
                  ),
                  SizedBox(width: 10),
                ],
              ),
              SizedBox(height: 10),
            ],
          ),
        );
      },
    );
  }

  Future<void> toggleReaction() async {
    setState(() => post.togglePostLike());
    await PostsRepository.togglePostReaction(post);
  }

  Future<void> toggleShareBtn() async {
    try {
      setState(() {
        authProvider.user!.toggleShare(widget.post.id);
        post.toggleShareInPosts();
      });
      await PostsRepository.toggleSharePost(widget.post);
      await UserRepository.toggleSharePosts(widget.post);

      Get.find<PagingController>().refresh();
    } catch (e) {
      logError(e);
    }
  }
}
