// ignore_for_file: always_use_package_imports

import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../models/post.dart';
import 'post_item.dart';

class UserPosts extends StatefulWidget {
  final String userId;
  final Stream<List<Post>> stream;
  const UserPosts(
    this.userId,
    this.stream,
  );
  @override
  _UserPostsState createState() => _UserPostsState();
}

class _UserPostsState extends State<UserPosts> {
  int offset = 10;
  final _refreshController = RefreshController();

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return StreamBuilder<List<Post>>(
      stream: widget
          .stream, //PostsRepository.getUserPostsStream(widget.userId, offset),
      builder: (_, snap) {
        if (snap.connectionState == ConnectionState.waiting) {
          return Align(
            alignment: Alignment.topCenter,
            child: LinearProgressIndicator(
              color: theme.primaryColor,
              backgroundColor: theme.primaryColor,
            ),
          );
        }
        final posts = snap.data ?? [];
        if (posts.isEmpty) return SizedBox();

        return SmartRefresher(
          controller: _refreshController,
          enablePullUp: offset <= posts.length,
          enablePullDown: false,
          onLoading: () async {
            setState(() {
              offset = posts.length + 10;
            });
            await Future.delayed(Duration(seconds: 2));
            _refreshController.loadComplete();
          },
          child: ListView.builder(
            // key: PageStorageKey('user_posts'),
            itemCount: posts.length,
            itemBuilder: (_, i) => PostWidget(posts[i]),
          ),
        );
      },
    );
  }
}
