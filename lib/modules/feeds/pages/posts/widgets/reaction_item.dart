// ignore_for_file: always_use_package_imports

import 'package:timeago/timeago.dart';

import '../../../../../imports.dart';
import '../../../models/post_reaction.dart';

class ReactionItem extends StatelessWidget {
  final PostReaction reaction;
  const ReactionItem({
    Key? key,
    required this.reaction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final theme = Theme.of(context);
    return ListTile(
      leading: AvatarWidget(
        reaction.reactionOwnerPhotoURL,
        radius: 40,
      ),
      title: AppTextSubTitleBold(
        reaction.isMine ? '${t.Me}:  ' : '${reaction.reactionOwnerName}: ',
      ),
      subtitle: Padding(
        padding: const EdgeInsets.only(top: 8),
        child: AppTextSmall(
          format(
            reaction.createdAt!,
            locale: LocaleSettings.currentLocale.languageTag,
          ),
          color: Colors.grey,
        ),
      ),
      onTap: reaction.isMine
          ? null
          : () => AppNavigator.toProfile(reaction.reactionOwnerID),
    );
  }
}
