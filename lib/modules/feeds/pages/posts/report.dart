// ignore_for_file: always_use_package_imports

import '../../../../components/appbar_icon.dart';
import '../../../../imports.dart';
import '../../data/posts.dart';
import '../../models/post.dart';

class ReportPage extends StatelessWidget {
  final Post? post;

  const ReportPage(
    this.post, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final theme = Theme.of(context);
    return Scaffold(
      appBar: Appbar(
        titleStr: t.Report,
        actions: const [
          AppBarIcon(Icons.report),
        ],
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 30),
            AppTextTitle(
              t.ReportDesc,
            ),
            SizedBox(height: 30),
            AppTextRegular(t.ReportReasons),
            Spacer(),
            AppTextTitle(
              t.ReportNote,
            ),
            SizedBox(height: 20),
            Center(
              child: AppButton(
                t.Report,
                onTap: () async {
                  Navigator.pop(context);
                  BotToast.showText(text: t.ReportThanks);
                  post!.reportedBy.add(authProvider.user!.id);
                  await PostsRepository.reportPost(post!);
                },
              ),
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
