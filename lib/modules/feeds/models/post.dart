// ignore_for_file: always_use_package_imports

import 'package:equatable/equatable.dart';
import '../../../core/models/audio.dart';

import '../../../imports.dart';

//import 'package:flutter_image_compress/flutter_image_compress.dart';

class Post extends Equatable {
  final String id;
  final String authorID;
  final String authorName;
  final String authorPhotoURL;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final String content;
  final List<String> category;
  final List<String> usersLikes;
  final List<String> commentsIDs;
  final List<String?> reportedBy;
  final List<String?> sharedBy;
  final List<String?> savedBy;
  final ImageModel? image;
  final AudioModel? audioFile;
  final int? sharesCount;

  const Post({
    required this.id,
    required this.authorID,
    required this.authorName,
    required this.authorPhotoURL,
    required this.createdAt,
    required this.updatedAt,
    required this.reportedBy,
    required this.sharedBy,
    required this.savedBy,
    required this.content,
    required this.category,
    required this.usersLikes,
    required this.commentsIDs,
    this.image,
    this.audioFile,
    this.sharesCount,
  });
  String get _uid => authProvider.user!.id;
  User get _u => authProvider.user!;
  bool get isSavedByMe => savedBy.contains(_uid);
  bool get isSharedByMe => sharedBy.contains(_uid);
  bool get isSharedByFollowing =>
      _u.following.any((item) => sharedBy.contains(item));
  bool get isPostOfFollowing => _u.following.contains(authorID);
  bool get liked => usersLikes.contains(_uid);
  bool get isMine => authorID == _uid;
  bool get isReported => reportedBy.isNotEmpty;
  bool get isReportedByMe => reportedBy.contains(_uid);
  bool get hasImage => image?.path.isNotEmpty == true;
  bool get show => true;
  // isSharedByMe ||
  // isPostOfFollowing ||
  // isMine; //&& !reportedBy.contains(_uid);
  void togglePostLike() =>
      liked ? usersLikes.remove(_uid) : usersLikes.add(_uid);
  void toggleSaveInPosts() =>
      isSavedByMe ? savedBy.remove(_uid) : savedBy.add(_uid);
  void toggleShareInPosts() =>
      isSharedByMe ? sharedBy.remove(_uid) : sharedBy.add(_uid);

  factory Post.create({
    String content = '',
    ImageModel? image,
    AudioModel? audioFile,
  }) {
    final currentUser = authProvider.user!;
    return Post(
      id: '${DateTime.now().millisecondsSinceEpoch}-${currentUser.username}',
      authorID: currentUser.id,
      authorName: currentUser.username,
      authorPhotoURL: currentUser.photoURL,
      content: content,
      category: const [],
      usersLikes: const [],
      createdAt: DateTime.now().toUtc(),
      updatedAt: DateTime.now().toUtc(),
      commentsIDs: const [],
      reportedBy: const [],
      sharedBy: const [],
      savedBy: const [],
      image: image,
      audioFile: audioFile,
    );
  }

  User get getUser => User.createNew(
        uid: authorID,
        username: authorName,
        photoURL: authorPhotoURL,
        phone: '',
      );

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'authorID': authorID,
      'authorName': authorName,
      'authorPhotoURL': authorPhotoURL,
      'createdAt': createdAt,
      'updatedAt': updatedAt,
      'content': content,
      'category': category,
      'usersLikes': usersLikes,
      'commentsIDs': commentsIDs,
      'reportedBy': reportedBy,
      'sharedBy': sharedBy,
      'savedBy': savedBy,
      'image': image?.toMap(),
      'audioFile': audioFile?.toMap(),
      'sharesCount': sharesCount
    }..removeWhere((_, v) => '${v ?? ''}'.isEmpty);
  }

  factory Post.fromMap(Map<String, dynamic> map) {
    return Post(
      id: map['id'] as String? ?? '',
      authorID: map['authorID'] as String? ?? '',
      authorName: map['authorName'] as String? ?? '',
      authorPhotoURL: map['authorPhotoURL'] as String? ?? '',
      createdAt: timeFromJson(map['createdAt']),
      updatedAt: timeFromJson(map['updatedAt']),
      content: map['content'] as String? ?? '',
      category: List<String>.from(map['category'] as List? ?? const []),
      usersLikes: List<String>.from(map['usersLikes'] as List? ?? const []),
      commentsIDs: List<String>.from(map['commentsIDs'] as List? ?? const []),
      reportedBy: List<String>.from(map['reportedBy'] as List? ?? const []),
      sharedBy: List<String>.from(map['sharedBy'] as List? ?? const []),
      savedBy: List<String>.from(map['savedBy'] as List? ?? const []),
      image: ImageModel.fromMap(
        Map<String, dynamic>.from(map['image'] as Map? ?? {}),
      ),
      audioFile: AudioModel.fromMap(
        Map<String, dynamic>.from(map['audioFile'] as Map? ?? {}),
      ),
      sharesCount: map['sharesCount'] as int? ?? 0,
    );
  }

  Post copyWith({
    String? id,
    String? authorID,
    String? authorName,
    String? authorPhotoURL,
    DateTime? createdAt,
    DateTime? updatedAt,
    String? content,
    List<String>? category,
    List<String>? usersLikes,
    List<String>? commentsIDs,
    List<String>? reportedBy,
    List<String>? sharedBy,
    List<String>? savedBy,
    ImageModel? image,
    AudioModel? audioFile,
    int? sharesCount,
  }) {
    return Post(
      id: id ?? this.id,
      authorID: authorID ?? this.authorID,
      authorName: authorName ?? this.authorName,
      authorPhotoURL: authorPhotoURL ?? this.authorPhotoURL,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
      content: content ?? this.content,
      category: category ?? this.category,
      usersLikes: usersLikes ?? this.usersLikes,
      commentsIDs: commentsIDs ?? this.commentsIDs,
      reportedBy: reportedBy ?? this.reportedBy,
      sharedBy: sharedBy ?? this.sharedBy,
      savedBy: savedBy ?? this.savedBy,
      image: image ?? this.image,
      audioFile: audioFile ?? this.audioFile,
      sharesCount: sharesCount ?? this.sharesCount,
    );
  }

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      id,
      authorID,
      authorName,
      authorPhotoURL,
      createdAt,
      updatedAt,
      content,
      category,
      usersLikes,
      commentsIDs,
      reportedBy,
      sharedBy,
      image,
      audioFile,
      sharesCount
    ];
  }
}

// class PostSharedBy extends Equatable {
//   final String postId;
//   final String byId;
//   final String ownerId;
//   final String byName;
//   final DateTime sharedTime;
//   const PostSharedBy({
//     required this.postId,
//     required this.byId,
//     required this.ownerId,
//     required this.byName,
//     required this.sharedTime,
//   });

//   factory PostSharedBy.fromMap(Map<String, dynamic> map) {
//     return PostSharedBy(
//       postId: map['postId'] as String? ?? '',
//       byId: map['byId'] as String? ?? '',
//       ownerId: map['ownerId'] as String? ?? '',
//       byName: map['byName'] as String? ?? '',
//       sharedTime: timeFromJson(map['sharedTime']),
//     );
//   }

//   Map<String, dynamic> toMap() {
//     return {
//       'postId': postId,
//       'byId': byId,
//       'ownerId': ownerId,
//       'byName': byName,
//       'sharedTime': sharedTime,
//     };
//   }

//   @override
//   bool get stringify => true;
//   @override
//   List<Object?> get props {
//     return [
//       postId,
//       byId,
//       ownerId,
//       byName,
//       sharedTime,
//     ];
//   }

//   PostSharedBy copyWith({
//     String? postId,
//     String? byId,
//     String? ownerId,
//     String? byName,
//     DateTime? sharedTime,
//   }) {
//     return PostSharedBy(
//       postId: postId ?? this.postId,
//       byId: byId ?? this.byId,
//       ownerId: ownerId ?? this.ownerId,
//       byName: byName ?? this.byName,
//       sharedTime: sharedTime ?? this.sharedTime,
//     );
//   }
// }
