// ignore_for_file: always_use_package_imports

import '../../../components/appbar_icon.dart';
import '../../../imports.dart';

class FAQPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //final theme = Theme.of(context);
    return Scaffold(
      appBar: Appbar(
        titleStr: t.FAQ,
        actions: const [
          AppBarIcon(Icons.question_mark_sharp),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 100),
            Center(
              child: Image.asset(
                Assets.appIcon.path,
                height: 100,
              ),
            ),
            SizedBox(height: 10),
            SizedBox(height: 50),
          ],
        ),
      ),
    );
  }
}
