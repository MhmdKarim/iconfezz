// ignore_for_file: always_use_package_imports

import '../../../components/appbar_icon.dart';
import '../../../imports.dart';

class CommunityGuidelinesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //final theme = Theme.of(context);
    return Scaffold(
      appBar: Appbar(
        titleStr: t.CommunityGuidelines,
        actions: const [
          AppBarIcon(Icons.groups),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 30),
            Center(
              child: Image.asset(
                Assets.appIcon.path,
                height: 100,
              ),
            ),
            SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(
                "\nCommunity guidelines template 1 \nIntroduction\nWelcome to the [brand] community!\n\nThis space is for [your mission statement].\n\nThe goals of our community are to [goals 1, 2, 3 etc.]\n\nTo meet the collective goals of our community, it’s important that all members feel safe and supported. To help everyone have the best possible experience, please take a look at our community guidelines:\n\nPositive guidelines \n[Introduce yourself to the group]\nFeel free to post questions and start discussions on any topics relating to [your community focus]\n[Treat others as you would treat them in real life]\n[Be polite and communicate with respect]\n[Respect the privacy of other community members]\n[Utilize mentors and community managers for support]\nContact us directly with feedback at [contact email]\nRules and restrictions\n[Don’t share posts without permission]\n[Don’t share personal or private information]\n[Don’t post irrelevant messages]\n[Don’t post promotional content]\n[Personal attacks, trolling and abuse will not be tolerated]\n[Don’t post explicit, rude or aggressive content]\nOptional: Consequences\nIn the event of someone violating these guidelines, we will take action to protect other members of the group. This might include a warning or in the event of extreme or repeat behavior, the member may be banned from the community. \n\nIf you experience or witness any behavior that doesn’t follow our community guidelines, please contact us directly at [contact email]. All reports are kept confidential.",
              ),
            ),
            SizedBox(height: 50),
          ],
        ),
      ),
    );
  }
}
