// ignore_for_file: always_use_package_imports

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../components/appbar_icon.dart';
import '../../../imports.dart';

class AppLanguagePage extends StatefulWidget {
  @override
  State<AppLanguagePage> createState() => _AppLanguagePageState();
}

class _AppLanguagePageState extends State<AppLanguagePage> {
  @override
  Widget build(BuildContext context) {
    //final theme = Theme.of(context);
    final theme = Theme.of(context);
    // final titleStyle = TextStyles.textStyleSubTitle(context);
    // final titleStyleBold =
    //     TextStyles.textStyleTitleBold(context, color: theme.primaryColor);

    return Scaffold(
      appBar: Appbar(
        titleStr: t.AppLanguage,
        actions: const [
          AppBarIcon(Icons.language),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.start,

          // lets loop over all supported locales
          children: AppLocale.values.map((locale) {
            final AppLocale activeLocale =
                LocaleSettings.currentLocale; // active locale
            final bool active = activeLocale ==
                locale; // typed version is preferred to avoid typos

            return Container(
              decoration: BoxDecoration(
                border: Border(
                  // top: BorderSide(
                  //   width: 0.5,
                  // ),
                  bottom: BorderSide(width: 0.5, color: Colors.grey),
                ),
              ),
              child: ListTile(
                // style: OutlinedButton.styleFrom(
                tileColor: active ? theme.primaryColor : theme.canvasColor,
                //   shape: const RoundedRectangleBorder(),
                // ),
                // shape: RoundedRectangleBorder(
                //   side: BorderSide(width: 0.5, color: Colors.grey),
                // ),

                onTap: () {
                  // locale change, will trigger a rebuild (no setState needed)
                  setState(() {
                    LocaleSettings.setLocale(locale);
                  });
                  Get.updateLocale(locale.flutterLocale);
                  // appPrefs.lang();
                  appPrefs.changeAppLanguage(locale.languageTag);
                },
                title: AppTextSubTitle(
                  locale.fullName(),
                ),
                trailing: Visibility(
                  visible: active,
                  child: Icon(
                    FontAwesomeIcons.squareCheck,
                    color: theme.iconTheme.color,
                  ),
                ),
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}

extension FullName on AppLocale {
  String fullName() {
    switch (languageTag) {
      case 'en':
        return 'English';
      case 'ar':
        return 'Arabic (العربية)';
      case 'zh':
        return 'Chinese (中文)';
      case 'fr':
        return 'French (Français)';
      case 'de':
        return 'German (Deutsch)';
      case 'hi':
        return 'Indian (हिन्दी)';
      case 'id':
        return 'Indonesian (Bahasa Indonesia)';
      case 'it':
        return 'Italian (Italiano)';
      case 'ja':
        return 'Japanese (日本語)';
      case 'ko':
        return 'Korean (한국어)';
      case 'fa':
        return 'Persian (فارسى)';
      case 'ru':
        return 'Russian (Русский язык)';
      case 'es':
        return 'Spanich (español)';
      case 'tr':
        return 'Turkish (Türkçe)';
    }
    return '';
  }
}
