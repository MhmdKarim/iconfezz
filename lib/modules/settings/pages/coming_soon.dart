// ignore_for_file: always_use_package_imports

import '../../../components/appbar_icon.dart';
import '../../../imports.dart';

class ComingSoonPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //final theme = Theme.of(context);

    return Scaffold(
      appBar: Appbar(
        titleStr: t.ComingSoon,
        actions: const [
          AppBarIcon(Icons.calendar_today),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 50),
              Center(
                child: Image.asset(
                  Assets.appIcon.path,
                  height: 100,
                ),
              ),
              SizedBox(height: 50),
              AppTextTitle('- Login by email'),
              SizedBox(height: 10),
              AppTextTitle('- Multi audio record in one post'),
              SizedBox(height: 10),
              AppTextTitle('- Upload audio/multi audio files from you device'),
              SizedBox(height: 10),
              AppTextTitle('- Multi language'),
              SizedBox(height: 10),
              AppTextTitle('- Voice changer'),
              SizedBox(height: 10),
              AppTextTitle(
                '- Audio Controls in notification Area in your device',
                maxLines: 2,
              ),
              SizedBox(height: 50),
            ],
          ),
        ),
      ),
    );
  }
}
