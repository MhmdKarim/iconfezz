// ignore_for_file: always_use_package_imports

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../components/appbar_icon.dart';
import '../../../imports.dart';

class TermsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //final theme = Theme.of(context);

    return Scaffold(
      appBar: Appbar(
        titleStr: t.TermsService,
        actions: const [
          AppBarIcon(FontAwesomeIcons.file),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 30),
            Center(
              child: Image.asset(
                Assets.appIcon.path,
                height: 100,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(
                "1. Terms\nBy accessing this Website, accessible from www.fezzzzz.com, you are agreeing to be bound by these Website Terms and Conditions of Use and agree that you are responsible for the agreement with any applicable local laws. If you disagree with any of these terms, you are prohibited from accessing this site. The materials contained in this Website are protected by copyright and trade mark law.\n\n2. Use License\nPermission is granted to temporarily download one copy of the materials on fezzzzz's Website for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:\n\nmodify or copy the materials;\nuse the materials for any commercial purpose or for any public display;\nattempt to reverse engineer any software contained on fezzzzz's Website;\nremove any copyright or other proprietary notations from the materials; or\ntransferring the materials to another person or \"mirror\" the materials on any other server.\nThis will let fezzzzz to terminate upon violations of any of these restrictions. Upon termination, your viewing right will also be terminated and you should destroy any downloaded materials in your possession whether it is printed or electronic format. These Terms of Service has been created with the help of the Terms Of Service Generator.\n\n3. Disclaimer\nAll the materials on fezzzzz’s Website are provided \"as is\". fezzzzz makes no warranties, may it be expressed or implied, therefore negates all other warranties. Furthermore, fezzzzz does not make any representations concerning the accuracy or reliability of the use of the materials on its Website or otherwise relating to such materials or any sites linked to this Website.\n\n4. Limitations\nfezzzzz or its suppliers will not be hold accountable for any damages that will arise with the use or inability to use the materials on fezzzzz’s Website, even if fezzzzz or an authorize representative of this Website has been notified, orally or written, of the possibility of such damage. Some jurisdiction does not allow limitations on implied warranties or limitations of liability for incidental damages, these limitations may not apply to you.\n\n5. Revisions and Errata\nThe materials appearing on fezzzzz’s Website may include technical, typographical, or photographic errors. fezzzzz will not promise that any of the materials in this Website are accurate, complete, or current. fezzzzz may change the materials contained on its Website at any time without notice. fezzzzz does not make any commitment to update the materials.\n\n6. Links\nfezzzzz has not reviewed all of the sites linked to its Website and is not responsible for the contents of any such linked site. The presence of any link does not imply endorsement by fezzzzz of the site. The use of any linked website is at the user’s own risk.\n\n7. Site Terms of Use Modifications\nfezzzzz may revise these Terms of Use for its Website at any time without prior notice. By using this Website, you are agreeing to be bound by the current version of these Terms and Conditions of Use.\n\n8. Your Privacy\nPlease read our Privacy Policy.\n\n9. Governing Law\nAny claim related to fezzzzz's Website shall be governed by the laws of us without regards to its conflict of law provisions.",
              ),
            ),
            SizedBox(height: 50),
          ],
        ),
      ),
    );
  }
}
