// ignore_for_file: always_use_package_imports
import 'package:flutter_settings_ui/flutter_settings_ui.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:share/share.dart';
import '../../components/appbar_icon.dart';
import '../../components/confirm_dialog.dart';
//import 'package:store_redirect/store_redirect.dart';
import '../../imports.dart';
import '../auth/data/user_repository.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  User? user = authProvider.user;
  bool isDark = false;
  @override
  void dispose() {
    save();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final titleStyle = TextStyles.textStyleRegularBold(context);
    final titleStyleBold =
        TextStyles.textStyleTitleBold(context, color: theme.primaryColor);
    return Scaffold(
      appBar: Appbar(
        titleStr: t.Settings,
        actions: const [
          AppBarIcon(Icons.settings),
        ],
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 10,
              ),
              SettingsSection(
                titleTextStyle: titleStyleBold,
                title: t.Account,
                titlePadding: EdgeInsets.only(bottom: 30, left: 20, right: 20),
                tiles: [
                  SettingsTile(
                    theme: SettingsTileTheme(
                      tileColor: theme.canvasColor,
                      // shape: Border.all(width: 0),
                    ),
                    title: t.EditProfile,
                    titleTextStyle: titleStyle,
                    leading: icon(context, Icons.person),
                    trailing: icon(context, Icons.chevron_right),
                    onPressed: (_) => AppNavigator.toEditProfile(),
                  ),
                  SettingsTile(
                    theme: SettingsTileTheme(tileColor: theme.canvasColor),
                    title: t.ShareProfile,
                    titleTextStyle: titleStyle,
                    leading: icon(context, FontAwesomeIcons.shareFromSquare),
                    trailing: icon(context, Icons.chevron_right),
                    onPressed: (BuildContext context) async {
                      await Share.share(
                        'This is a link to my Profile on webApp iConfezz.com', //mkmkmk should be link to account and no token in url
                        subject: 'iConFezz',
                      );
                    },
                  ),
                  SettingsTile.switchTile(
                    theme: SettingsTileTheme(
                      tileColor: theme.canvasColor,
                      //  shape: Border.all(width: 0),
                    ),
                    title: t.OnlineStatus,

                    titleTextStyle: titleStyle,
                    titleMaxLines: 2,
                    subtitleMaxLines: 2,
                    switchActiveColor: theme.primaryColor,
                    // subtitle: t.OnlineDescription,
                    leading: icon(
                      context,
                      user!.onlineStatus
                          ? Icons.visibility
                          : Icons.visibility_off,
                    ),
                    switchValue: user!.onlineStatus,
                    onToggle: (v) {
                      UserRepository.onLineStatus(v);
                      setState(() {
                        user = user!.copyWith(onlineStatus: v);
                      });
                    },
                  ),
                  SettingsTile(
                    theme: SettingsTileTheme(tileColor: theme.canvasColor),
                    title: t.Logout,
                    titleTextStyle: titleStyle,
                    leading: icon(context, FontAwesomeIcons.powerOff),
                    trailing: icon(context, Icons.chevron_right),
                    onPressed: (_) async {
                      showConfirmDialog(
                        context,
                        title: '${t.AreYouSure} ${t.Logout}',
                        onConfirm: () {
                          save();
                          authProvider.logout();
                        },
                      );
                      // showConfirmDialog(
                      //   context,
                      //   title: '${t.AreYouSure} ${t.Logout}',
                      //   onConfirm: () {
                      //     save();
                      //     authProvider.logout();
                      //   },
                      // );
                    },
                  ),
                  SettingsTile(
                    theme: SettingsTileTheme(tileColor: theme.canvasColor),
                    title: t.DeleteAccount,
                    titleTextStyle: titleStyle,
                    leading: icon(context, Icons.delete),
                    trailing: icon(context, Icons.chevron_right),
                    onPressed: (_) async {
                      showConfirmDialog(
                        context,
                        title: '${t.AreYouSure} ${t.DeleteYourAccount}',
                        onConfirm: () {
                          authProvider.logout();

                          UserRepository.deleteUser(user!.id);
                          // Navigator.of(context).pop();
                        },
                      );
                    },
                  ),

                  //  SettingsTile.switchTile(
                  //   title: t.GroupsMsgs,
                  //   subtitle: t.GroupsMsgsDesc,
                  //   leading: Icon(
                  //     user!.groupsNotify
                  //         ? Icons.notifications_active
                  //         : Icons.notifications_off,
                  //   ),
                  //   switchValue: user!.groupsNotify,
                  //   onToggle: (v) {
                  //     user = user!.copyWith(groupsNotify: v);
                  //     setState(() {});
                  //   },
                  // ),
                ],
              ),
              SizedBox(height: 10),
              SettingsSection(
                title: t.PushNotifications,
                titleTextStyle: titleStyleBold,
                titlePadding: EdgeInsets.only(bottom: 30, left: 20, right: 20),
                tiles: [
                  SettingsTile.switchTile(
                    theme: SettingsTileTheme(tileColor: theme.canvasColor),
                    switchActiveColor: theme.primaryColor,
                    title: t.DirectMsgs,
                    titleTextStyle: titleStyle,
                    titleMaxLines: 2,
                    subtitleMaxLines: 2,
                    // subtitle: t.DirectMsgsDescr,
                    leading: icon(
                      context,
                      user!.chatNotify
                          ? FontAwesomeIcons.bell
                          : FontAwesomeIcons.bellSlash,
                    ),
                    switchValue: user!.chatNotify,
                    onToggle: (v) {
                      // print(v);
                      UserRepository.chatNotify(v);
                      setState(() {
                        user = user!.copyWith(chatNotify: v);
                      });
                    },
                  ),
                  // SettingsTile.switchTile(
                  //   switchActiveColor: theme.primaryColor,
                  //   title: t.PostsLikes,
                  //   titleTextStyle: titleStyle,
                  //   titleMaxLines: 2,
                  //   subtitleMaxLines: 2,
                  //   // subtitle: t.DirectMsgsDescr,
                  //   leading: icon(
                  //     context,
                  //     user!.postLikesNotify
                  //         ? FontAwesomeIcons.bell
                  //         : FontAwesomeIcons.bellSlash,
                  //   ),
                  //   switchValue: user!.postLikesNotify,
                  //   onToggle: (v) {
                  //     UserRepository.postLikesNotify(v);
                  //     setState(() {
                  //       user = user!.copyWith(postLikesNotify: v);
                  //     });
                  //   },
                  // ),
                  // SettingsTile.switchTile(
                  //   switchActiveColor: theme.primaryColor,
                  //   title: t.CommentsLikes,
                  //   titleTextStyle: titleStyle,
                  //   titleMaxLines: 2,
                  //   subtitleMaxLines: 2,
                  //   // subtitle: t.DirectMsgsDescr,
                  //   leading: icon(
                  //     context,
                  //     user!.commentLikesNotify
                  //         ? FontAwesomeIcons.bell
                  //         : FontAwesomeIcons.bellSlash,
                  //   ),
                  //   switchValue: user!.commentLikesNotify,
                  //   onToggle: (v) {
                  //     UserRepository.commentLikesNotify(v);
                  //     setState(() {
                  //       user = user!.copyWith(commentLikesNotify: v);
                  //     });
                  //   },
                  // ),
                  // SettingsTile.switchTile(
                  //   switchActiveColor: theme.primaryColor,
                  //   title: t.RepliesLikes,
                  //   titleTextStyle: titleStyle,
                  //   titleMaxLines: 2,
                  //   subtitleMaxLines: 2,
                  //   // subtitle: t.DirectMsgsDescr,
                  //   leading: icon(
                  //     context,
                  //     user!.replyLikesNotify
                  //         ? FontAwesomeIcons.bell
                  //         : FontAwesomeIcons.bellSlash,
                  //   ),
                  //   switchValue: user!.replyLikesNotify,
                  //   onToggle: (v) {
                  //     UserRepository.replyLikesNotify(v);
                  //     setState(() {
                  //       user = user!.copyWith(replyLikesNotify: v);
                  //     });
                  //   },
                  // ),
                ],
              ),
              SizedBox(height: 10),
              SettingsSection(
                title: t.Theme,
                titleTextStyle: titleStyleBold,
                titlePadding: EdgeInsets.only(bottom: 30, left: 20, right: 20),
                tiles: [
                  SettingsTile.switchTile(
                    theme: SettingsTileTheme(tileColor: theme.canvasColor),
                    title: t.DarkMode,
                    titleTextStyle: titleStyle,
                    titleMaxLines: 2,
                    subtitleMaxLines: 2,
                    switchActiveColor: theme.primaryColor,
                    leading: icon(
                      context,
                      appPrefs.isDarkMode() ? Icons.nightlight : Icons.wb_sunny,
                    ),
                    switchValue: appPrefs.isDarkMode(),
                    onToggle: (_) => appPrefs.changeThemeMode(),
                  ),

                  // SettingsTile(
                  //   title: t.DarkMode,
                  //   leading: Icon(Icons.nightlight_round),

                  // trailing: Obx(
                  //   () => DayNightSwitcherIcon(
                  //     isDarkModeEnabled: !appPrefs.isDarkMode(),
                  //     onStateChanged: (_) => appPrefs.changeThemeMode(),
                  //   ),
                  // ),
                  //)
                ],
              ),
              SizedBox(height: 10),
              SettingsSection(
                title: t.AppLanguage,
                titleTextStyle: titleStyleBold,
                titlePadding: EdgeInsets.only(bottom: 30, left: 20, right: 20),
                tiles: [
                  SettingsTile(
                    theme: SettingsTileTheme(tileColor: theme.canvasColor),
                    title: t.AppLanguage,
                    titleTextStyle: titleStyle,
                    titleMaxLines: 2,
                    subtitleMaxLines: 2,
                    leading: icon(context, Icons.language),
                    onPressed: (_) => AppNavigator.toAppLanguage(),
                    trailing: icon(context, Icons.chevron_right),
                  ),
                ],
              ),
              SizedBox(height: 10),
              SettingsSection(
                title: t.Others,
                titleTextStyle: titleStyleBold,
                titlePadding: EdgeInsets.only(bottom: 30, left: 20, right: 20),
                tiles: [
                  SettingsTile(
                    theme: SettingsTileTheme(tileColor: theme.canvasColor),
                    title: t.About,
                    titleTextStyle: titleStyle,
                    titleMaxLines: 2,
                    subtitleMaxLines: 2,
                    leading: icon(context, Icons.info),
                    onPressed: (_) => AppNavigator.toAbout(),
                    trailing: icon(context, Icons.chevron_right),
                  ),
                  SettingsTile(
                    theme: SettingsTileTheme(tileColor: theme.canvasColor),
                    title: t.FAQ,
                    titleTextStyle: titleStyle,
                    titleMaxLines: 2,
                    subtitleMaxLines: 2,
                    leading: icon(context, Icons.question_mark),
                    onPressed: (_) => AppNavigator.toFAQ(),
                    trailing: icon(context, Icons.chevron_right),
                  ),
                  SettingsTile(
                    theme: SettingsTileTheme(tileColor: theme.canvasColor),
                    title: t.CommunityGuidelines,
                    titleTextStyle: titleStyle,
                    titleMaxLines: 2,
                    subtitleMaxLines: 2,
                    leading: icon(context, Icons.groups),
                    onPressed: (_) => AppNavigator.toCommunityGuidelines(),
                    trailing: icon(context, Icons.chevron_right),
                  ),
                  SettingsTile(
                    theme: SettingsTileTheme(tileColor: theme.canvasColor),
                    title: t.PrivacyPolicy,
                    titleTextStyle: titleStyle,
                    titleMaxLines: 2,
                    subtitleMaxLines: 2,
                    leading: icon(context, Icons.privacy_tip),
                    onPressed: (_) => AppNavigator.toPrivacy(),
                    trailing: icon(context, Icons.chevron_right),
                  ),
                  SettingsTile(
                    theme: SettingsTileTheme(tileColor: theme.canvasColor),
                    title: t.TermsService,
                    titleTextStyle: titleStyle,
                    titleMaxLines: 2,
                    subtitleMaxLines: 2,
                    leading: icon(context, FontAwesomeIcons.file),
                    onPressed: (_) => AppNavigator.toTerms(),
                    trailing: icon(context, Icons.chevron_right),
                  ),
                  SettingsTile(
                    theme: SettingsTileTheme(tileColor: theme.canvasColor),
                    title: t.ComingSoon,
                    titleTextStyle: titleStyle,
                    titleMaxLines: 2,
                    subtitleMaxLines: 2,
                    leading: icon(context, FontAwesomeIcons.calendar),
                    onPressed: (_) => AppNavigator.toComingSoon(),
                    trailing: icon(context, Icons.chevron_right),
                  ),
                  SettingsTile(
                    theme: SettingsTileTheme(tileColor: theme.canvasColor),
                    title: t.ReportProblem,
                    titleTextStyle: titleStyle,
                    titleMaxLines: 2,
                    subtitleMaxLines: 2,
                    leading: icon(context, Icons.report),
                    onPressed: (_) => AppNavigator.toReportProblem(),
                    trailing: icon(context, Icons.chevron_right),
                  ),
                  SettingsTile(
                    theme: SettingsTileTheme(tileColor: theme.canvasColor),
                    title: t.ShareiConfezz,
                    titleTextStyle: titleStyle,
                    titleMaxLines: 2,
                    subtitleMaxLines: 2,
                    leading: icon(context, Icons.share),
                    onPressed: (BuildContext context) async {
                      await Share.share(
                        'This is a link to iConfezz App', //mkmkmk should be link to account and no token in url
                        subject: 'iConFezz',
                      );
                    },
                    // (_) {},
                    // () {
                    //   StoreRedirect.redirect(
                    //       androidAppId: "Add id here",
                    //       iOSAppId: "Add id here");
                    // },
                    trailing: icon(context, Icons.chevron_right),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Icon icon(BuildContext context, IconData iconData) => Icon(
        iconData,
        color: Theme.of(context).iconTheme.color,
      );

  Future<void> save() async {
    if (user == authProvider.user) return;
    await 1.delay();
    authProvider.rxUser(user);
    await UserRepository.saveMyInfo();
  }

  Widget section({required Widget child}) {
    final theme = Theme.of(context);

    return Container(
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        border: Border.all(color: theme.iconTheme.color!, width: 0.5),
        borderRadius: BorderRadius.circular(10),
        color: theme.canvasColor.withAlpha(100),
      ),
      child: child,
    );
  }
}
