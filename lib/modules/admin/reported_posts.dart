// ignore_for_file: always_use_package_imports

import '../../imports.dart';
import '../feeds/data/posts.dart';
import '../feeds/models/post.dart';
import '../feeds/pages/posts/widgets/post_item.dart';

class ReportedPostsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Appbar(
        titleStr: t.ReportedPosts,
      ),
      body: StreamBuilder<List<Post>>(
        stream: PostsRepository.reportedPostsStream(),
        builder: (_, snap) {
          final posts = snap.data ?? <Post>[];
          return ListView.builder(
            itemCount: posts.length,
            itemBuilder: (_, i) => PostWidget(posts[i]),
          );
        },
      ),
    );
  }
}
