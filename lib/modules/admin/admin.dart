// ignore_for_file: always_use_package_imports

import '../../imports.dart';
import 'reported_posts.dart';

class AdminPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: AppTextRegular(t.Admin),
      ),
      body: Column(
        children: [
          ListTile(
            title: AppTextRegular(t.ReportedPosts),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: () => Get.to(() => ReportedPostsPage()),
          ),
        ],
      ),
    );
  }
}
