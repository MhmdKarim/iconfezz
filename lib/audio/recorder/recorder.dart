// ignore_for_file: always_use_package_imports

import 'dart:async';

import 'package:record/record.dart';

import '../../imports.dart';

class AudioRecorder extends StatefulWidget {
  final String path;
  final VoidCallback onStop;

  const AudioRecorder({required this.path, required this.onStop});

  @override
  _AudioRecorderState createState() => _AudioRecorderState();
}

class _AudioRecorderState extends State<AudioRecorder> {
  bool _isRecording = false;
  bool _isPaused = false;
  int _recordDuration = 0;
  Timer? _timer;
  final _audioRecorder = Record();

  @override
  void initState() {
    _isRecording = false;
    super.initState();
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _buildRecordStopControl(context),
          const SizedBox(width: 5),
          _buildPauseResumeControl(context),
          const SizedBox(width: 5),
          _buildText(context),
        ],
      ),
    );
  }

  Widget _buildRecordStopControl(BuildContext context) {
    late Icon icon;
    late Color color;
    final theme = Theme.of(context);
    if (_isRecording || _isPaused) {
      icon = Icon(Icons.stop, color: theme.primaryColor, size: 30);
      color = theme.primaryColor.withOpacity(0.1);
    } else {
      icon = Icon(Icons.mic, color: theme.primaryColor, size: 30);
      color = theme.primaryColor.withOpacity(0.1);
    }

    return ClipOval(
      child: Material(
        color: color,
        child: InkWell(
          onTap: () {
            _isRecording ? _stop() : _start();
          },
          child: SizedBox(width: 56, height: 56, child: icon),
        ),
      ),
    );
  }

  Widget _buildPauseResumeControl(BuildContext context) {
    final theme = Theme.of(context);
    if (!_isRecording && !_isPaused) {
      return const SizedBox.shrink();
    }

    late Icon icon;
    late Color color;

    if (!_isPaused) {
      icon = Icon(Icons.pause, color: theme.primaryColor, size: 30);
      color = theme.primaryColor.withOpacity(0.1);
    } else {
      final theme = Theme.of(context);
      icon = Icon(Icons.play_arrow, color: theme.primaryColor, size: 30);
      color = theme.primaryColor.withOpacity(0.1);
    }

    return ClipOval(
      child: Material(
        color: color,
        child: InkWell(
          onTap: () {
            _isPaused ? _resume() : _pause();
          },
          child: SizedBox(width: 56, height: 56, child: icon),
        ),
      ),
    );
  }

  Widget _buildText(BuildContext context) {
    if (_isRecording || _isPaused) {
      return _buildTimer(context);
    }

    return AppTextRegular(
      // t.WaitingToRecord
      '',
    );
  }

  Widget _buildTimer(BuildContext context) {
    final String minutes = _formatNumber(_recordDuration ~/ 60);
    final String seconds = _formatNumber(_recordDuration % 60);
    final theme = Theme.of(context);
    return AppTextRegular(
      '$minutes : $seconds',
      color: theme.primaryColor,
    );
  }

  String _formatNumber(int number) {
    String numberStr = number.toString();
    if (number < 10) {
      numberStr = '0$numberStr';
    }

    return numberStr;
  }

  Future<void> _start() async {
    try {
      if (await _audioRecorder.hasPermission()) {
        await _audioRecorder.start(path: widget.path);

        final bool isRecording = await _audioRecorder.isRecording();
        setState(() {
          _isRecording = isRecording;
          _recordDuration = 0;
        });

        _startTimer();
      }
    } catch (e) {
      // print(e);
    }
  }

  Future<void> _stop() async {
    _timer?.cancel();
    await _audioRecorder.stop();

    setState(() => _isRecording = false);

    widget.onStop();
  }

  Future<void> _pause() async {
    _timer?.cancel();
    await _audioRecorder.pause();

    setState(() => _isPaused = true);
  }

  Future<void> _resume() async {
    _startTimer();
    await _audioRecorder.resume();

    setState(() => _isPaused = false);
  }

  void _startTimer() {
    const tick = Duration(seconds: 1);

    _timer?.cancel();

    _timer = Timer.periodic(tick, (Timer t) {
      setState(() => _recordDuration++);
    });
  }
}
