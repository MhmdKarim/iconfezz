/*
 * Generated file. Do not edit.
 *
 * Locales: 14
 * Strings: 3360 (240.0 per locale)
 *
 * Built on 2022-05-21 at 13:48 UTC
 */

import 'package:flutter/widgets.dart';

const AppLocale _baseLocale = AppLocale.en;
AppLocale _currLocale = _baseLocale;

/// Supported locales, see extension methods below.
///
/// Usage:
/// - LocaleSettings.setLocale(AppLocale.en) // set locale
/// - Locale locale = AppLocale.en.flutterLocale // get flutter locale from enum
/// - if (LocaleSettings.currentLocale == AppLocale.en) // locale check
enum AppLocale {
  en, // 'en' (base locale, fallback)
  ar, // 'ar'
  de, // 'de'
  es, // 'es'
  fa, // 'fa'
  fr, // 'fr'
  hi, // 'hi'
  id, // 'id'
  it, // 'it'
  ja, // 'ja'
  ko, // 'ko'
  ru, // 'ru'
  tr, // 'tr'
  zh, // 'zh'
}

/// Method A: Simple
///
/// No rebuild after locale change.
/// Translation happens during initialization of the widget (call of t).
///
/// Usage:
/// String a = t.someKey.anotherKey;
/// String b = t['someKey.anotherKey']; // Only for edge cases!
_StringsEn _t = _currLocale.translations;
_StringsEn get t => _t;

/// Method B: Advanced
///
/// All widgets using this method will trigger a rebuild when locale changes.
/// Use this if you have e.g. a settings page where the user can select the locale during runtime.
///
/// Step 1:
/// wrap your App with
/// TranslationProvider(
/// 	child: MyApp()
/// );
///
/// Step 2:
/// final t = Translations.of(context); // Get t variable.
/// String a = t.someKey.anotherKey; // Use t variable.
/// String b = t['someKey.anotherKey']; // Only for edge cases!
class Translations {
  Translations._(); // no constructor

  static _StringsEn of(BuildContext context) {
    final inheritedWidget =
        context.dependOnInheritedWidgetOfExactType<_InheritedLocaleData>();
    if (inheritedWidget == null) {
      throw 'Please wrap your app with "TranslationProvider".';
    }
    return inheritedWidget.translations;
  }
}

class LocaleSettings {
  LocaleSettings._(); // no constructor

  /// Uses locale of the device, fallbacks to base locale.
  /// Returns the locale which has been set.
  static AppLocale useDeviceLocale() {
    final locale = AppLocaleUtils.findDeviceLocale();
    return setLocale(locale);
  }

  /// Sets locale
  /// Returns the locale which has been set.
  static AppLocale setLocale(AppLocale locale) {
    _currLocale = locale;
    _t = _currLocale.translations;

    if (WidgetsBinding.instance != null) {
      // force rebuild if TranslationProvider is used
      _translationProviderKey.currentState?.setLocale(_currLocale);
    }

    return _currLocale;
  }

  /// Sets locale using string tag (e.g. en_US, de-DE, fr)
  /// Fallbacks to base locale.
  /// Returns the locale which has been set.
  static AppLocale setLocaleRaw(String rawLocale) {
    final locale = AppLocaleUtils.parse(rawLocale);
    return setLocale(locale);
  }

  /// Gets current locale.
  static AppLocale get currentLocale {
    return _currLocale;
  }

  /// Gets base locale.
  static AppLocale get baseLocale {
    return _baseLocale;
  }

  /// Gets supported locales in string format.
  static List<String> get supportedLocalesRaw {
    return AppLocale.values.map((locale) => locale.languageTag).toList();
  }

  /// Gets supported locales (as Locale objects) with base locale sorted first.
  static List<Locale> get supportedLocales {
    return AppLocale.values.map((locale) => locale.flutterLocale).toList();
  }
}

/// Provides utility functions without any side effects.
class AppLocaleUtils {
  AppLocaleUtils._(); // no constructor

  /// Returns the locale of the device as the enum type.
  /// Fallbacks to base locale.
  static AppLocale findDeviceLocale() {
    final String? deviceLocale =
        WidgetsBinding.instance?.window.locale.toLanguageTag();
    if (deviceLocale != null) {
      final typedLocale = _selectLocale(deviceLocale);
      if (typedLocale != null) {
        return typedLocale;
      }
    }
    return _baseLocale;
  }

  /// Returns the enum type of the raw locale.
  /// Fallbacks to base locale.
  static AppLocale parse(String rawLocale) {
    return _selectLocale(rawLocale) ?? _baseLocale;
  }
}

// context enums

// interfaces generated as mixins

// translation instances

late _StringsEn _translationsEn = _StringsEn.build();
late _StringsAr _translationsAr = _StringsAr.build();
late _StringsDe _translationsDe = _StringsDe.build();
late _StringsEs _translationsEs = _StringsEs.build();
late _StringsFa _translationsFa = _StringsFa.build();
late _StringsFr _translationsFr = _StringsFr.build();
late _StringsHi _translationsHi = _StringsHi.build();
late _StringsId _translationsId = _StringsId.build();
late _StringsIt _translationsIt = _StringsIt.build();
late _StringsJa _translationsJa = _StringsJa.build();
late _StringsKo _translationsKo = _StringsKo.build();
late _StringsRu _translationsRu = _StringsRu.build();
late _StringsTr _translationsTr = _StringsTr.build();
late _StringsZh _translationsZh = _StringsZh.build();

// extensions for AppLocale

extension AppLocaleExtensions on AppLocale {
  /// Gets the translation instance managed by this library.
  /// [TranslationProvider] is using this instance.
  /// The plural resolvers are set via [LocaleSettings].
  _StringsEn get translations {
    switch (this) {
      case AppLocale.en:
        return _translationsEn;
      case AppLocale.ar:
        return _translationsAr;
      case AppLocale.de:
        return _translationsDe;
      case AppLocale.es:
        return _translationsEs;
      case AppLocale.fa:
        return _translationsFa;
      case AppLocale.fr:
        return _translationsFr;
      case AppLocale.hi:
        return _translationsHi;
      case AppLocale.id:
        return _translationsId;
      case AppLocale.it:
        return _translationsIt;
      case AppLocale.ja:
        return _translationsJa;
      case AppLocale.ko:
        return _translationsKo;
      case AppLocale.ru:
        return _translationsRu;
      case AppLocale.tr:
        return _translationsTr;
      case AppLocale.zh:
        return _translationsZh;
    }
  }

  /// Gets a new translation instance.
  /// [LocaleSettings] has no effect here.
  /// Suitable for dependency injection and unit tests.
  ///
  /// Usage:
  /// final t = AppLocale.en.build(); // build
  /// String a = t.my.path; // access
  _StringsEn build() {
    switch (this) {
      case AppLocale.en:
        return _StringsEn.build();
      case AppLocale.ar:
        return _StringsAr.build();
      case AppLocale.de:
        return _StringsDe.build();
      case AppLocale.es:
        return _StringsEs.build();
      case AppLocale.fa:
        return _StringsFa.build();
      case AppLocale.fr:
        return _StringsFr.build();
      case AppLocale.hi:
        return _StringsHi.build();
      case AppLocale.id:
        return _StringsId.build();
      case AppLocale.it:
        return _StringsIt.build();
      case AppLocale.ja:
        return _StringsJa.build();
      case AppLocale.ko:
        return _StringsKo.build();
      case AppLocale.ru:
        return _StringsRu.build();
      case AppLocale.tr:
        return _StringsTr.build();
      case AppLocale.zh:
        return _StringsZh.build();
    }
  }

  String get languageTag {
    switch (this) {
      case AppLocale.en:
        return 'en';
      case AppLocale.ar:
        return 'ar';
      case AppLocale.de:
        return 'de';
      case AppLocale.es:
        return 'es';
      case AppLocale.fa:
        return 'fa';
      case AppLocale.fr:
        return 'fr';
      case AppLocale.hi:
        return 'hi';
      case AppLocale.id:
        return 'id';
      case AppLocale.it:
        return 'it';
      case AppLocale.ja:
        return 'ja';
      case AppLocale.ko:
        return 'ko';
      case AppLocale.ru:
        return 'ru';
      case AppLocale.tr:
        return 'tr';
      case AppLocale.zh:
        return 'zh';
    }
  }

  Locale get flutterLocale {
    switch (this) {
      case AppLocale.en:
        return const Locale.fromSubtags(languageCode: 'en');
      case AppLocale.ar:
        return const Locale.fromSubtags(languageCode: 'ar');
      case AppLocale.de:
        return const Locale.fromSubtags(languageCode: 'de');
      case AppLocale.es:
        return const Locale.fromSubtags(languageCode: 'es');
      case AppLocale.fa:
        return const Locale.fromSubtags(languageCode: 'fa');
      case AppLocale.fr:
        return const Locale.fromSubtags(languageCode: 'fr');
      case AppLocale.hi:
        return const Locale.fromSubtags(languageCode: 'hi');
      case AppLocale.id:
        return const Locale.fromSubtags(languageCode: 'id');
      case AppLocale.it:
        return const Locale.fromSubtags(languageCode: 'it');
      case AppLocale.ja:
        return const Locale.fromSubtags(languageCode: 'ja');
      case AppLocale.ko:
        return const Locale.fromSubtags(languageCode: 'ko');
      case AppLocale.ru:
        return const Locale.fromSubtags(languageCode: 'ru');
      case AppLocale.tr:
        return const Locale.fromSubtags(languageCode: 'tr');
      case AppLocale.zh:
        return const Locale.fromSubtags(languageCode: 'zh');
    }
  }
}

extension StringAppLocaleExtensions on String {
  AppLocale? toAppLocale() {
    switch (this) {
      case 'en':
        return AppLocale.en;
      case 'ar':
        return AppLocale.ar;
      case 'de':
        return AppLocale.de;
      case 'es':
        return AppLocale.es;
      case 'fa':
        return AppLocale.fa;
      case 'fr':
        return AppLocale.fr;
      case 'hi':
        return AppLocale.hi;
      case 'id':
        return AppLocale.id;
      case 'it':
        return AppLocale.it;
      case 'ja':
        return AppLocale.ja;
      case 'ko':
        return AppLocale.ko;
      case 'ru':
        return AppLocale.ru;
      case 'tr':
        return AppLocale.tr;
      case 'zh':
        return AppLocale.zh;
      default:
        return null;
    }
  }
}

// wrappers

GlobalKey<_TranslationProviderState> _translationProviderKey =
    GlobalKey<_TranslationProviderState>();

class TranslationProvider extends StatefulWidget {
  TranslationProvider({required this.child})
      : super(key: _translationProviderKey);

  final Widget child;

  @override
  _TranslationProviderState createState() => _TranslationProviderState();

  static _InheritedLocaleData of(BuildContext context) {
    final inheritedWidget =
        context.dependOnInheritedWidgetOfExactType<_InheritedLocaleData>();
    if (inheritedWidget == null) {
      throw 'Please wrap your app with "TranslationProvider".';
    }
    return inheritedWidget;
  }
}

class _TranslationProviderState extends State<TranslationProvider> {
  AppLocale locale = _currLocale;

  void setLocale(AppLocale newLocale) {
    setState(() {
      locale = newLocale;
    });
  }

  @override
  Widget build(BuildContext context) {
    return _InheritedLocaleData(
      locale: locale,
      child: widget.child,
    );
  }
}

class _InheritedLocaleData extends InheritedWidget {
  final AppLocale locale;
  Locale get flutterLocale => locale.flutterLocale; // shortcut
  final _StringsEn translations; // store translations to avoid switch call

  _InheritedLocaleData({required this.locale, required Widget child})
      : translations = locale.translations,
        super(child: child);

  @override
  bool updateShouldNotify(_InheritedLocaleData oldWidget) {
    return oldWidget.locale != locale;
  }
}

// pluralization feature not used

// helpers

final _localeRegex =
    RegExp(r'^([a-z]{2,8})?([_-]([A-Za-z]{4}))?([_-]?([A-Z]{2}|[0-9]{3}))?$');
AppLocale? _selectLocale(String localeRaw) {
  final match = _localeRegex.firstMatch(localeRaw);
  AppLocale? selected;
  if (match != null) {
    final language = match.group(1);
    final country = match.group(5);

    // match exactly
    selected = AppLocale.values.cast<AppLocale?>().firstWhere(
        (supported) => supported?.languageTag == localeRaw.replaceAll('_', '-'),
        orElse: () => null);

    if (selected == null && language != null) {
      // match language
      selected = AppLocale.values.cast<AppLocale?>().firstWhere(
          (supported) => supported?.languageTag.startsWith(language) == true,
          orElse: () => null);
    }

    if (selected == null && country != null) {
      // match country
      selected = AppLocale.values.cast<AppLocale?>().firstWhere(
          (supported) => supported?.languageTag.contains(country) == true,
          orElse: () => null);
    }
  }
  return selected;
}

// translations

// Path: <root>
class _StringsEn {
  /// You can call this constructor and build your own translation instance of this locale.
  /// Constructing via the enum [AppLocale.build] is preferred.
  _StringsEn.build();

  /// Access flat map
  dynamic operator [](String key) => _flatMap[key];

  // Internal flat map initialized lazily
  late final Map<String, dynamic> _flatMap = _buildFlatMap();

  late final _StringsEn _root = this; // ignore: unused_field

  // Translations
  String get AppName => 'iConfezz';
  String get AboutApp =>
      'iConfezz is a community dedicated to hear voice of others.';
  String get AppVersion => '2.0.0';
  String get UnknownError => 'Oops, Something went Wrong!';
  String get Next => 'Next';
  String get EmailUser => 'Are you Email User, Login here!';
  String get Email => 'Email';
  String get bio => 'Biography';
  String get city => 'City';
  String get dob => 'Date of birth';
  String get Password => 'Password';
  String get Login => 'Login';
  String get PhoneNumber => 'Phone Number';
  String get PhoneVerification => 'Phone Number Verification';
  String get EnterCode => 'Enter the code sent to ';
  String get DidntRecieveCode => 'Didn\'t receive the code? ';
  String get InvalidSMSCode => 'The SMS Verification Code is Invalid';
  String get RESEND => 'RESEND';
  String get VERIFY => 'VERIFY';
  String get POSTS => 'POSTS';
  String get FOLLOWERS => 'Followers';
  String get FOLLOWINGS => 'Followings';
  String get Follow => 'Follow';
  String get Following => 'Following';
  String get Register => 'Register';
  String get Username => 'Username';
  String get FirstName => 'First Name';
  String get LastName => 'Last Name';
  String get FullName => 'Full Name';
  String get Status => 'Status';
  String get AddValidUsername => 'Add a valid username';
  String get AddValidFirstName => 'Add a valid first name';
  String get AddValidLastName => 'Add a valid last name';
  String get SelectGender => 'Please select your gender.';
  String get Male => 'Male';
  String get Female => 'Female';
  String get Other => 'Other';
  String get AcceptTerms => 'I Accept All Terms & Conditions';
  String get Save => 'Save';
  String get Search => 'Search';
  String get Searching => 'Searching...';
  String get SelectPhoneCode => 'Select a phone code';
  String get InvalidEmail => 'Invalid Email';
  String get ShortPassword => 'Password Must Contain At Least 8 characters';
  String get UsernameTaken => 'Username is Already taken';
  String get InvalidPhone => 'Please fill up all the cells properly';
  String get Message => 'Message';
  String get LoginFirst => 'Oops, Please login first';
  String get START => 'GET STARTED';
  String get Wallpapers => 'Wallpapers';
  String get Profile => 'Profile';
  String get NoInternet => 'No Internet Access';
  String get Settings => 'Settings';
  String get Account => 'Account';
  String get EditProfile => 'Edit Profile';
  String get OnlineStatus => 'Online Status';
  String get OnlineDescription => 'Anyone can see when you\'re last Online';
  String get Logout => 'Logout';
  String get DirectMsgs => 'Chat Messages';
  String get DirectMsgsDescr => 'Recieve chat notifications';
  String get GroupsMsgs => 'Groups Messages';
  String get GroupsMsgsDesc => 'Recieve all Groups Notifications';
  String get AddGroupName => 'Please! Add Group Name';
  String get About => 'About Us';
  String get RateUs => 'Rate Us';
  String get EULA => 'EULA Agreement';
  String get PrivacyPolicy => 'Privacy Policy';
  String get Feed => 'Feed';
  String get Post => 'Post';
  String get Gallery => 'Gallery';
  String get Stories => 'Stories';
  String get MyPosts => 'My Posts';
  String get Favorites => 'Favorites';
  String get Announcement => 'Announcement';
  String get WhatOnMind => 'What to Share?';
  String get Likes => 'Likes';
  String get Comments => 'Comments';
  String get CreatePost => 'Create Post';
  String get Share => 'Share';
  String get Edit => 'Edit';
  String get Delete => 'Delete';
  String get Copy => 'Copy';
  String get Copied => 'Copied';
  String get ReadMore => '...Read More';
  String get SuccessPostPublish => 'Post has been published Successfully';
  String get SuccessPostEdited => 'Post has been edited Successfully';
  String get TypeComment => 'Type a comment...';
  String get TypeReply => 'Type a reply...';
  String get NotAllowedToPublish =>
      'Sorry! You are not allowed to publish anymore';
  String get NotAllowedToComment => 'Sorry! You are not allowed to comment';
  String get PostRemoveConfirm => 'Are you sure you want to delete this post?';
  String get CommentRemoveConfirm =>
      'Are you sure you want to delete this comment?';
  String get AddSomeContent => 'Please add some content before posting';
  String get Reply => 'Reply';
  String get Replies => 'Replies';
  String get RepliedToComment => 'replied to your comment';
  String get Chats => 'Chats';
  String get OnlineUsers => 'ONLINE USERS';
  String get RecentChats => 'RECENT CHATS';
  String get RemoveConversation => 'Remove Conversation';
  String get Messaging => 'Message...';
  String get CannotChatWithUser => 'Sorry! You can\'t chat wih this user';
  String get MsgDeleteConfirm => 'Are you sure you want to delete the message?';
  String get NoChatFound => 'No Recents chat yet, start chatting!';
  String get Online => 'Online';
  String get Typing => 'Typing...';
  String get Image => 'Image';
  String get Voice => 'Voice';
  String get Video => 'Video';
  String get Emoji => 'Emoji';
  String get GIF => 'GIF';
  String get Sticker => 'Sticker';
  String get Groups => 'Groups';
  String get CreateGroup => 'Create a Group';
  String get EditGroup => 'Edit your Group';
  String get Members => 'Members';
  String get PhotosLibrary => 'Photos Library';
  String get TakePicture => 'Take Picture';
  String get VideosLibrary => 'Videos Library';
  String get RecordVideo => 'Record Video';
  String get Cancel => 'Cancel';
  String get SlideToCancel => 'Slide to cancel';
  String get On => 'On';
  String get Off => 'Off';
  String get Group => 'Group';
  String get LeaveGroup => 'Leave Group';
  String get GroupName => 'Group Name';
  String get Join => 'Join';
  String get Joined => 'Joined';
  String get Public => 'Public';
  String get Private => 'Private';
  String get GroupType => 'Group Type';
  String get RemoveMember => 'Remove member';
  String get AddMembers => 'Add Members';
  String get Block => 'Block';
  String get Unblock => 'UnBlock';
  String get Ban => 'Ban';
  String get Unban => 'Unban';
  String get Banned => 'Banned';
  String get Newest => 'Newest';
  String get Trending => 'Trending';
  String get MostDownloaded => 'Most Downloaded';
  String get Categories => 'Categories';
  String get AddWallpaper => 'Add Wallpaper';
  String get WallpaperName => 'Wallpaper Name';
  String get Category => 'Category';
  String get Upload => 'Upload';
  String get Home => 'Home';
  String get Lock => 'Lock';
  String get Both => 'Both';
  String get SetAsWallpaper => 'Set as Wallpaper';
  String get WallpaperSet => 'Wallpaper Set Successfully';
  String get FailedToUpload => 'Oops! Upload Failed, Please try again';
  String get UploadFinished => 'Upload Finished';
  String get Downloading => 'Downloading';
  String get NoWallpaperSelectedMsg => 'Please Select Wallpaper to continue';
  String get NoImgSelected => 'No Image Selected';
  String get Notifications => 'Notifications';
  String get StartFollowingMsg => 'started following you';
  String get PostReactionMsg => 'reacted to your post';
  String get PostCommentMsg => 'commented your post';
  String get ReplyMsg => 'replied to your comment';
  String get CommentReactedMsg => 'reacted to your comment';
  String get CannotFindFile => 'Oops! Cannot find the file';
  String get NoFilePicked => 'Trying to upload and no file is picked';
  String get SentYouMsg => 'Sent you message';
  String get Report => 'Report';
  String get Unreport => 'Unreport';
  String get ReportDesc => 'We remove post that has: ';
  String get ReportReasons =>
      '⚫️ Sexual content. \n\n⚫️ Violent or repulsive content. \n\n⚫️ Hateful or abusive content. \n\n⚫️ Spam or misleading.';
  String get ReportNote => 'We wont let them know if you take this action.';
  String get ReportThanks =>
      'We will check your request, Thanks for helping improve our community';
  String get Admin => 'Admin';
  String get ProfanityDetected =>
      'Bad words detected, your account may get suspended!';
  String get AreYouSure => 'Are you sure to';
  String get ConfirmChatDeletion => 'Are you sure to delete chat';
  String get ReportedPosts => 'Reported Posts';
  String get AllChats => 'All Chats';
  String get Users => 'Users';
  String get TermsService => 'Terms of Service';
  String get WaitingToRecord => 'Waiting to record';
  String get AdjustVolume => 'Adjust volume';
  String get AdjustSpeed => 'Adjust speed';
  String get Ok => 'Ok';
  String get ShareExternally => 'Share externally';
  String get ShareOnYourWall => 'Share on your wall';
  String get Error => 'Error';
  String get Me => 'Me';
  String get OopsCode => 'Oops! vefication code is wrong';
  String get OopsNetwork =>
      'Oops! something went wrong, Please check your network!';
  String get OopsEmailorPassword => 'Oops! email or password is not correct!';
  String get OopsNotRegister =>
      'Oops! no user found, if not register consider sign up first!';
  String get OopsEmailUsed =>
      'Oops! this email is already used by another account!';
  String get OopsPhoneFormat => 'Oops! phone format is invalid';
  String get NoAudioRecorded => 'No audio added';
  String get OopsWrong => 'Oops, something went wrong';
  String get Optional => 'Optional';
  String get Sending => 'Sending... ';
  String get NotificationPermission => 'Notification permission is garanted';
  String get Theme => 'Theme';
  String get DarkMode => 'Dark Mode';
  String get Others => 'Others';
  String get PeopleWhoReacted => 'People who reacted';
  String get BadWords => 'Bad words detected, your account may get suspended!';
  String get PickFile => 'Pick file';
  String get Loading => 'Loading...';
  String get PleaseAddAudio => 'Please add audio';
  String get PeopleYouMayFollow => 'People you may follow';
  String get Unsave => 'Unsave';
  String get PostsLikes => 'Posts Likes';
  String get CommentsLikes => 'Comments Likes';
  String get RepliesLikes => 'Replies Likes';
  String get ShareiConfezz => 'Share iConfezz';
  String get MemberSince => 'Member since';
  String get Comment => 'Comment';
  String get UnShare => 'UnShare';
  String get Liked => 'Liked';
  String get Like => 'Like';
  String get SAVED => 'SAVED';
  String get SHARED => 'SHARED';
  String get AppLanguage => 'App Language';
  String get ComingSoon => 'Coming Soon';
  String get ReportProblem => 'Report a Problem';
  String get PushNotifications => 'Push Notifications';
  String get ShareProfile => 'Share Profile';
  String get DeleteAccount => 'Delete Account';
  String get DeleteYourAccount => 'Delete Your Account';
  String get Record => 'Record';
  String get Or => 'Or';
  String get NoPhotoPicked => 'No Photo Picked';
  String get SearchForUsers => 'Search for Users';
  String get NoResultsFound => 'No results found';
  String get TemporaryFilesRemoved => 'Temporary files removed with success';
  String get FailedToClean => 'Failed to clean temporary files';
  String get exit => 'exit';
  String get CommunityGuidelines => 'Community Guidelines';
  String get FAQ => 'FAQ';
  String get Paste => 'Paste';
  String get DoYouWantToPasteThisCode => 'Do you want to paste this code?';
  String get PasteCode => 'Paste Code';
}

// Path: <root>
class _StringsAr implements _StringsEn {
  /// You can call this constructor and build your own translation instance of this locale.
  /// Constructing via the enum [AppLocale.build] is preferred.
  _StringsAr.build();

  /// Access flat map
  @override
  dynamic operator [](String key) => _flatMap[key];

  // Internal flat map initialized lazily
  @override
  late final Map<String, dynamic> _flatMap = _buildFlatMap();

  @override
  late final _StringsAr _root = this; // ignore: unused_field

  // Translations
  @override
  String get AppName => 'iConfezz';
  @override
  String get AboutApp => 'iConfezz هو مجتمع مخصص لسماع صوت الآخرين.';
  @override
  String get AppVersion => '2.0.0';
  @override
  String get UnknownError => 'عفوا, حدث خطأ ما!';
  @override
  String get Next => 'التالي';
  @override
  String get EmailUser => 'هل أنت مستخدم البريد الإلكتروني ، تسجيل الدخول هنا!';
  @override
  String get Email => 'البريد الإلكتروني';
  @override
  String get bio => 'السيرة الذاتية';
  @override
  String get city => 'المدينة';
  @override
  String get dob => 'تاريخ الميلاد';
  @override
  String get Password => 'كلمة المرور';
  @override
  String get Login => 'تسجيل الدخول';
  @override
  String get PhoneNumber => 'رقم الهاتف';
  @override
  String get PhoneVerification => 'التحقق من رقم الهاتف';
  @override
  String get EnterCode => 'أدخل الرمز المرسل إلى ';
  @override
  String get DidntRecieveCode => 'لم تتلق الرمز؟ ';
  @override
  String get InvalidSMSCode => 'رمز التحقق غير صالح';
  @override
  String get RESEND => 'إعادة إرسال';
  @override
  String get VERIFY => 'تحقق';
  @override
  String get POSTS => 'المشاركات';
  @override
  String get FOLLOWERS => 'المتابعون';
  @override
  String get FOLLOWINGS => 'اتابع';
  @override
  String get Follow => 'اتبع';
  @override
  String get Following => 'متابع';
  @override
  String get Register => 'تسجيل';
  @override
  String get Username => 'اسم المستخدم';
  @override
  String get FirstName => 'الاسم الأول';
  @override
  String get LastName => 'اسم العائلة';
  @override
  String get FullName => 'الاسم الكامل';
  @override
  String get Status => 'الحالة';
  @override
  String get AddValidUsername => 'إضافة اسم مستخدم صحيح';
  @override
  String get AddValidFirstName => 'إضافة اسم أول صحيح';
  @override
  String get AddValidLastName => 'إضافة اسم العائلة صحيح';
  @override
  String get SelectGender => 'يرجى تحديد جنسك';
  @override
  String get Male => 'ذكر';
  @override
  String get Female => 'أنثى';
  @override
  String get Other => 'أخرى';
  @override
  String get AcceptTerms => 'أنا أقبل جميع الشروط والأحكام';
  @override
  String get Save => 'حفظ';
  @override
  String get Search => 'بحث';
  @override
  String get Searching => 'البحث...';
  @override
  String get SelectPhoneCode => 'حدد رمز الهاتف';
  @override
  String get InvalidEmail => 'بريد إلكتروني غير صحيح';
  @override
  String get ShortPassword => 'يجب أن تحتوي كلمة المرور على 8 أحرف على الأقل';
  @override
  String get UsernameTaken => 'اسم المستخدم مأخوذ بالفعل';
  @override
  String get InvalidPhone => 'رقم هاتف غير صحيح';
  @override
  String get Message => 'رسالة';
  @override
  String get LoginFirst => 'عفوا ، يرجى الدخول أولا';
  @override
  String get START => 'ابدأ';
  @override
  String get Wallpapers => 'خلفيات';
  @override
  String get Profile => 'الملف الشخصي';
  @override
  String get NoInternet => 'لا يوجد اتصال بالإنترنت';
  @override
  String get Settings => 'الإعدادات';
  @override
  String get Account => 'الحساب';
  @override
  String get EditProfile => 'تعديل الملف الشخصي';
  @override
  String get OnlineStatus => 'حالة الاتصال';
  @override
  String get OnlineDescription =>
      'يمكن لأي شخص أن يرى عندما كنت آخر على الانترنت';
  @override
  String get Logout => 'الخروج';
  @override
  String get DirectMsgs => 'رسائل الدردشة';
  @override
  String get DirectMsgsDescr => 'تلقي إشعارات الدردشة';
  @override
  String get GroupsMsgs => 'رسائل المجموعات';
  @override
  String get GroupsMsgsDesc => 'تلقي جميع إخطارات المجموعات';
  @override
  String get AddGroupName => 'من فضلك! إضافة اسم المجموعة';
  @override
  String get About => 'معلومات عنا';
  @override
  String get RateUs => 'قيمنا';
  @override
  String get EULA => 'اتفاقية EULA';
  @override
  String get PrivacyPolicy => 'سياسة الخصوصية';
  @override
  String get Feed => 'تغذية';
  @override
  String get Post => 'بوست';
  @override
  String get Gallery => 'معرض';
  @override
  String get Stories => 'قصص';
  @override
  String get MyPosts => 'مشاركاتي';
  @override
  String get Favorites => 'المفضلة';
  @override
  String get Announcement => 'الإعلان';
  @override
  String get WhatOnMind => 'ماذا الذي تريد مشاركته';
  @override
  String get Likes => 'إعجاب';
  @override
  String get Comments => 'تعليقات';
  @override
  String get CreatePost => 'إنشاء مشاركة';
  @override
  String get Share => 'مشاركة';
  @override
  String get Edit => 'تحرير';
  @override
  String get Delete => 'حذف';
  @override
  String get Copy => 'نسخ';
  @override
  String get Copied => 'نسخ';
  @override
  String get ReadMore => '...اقرأ المزيد';
  @override
  String get SuccessPostPublish => 'تم نشر المشاركة بنجاح';
  @override
  String get SuccessPostEdited => 'تم تحرير المشاركة بنجاح';
  @override
  String get TypeComment => 'اكتب تعليق...';
  @override
  String get TypeReply => 'اكتب الرد...';
  @override
  String get NotAllowedToPublish => 'آسف! لا يسمح لك بنشر بعد الآن';
  @override
  String get NotAllowedToComment => 'آسف! لا يسمح لك تعليق';
  @override
  String get PostRemoveConfirm => 'هل أنت متأكد من أنك تريد حذف هذا المنشور ؟ ';
  @override
  String get CommentRemoveConfirm =>
      'هل أنت متأكد من أنك تريد حذف هذا التعليق ؟ ';
  @override
  String get AddSomeContent => 'يرجى إضافة بعض المحتوى قبل النشر';
  @override
  String get Reply => 'الرد';
  @override
  String get Replies => 'الردود';
  @override
  String get RepliedToComment => 'رد على تعليقك';
  @override
  String get Chats => 'الدردشات';
  @override
  String get OnlineUsers => 'مستخدمون متصلون';
  @override
  String get RecentChats => 'الدردشات الأخيرة';
  @override
  String get RemoveConversation => 'إزالة المحادثة';
  @override
  String get Messaging => 'رسالة...';
  @override
  String get CannotChatWithUser => 'آسف! لا يمكنك الدردشة مع المستخدم';
  @override
  String get MsgDeleteConfirm => 'هل أنت متأكد من أنك تريد حذف الرسالة ؟ ';
  @override
  String get NoChatFound => 'لا توجد دردشة حديثة بعد ، ابدأ الدردشة!';
  @override
  String get Online => 'متصل';
  @override
  String get Typing => 'الكتابة...';
  @override
  String get Image => 'صورة';
  @override
  String get Voice => 'صوت';
  @override
  String get Video => 'فيديو';
  @override
  String get Emoji => 'ايموجي';
  @override
  String get GIF => 'GIF';
  @override
  String get Sticker => 'ملصق';
  @override
  String get Groups => 'المجموعات';
  @override
  String get CreateGroup => 'إنشاء مجموعة';
  @override
  String get EditGroup => 'تحرير مجموعتك';
  @override
  String get Members => 'أعضاء';
  @override
  String get PhotosLibrary => 'مكتبة الصور';
  @override
  String get TakePicture => 'التقاط صورة';
  @override
  String get VideosLibrary => 'مكتبة الفيديو';
  @override
  String get RecordVideo => 'تسجيل الفيديو';
  @override
  String get Cancel => 'إلغاء';
  @override
  String get SlideToCancel => 'Slide to cancel';
  @override
  String get On => 'على';
  @override
  String get Off => 'إيقاف';
  @override
  String get Group => 'المجموعة';
  @override
  String get LeaveGroup => 'مغادرة المجموعة';
  @override
  String get GroupName => 'اسم المجموعة';
  @override
  String get Join => 'انضم';
  @override
  String get Joined => 'منضم';
  @override
  String get Public => 'عامة';
  @override
  String get Private => 'خاصة';
  @override
  String get GroupType => 'نوع المجموعة';
  @override
  String get RemoveMember => 'حذف الأعضاء';
  @override
  String get AddMembers => 'إضافة أعضاء';
  @override
  String get Block => 'حظر';
  @override
  String get Unblock => 'إلغاء الحظر';
  @override
  String get Ban => 'بان';
  @override
  String get Unban => 'Unban';
  @override
  String get Banned => 'المحظورة';
  @override
  String get Newest => 'الأحدث';
  @override
  String get Trending => 'شائع';
  @override
  String get MostDownloaded => 'الأكثر تنزيلا';
  @override
  String get Categories => 'الفئات';
  @override
  String get AddWallpaper => 'إضافة خلفية';
  @override
  String get WallpaperName => 'Wallpapername';
  @override
  String get Category => 'الفئة';
  @override
  String get Upload => 'تحميل';
  @override
  String get Home => 'الصفحة الرئيسية';
  @override
  String get Lock => 'قفل';
  @override
  String get Both => 'كلاهما';
  @override
  String get SetAsWallpaper => 'تعيين كخلفية';
  @override
  String get WallpaperSet => 'خلفية تعيين بنجاح';
  @override
  String get FailedToUpload => 'عفوا! فشل التحميل ، يرجى المحاولة مرة أخرى';
  @override
  String get UploadFinished => 'Uploadfinished';
  @override
  String get Downloading => 'التنزيل';
  @override
  String get NoWallpaperSelectedMsg => 'الرجاء تحديد خلفية للمتابعة';
  @override
  String get NoImgSelected => 'لا توجد صورة محددة';
  @override
  String get Notifications => 'الإشعارات';
  @override
  String get StartFollowingMsg => 'بدأ في متابعتك';
  @override
  String get PostReactionMsg => 'اعجب بمشاركتك';
  @override
  String get PostCommentMsg => 'علق علي مشاركتك';
  @override
  String get ReplyMsg => 'رد على تعليقك';
  @override
  String get CommentReactedMsg => 'اعجب بتعليقك';
  @override
  String get CannotFindFile => 'عفوا! لا يمكن العثور على الملف';
  @override
  String get NoFilePicked => 'محاولة تحميل ويتم اختيار أي ملف';
  @override
  String get SentYouMsg => 'أرسل لك رسالة';
  @override
  String get Report => 'ابلاغ';
  @override
  String get Unreport => 'عدم ابلاغ';
  @override
  String get ReportDesc => 'نحن نزيل اي مشاركة تحتوي على: ';
  @override
  String get ReportReasons =>
      ' المحتوى الجنسي. \n\n ⚫ كبيرة العنيفة أو مثيرة للاشمئزاز المحتوى. \n\n ⚫ كبيرة البغيضة أو المحتوى المسيء. \n \n Spam️ البريد المزعج أو مضللة.';
  @override
  String get ReportNote => 'لن نعلمهم إذا اتخذت هذا الإجراء.';
  @override
  String get ReportThanks =>
      'سوف نتحقق من طلبك ، شكرا للمساعدة في تحسين مجتمعنا';
  @override
  String get Admin => 'المشرف';
  @override
  String get ProfanityDetected =>
      'الكلمات السيئة المكتشفة ، قد يتم تعليق حسابك!';
  @override
  String get AreYouSure => 'هل أنت متأكد من ';
  @override
  String get ConfirmChatDeletion => 'هل أنت متأكد من حذف الدردشة';
  @override
  String get ReportedPosts => 'المشاركات المبلغ عنها';
  @override
  String get AllChats => 'جميع الدردشات';
  @override
  String get Users => 'المستخدمون';
  @override
  String get TermsService => 'الشروط والأحكام';
  @override
  String get WaitingToRecord => 'انتظار للتسجيل';
  @override
  String get AdjustVolume => 'ضبط مستوى الصوت';
  @override
  String get AdjustSpeed => 'ضبط السرعة';
  @override
  String get Ok => 'موافق';
  @override
  String get ShareExternally => 'شارك خارجيا';
  @override
  String get ShareOnYourWall => 'شارك على الحائط الخاص بك';
  @override
  String get Error => 'خطأ';
  @override
  String get Me => 'أنا';
  @override
  String get OopsCode => 'عفوا! رمز تحقق خاطئ';
  @override
  String get OopsNetwork => 'عفوا! حدث خطأ ما ، يرجى التحقق من شبكتك!';
  @override
  String get OopsEmailorPassword =>
      'عفوا! البريد الإلكتروني أو كلمة المرور غير صحيحة!';
  @override
  String get OopsNotRegister =>
      'عفوا! لم يتم العثور على المستخدم, إن لم يكن مسجل يرجي الاشتراك أولا!';
  @override
  String get OopsEmailUsed =>
      'عفوا! يستخدم هذا البريد الإلكتروني بالفعل من قبل حساب آخر!';
  @override
  String get OopsPhoneFormat => 'عفوا! شكل الهاتف غير صالح';
  @override
  String get NoAudioRecorded => 'لم يضاف اي تسجيل صوتي';
  @override
  String get OopsWrong => 'عفوا, حدث خطأ';
  @override
  String get Optional => 'اختياري';
  @override
  String get Sending => 'إرسال... ';
  @override
  String get NotificationPermission => 'تم السماح بالاشعارات';
  @override
  String get Theme => 'الشكل';
  @override
  String get DarkMode => 'الوضع الليلي';
  @override
  String get Others => 'اخري';
  @override
  String get PeopleWhoReacted => 'الناس الذين تفاعلوا';
  @override
  String get BadWords => 'تم اكتشاف الكلمات السيئة ، قد يتم تعليق حسابك!';
  @override
  String get PickFile => 'اختر ملف';
  @override
  String get Loading => 'تحميل...';
  @override
  String get PleaseAddAudio => 'يرجى إضافة الصوت';
  @override
  String get PeopleYouMayFollow => 'أشخاص يمكن ان تتابعهم';
  @override
  String get Unsave => 'الغاء الحفظ';
  @override
  String get PostsLikes => 'اعجابات المنشورات';
  @override
  String get CommentsLikes => 'اعجابات التعليقات';
  @override
  String get RepliesLikes => 'اعجابات الردود';
  @override
  String get ShareiConfezz => ' iConfezz مشاركة';
  @override
  String get MemberSince => 'مشترك منذ';
  @override
  String get Comment => 'تعليق';
  @override
  String get UnShare => 'الغاء مشاركة';
  @override
  String get Liked => 'اعجبني';
  @override
  String get Like => 'اعجاب';
  @override
  String get SAVED => 'المحفوظ';
  @override
  String get SHARED => 'المشارك';
  @override
  String get AppLanguage => 'لغة التطبيق';
  @override
  String get ComingSoon => 'قادم قريبا';
  @override
  String get ReportProblem => 'ابلاغ عن مشكله';
  @override
  String get PushNotifications => 'إشعارات لحظية';
  @override
  String get ShareProfile => 'مشاركة الملف الشخصي';
  @override
  String get DeleteAccount => 'حذف الحساب';
  @override
  String get DeleteYourAccount => 'حذف حسابك';
  @override
  String get Record => 'تسجيل';
  @override
  String get Or => 'أو';
  @override
  String get NoPhotoPicked => 'لم يتم اختيار أي صورة';
  @override
  String get SearchForUsers => 'بحث عن مستخدمين';
  @override
  String get NoResultsFound => 'لم يتم العثور على نتائج';
  @override
  String get TemporaryFilesRemoved => 'تمت إزالة الملفات المؤقتة بنجاح';
  @override
  String get FailedToClean => 'فشل إزالة الملفات المؤقتة';
  @override
  String get exit => 'الخروج';
  @override
  String get CommunityGuidelines => 'إرشادات المجتمع';
  @override
  String get FAQ => 'التعليمات';
  @override
  String get Paste => 'لصق';
  @override
  String get DoYouWantToPasteThisCode => 'هل تريد لصق هذا الرمز؟';
  @override
  String get PasteCode => 'لصق الرمز';
}

// Path: <root>
class _StringsDe implements _StringsEn {
  /// You can call this constructor and build your own translation instance of this locale.
  /// Constructing via the enum [AppLocale.build] is preferred.
  _StringsDe.build();

  /// Access flat map
  @override
  dynamic operator [](String key) => _flatMap[key];

  // Internal flat map initialized lazily
  @override
  late final Map<String, dynamic> _flatMap = _buildFlatMap();

  @override
  late final _StringsDe _root = this; // ignore: unused_field

  // Translations
  @override
  String get AppName => 'iConfezz';
  @override
  String get AboutApp =>
      'iConfezz ist eine Community, die sich dafür einsetzt, die Stimme anderer zu hören.';
  @override
  String get AppVersion => '2.0.0';
  @override
  String get UnknownError => 'Etwas ist schief gelaufen';
  @override
  String get Next => 'Nächste';
  @override
  String get EmailUser => 'Sind Sie E-Mail-Benutzer, melden Sie sich hier an!';
  @override
  String get Email => 'Email';
  @override
  String get bio => 'Biografie';
  @override
  String get city => 'Stadt';
  @override
  String get dob => 'Geburtsdatum';
  @override
  String get Password => 'Passwort';
  @override
  String get Login => 'Anmeldung';
  @override
  String get PhoneNumber => 'Telefonnummer';
  @override
  String get PhoneVerification => 'Verifizierung der Telefonnummer';
  @override
  String get EnterCode => 'Geben Sie den gesendeten Code ein an ';
  @override
  String get DidntRecieveCode => 'Sie haben den Code nicht erhalten?';
  @override
  String get InvalidSMSCode => 'Der SMS-Bestätigungscode ist ungültig';
  @override
  String get RESEND => 'ERNEUT SENDEN';
  @override
  String get VERIFY => 'VERIFIZIEREN';
  @override
  String get POSTS => 'POSTEN';
  @override
  String get FOLLOWERS => 'Follower';
  @override
  String get FOLLOWINGS => 'Folgen';
  @override
  String get Follow => 'Folgen';
  @override
  String get Following => 'folgend';
  @override
  String get Register => 'Registrieren';
  @override
  String get Username => 'Nutzername';
  @override
  String get FirstName => 'Vorname';
  @override
  String get LastName => 'Familienname, Nachname';
  @override
  String get FullName => 'Vollständiger Name';
  @override
  String get Status => 'Status';
  @override
  String get AddValidUsername => 'Gültigen Benutzernamen hinzufügen';
  @override
  String get AddValidFirstName => 'Gültigen Vornamen hinzufügen';
  @override
  String get AddValidLastName => 'Gültigen Nachnamen hinzufügen';
  @override
  String get SelectGender => 'Bitte wählen Sie Ihr Geschlecht.';
  @override
  String get Male => 'Männlich';
  @override
  String get Female => 'Weiblich';
  @override
  String get Other => 'Sonstiges';
  @override
  String get AcceptTerms => 'Ich akzeptiere alle Geschäftsbedingungen';
  @override
  String get Save => 'Speichern';
  @override
  String get Search => 'Suche';
  @override
  String get Searching => 'Suche...';
  @override
  String get SelectPhoneCode => 'Wählen Sie einen Telefoncode';
  @override
  String get InvalidEmail => 'Ungültige E-Mail';
  @override
  String get ShortPassword => 'Passwort muss mindestens 8 Zeichen enthalten';
  @override
  String get UsernameTaken => 'Benutzername ist bereits vergeben';
  @override
  String get InvalidPhone => 'Bitte füllen Sie alle Zellen richtig aus';
  @override
  String get Message => 'Nachricht';
  @override
  String get LoginFirst => 'Hoppla, bitte melden Sie sich zuerst an';
  @override
  String get START => 'LOSLEGEN';
  @override
  String get Wallpapers => 'Hintergründe';
  @override
  String get Profile => 'Profil';
  @override
  String get NoInternet => 'Kein Internetzugang';
  @override
  String get Settings => 'Einstellungen';
  @override
  String get Account => 'Konto';
  @override
  String get EditProfile => 'Profil bearbeiten';
  @override
  String get OnlineStatus => 'Online Status';
  @override
  String get OnlineDescription =>
      'Jeder kann sehen, wann du zuletzt online warst';
  @override
  String get Logout => 'Ausloggen';
  @override
  String get DirectMsgs => 'Chat-Nachrichten';
  @override
  String get DirectMsgsDescr => 'Chat-Benachrichtigungen erhalten';
  @override
  String get GroupsMsgs => 'Gruppennachrichten';
  @override
  String get GroupsMsgsDesc => 'Alle Gruppenbenachrichtigungen erhalten';
  @override
  String get AddGroupName => 'Bitte! Gruppennamen hinzufügen';
  @override
  String get About => 'über uns';
  @override
  String get RateUs => 'Bewerten Sie uns';
  @override
  String get EULA => 'EULA-Vereinbarung';
  @override
  String get PrivacyPolicy => 'Datenschutz-Bestimmungen';
  @override
  String get Feed => 'Füttern';
  @override
  String get Post => 'Post';
  @override
  String get Gallery => 'Galerie';
  @override
  String get Stories => 'Geschichten';
  @override
  String get MyPosts => 'Meine Posts';
  @override
  String get Favorites => 'Favoriten';
  @override
  String get Announcement => 'Bekanntmachung';
  @override
  String get WhatOnMind => 'Was teilen?';
  @override
  String get Likes => 'Likes';
  @override
  String get Comments => 'Bemerkungen';
  @override
  String get CreatePost => 'Beitrag erstellen';
  @override
  String get Share => 'Teilen';
  @override
  String get Edit => 'Bearbeiten';
  @override
  String get Delete => 'Löschen';
  @override
  String get Copy => 'Kopieren';
  @override
  String get Copied => 'Kopiert';
  @override
  String get ReadMore => '...Weiterlesen';
  @override
  String get SuccessPostPublish => 'Beitrag wurde erfolgreich veröffentlicht';
  @override
  String get SuccessPostEdited => 'Beitrag wurde erfolgreich bearbeitet';
  @override
  String get TypeComment => 'Geben Sie einen Kommentar ein...';
  @override
  String get TypeReply => 'Geben Sie eine Antwort ein...';
  @override
  String get NotAllowedToPublish =>
      'Entschuldigung! Sie dürfen nicht mehr veröffentlichen';
  @override
  String get NotAllowedToComment =>
      'Entschuldigung! Sie dürfen nicht kommentieren';
  @override
  String get PostRemoveConfirm =>
      'Sind Sie sicher, dass Sie diesen Beitrag löschen möchten?';
  @override
  String get CommentRemoveConfirm =>
      'Sind Sie sicher, dass Sie diesen Kommentar löschen möchten?';
  @override
  String get AddSomeContent => 'Bitte fügen Sie vor dem Posten Inhalte hinzu';
  @override
  String get Reply => 'Antwort';
  @override
  String get Replies => 'Antworten';
  @override
  String get RepliedToComment => 'hat auf deinen Kommentar geantwortet';
  @override
  String get Chats => 'Chats';
  @override
  String get OnlineUsers => 'ONLINE BENUTZER';
  @override
  String get RecentChats => 'LETZTE CHATS';
  @override
  String get RemoveConversation => 'Gespräch entfernen';
  @override
  String get Messaging => 'Nachricht...';
  @override
  String get CannotChatWithUser =>
      'Entschuldigung! Sie können mit diesem Benutzer nicht chatten';
  @override
  String get MsgDeleteConfirm =>
      'Sind Sie sicher, dass Sie die Nachricht löschen möchten?';
  @override
  String get NoChatFound =>
      'Noch kein Chat mit den neuesten Nachrichten, beginnen Sie mit dem Chat!';
  @override
  String get Online => 'Online';
  @override
  String get Typing => 'Schreiben...';
  @override
  String get Image => 'Bild';
  @override
  String get Voice => 'Stimme';
  @override
  String get Video => 'Video';
  @override
  String get Emoji => 'Emoji';
  @override
  String get GIF => 'GIF';
  @override
  String get Sticker => 'Aufkleber';
  @override
  String get Groups => 'Gruppen';
  @override
  String get CreateGroup => 'Erstellen Sie eine Gruppe';
  @override
  String get EditGroup => 'Gruppe bearbeiten';
  @override
  String get Members => 'Mitglieder';
  @override
  String get PhotosLibrary => 'Fotobibliothek';
  @override
  String get TakePicture => 'Ein Bild machen';
  @override
  String get VideosLibrary => 'Videobibliothek';
  @override
  String get RecordVideo => 'Ein Video aufnehmen';
  @override
  String get Cancel => 'Absagen';
  @override
  String get SlideToCancel => 'Zum Abbrechen schieben';
  @override
  String get On => 'Auf';
  @override
  String get Off => 'Aus';
  @override
  String get Group => 'Gruppe';
  @override
  String get LeaveGroup => 'Gruppe verlassen';
  @override
  String get GroupName => 'Gruppenname';
  @override
  String get Join => 'Beitreten';
  @override
  String get Joined => 'Trat bei';
  @override
  String get Public => 'Öffentlich';
  @override
  String get Private => 'Privat';
  @override
  String get GroupType => 'Gruppentyp';
  @override
  String get RemoveMember => 'Mitglied entfernen';
  @override
  String get AddMembers => 'Fügen Sie Mitglieder hinzu';
  @override
  String get Block => 'Block';
  @override
  String get Unblock => 'Blockierung aufheben';
  @override
  String get Ban => 'Verbot';
  @override
  String get Unban => 'Bann aufheben';
  @override
  String get Banned => 'Verboten';
  @override
  String get Newest => 'Neueste';
  @override
  String get Trending => 'Trend';
  @override
  String get MostDownloaded => 'Am meisten heruntergeladen';
  @override
  String get Categories => 'Kategorien';
  @override
  String get AddWallpaper => 'Hintergrundbild hinzufügen';
  @override
  String get WallpaperName => 'Hintergrundname';
  @override
  String get Category => 'Kategorie';
  @override
  String get Upload => 'Hochladen';
  @override
  String get Home => 'Heim';
  @override
  String get Lock => 'Sperren';
  @override
  String get Both => 'Beide';
  @override
  String get SetAsWallpaper => 'Als Hintergrundbild festlegen';
  @override
  String get WallpaperSet => 'Wallpaper erfolgreich eingestellt';
  @override
  String get FailedToUpload =>
      'Hoppla! Hochladen fehlgeschlagen, bitte versuchen Sie es erneut';
  @override
  String get UploadFinished => 'Hochladen abgeschlossen';
  @override
  String get Downloading => 'Wird heruntergeladen';
  @override
  String get NoWallpaperSelectedMsg =>
      'Bitte Hintergrundbild auswählen, um fortzufahren';
  @override
  String get NoImgSelected => 'Kein Bild ausgewählt';
  @override
  String get Notifications => 'Benachrichtigungen';
  @override
  String get StartFollowingMsg => 'Begann dir zu folgen';
  @override
  String get PostReactionMsg => 'auf deinen Beitrag reagiert';
  @override
  String get PostCommentMsg => 'hat deinen Beitrag kommentiert';
  @override
  String get ReplyMsg => 'hat auf deinen Kommentar geantwortet';
  @override
  String get CommentReactedMsg => 'auf Ihren Kommentar reagiert';
  @override
  String get CannotFindFile => 'Hoppla! Kann die Datei nicht finden';
  @override
  String get NoFilePicked =>
      'Versuche hochzuladen und es wurde keine Datei ausgewählt';
  @override
  String get SentYouMsg => 'Ich habe Ihnen eine Nachricht gesendet';
  @override
  String get Report => 'Prüfbericht';
  @override
  String get Unreport => 'Nicht melden';
  @override
  String get ReportDesc => 'Wir entfernen Beiträge mit: ';
  @override
  String get ReportReasons =>
      '⚫️ Sexuelle Inhalte. \n\n⚫️ Gewalttätige oder abstoßende Inhalte. \n\n⚫️ Hasserfüllte oder beleidigende Inhalte. \n\n⚫️ Spam oder irreführend.';
  @override
  String get ReportNote =>
      'Wir werden es ihnen nicht mitteilen, wenn Sie diese Maßnahme ergreifen.';
  @override
  String get ReportThanks =>
      'Wir werden Ihre Anfrage prüfen. Vielen Dank für Ihre Hilfe bei der Verbesserung unserer Community';
  @override
  String get Admin => 'Administrator';
  @override
  String get ProfanityDetected =>
      'Schlimme Wörter entdeckt, Ihr Konto wird möglicherweise gesperrt!';
  @override
  String get AreYouSure => 'Sind Sie sicher';
  @override
  String get ConfirmChatDeletion =>
      'Sind Sie sicher, dass Sie den Chat löschen möchten';
  @override
  String get ReportedPosts => 'Gemeldete Beiträge';
  @override
  String get AllChats => 'Alle Chats';
  @override
  String get Users => 'Benutzer';
  @override
  String get TermsService => 'Nutzungsbedingungen';
  @override
  String get WaitingToRecord => 'Auf Aufnahme warten';
  @override
  String get AdjustVolume => 'Lautstärke anpassen';
  @override
  String get AdjustSpeed => 'Geschwindigkeit anpassen';
  @override
  String get Ok => 'In Ordnung';
  @override
  String get ShareExternally => 'Extern teilen';
  @override
  String get ShareOnYourWall => 'Teil es auf deiner Pinnwand';
  @override
  String get Error => 'Fehler';
  @override
  String get Me => 'Mir';
  @override
  String get OopsCode => 'Ups! Bestätigungscode ist falsch';
  @override
  String get OopsNetwork =>
      'Hoppla! etwas ist schief gelaufen, bitte überprüfen Sie Ihr Netzwerk!';
  @override
  String get OopsEmailorPassword =>
      'Ups! E-Mail oder Passwort ist nicht korrekt!';
  @override
  String get OopsNotRegister =>
      'Hoppla! kein Benutzer gefunden, wenn nicht, erwägen Sie, sich zuerst anzumelden!';
  @override
  String get OopsEmailUsed =>
      'Hoppla! Diese E-Mail wird bereits von einem anderen Konto verwendet!';
  @override
  String get OopsPhoneFormat => 'Hoppla! Telefonformat ist ungültig';
  @override
  String get NoAudioRecorded => 'Kein Audio hinzugefügt';
  @override
  String get OopsWrong => 'Ups! Irgendwas lief schief';
  @override
  String get Optional => 'Optional';
  @override
  String get Sending => 'Senden...';
  @override
  String get NotificationPermission =>
      'Benachrichtigungserlaubnis ist gewährleistet';
  @override
  String get Theme => 'Thema';
  @override
  String get DarkMode => 'Dunkler Modus';
  @override
  String get Others => 'Andere';
  @override
  String get PeopleWhoReacted => 'Menschen, die reagiert haben';
  @override
  String get BadWords =>
      'Schlimme Wörter entdeckt, Ihr Konto wird möglicherweise gesperrt!';
  @override
  String get PickFile => 'Datei auswählen';
  @override
  String get Loading => 'Wird geladen...';
  @override
  String get PleaseAddAudio => 'Bitte Audio hinzufügen';
  @override
  String get PeopleYouMayFollow => 'Menschen, denen Sie folgen können';
  @override
  String get Unsave => 'Nicht speichern';
  @override
  String get PostsLikes => 'Gefällt mir für Beiträge';
  @override
  String get CommentsLikes => 'Gefällt mir Kommentare';
  @override
  String get RepliesLikes => 'Antworten Likes';
  @override
  String get ShareiConfezz => 'iConfezz teilen';
  @override
  String get MemberSince => 'Mitglied seit';
  @override
  String get Comment => 'Kommentar';
  @override
  String get UnShare => 'Freigabe aufheben';
  @override
  String get Liked => 'Gefallen';
  @override
  String get Like => 'Wie';
  @override
  String get SAVED => 'GERETTET';
  @override
  String get SHARED => 'GETEILT';
  @override
  String get AppLanguage => 'App-Sprache';
  @override
  String get ComingSoon => 'Kommt bald';
  @override
  String get ReportProblem => 'Ein Problem melden';
  @override
  String get PushNotifications => 'Mitteilungen';
  @override
  String get ShareProfile => 'Profil teilen';
  @override
  String get DeleteAccount => 'Konto löschen';
  @override
  String get DeleteYourAccount => 'Lösche deinen Account';
  @override
  String get Record => 'Aufzeichnung';
  @override
  String get Or => 'Oder';
  @override
  String get NoPhotoPicked => 'Kein Foto ausgewählt';
  @override
  String get SearchForUsers => 'Benutzer suchen';
  @override
  String get NoResultsFound => 'Keine Ergebnisse gefunden';
  @override
  String get TemporaryFilesRemoved => 'Temporäre Dateien erfolgreich entfernt';
  @override
  String get FailedToClean => 'Fehler beim Bereinigen temporärer Dateien';
  @override
  String get exit => 'Ausfahrt';
  @override
  String get CommunityGuidelines => 'Community-Richtlinien';
  @override
  String get FAQ => 'FAQ';
  @override
  String get Paste => 'Paste';
  @override
  String get DoYouWantToPasteThisCode => 'Do you want to paste this code?';
  @override
  String get PasteCode => 'Paste Code';
}

// Path: <root>
class _StringsEs implements _StringsEn {
  /// You can call this constructor and build your own translation instance of this locale.
  /// Constructing via the enum [AppLocale.build] is preferred.
  _StringsEs.build();

  /// Access flat map
  @override
  dynamic operator [](String key) => _flatMap[key];

  // Internal flat map initialized lazily
  @override
  late final Map<String, dynamic> _flatMap = _buildFlatMap();

  @override
  late final _StringsEs _root = this; // ignore: unused_field

  // Translations
  @override
  String get AppName => 'iConfezz';
  @override
  String get AboutApp =>
      'iConfezz es una comunidad dedicada a escuchar la voz de los demás.';
  @override
  String get AppVersion => '2.0.0';
  @override
  String get UnknownError => 'Algo salió mal';
  @override
  String get Next => 'Próximo';
  @override
  String get EmailUser =>
      '¿Eres un usuario de correo electrónico? ¡Inicia sesión aquí!';
  @override
  String get Email => 'Correo electrónico';
  @override
  String get bio => 'Biografía';
  @override
  String get city => 'Ciudad';
  @override
  String get dob => 'Fecha de cumpleaños';
  @override
  String get Password => 'Clave';
  @override
  String get Login => 'Acceso';
  @override
  String get PhoneNumber => 'Número de teléfono';
  @override
  String get PhoneVerification => 'Verificación del número de teléfono';
  @override
  String get EnterCode => 'Ingrese el código enviado a ';
  @override
  String get DidntRecieveCode => '¿No recibiste el código?';
  @override
  String get InvalidSMSCode => 'El código de verificación de SMS no es válido';
  @override
  String get RESEND => 'REENVIAR';
  @override
  String get VERIFY => 'VERIFICAR';
  @override
  String get POSTS => 'PUBLICACIONES';
  @override
  String get FOLLOWERS => 'Seguidores';
  @override
  String get FOLLOWINGS => 'Seguimientos';
  @override
  String get Follow => 'Seguir';
  @override
  String get Following => 'Siguiente';
  @override
  String get Register => 'Registro';
  @override
  String get Username => 'Nombre de usuario';
  @override
  String get FirstName => 'Primer nombre';
  @override
  String get LastName => 'Apellido';
  @override
  String get FullName => 'Nombre completo';
  @override
  String get Status => 'Estado';
  @override
  String get AddValidUsername => 'Agregar un nombre de usuario válido';
  @override
  String get AddValidFirstName => 'Añadir un nombre válido';
  @override
  String get AddValidLastName => 'Añadir un apellido válido';
  @override
  String get SelectGender => 'Por favor seleccione su género.';
  @override
  String get Male => 'Masculino';
  @override
  String get Female => 'Femenino';
  @override
  String get Other => 'Otro';
  @override
  String get AcceptTerms => 'Acepto todos los términos y condiciones';
  @override
  String get Save => 'Guardar';
  @override
  String get Search => 'Búsqueda';
  @override
  String get Searching => 'Buscando...';
  @override
  String get SelectPhoneCode => 'Seleccione un código de teléfono';
  @override
  String get InvalidEmail => 'Email inválido';
  @override
  String get ShortPassword =>
      'La contraseña debe contener al menos 8 caracteres';
  @override
  String get UsernameTaken => 'Este nombre de usuario ya está tomado';
  @override
  String get InvalidPhone => 'Por favor llene todas las celdas correctamente';
  @override
  String get Message => 'Mensaje';
  @override
  String get LoginFirst => 'Vaya, inicie sesión primero';
  @override
  String get START => 'EMPEZAR';
  @override
  String get Wallpapers => 'Fondos de pantalla';
  @override
  String get Profile => 'Perfil';
  @override
  String get NoInternet => 'Sin acceso a Internet';
  @override
  String get Settings => 'Ajustes';
  @override
  String get Account => 'Cuenta';
  @override
  String get EditProfile => 'Editar perfil';
  @override
  String get OnlineStatus => 'Estado en línea';
  @override
  String get OnlineDescription =>
      'Cualquiera puede ver cuándo estuvo en línea por última vez';
  @override
  String get Logout => 'Cerrar sesión';
  @override
  String get DirectMsgs => 'Mensajes de chat';
  @override
  String get DirectMsgsDescr => 'Recibir notificaciones de chat';
  @override
  String get GroupsMsgs => 'Mensajes de Grupos';
  @override
  String get GroupsMsgsDesc => 'Recibir Notificaciones de todos los Grupos';
  @override
  String get AddGroupName => '¡Por favor! Agregue el nombre del grupo';
  @override
  String get About => 'Sobre nosotros';
  @override
  String get RateUs => 'Nos califica';
  @override
  String get EULA => 'Acuerdo CLUF';
  @override
  String get PrivacyPolicy => 'Política de privacidad';
  @override
  String get Feed => 'Alimentación';
  @override
  String get Post => 'Correo';
  @override
  String get Gallery => 'Galería';
  @override
  String get Stories => 'Cuentos';
  @override
  String get MyPosts => 'Mis publicaciones';
  @override
  String get Favorites => 'Favoritos';
  @override
  String get Announcement => 'Anuncio';
  @override
  String get WhatOnMind => '¿Qué compartir?';
  @override
  String get Likes => 'Gustos';
  @override
  String get Comments => 'Comentarios';
  @override
  String get CreatePost => 'Crear publicación';
  @override
  String get Share => 'Cuota';
  @override
  String get Edit => 'Editar';
  @override
  String get Delete => 'Borrar';
  @override
  String get Copy => 'Dupdo';
  @override
  String get Copied => 'Copiado';
  @override
  String get ReadMore => '...Lee mas';
  @override
  String get SuccessPostPublish => 'La publicación ha sido publicada con éxito';
  @override
  String get SuccessPostEdited => 'La publicación ha sido editada con éxito';
  @override
  String get TypeComment => 'Escribe un comentario...';
  @override
  String get TypeReply => 'Escribe una respuesta...';
  @override
  String get NotAllowedToPublish =>
      '¡Lo siento! Ya no tienes permiso para publicar';
  @override
  String get NotAllowedToComment => '¡Lo siento! No tienes permitido comentar';
  @override
  String get PostRemoveConfirm =>
      '¿Estás seguro de que quieres eliminar esta publicación?';
  @override
  String get CommentRemoveConfirm =>
      '¿Está seguro de que desea eliminar este comentario?';
  @override
  String get AddSomeContent => 'Agregue contenido antes de publicar';
  @override
  String get Reply => 'Respuesta';
  @override
  String get Replies => 'Respuestas';
  @override
  String get RepliedToComment => 'respondí a tu comentario';
  @override
  String get Chats => 'Chats';
  @override
  String get OnlineUsers => 'USUARIOS EN LÍNEA';
  @override
  String get RecentChats => 'CHATS RECIENTES';
  @override
  String get RemoveConversation => 'Eliminar conversación';
  @override
  String get Messaging => 'Mensaje...';
  @override
  String get CannotChatWithUser =>
      '¡Lo siento! No puedes chatear con este usuario';
  @override
  String get MsgDeleteConfirm =>
      '¿Está seguro de que desea eliminar el mensaje?';
  @override
  String get NoChatFound =>
      'Todavía no hay chat de Recientes, ¡empieza a chatear!';
  @override
  String get Online => 'En línea';
  @override
  String get Typing => 'Mecanografía...';
  @override
  String get Image => 'Imagen';
  @override
  String get Voice => 'Voz';
  @override
  String get Video => 'Video';
  @override
  String get Emoji => 'Emoji';
  @override
  String get GIF => 'GIF';
  @override
  String get Sticker => 'Pegatina';
  @override
  String get Groups => 'Grupos';
  @override
  String get CreateGroup => 'Crear un grupo';
  @override
  String get EditGroup => 'Edita tu Grupo';
  @override
  String get Members => 'Miembros';
  @override
  String get PhotosLibrary => 'Biblioteca de Fotos';
  @override
  String get TakePicture => 'Tomar la foto';
  @override
  String get VideosLibrary => 'Biblioteca de videos';
  @override
  String get RecordVideo => 'Grabar video';
  @override
  String get Cancel => 'Cancelar';
  @override
  String get SlideToCancel => 'Deslizar para cancelar';
  @override
  String get On => 'Sobre';
  @override
  String get Off => 'Apagado';
  @override
  String get Group => 'Grupo';
  @override
  String get LeaveGroup => 'Abandonar grupo';
  @override
  String get GroupName => 'Nombre del grupo';
  @override
  String get Join => 'Entrar';
  @override
  String get Joined => 'Unido';
  @override
  String get Public => 'Público';
  @override
  String get Private => 'Privado';
  @override
  String get GroupType => 'Tipo de grupo';
  @override
  String get RemoveMember => 'Eliminar miembro';
  @override
  String get AddMembers => 'Añadir miembros';
  @override
  String get Block => 'Cuadra';
  @override
  String get Unblock => 'Desatascar';
  @override
  String get Ban => 'Prohibición';
  @override
  String get Unban => 'Desbloquear';
  @override
  String get Banned => 'Prohibido';
  @override
  String get Newest => 'El más nuevo';
  @override
  String get Trending => 'Tendencias';
  @override
  String get MostDownloaded => 'Más descargados';
  @override
  String get Categories => 'Categorías';
  @override
  String get AddWallpaper => 'Agregar fondo de pantalla';
  @override
  String get WallpaperName => 'Nombre del fondo de pantalla';
  @override
  String get Category => 'Categoría';
  @override
  String get Upload => 'Subir';
  @override
  String get Home => 'Casa';
  @override
  String get Lock => 'Cerrar con llave';
  @override
  String get Both => 'Ambas cosas';
  @override
  String get SetAsWallpaper => 'Establecer como fondo de pantalla';
  @override
  String get WallpaperSet => 'Fondo de pantalla establecido con éxito';
  @override
  String get FailedToUpload => '¡Vaya! Carga fallida, inténtalo de nuevo';
  @override
  String get UploadFinished => 'Carga Finalizada';
  @override
  String get Downloading => 'Descargando';
  @override
  String get NoWallpaperSelectedMsg =>
      'Seleccione el fondo de pantalla para continuar';
  @override
  String get NoImgSelected => 'Ninguna imagen seleccionada';
  @override
  String get Notifications => 'Notificaciones';
  @override
  String get StartFollowingMsg => 'empecé a seguirte';
  @override
  String get PostReactionMsg => 'reaccioné a tu publicación';
  @override
  String get PostCommentMsg => 'comenté tu publicación';
  @override
  String get ReplyMsg => 'respondí a tu comentario';
  @override
  String get CommentReactedMsg => 'reaccioné a tu comentario';
  @override
  String get CannotFindFile => '¡Vaya! No se puede encontrar el archivo';
  @override
  String get NoFilePicked =>
      'Intentando cargar y no se selecciona ningún archivo';
  @override
  String get SentYouMsg => 'Te envié un mensaje';
  @override
  String get Report => 'Reporte';
  @override
  String get Unreport => 'No denunciar';
  @override
  String get ReportDesc => 'Eliminamos la publicación que tiene: ';
  @override
  String get ReportReasons =>
      '⚫️ Contenido sexual. \n\n⚫️ Contenido violento o repulsivo. \n\n⚫️ Contenido odioso o abusivo. \n\n⚫️ Spam o engañoso.';
  @override
  String get ReportNote => 'No les haremos saber si tomas esta acción.';
  @override
  String get ReportThanks =>
      'Revisaremos su solicitud, gracias por ayudar a mejorar nuestra comunidad';
  @override
  String get Admin => 'Administración';
  @override
  String get ProfanityDetected =>
      '¡Se detectaron malas palabras, su cuenta puede ser suspendida!';
  @override
  String get AreYouSure => '¿Estás seguro de?';
  @override
  String get ConfirmChatDeletion => '¿Estás seguro de eliminar el chat?';
  @override
  String get ReportedPosts => 'Publicaciones reportadas';
  @override
  String get AllChats => 'Todos los chats';
  @override
  String get Users => 'Usuarios';
  @override
  String get TermsService => 'Términos de servicio';
  @override
  String get WaitingToRecord => 'Esperando para grabar';
  @override
  String get AdjustVolume => 'Ajusta el volúmen';
  @override
  String get AdjustSpeed => 'Ajustar velocidad';
  @override
  String get Ok => 'De acuerdo';
  @override
  String get ShareExternally => 'Compartir externamente';
  @override
  String get ShareOnYourWall => 'Compartir en tu muro';
  @override
  String get Error => 'Error';
  @override
  String get Me => 'Me';
  @override
  String get OopsCode => '¡Ups! El código de verificación es incorrecto';
  @override
  String get OopsNetwork => '¡Vaya! Algo salió mal. ¡Por favor revise su red!';
  @override
  String get OopsEmailorPassword =>
      '¡Vaya! ¡El correo electrónico o la contraseña no son correctos!';
  @override
  String get OopsNotRegister =>
      '¡Vaya! No se encontró ningún usuario, si no se registra, ¡considere registrarse primero!';
  @override
  String get OopsEmailUsed =>
      '¡Vaya! ¡Otra cuenta ya usa este correo electrónico!';
  @override
  String get OopsPhoneFormat => '¡Vaya! El formato del teléfono no es válido';
  @override
  String get NoAudioRecorded => 'Sin audio agregado';
  @override
  String get OopsWrong => 'Huy! Algo salió mal';
  @override
  String get Optional => 'Opcional';
  @override
  String get Sending => 'Enviando... ';
  @override
  String get NotificationPermission =>
      'El permiso de notificación está garantizado';
  @override
  String get Theme => 'Temática';
  @override
  String get DarkMode => 'Modo oscuro';
  @override
  String get Others => 'Otros';
  @override
  String get PeopleWhoReacted => 'Gente que reaccionó';
  @override
  String get BadWords =>
      '¡Se detectaron malas palabras, su cuenta puede ser suspendida!';
  @override
  String get PickFile => 'Seleccionar archivo';
  @override
  String get Loading => 'Cargando...';
  @override
  String get PleaseAddAudio => 'Por favor agregue audio';
  @override
  String get PeopleYouMayFollow => 'Personas a las que puedes seguir';
  @override
  String get Unsave => 'No guardar';
  @override
  String get PostsLikes => 'Me gusta de las publicaciones';
  @override
  String get CommentsLikes => 'Me gusta comentarios';
  @override
  String get RepliesLikes => 'Me gusta de respuestas';
  @override
  String get ShareiConfezz => 'Compartir iConfezz';
  @override
  String get MemberSince => 'Miembro desde';
  @override
  String get Comment => 'Comentario';
  @override
  String get UnShare => 'Dejar de compartir';
  @override
  String get Liked => 'Gustó';
  @override
  String get Like => 'Me gusta';
  @override
  String get SAVED => 'SALVADO';
  @override
  String get SHARED => 'COMPARTIDO';
  @override
  String get AppLanguage => 'Idioma de la aplicación';
  @override
  String get ComingSoon => 'Próximamente';
  @override
  String get ReportProblem => 'Informar de un problema';
  @override
  String get PushNotifications => 'Notificaciones push';
  @override
  String get ShareProfile => 'Compartir perfil';
  @override
  String get DeleteAccount => 'Borrar cuenta';
  @override
  String get DeleteYourAccount => 'Eliminar su cuenta';
  @override
  String get Record => 'Registro';
  @override
  String get Or => 'O';
  @override
  String get NoPhotoPicked => 'Sin foto seleccionada';
  @override
  String get SearchForUsers => 'Buscar Usuarios';
  @override
  String get NoResultsFound => 'No se han encontrado resultados';
  @override
  String get TemporaryFilesRemoved =>
      'Archivos temporales eliminados con éxito';
  @override
  String get FailedToClean => 'Error al limpiar archivos temporales';
  @override
  String get exit => 'salida';
  @override
  String get CommunityGuidelines => 'Principios de la Comunidad';
  @override
  String get FAQ => 'PREGUNTAS MÁS FRECUENTES';
  @override
  String get Paste => 'Paste';
  @override
  String get DoYouWantToPasteThisCode => 'Do you want to paste this code?';
  @override
  String get PasteCode => 'Paste Code';
}

// Path: <root>
class _StringsFa implements _StringsEn {
  /// You can call this constructor and build your own translation instance of this locale.
  /// Constructing via the enum [AppLocale.build] is preferred.
  _StringsFa.build();

  /// Access flat map
  @override
  dynamic operator [](String key) => _flatMap[key];

  // Internal flat map initialized lazily
  @override
  late final Map<String, dynamic> _flatMap = _buildFlatMap();

  @override
  late final _StringsFa _root = this; // ignore: unused_field

  // Translations
  @override
  String get AppName => 'iConfezz';
  @override
  String get AboutApp =>
      'iConfezz انجمنی است که برای شنیدن صدای دیگران اختصاص داده شده است.';
  @override
  String get AppVersion => '2.0.0';
  @override
  String get UnknownError => 'چیزی اشتباه شد';
  @override
  String get Next => 'بعد';
  @override
  String get EmailUser => 'آیا شما کاربر ایمیل هستید, اینجا وارد شوید!';
  @override
  String get Email => 'پست الکترونیک';
  @override
  String get bio => 'زندگینامه';
  @override
  String get city => 'شهر';
  @override
  String get dob => 'تاریخ تولد';
  @override
  String get Password => 'کلمه عبور';
  @override
  String get Login => 'وارد شدن';
  @override
  String get PhoneNumber => 'شماره تلفن';
  @override
  String get PhoneVerification => 'تأیید شماره تلفن';
  @override
  String get EnterCode => 'کد ارسال شده را وارد کنید';
  @override
  String get DidntRecieveCode => 'کد را دریافت نکردید؟';
  @override
  String get InvalidSMSCode => 'کد تایید پیامک نامعتبر است';
  @override
  String get RESEND => 'ارسال مجدد';
  @override
  String get VERIFY => 'تأیید';
  @override
  String get POSTS => 'نوشته ها';
  @override
  String get FOLLOWERS => 'پیروان';
  @override
  String get FOLLOWINGS => 'پیروان';
  @override
  String get Follow => 'دنبال کردن';
  @override
  String get Following => 'ذیل';
  @override
  String get Register => 'ثبت نام';
  @override
  String get Username => 'نام کاربری';
  @override
  String get FirstName => 'نام کوچک';
  @override
  String get LastName => 'نام خانوادگی';
  @override
  String get FullName => 'نام و نام خانوادگی';
  @override
  String get Status => 'وضعیت';
  @override
  String get AddValidUsername => 'یک نام کاربری معتبر اضافه کنید';
  @override
  String get AddValidFirstName => 'یک نام معتبر اضافه کنید';
  @override
  String get AddValidLastName => 'یک نام خانوادگی معتبر اضافه کنید';
  @override
  String get SelectGender => 'لطفا جنسیت خود را انتخاب کنید.';
  @override
  String get Male => 'نر';
  @override
  String get Female => 'مونث';
  @override
  String get Other => 'دیگر';
  @override
  String get AcceptTerms => 'من همه شرایط و ضوابط را می پذیرم';
  @override
  String get Save => 'صرفه جویی';
  @override
  String get Search => 'جستجو کردن';
  @override
  String get Searching => 'جستجوکردن...';
  @override
  String get SelectPhoneCode => 'انتخاب یک کد تلفن';
  @override
  String get InvalidEmail => 'ایمیل نامعتبر';
  @override
  String get ShortPassword => 'رمز عبور باید حداقل 8 کاراکتر داشته باشد';
  @override
  String get UsernameTaken => 'نام کاربری قبلاً گرفته شده است';
  @override
  String get InvalidPhone => 'لطفاً تمام سلول ها را به درستی پر کنید';
  @override
  String get Message => 'پیام';
  @override
  String get LoginFirst => 'اوه, لطفا ابتدا وارد شوید';
  @override
  String get START => 'شروع کنید';
  @override
  String get Wallpapers => 'تصاویر پس زمینه';
  @override
  String get Profile => 'مشخصات';
  @override
  String get NoInternet => 'بدون دسترسی به اینترنت';
  @override
  String get Settings => 'تنظیمات';
  @override
  String get Account => 'حساب';
  @override
  String get EditProfile => 'ویرایش نمایه';
  @override
  String get OnlineStatus => 'وضعیت آنلاین';
  @override
  String get OnlineDescription =>
      'همه می توانند ببینند آخرین بار چه زمانی آنلاین هستید';
  @override
  String get Logout => 'خروج';
  @override
  String get DirectMsgs => 'پیام های چت';
  @override
  String get DirectMsgsDescr => 'دریافت اعلان های چت';
  @override
  String get GroupsMsgs => 'پیام های گروهی';
  @override
  String get GroupsMsgsDesc => 'دریافت اعلان های همه گروه ها';
  @override
  String get AddGroupName => 'لطفا! نام گروه را اضافه کنید';
  @override
  String get About => 'درباره ما';
  @override
  String get RateUs => 'به ما رای دهید';
  @override
  String get EULA => 'توافقنامه EULA';
  @override
  String get PrivacyPolicy => 'سیاست حفظ حریم خصوصی';
  @override
  String get Feed => 'خوراک';
  @override
  String get Post => 'پست';
  @override
  String get Gallery => 'آلبوم عکس';
  @override
  String get Stories => 'داستان ها';
  @override
  String get MyPosts => 'پست های من';
  @override
  String get Favorites => 'موارد دلخواه';
  @override
  String get Announcement => 'اطلاعیه';
  @override
  String get WhatOnMind => 'چه چیزی را به اشتراک بگذاریم؟';
  @override
  String get Likes => 'دوست دارد';
  @override
  String get Comments => 'نظرات';
  @override
  String get CreatePost => 'ایجاد پست';
  @override
  String get Share => 'اشتراک گذاری';
  @override
  String get Edit => 'ویرایش';
  @override
  String get Delete => 'حذف';
  @override
  String get Copy => 'کپی 🀄';
  @override
  String get Copied => 'کپی شده';
  @override
  String get ReadMore => '...ادامه مطلب';
  @override
  String get SuccessPostPublish => 'پست با موفقیت منتشر شد';
  @override
  String get SuccessPostEdited => 'پست با موفقیت ویرایش شد';
  @override
  String get TypeComment => 'یک نظر تایپ کنید...';
  @override
  String get TypeReply => 'پاسخ را تایپ کنید...';
  @override
  String get NotAllowedToPublish => 'با عرض پوزش! شما دیگر اجازه انتشار ندارید';
  @override
  String get NotAllowedToComment => 'با عرض پوزش! شما مجاز به نظر دادن نیستید';
  @override
  String get PostRemoveConfirm =>
      'آیا مطمئن هستید که می خواهید این پست را حذف کنید؟';
  @override
  String get CommentRemoveConfirm =>
      'آیا مطمئن هستید که می خواهید این نظر را حذف کنید؟';
  @override
  String get AddSomeContent => 'لطفا قبل از ارسال مقداری محتوا اضافه کنید';
  @override
  String get Reply => 'پاسخ';
  @override
  String get Replies => 'پاسخ ها';
  @override
  String get RepliedToComment => 'به نظر شما پاسخ دادم';
  @override
  String get Chats => 'چت ها';
  @override
  String get OnlineUsers => 'کاربران آنلاین';
  @override
  String get RecentChats => 'چت های اخیر';
  @override
  String get RemoveConversation => 'حذف مکالمه';
  @override
  String get Messaging => 'پیام...';
  @override
  String get CannotChatWithUser =>
      'متأسفیم! شما نمی توانید با این کاربر چت کنید';
  @override
  String get MsgDeleteConfirm =>
      'آیا مطمئن هستید که می خواهید پیام را حذف کنید؟';
  @override
  String get NoChatFound => 'هنوز گفتگوی اخیر وجود ندارد, چت را شروع کنید!';
  @override
  String get Online => 'برخط';
  @override
  String get Typing => 'تایپ کردن...';
  @override
  String get Image => 'تصویر';
  @override
  String get Voice => 'صدا';
  @override
  String get Video => 'ویدئو';
  @override
  String get Emoji => 'اموجی';
  @override
  String get GIF => 'GIF';
  @override
  String get Sticker => 'استیکر';
  @override
  String get Groups => 'گروه ها';
  @override
  String get CreateGroup => 'ایجاد یک گروه';
  @override
  String get EditGroup => 'گروه خود را ویرایش کنید';
  @override
  String get Members => 'اعضا';
  @override
  String get PhotosLibrary => 'کتابخانه عکس';
  @override
  String get TakePicture => 'عکس بگیر';
  @override
  String get VideosLibrary => 'کتابخانه فیلم';
  @override
  String get RecordVideo => 'ضبط تصویر';
  @override
  String get Cancel => 'لغو';
  @override
  String get SlideToCancel => 'برای لغو اسلاید کنید';
  @override
  String get On => 'بر';
  @override
  String get Off => 'خاموش';
  @override
  String get Group => 'گروه';
  @override
  String get LeaveGroup => 'گروه را ترک کن';
  @override
  String get GroupName => 'اسم گروه';
  @override
  String get Join => 'پیوستن';
  @override
  String get Joined => 'پیوستن';
  @override
  String get Public => 'عمومی';
  @override
  String get Private => 'خصوصی';
  @override
  String get GroupType => 'نوع گروه';
  @override
  String get RemoveMember => 'حذف عضو';
  @override
  String get AddMembers => 'افزودن اعضا';
  @override
  String get Block => 'مسدود کردن';
  @override
  String get Unblock => 'لغو انسداد';
  @override
  String get Ban => 'ممنوع کردن';
  @override
  String get Unban => 'لغو ممنوع';
  @override
  String get Banned => 'ممنوع';
  @override
  String get Newest => 'جدیدترین';
  @override
  String get Trending => 'روند';
  @override
  String get MostDownloaded => 'بیشترین میزان دانلود شده';
  @override
  String get Categories => 'دسته بندی ها';
  @override
  String get AddWallpaper => 'افزودن تصویر زمینه';
  @override
  String get WallpaperName => 'نام کاغذ دیواری';
  @override
  String get Category => 'دسته بندی';
  @override
  String get Upload => 'بارگذاری';
  @override
  String get Home => 'خانه';
  @override
  String get Lock => 'قفل کردن';
  @override
  String get Both => 'هر دو';
  @override
  String get SetAsWallpaper => 'تنظیم به عنوان تصویر زمینه';
  @override
  String get WallpaperSet => 'کاغذ دیواری با موفقیت تنظیم شد';
  @override
  String get FailedToUpload => 'اوه! آپلود انجام نشد, لطفا دوباره امتحان کنید';
  @override
  String get UploadFinished => 'آپلود به پایان رسید';
  @override
  String get Downloading => 'دانلود';
  @override
  String get NoWallpaperSelectedMsg =>
      'لطفا برای ادامه تصویر زمینه را انتخاب کنید';
  @override
  String get NoImgSelected => 'هیچ تصویری انتخاب نشده است';
  @override
  String get Notifications => 'اطلاعیه';
  @override
  String get StartFollowingMsg => 'شروع به تعقیب تو کرد';
  @override
  String get PostReactionMsg => 'به پست شما واکنش نشان داد';
  @override
  String get PostCommentMsg => 'پست خود را کامنت کرد';
  @override
  String get ReplyMsg => 'به نظر شما پاسخ دادم';
  @override
  String get CommentReactedMsg => 'به نظر شما واکنش نشان داد';
  @override
  String get CannotFindFile => 'اوه! فایل را نمی توان یافت';
  @override
  String get NoFilePicked =>
      'در حال تلاش برای آپلود و هیچ فایلی انتخاب نشده است';
  @override
  String get SentYouMsg => 'پیام برای شما ارسال شد';
  @override
  String get Report => 'گزارش';
  @override
  String get Unreport => 'عدم گزارش';
  @override
  String get ReportDesc => 'ما پستی را حذف می کنیم که دارای: ';
  @override
  String get ReportReasons =>
      '⚫️ محتوای جنسی. \n\n⚫️ محتوای خشونت آمیز یا زننده. \n\n⚫️ محتوای نفرت انگیز یا توهین آمیز. \n\n⚫️ هرزنامه یا گمراه کننده.';
  @override
  String get ReportNote =>
      'اگر شما این اقدام را انجام دهید به آنها اطلاع نمی دهیم.';
  @override
  String get ReportThanks =>
      'ما درخواست شما را بررسی خواهیم کرد, برای کمک به بهبود جامعه ما متشکریم';
  @override
  String get Admin => 'مدیر';
  @override
  String get ProfanityDetected =>
      'کلمات بد شناسایی شد, حساب شما ممکن است به حالت تعلیق درآید!';
  @override
  String get AreYouSure => 'آیا مطمئنی';
  @override
  String get ConfirmChatDeletion => 'آیا مطمئن هستید که چت را حذف می کنید';
  @override
  String get ReportedPosts => 'پست های گزارش شده';
  @override
  String get AllChats => 'همه چت ها';
  @override
  String get Users => 'کاربران';
  @override
  String get TermsService => 'شرایط استفاده از خدمات';
  @override
  String get WaitingToRecord => 'در انتظار ضبط';
  @override
  String get AdjustVolume => 'تنظیم صدا';
  @override
  String get AdjustSpeed => 'تنظیم سرعت';
  @override
  String get Ok => 'خوب';
  @override
  String get ShareExternally => 'به اشتراک گذاری خارجی';
  @override
  String get ShareOnYourWall => 'روی دیوار خود به اشتراک بگذارید';
  @override
  String get Error => 'خطا';
  @override
  String get Me => 'من';
  @override
  String get OopsCode => 'اوه! کد تایید اشتباه است';
  @override
  String get OopsNetwork => 'اوه! مشکلی پیش آمد, لطفاً شبکه خود را بررسی کنید!';
  @override
  String get OopsEmailorPassword => 'اوه! ایمیل یا رمز عبور صحیح نیست!';
  @override
  String get OopsNotRegister =>
      'اوه! هیچ کاربری پیدا نشد, اگر ثبت نام نکردید ابتدا ثبت نام کنید!';
  @override
  String get OopsEmailUsed =>
      'اوه! این ایمیل قبلا توسط حساب دیگری استفاده شده است!';
  @override
  String get OopsPhoneFormat => 'اوه! قالب تلفن نامعتبر است';
  @override
  String get NoAudioRecorded => 'صوتی اضافه نشده است';
  @override
  String get OopsWrong => 'اوه, مشکلی پیش آمد';
  @override
  String get Optional => 'اختیاری';
  @override
  String get Sending => 'در حال ارسال... ';
  @override
  String get NotificationPermission => 'اجازه اطلاع رسانی تضمین شده است';
  @override
  String get Theme => 'موضوع';
  @override
  String get DarkMode => 'حالت تاریک';
  @override
  String get Others => 'دیگران';
  @override
  String get PeopleWhoReacted => 'افرادی که واکنش نشان دادند';
  @override
  String get BadWords =>
      'کلمات بد شناسایی شد, حساب شما ممکن است به حالت تعلیق درآید!';
  @override
  String get PickFile => 'انتخاب فایل';
  @override
  String get Loading => 'بارگذاری...';
  @override
  String get PleaseAddAudio => 'لطفاً صدا را اضافه کنید';
  @override
  String get PeopleYouMayFollow => 'افرادی که ممکن است آنها را دنبال کنید';
  @override
  String get Unsave => 'غیر ذخیره';
  @override
  String get PostsLikes => 'پسند پست ها';
  @override
  String get CommentsLikes => 'نظرات لایک';
  @override
  String get RepliesLikes => 'پاسخ ها لایک';
  @override
  String get ShareiConfezz => 'iConfezz را به اشتراک بگذارید';
  @override
  String get MemberSince => 'عضو از زمان';
  @override
  String get Comment => 'اظهار نظر';
  @override
  String get UnShare => 'لغو اشتراک';
  @override
  String get Liked => 'دوست داشت';
  @override
  String get Like => 'پسندیدن';
  @override
  String get SAVED => 'ذخیره';
  @override
  String get SHARED => 'به اشتراک گذاشته شده';
  @override
  String get AppLanguage => 'زبان برنامه';
  @override
  String get ComingSoon => 'به زودی';
  @override
  String get ReportProblem => 'گزارش یک مشکل';
  @override
  String get PushNotifications => 'اعلان های فشاری';
  @override
  String get ShareProfile => 'اشتراک گذاری نمایه';
  @override
  String get DeleteAccount => 'حذف حساب کاربری';
  @override
  String get DeleteYourAccount => 'حذف حساب خود';
  @override
  String get Record => 'رکورد';
  @override
  String get Or => 'یا';
  @override
  String get NoPhotoPicked => 'عکسی انتخاب نشده است';
  @override
  String get SearchForUsers => 'جستجوی کاربران';
  @override
  String get NoResultsFound => 'نتیجه ای پیدا نشد';
  @override
  String get TemporaryFilesRemoved => 'فایل های موقت با موفقیت حذف شدند';
  @override
  String get FailedToClean => 'فایل های موقت پاک نشد';
  @override
  String get exit => 'خروج';
  @override
  String get CommunityGuidelines => 'دستورالعمل های جامعه';
  @override
  String get FAQ => 'سؤالات متداول';
  @override
  String get Paste => 'Paste';
  @override
  String get DoYouWantToPasteThisCode => 'Do you want to paste this code?';
  @override
  String get PasteCode => 'Paste Code';
}

// Path: <root>
class _StringsFr implements _StringsEn {
  /// You can call this constructor and build your own translation instance of this locale.
  /// Constructing via the enum [AppLocale.build] is preferred.
  _StringsFr.build();

  /// Access flat map
  @override
  dynamic operator [](String key) => _flatMap[key];

  // Internal flat map initialized lazily
  @override
  late final Map<String, dynamic> _flatMap = _buildFlatMap();

  @override
  late final _StringsFr _root = this; // ignore: unused_field

  // Translations
  @override
  String get AppName => 'iConfezz';
  @override
  String get AboutApp =>
      'iConfezz est une communauté dédiée à entendre la voix des autres.';
  @override
  String get AppVersion => '2.0.0';
  @override
  String get UnknownError => 'Quelque chose s\'est mal passé';
  @override
  String get Next => 'Suivant';
  @override
  String get EmailUser =>
      'Êtes-vous un utilisateur de messagerie, connectez-vous ici !';
  @override
  String get Email => 'E-mail';
  @override
  String get bio => 'Biographie';
  @override
  String get city => 'Ville';
  @override
  String get dob => 'Date de naissance';
  @override
  String get Password => 'Mot de passe';
  @override
  String get Login => 'Connexion';
  @override
  String get PhoneNumber => 'Numéro de téléphone';
  @override
  String get PhoneVerification => 'Vérification du numéro de téléphone';
  @override
  String get EnterCode => 'Entrez le code envoyé à';
  @override
  String get DidntRecieveCode => 'Vous n\'avez pas reçu le code ?';
  @override
  String get InvalidSMSCode => 'Le code de vérification par SMS est invalide';
  @override
  String get RESEND => 'RENVOYER';
  @override
  String get VERIFY => 'VÉRIFIER';
  @override
  String get POSTS => 'DES POSTES';
  @override
  String get FOLLOWERS => 'Suiveurs';
  @override
  String get FOLLOWINGS => 'Suivis';
  @override
  String get Follow => 'Suivre';
  @override
  String get Following => 'Suivant';
  @override
  String get Register => 'S\'inscrire';
  @override
  String get Username => 'Nom d\'utilisateur';
  @override
  String get FirstName => 'Prénom';
  @override
  String get LastName => 'Nom de famille';
  @override
  String get FullName => 'Nom complet';
  @override
  String get Status => 'Statut';
  @override
  String get AddValidUsername => 'Ajouter un nom d\'utilisateur valide';
  @override
  String get AddValidFirstName => 'Ajouter un prénom valide';
  @override
  String get AddValidLastName => 'Ajouter un nom de famille valide';
  @override
  String get SelectGender => 'S\'il vous plait selectionnez votre genre.';
  @override
  String get Male => 'Homme';
  @override
  String get Female => 'Femelle';
  @override
  String get Other => 'Autre';
  @override
  String get AcceptTerms => 'J\'accepte tous les termes et conditions';
  @override
  String get Save => 'Sauver';
  @override
  String get Search => 'Recherche';
  @override
  String get Searching => 'Recherche...';
  @override
  String get SelectPhoneCode => 'Sélectionner un code téléphone';
  @override
  String get InvalidEmail => 'Email invalide';
  @override
  String get ShortPassword =>
      'Le mot de passe doit contenir au moins 8 caractères';
  @override
  String get UsernameTaken => 'Nom d\'utilisateur déjà pris';
  @override
  String get InvalidPhone =>
      'Veuillez remplir correctement toutes les cellules';
  @override
  String get Message => 'Message';
  @override
  String get LoginFirst => 'Oups, veuillez d\'abord vous connecter';
  @override
  String get START => 'COMMENCER';
  @override
  String get Wallpapers => 'Fonds d\'écran';
  @override
  String get Profile => 'Profil';
  @override
  String get NoInternet => 'Pas d\'accès Internet';
  @override
  String get Settings => 'Réglages';
  @override
  String get Account => 'Compte';
  @override
  String get EditProfile => 'Editer le profil';
  @override
  String get OnlineStatus => 'Statut en ligne';
  @override
  String get OnlineDescription =>
      'Tout le monde peut voir quand vous êtes en ligne pour la dernière fois';
  @override
  String get Logout => 'Se déconnecter';
  @override
  String get DirectMsgs => 'Chat Messages';
  @override
  String get DirectMsgsDescr => 'Recevoir des notifications de chat';
  @override
  String get GroupsMsgs => ' Messages de groupes ';
  @override
  String get GroupsMsgsDesc => 'Recevoir toutes les notifications de groupes';
  @override
  String get AddGroupName => 'S\'il vous plaît ! Ajoutez le nom du groupe';
  @override
  String get About => 'à propos de nous';
  @override
  String get RateUs => 'Évaluez nous';
  @override
  String get EULA => 'Accord CLUF';
  @override
  String get PrivacyPolicy => 'Politique de confidentialité';
  @override
  String get Feed => 'Nourrir';
  @override
  String get Post => 'Poster';
  @override
  String get Gallery => 'Galerie';
  @override
  String get Stories => 'Histoires';
  @override
  String get MyPosts => 'Mes publications';
  @override
  String get Favorites => 'Favoris';
  @override
  String get Announcement => 'Annonce';
  @override
  String get WhatOnMind => 'Quoi partager ?';
  @override
  String get Likes => 'Aime';
  @override
  String get Comments => 'Commentaires';
  @override
  String get CreatePost => 'Créer un article';
  @override
  String get Share => 'Partager';
  @override
  String get Edit => 'Modifier';
  @override
  String get Delete => 'Supprimer';
  @override
  String get Copy => 'Copie';
  @override
  String get Copied => 'Copié';
  @override
  String get ReadMore => '...Lire la suite';
  @override
  String get SuccessPostPublish => 'Le message a été publié avec succès';
  @override
  String get SuccessPostEdited => 'Le message a été modifié avec succès';
  @override
  String get TypeComment => 'Tapez un commentaire...';
  @override
  String get TypeReply => 'Tapez une réponse...';
  @override
  String get NotAllowedToPublish =>
      'Désolé ! Vous n\'êtes plus autorisé à publier';
  @override
  String get NotAllowedToComment =>
      'Désolé ! Vous n\'êtes pas autorisé à commenter';
  @override
  String get PostRemoveConfirm =>
      'Es-tu sur de vouloir supprimer cette annonce?';
  @override
  String get CommentRemoveConfirm =>
      'Êtes-vous sûr de vouloir supprimer ce commentaire?';
  @override
  String get AddSomeContent => 'Veuillez ajouter du contenu avant de publier';
  @override
  String get Reply => 'Réponse';
  @override
  String get Replies => 'Réponses';
  @override
  String get RepliedToComment => 'répondu à votre commentaire';
  @override
  String get Chats => 'Chats';
  @override
  String get OnlineUsers => 'UTILISATEURS EN LIGNE';
  @override
  String get RecentChats => 'CHATS RÉCENTES';
  @override
  String get RemoveConversation => 'Supprimer la conversation';
  @override
  String get Messaging => 'Message...';
  @override
  String get CannotChatWithUser =>
      'Désolé ! Vous ne pouvez pas discuter avec cet utilisateur';
  @override
  String get MsgDeleteConfirm =>
      'Êtes-vous sûr de vouloir supprimer le message ?';
  @override
  String get NoChatFound =>
      'Aucun chat récent pour le moment, commencez à chatter !';
  @override
  String get Online => 'En ligne';
  @override
  String get Typing => 'Dactylographie...';
  @override
  String get Image => 'Image';
  @override
  String get Voice => 'Voix';
  @override
  String get Video => 'Vidéo';
  @override
  String get Emoji => 'Émoji';
  @override
  String get GIF => 'GIF';
  @override
  String get Sticker => 'Autocollant';
  @override
  String get Groups => 'Groupes';
  @override
  String get CreateGroup => 'Créer un groupe';
  @override
  String get EditGroup => 'Modifier votre groupe';
  @override
  String get Members => 'Membres';
  @override
  String get PhotosLibrary => 'Photothèque';
  @override
  String get TakePicture => 'Prendre une photo';
  @override
  String get VideosLibrary => 'Vidéothèque';
  @override
  String get RecordVideo => 'Enregistrer une vidéo';
  @override
  String get Cancel => 'Annuler';
  @override
  String get SlideToCancel => 'Glisser pour annuler';
  @override
  String get On => 'Sur';
  @override
  String get Off => 'Désactivé';
  @override
  String get Group => 'Grouper';
  @override
  String get LeaveGroup => 'Quitter le groupe';
  @override
  String get GroupName => 'Nom de groupe';
  @override
  String get Join => 'Rejoindre';
  @override
  String get Joined => 'Inscrit';
  @override
  String get Public => 'Public';
  @override
  String get Private => 'Privé';
  @override
  String get GroupType => 'Type de groupe';
  @override
  String get RemoveMember => 'Supprimer le membre';
  @override
  String get AddMembers => 'Ajouter des membres';
  @override
  String get Block => 'Bloc';
  @override
  String get Unblock => 'Débloquer';
  @override
  String get Ban => 'Interdire';
  @override
  String get Unban => 'Unban';
  @override
  String get Banned => 'Banni';
  @override
  String get Newest => 'Le plus récent';
  @override
  String get Trending => 'Tendance';
  @override
  String get MostDownloaded => 'Le plus téléchargé';
  @override
  String get Categories => 'Catégories';
  @override
  String get AddWallpaper => 'Ajouter un fond d\'écran';
  @override
  String get WallpaperName => 'Nom du fond d\'écran';
  @override
  String get Category => 'Catégorie';
  @override
  String get Upload => 'Télécharger';
  @override
  String get Home => 'Domicile';
  @override
  String get Lock => 'Fermer à clé';
  @override
  String get Both => 'Tous les deux';
  @override
  String get SetAsWallpaper => 'Définir en tant que fond d\'écran';
  @override
  String get WallpaperSet => 'Fond d\'écran défini avec succès';
  @override
  String get FailedToUpload =>
      'Oups ! Le téléchargement a échoué, veuillez réessayer';
  @override
  String get UploadFinished => 'Téléchargement terminé';
  @override
  String get Downloading => 'Téléchargement';
  @override
  String get NoWallpaperSelectedMsg =>
      'Veuillez sélectionner le fond d\'écran pour continuer';
  @override
  String get NoImgSelected => 'Aucune image sélectionnée';
  @override
  String get Notifications => 'Notifications';
  @override
  String get StartFollowingMsg => 'commencé à te suivre';
  @override
  String get PostReactionMsg => 'a réagi à votre message';
  @override
  String get PostCommentMsg => 'a commenté votre message';
  @override
  String get ReplyMsg => 'répondu à votre commentaire';
  @override
  String get CommentReactedMsg => 'réagi à votre commentaire';
  @override
  String get CannotFindFile => 'Oups ! Impossible de trouver le fichier';
  @override
  String get NoFilePicked =>
      'Tentative de téléchargement et aucun fichier n\'est sélectionné';
  @override
  String get SentYouMsg => 'Je vous ai envoyé un message';
  @override
  String get Report => 'Signaler';
  @override
  String get Unreport => 'Annuler le signalement';
  @override
  String get ReportDesc => 'Nous supprimons le message qui contient : ';
  @override
  String get ReportReasons =>
      '⚫️ Contenu sexuel. \n\n⚫️ Contenu violent ou répulsif. \n\n⚫️ Contenu haineux ou abusif. \n\n⚫️ Spam ou trompeur.';
  @override
  String get ReportNote =>
      'Nous ne leur ferons pas savoir si vous prenez cette mesure.';
  @override
  String get ReportThanks =>
      'Nous allons vérifier votre demande, merci d\'aider à améliorer notre communauté';
  @override
  String get Admin => 'Administrateur';
  @override
  String get ProfanityDetected =>
      'Mots grossiers détectés, votre compte peut être suspendu !';
  @override
  String get AreYouSure => 'Êtes-vous sûr de';
  @override
  String get ConfirmChatDeletion => 'Etes-vous sûr de supprimer le chat';
  @override
  String get ReportedPosts => 'Messages signalés';
  @override
  String get AllChats => 'Tous les chats';
  @override
  String get Users => 'Utilisateurs';
  @override
  String get TermsService => 'Conditions d\'utilisation';
  @override
  String get WaitingToRecord => 'En attente d\'enregistrement';
  @override
  String get AdjustVolume => 'Régler le volume';
  @override
  String get AdjustSpeed => 'Régler la vitesse';
  @override
  String get Ok => 'D\'accord';
  @override
  String get ShareExternally => 'Partager en externe';
  @override
  String get ShareOnYourWall => 'Partager sur votre mur';
  @override
  String get Error => 'Erreur';
  @override
  String get Me => 'Moi';
  @override
  String get OopsCode => 'Oups ! Le code de vérification est erroné';
  @override
  String get OopsNetwork =>
      'Oups ! quelque chose s\'est mal passé, veuillez vérifier votre réseau !';
  @override
  String get OopsEmailorPassword =>
      'Oups ! l\'e-mail ou le mot de passe n\'est pas correct !';
  @override
  String get OopsNotRegister =>
      'Oups ! Aucun utilisateur trouvé, sinon inscrivez-vous d\'abord !';
  @override
  String get OopsEmailUsed =>
      'Oups ! cet e-mail est déjà utilisé par un autre compte !';
  @override
  String get OopsPhoneFormat =>
      'Oups ! le format du téléphone n\'est pas valide';
  @override
  String get NoAudioRecorded => 'Aucun audio ajouté';
  @override
  String get OopsWrong => 'Oups, quelque chose s\'est mal passé';
  @override
  String get Optional => 'Optionnel';
  @override
  String get Sending => 'Envoi en cours... ';
  @override
  String get NotificationPermission =>
      'L\'autorisation de notification est garantie';
  @override
  String get Theme => 'Thème';
  @override
  String get DarkMode => 'Mode sombre';
  @override
  String get Others => 'Autres';
  @override
  String get PeopleWhoReacted => 'Des gens qui ont réagi';
  @override
  String get BadWords =>
      'Mots grossiers détectés, votre compte peut être suspendu !';
  @override
  String get PickFile => 'Choisir un fichier';
  @override
  String get Loading => 'Chargement...';
  @override
  String get PleaseAddAudio => 'Veuillez ajouter un son';
  @override
  String get PeopleYouMayFollow => 'Les gens que vous pouvez suivre';
  @override
  String get Unsave => 'Désenregistrer';
  @override
  String get PostsLikes => 'Messages J\'aime';
  @override
  String get CommentsLikes => 'Commentaires J\'aime';
  @override
  String get RepliesLikes => 'Réponds J\'aime';
  @override
  String get ShareiConfezz => 'Partager iConfezz';
  @override
  String get MemberSince => 'Membre depuis';
  @override
  String get Comment => 'Commenter';
  @override
  String get UnShare => 'Annuler le partage';
  @override
  String get Liked => 'Aimé';
  @override
  String get Like => 'Aimer';
  @override
  String get SAVED => 'ENREGISTRÉ';
  @override
  String get SHARED => 'PARTAGÉ';
  @override
  String get AppLanguage => 'Langue de l\'application';
  @override
  String get ComingSoon => 'À venir';
  @override
  String get ReportProblem => 'Signaler un problème';
  @override
  String get PushNotifications => 'Notifications push';
  @override
  String get ShareProfile => 'Partager le profil';
  @override
  String get DeleteAccount => 'Supprimer le compte';
  @override
  String get DeleteYourAccount => 'Supprimer votre compte';
  @override
  String get Record => 'Record';
  @override
  String get Or => 'Ou alors';
  @override
  String get NoPhotoPicked => 'Aucune photo choisie';
  @override
  String get SearchForUsers => 'Rechercher des utilisateurs';
  @override
  String get NoResultsFound => 'Aucun résultat trouvé';
  @override
  String get TemporaryFilesRemoved =>
      'Fichiers temporaires supprimés avec succès';
  @override
  String get FailedToClean => 'Échec du nettoyage des fichiers temporaires';
  @override
  String get exit => 'sortie';
  @override
  String get CommunityGuidelines => 'Règles de la communauté';
  @override
  String get FAQ => 'FAQ';
  @override
  String get Paste => 'Paste';
  @override
  String get DoYouWantToPasteThisCode => 'Do you want to paste this code?';
  @override
  String get PasteCode => 'Paste Code';
}

// Path: <root>
class _StringsHi implements _StringsEn {
  /// You can call this constructor and build your own translation instance of this locale.
  /// Constructing via the enum [AppLocale.build] is preferred.
  _StringsHi.build();

  /// Access flat map
  @override
  dynamic operator [](String key) => _flatMap[key];

  // Internal flat map initialized lazily
  @override
  late final Map<String, dynamic> _flatMap = _buildFlatMap();

  @override
  late final _StringsHi _root = this; // ignore: unused_field

  // Translations
  @override
  String get AppName => 'आईकॉन्फ़ेज़';
  @override
  String get AboutApp =>
      'iConfez एक ऐसा समुदाय है जो दूसरों की आवाज़ सुनने के लिए समर्पित है।';
  @override
  String get AppVersion => '2.0.0';
  @override
  String get UnknownError => 'कुछ गलत हो गया';
  @override
  String get Next => 'अगला';
  @override
  String get EmailUser => 'क्या आप ईमेल उपयोगकर्ता हैं, यहां लॉग इन करें!';
  @override
  String get Email => 'ईमेल';
  @override
  String get bio => 'जीवनी';
  @override
  String get city => 'शहर';
  @override
  String get dob => 'जन्म की तारीख';
  @override
  String get Password => 'पासवर्ड';
  @override
  String get Login => 'लॉग इन करें';
  @override
  String get PhoneNumber => 'फ़ोन नंबर';
  @override
  String get PhoneVerification => 'फ़ोन नंबर सत्यापन';
  @override
  String get EnterCode => 'को भेजा गया कोड दर्ज करें';
  @override
  String get DidntRecieveCode => 'कोड प्राप्त नहीं हुआ?';
  @override
  String get InvalidSMSCode => 'एसएमएस सत्यापन कोड अमान्य है';
  @override
  String get RESEND => 'फिर से भेजें';
  @override
  String get VERIFY => 'सत्यापित करना';
  @override
  String get POSTS => 'पोस्ट';
  @override
  String get FOLLOWERS => 'अनुयायियों';
  @override
  String get FOLLOWINGS => 'निम्नलिखित';
  @override
  String get Follow => 'अनुसरण';
  @override
  String get Following => 'अगले';
  @override
  String get Register => 'पंजीकरण करवाना';
  @override
  String get Username => 'उपयोगकर्ता नाम';
  @override
  String get FirstName => 'पहला नाम';
  @override
  String get LastName => 'उपनाम';
  @override
  String get FullName => 'पूरा नाम';
  @override
  String get Status => 'स्थिति';
  @override
  String get AddValidUsername => 'एक वैध उपयोगकर्ता नाम जोड़ें';
  @override
  String get AddValidFirstName => 'एक मान्य प्रथम नाम जोड़ें';
  @override
  String get AddValidLastName => 'एक मान्य अंतिम नाम जोड़ें';
  @override
  String get SelectGender => 'कृपया अपना लिंग चुनें।';
  @override
  String get Male => 'नर';
  @override
  String get Female => 'महिला';
  @override
  String get Other => 'अन्य';
  @override
  String get AcceptTerms => 'मैं सभी नियम और शर्तें स्वीकार करता हूं';
  @override
  String get Save => 'बचाना';
  @override
  String get Search => 'खोज';
  @override
  String get Searching => 'खोज कर...';
  @override
  String get SelectPhoneCode => 'फ़ोन कोड चुनें';
  @override
  String get InvalidEmail => 'अमान्य ईमेल';
  @override
  String get ShortPassword => 'पासवर्ड में कम से कम 8 अक्षर होने चाहिए';
  @override
  String get UsernameTaken => 'उपयोगकर्ता का नाम पहले से लिया है';
  @override
  String get InvalidPhone => 'कृपया सभी कक्षों को ठीक से भरें';
  @override
  String get Message => 'संदेश';
  @override
  String get LoginFirst => 'ओह, कृपया पहले लॉगिन करें';
  @override
  String get START => 'शुरू हो जाओ';
  @override
  String get Wallpapers => 'वॉलपेपर';
  @override
  String get Profile => 'प्रोफ़ाइल';
  @override
  String get NoInternet => 'इन्टरनेट उपलब्ध नहीँ है';
  @override
  String get Settings => 'समायोजन';
  @override
  String get Account => 'खाता';
  @override
  String get EditProfile => 'प्रोफ़ाइल संपादित करें';
  @override
  String get OnlineStatus => 'ऑनलाइन स्थिति';
  @override
  String get OnlineDescription =>
      'कोई भी देख सकता है कि आप आखिरी बार कब ऑनलाइन थे';
  @override
  String get Logout => 'लॉग आउट';
  @override
  String get DirectMsgs => 'चैट संदेश';
  @override
  String get DirectMsgsDescr => 'चैट सूचनाएं प्राप्त करें';
  @override
  String get GroupsMsgs => 'समूह संदेश';
  @override
  String get GroupsMsgsDesc => 'सभी समूह सूचनाएं प्राप्त करें';
  @override
  String get AddGroupName => 'कृपया! समूह का नाम जोड़ें';
  @override
  String get About => 'हमारे बारे में';
  @override
  String get RateUs => 'हमें रेटिंग दें';
  @override
  String get EULA => 'ईयूएलए समझौता';
  @override
  String get PrivacyPolicy => 'गोपनीयता नीति';
  @override
  String get Feed => 'चारा';
  @override
  String get Post => 'डाक';
  @override
  String get Gallery => 'गेलरी';
  @override
  String get Stories => 'कहानियों';
  @override
  String get MyPosts => 'मेरी पोस्ट';
  @override
  String get Favorites => 'पसंदीदा';
  @override
  String get Announcement => 'घोषणा';
  @override
  String get WhatOnMind => 'क्या साझा करें?';
  @override
  String get Likes => 'को यह पसंद है';
  @override
  String get Comments => 'टिप्पणियाँ';
  @override
  String get CreatePost => 'पोस्ट बनाएं';
  @override
  String get Share => 'साझा करना';
  @override
  String get Edit => 'संपादन करना';
  @override
  String get Delete => 'मिटाना';
  @override
  String get Copy => 'प्रतिलिपि';
  @override
  String get Copied => 'प्रतिलिपि';
  @override
  String get ReadMore => '...अधिक पढ़ें';
  @override
  String get SuccessPostPublish => 'पोस्ट सफलतापूर्वक प्रकाशित किया गया है';
  @override
  String get SuccessPostEdited => 'पोस्ट सफलतापूर्वक संपादित किया गया है';
  @override
  String get TypeComment => 'टिप्पणी लिखें...';
  @override
  String get TypeReply => 'जवाब लिखें...';
  @override
  String get NotAllowedToPublish =>
      'क्षमा करें! आपको अब और प्रकाशित करने की अनुमति नहीं है';
  @override
  String get NotAllowedToComment =>
      'क्षमा करें! आपको टिप्पणी करने की अनुमति नहीं है';
  @override
  String get PostRemoveConfirm => 'क्या आप वाकई इस पोस्ट को हटाना चाहते हैं?';
  @override
  String get CommentRemoveConfirm =>
      'क्या आप इस कमेंट को मिटाने के बारे में पक्के हैं?';
  @override
  String get AddSomeContent => 'कृपया पोस्ट करने से पहले कुछ सामग्री जोड़ें';
  @override
  String get Reply => 'जवाब';
  @override
  String get Replies => 'जवाब';
  @override
  String get RepliedToComment => 'आपकी टिप्पणी का जवाब दिया';
  @override
  String get Chats => 'चैट';
  @override
  String get OnlineUsers => 'ऑनलाइन उपयोगकर्ता';
  @override
  String get RecentChats => 'हालिया चैट';
  @override
  String get RemoveConversation => 'बातचीत हटाएं';
  @override
  String get Messaging => 'संदेश...';
  @override
  String get CannotChatWithUser =>
      'क्षमा करें! आप इस उपयोगकर्ता के साथ चैट नहीं कर सकते';
  @override
  String get MsgDeleteConfirm => 'क्या आप वाकई संदेश हटाना चाहते हैं?';
  @override
  String get NoChatFound =>
      'अभी तक कोई हाल की चैट नहीं है, चैट करना शुरू करें!';
  @override
  String get Online => 'ऑनलाइन';
  @override
  String get Typing => 'टाइपिंग...';
  @override
  String get Image => 'छवि';
  @override
  String get Voice => 'आवाज़';
  @override
  String get Video => 'वीडियो';
  @override
  String get Emoji => 'इमोजी';
  @override
  String get GIF => 'जीआईएफ';
  @override
  String get Sticker => 'स्टिकर';
  @override
  String get Groups => 'समूह';
  @override
  String get CreateGroup => 'एक समूह बनाएं';
  @override
  String get EditGroup => 'अपना समूह संपादित करें';
  @override
  String get Members => 'सदस्य';
  @override
  String get PhotosLibrary => 'फोटो लाइब्रेरी';
  @override
  String get TakePicture => 'तस्वीर ले लो';
  @override
  String get VideosLibrary => 'वीडियो लाइब्रेरी';
  @override
  String get RecordVideo => 'वीडियो रिकॉर्ड करो';
  @override
  String get Cancel => 'रद्द करना';
  @override
  String get SlideToCancel => 'रद्द करने के लिए स्लाइड';
  @override
  String get On => 'पर';
  @override
  String get Off => 'बंद';
  @override
  String get Group => 'समूह';
  @override
  String get LeaveGroup => 'समूह छोड़ दें';
  @override
  String get GroupName => 'समूह का नाम';
  @override
  String get Join => 'जोड़ना';
  @override
  String get Joined => 'में शामिल हो गए';
  @override
  String get Public => 'जनता';
  @override
  String get Private => 'निजी';
  @override
  String get GroupType => 'समूह प्रकार';
  @override
  String get RemoveMember => 'सदस्य निकालें';
  @override
  String get AddMembers => 'सदस्य जोड़ें';
  @override
  String get Block => 'अवरोध पैदा करना';
  @override
  String get Unblock => 'अनब्लॉक करें';
  @override
  String get Ban => 'प्रतिबंध';
  @override
  String get Unban => 'अनबन';
  @override
  String get Banned => 'प्रतिबंधित';
  @override
  String get Newest => 'नवीनतम';
  @override
  String get Trending => 'रुझान';
  @override
  String get MostDownloaded => 'सबसे ज्यादा डाउनलोड किया जाने वाला';
  @override
  String get Categories => 'श्रेणियाँ';
  @override
  String get AddWallpaper => 'वॉलपेपर जोड़ें';
  @override
  String get WallpaperName => 'वॉलपेपर का नाम';
  @override
  String get Category => 'श्रेणी';
  @override
  String get Upload => 'डालना';
  @override
  String get Home => 'घर';
  @override
  String get Lock => 'ताला';
  @override
  String get Both => 'दोनों';
  @override
  String get SetAsWallpaper => 'वॉलपेपर के रूप में सेट';
  @override
  String get WallpaperSet => 'वॉलपेपर सफलतापूर्वक सेट';
  @override
  String get FailedToUpload => 'उफ़! अपलोड विफल, कृपया पुनः प्रयास करें';
  @override
  String get UploadFinished => 'अपलोड समाप्त';
  @override
  String get Downloading => 'डाउनलोडिंग';
  @override
  String get NoWallpaperSelectedMsg => 'कृपया जारी रखने के लिए वॉलपेपर चुनें';
  @override
  String get NoImgSelected => 'कोई छवि चयनित नहीं';
  @override
  String get Notifications => 'सूचनाएं';
  @override
  String get StartFollowingMsg => 'ने आपको फॉलो करना शुरू किया';
  @override
  String get PostReactionMsg => 'आपकी पोस्ट पर प्रतिक्रिया दी';
  @override
  String get PostCommentMsg => 'आपकी पोस्ट पर टिप्पणी की';
  @override
  String get ReplyMsg => 'आपकी टिप्पणी का जवाब दिया';
  @override
  String get CommentReactedMsg => 'आपकी टिप्पणी पर प्रतिक्रिया दी';
  @override
  String get CannotFindFile => 'ओह! फ़ाइल नहीं ढूँढ सकता';
  @override
  String get NoFilePicked =>
      'अपलोड करने का प्रयास किया जा रहा है और कोई फ़ाइल नहीं चुनी गई';
  @override
  String get SentYouMsg => 'आपको संदेश भेजा';
  @override
  String get Report => 'शिकायत करना';
  @override
  String get Unreport => 'अनरिपोर्ट';
  @override
  String get ReportDesc => 'हम उस पोस्ट को हटा देते हैं जिसमें:';
  @override
  String get ReportReasons =>
      '⚫️ यौन सामग्री। \n\n⚫️ हिंसक या प्रतिकूल सामग्री। \n\n⚫️ घृणित या अपमानजनक सामग्री। \n\n⚫️ स्पैम या भ्रामक।';
  @override
  String get ReportNote =>
      'यदि आप यह कार्रवाई करते हैं तो हम उन्हें इसकी सूचना नहीं देंगे।';
  @override
  String get ReportThanks =>
      'हम आपके अनुरोध की जांच करेंगे, हमारे समुदाय को बेहतर बनाने में मदद करने के लिए धन्यवाद';
  @override
  String get Admin => 'व्यवस्थापक';
  @override
  String get ProfanityDetected =>
      'गलत शब्दों का पता चला है, आपका खाता निलंबित हो सकता है!';
  @override
  String get AreYouSure => 'क्या आपको यकीन है';
  @override
  String get ConfirmChatDeletion =>
      'क्या आप निश्चित रूप से चैट हटाना चाहते हैं';
  @override
  String get ReportedPosts => 'रिपोर्ट की गई पोस्ट';
  @override
  String get AllChats => 'सभी चैट';
  @override
  String get Users => 'उपयोगकर्ता';
  @override
  String get TermsService => 'सेवा की शर्तें';
  @override
  String get WaitingToRecord => 'रिकॉर्ड करने की प्रतीक्षा कर रहा है';
  @override
  String get AdjustVolume => 'वॉल्यूम समायोजित करें';
  @override
  String get AdjustSpeed => 'गति समायोजित करें';
  @override
  String get Ok => 'ठीक है';
  @override
  String get ShareExternally => 'बाहरी रूप से साझा करें';
  @override
  String get ShareOnYourWall => 'अपनी वॉल पर शेयर करें';
  @override
  String get Error => 'त्रुटि';
  @override
  String get Me => 'मैं';
  @override
  String get OopsCode => 'उफ़! सत्यापन कोड गलत है';
  @override
  String get OopsNetwork => 'ओह! कुछ गलत हो गया, कृपया अपना नेटवर्क जांचें!';
  @override
  String get OopsEmailorPassword => 'उफ़! ईमेल या पासवर्ड सही नहीं है!';
  @override
  String get OopsNotRegister =>
      'उफ़! कोई उपयोगकर्ता नहीं मिला, अगर रजिस्टर नहीं है तो पहले साइन अप करने पर विचार करें!';
  @override
  String get OopsEmailUsed =>
      'ओह! यह ईमेल पहले से ही किसी अन्य खाते द्वारा उपयोग किया जा रहा है!';
  @override
  String get OopsPhoneFormat => 'उफ़! फ़ोन प्रारूप अमान्य है';
  @override
  String get NoAudioRecorded => 'कोई ऑडियो नहीं जोड़ा गया';
  @override
  String get OopsWrong => 'ओह! कुछ गलत हो गया है';
  @override
  String get Optional => 'वैकल्पिक';
  @override
  String get Sending => 'भेजना... ';
  @override
  String get NotificationPermission => 'अधिसूचना अनुमति की गारंटी है';
  @override
  String get Theme => 'थीम';
  @override
  String get DarkMode => 'डार्क मोड';
  @override
  String get Others => 'अन्य';
  @override
  String get PeopleWhoReacted => 'प्रतिक्रिया देने वाले लोग';
  @override
  String get BadWords =>
      'गलत शब्दों का पता चला है, आपका खाता निलंबित हो सकता है!';
  @override
  String get PickFile => 'फ़ाइल चुनें';
  @override
  String get Loading => 'लोड हो रहा है...';
  @override
  String get PleaseAddAudio => 'कृपया ऑडियो जोड़ें';
  @override
  String get PeopleYouMayFollow => 'वे लोग जिनका आप अनुसरण कर सकते हैं';
  @override
  String get Unsave => 'अनसेव';
  @override
  String get PostsLikes => 'पोस्ट लाइक';
  @override
  String get CommentsLikes => 'टिप्पणी पसंद';
  @override
  String get RepliesLikes => 'जवाब पसंद है';
  @override
  String get ShareiConfezz => 'शेयर आईकॉन्फेज़';
  @override
  String get MemberSince => 'से सदस्ये';
  @override
  String get Comment => 'टिप्पणी';
  @override
  String get UnShare => 'अनशेयर';
  @override
  String get Liked => 'पसंद किया';
  @override
  String get Like => 'पसंद करना';
  @override
  String get SAVED => 'बचाया';
  @override
  String get SHARED => 'साझा';
  @override
  String get AppLanguage => 'ऐप भाषा';
  @override
  String get ComingSoon => 'जल्द आ रहा है';
  @override
  String get ReportProblem => 'एक समस्या का आख्या';
  @override
  String get PushNotifications => 'सूचनाएं धक्का';
  @override
  String get ShareProfile => 'प्रोफ़ाइल साझा करें';
  @override
  String get DeleteAccount => 'खाता हटा दो';
  @override
  String get DeleteYourAccount => 'अपने खाते को नष्ट करो';
  @override
  String get Record => 'अभिलेख';
  @override
  String get Or => 'या';
  @override
  String get NoPhotoPicked => 'कोई फोटो नहीं उठाया';
  @override
  String get SearchForUsers => 'उपयोगकर्ताओं के लिए खोजें';
  @override
  String get NoResultsFound => 'कोई परिणाम नहीं मिला';
  @override
  String get TemporaryFilesRemoved => 'अस्थायी फ़ाइलें सफलतापूर्वक हटाई गईं';
  @override
  String get FailedToClean => 'अस्थायी फ़ाइलों को साफ करने में विफल';
  @override
  String get exit => 'बाहर निकलना';
  @override
  String get CommunityGuidelines => 'समुदाय दिशानिर्देश';
  @override
  String get FAQ => 'सामान्य प्रश्न';
  @override
  String get Paste => 'Paste';
  @override
  String get DoYouWantToPasteThisCode => 'Do you want to paste this code?';
  @override
  String get PasteCode => 'Paste Code';
}

// Path: <root>
class _StringsId implements _StringsEn {
  /// You can call this constructor and build your own translation instance of this locale.
  /// Constructing via the enum [AppLocale.build] is preferred.
  _StringsId.build();

  /// Access flat map
  @override
  dynamic operator [](String key) => _flatMap[key];

  // Internal flat map initialized lazily
  @override
  late final Map<String, dynamic> _flatMap = _buildFlatMap();

  @override
  late final _StringsId _root = this; // ignore: unused_field

  // Translations
  @override
  String get AppName => 'iConfezz';
  @override
  String get AboutApp =>
      'iConfezz adalah komunitas yang didedikasikan untuk mendengarkan suara orang lain.';
  @override
  String get AppVersion => '2.0.0';
  @override
  String get UnknownError => 'Ada yang salah';
  @override
  String get Next => 'Berikutnya';
  @override
  String get EmailUser => 'Apakah Anda Pengguna Email, Login di sini!';
  @override
  String get Email => 'Surel';
  @override
  String get bio => 'Biografi';
  @override
  String get city => 'Kota';
  @override
  String get dob => 'Tanggal lahir';
  @override
  String get Password => 'Kata sandi';
  @override
  String get Login => 'Gabung';
  @override
  String get PhoneNumber => 'Nomor telepon';
  @override
  String get PhoneVerification => 'Verifikasi Nomor Telepon';
  @override
  String get EnterCode => 'Masukkan kode yang dikirim ke ';
  @override
  String get DidntRecieveCode => 'Tidak menerima kode?';
  @override
  String get InvalidSMSCode => 'Kode Verifikasi SMS Tidak Valid';
  @override
  String get RESEND => 'KIRIM ULANG';
  @override
  String get VERIFY => 'MEMERIKSA';
  @override
  String get POSTS => 'POSTINGAN';
  @override
  String get FOLLOWERS => 'Pengikut';
  @override
  String get FOLLOWINGS => 'Berikut';
  @override
  String get Follow => 'Mengikuti';
  @override
  String get Following => 'Mengikuti';
  @override
  String get Register => 'Daftar';
  @override
  String get Username => 'Nama belakang';
  @override
  String get FirstName => 'Nama depan';
  @override
  String get LastName => 'Nama keluarga';
  @override
  String get FullName => 'Nama lengkap';
  @override
  String get Status => 'Status';
  @override
  String get AddValidUsername => 'Tambahkan nama pengguna yang valid';
  @override
  String get AddValidFirstName => 'Tambahkan nama depan yang valid';
  @override
  String get AddValidLastName => 'Tambahkan nama belakang yang valid';
  @override
  String get SelectGender => 'Silakan pilih jenis kelamin Anda.';
  @override
  String get Male => 'Pria';
  @override
  String get Female => 'Perempuan';
  @override
  String get Other => 'Lainnya';
  @override
  String get AcceptTerms => 'Saya Menerima Semua Syarat & Ketentuan';
  @override
  String get Save => 'Menyimpan';
  @override
  String get Search => 'Mencari';
  @override
  String get Searching => 'Mencari...';
  @override
  String get SelectPhoneCode => 'Pilih kode telepon';
  @override
  String get InvalidEmail => 'Email tidak valid';
  @override
  String get ShortPassword => 'Kata Sandi Harus Berisi Setidaknya 8 karakter';
  @override
  String get UsernameTaken => 'Nama Pengguna Sudah Diambil';
  @override
  String get InvalidPhone => 'Silakan isi semua sel dengan benar';
  @override
  String get Message => 'Pesan';
  @override
  String get LoginFirst => 'Ups, Silahkan login dulu';
  @override
  String get START => 'MEMULAI';
  @override
  String get Wallpapers => 'Wallpaper';
  @override
  String get Profile => 'Profil';
  @override
  String get NoInternet => 'Tidak ada akses internet';
  @override
  String get Settings => 'Pengaturan';
  @override
  String get Account => 'Akun';
  @override
  String get EditProfile => 'Sunting profil';
  @override
  String get OnlineStatus => 'Status Daring';
  @override
  String get OnlineDescription =>
      'Siapa pun dapat melihat kapan Anda terakhir Online';
  @override
  String get Logout => 'Keluar';
  @override
  String get DirectMsgs => 'Pesan Obrolan';
  @override
  String get DirectMsgsDescr => 'Terima notifikasi obrolan';
  @override
  String get GroupsMsgs => 'Pesan Grup';
  @override
  String get GroupsMsgsDesc => 'Terima semua Pemberitahuan Grup';
  @override
  String get AddGroupName => 'Silahkan! Tambahkan Nama Grup';
  @override
  String get About => 'tentang kami';
  @override
  String get RateUs => 'Nilai Kami';
  @override
  String get EULA => 'Perjanjian EULA';
  @override
  String get PrivacyPolicy => 'Kebijakan pribadi';
  @override
  String get Feed => 'Memberi makan';
  @override
  String get Post => 'Pos';
  @override
  String get Gallery => 'Galeri';
  @override
  String get Stories => 'Cerita';
  @override
  String get MyPosts => 'Postingan Saya';
  @override
  String get Favorites => 'Favorit';
  @override
  String get Announcement => 'Pengumuman';
  @override
  String get WhatOnMind => 'Apa yang Harus Dibagikan?';
  @override
  String get Likes => 'Suka';
  @override
  String get Comments => 'Komentar';
  @override
  String get CreatePost => 'Buat Postingan';
  @override
  String get Share => 'Membagikan';
  @override
  String get Edit => 'Mengedit';
  @override
  String get Delete => 'Menghapus';
  @override
  String get Copy => 'Salinan';
  @override
  String get Copied => 'Disalin';
  @override
  String get ReadMore => '...Baca selengkapnya';
  @override
  String get SuccessPostPublish => 'Posting telah berhasil diterbitkan';
  @override
  String get SuccessPostEdited => 'Posting telah berhasil diedit';
  @override
  String get TypeComment => 'Ketik komentar...';
  @override
  String get TypeReply => 'Ketik balasan...';
  @override
  String get NotAllowedToPublish =>
      'Maaf! Anda tidak diperbolehkan untuk mempublikasikan lagi';
  @override
  String get NotAllowedToComment =>
      'Maaf! Anda tidak diperbolehkan berkomentar';
  @override
  String get PostRemoveConfirm =>
      'Apakah Anda yakin ingin menghapus postingan ini?';
  @override
  String get CommentRemoveConfirm =>
      'Apakah Anda yakin ingin menghapus komentar ini?';
  @override
  String get AddSomeContent =>
      'Tolong tambahkan beberapa konten sebelum memposting';
  @override
  String get Reply => 'Membalas';
  @override
  String get Replies => 'Balasan';
  @override
  String get RepliedToComment => 'membalas komentar Anda';
  @override
  String get Chats => 'Obrolan';
  @override
  String get OnlineUsers => 'PENGGUNA ONLINE';
  @override
  String get RecentChats => 'CHAT TERBARU';
  @override
  String get RemoveConversation => 'Hapus Percakapan';
  @override
  String get Messaging => 'Pesan...';
  @override
  String get CannotChatWithUser =>
      'Maaf! Anda tidak dapat mengobrol dengan pengguna ini';
  @override
  String get MsgDeleteConfirm => 'Apakah Anda yakin ingin menghapus pesan itu?';
  @override
  String get NoChatFound => 'Belum ada obrolan Terbaru, mulailah mengobrol!';
  @override
  String get Online => 'On line';
  @override
  String get Typing => 'Mengetik...';
  @override
  String get Image => 'Gambar';
  @override
  String get Voice => 'Suara';
  @override
  String get Video => 'Video';
  @override
  String get Emoji => 'Emoji';
  @override
  String get GIF => 'GIF';
  @override
  String get Sticker => 'Stiker';
  @override
  String get Groups => 'Grup';
  @override
  String get CreateGroup => 'Buat Grup';
  @override
  String get EditGroup => 'Edit Grup Anda';
  @override
  String get Members => 'Anggota';
  @override
  String get PhotosLibrary => 'Perpustakaan Foto';
  @override
  String get TakePicture => 'Mengambil gambar';
  @override
  String get VideosLibrary => 'Perpustakaan Video';
  @override
  String get RecordVideo => 'Merekam video';
  @override
  String get Cancel => 'Membatalkan';
  @override
  String get SlideToCancel => 'Geser untuk membatalkan';
  @override
  String get On => 'Pada';
  @override
  String get Off => 'Mati';
  @override
  String get Group => 'Kelompok';
  @override
  String get LeaveGroup => 'Meninggalkan grup';
  @override
  String get GroupName => 'Nama grup';
  @override
  String get Join => 'Bergabung';
  @override
  String get Joined => 'Bergabung';
  @override
  String get Public => 'Publik';
  @override
  String get Private => 'Pribadi';
  @override
  String get GroupType => 'Jenis Grup';
  @override
  String get RemoveMember => 'Hapus anggota';
  @override
  String get AddMembers => 'Tambah Anggota';
  @override
  String get Block => 'Memblokir';
  @override
  String get Unblock => 'Buka Blokir';
  @override
  String get Ban => 'Melarang';
  @override
  String get Unban => 'Batalkan larangan';
  @override
  String get Banned => 'Dilarang';
  @override
  String get Newest => 'Terbaru';
  @override
  String get Trending => 'Tren';
  @override
  String get MostDownloaded => 'Paling banyak di download';
  @override
  String get Categories => 'Kategori';
  @override
  String get AddWallpaper => 'Tambahkan Wallpaper';
  @override
  String get WallpaperName => 'Nama Wallpaper';
  @override
  String get Category => 'Kategori';
  @override
  String get Upload => 'Mengunggah';
  @override
  String get Home => 'Rumah';
  @override
  String get Lock => 'Kunci';
  @override
  String get Both => 'Keduanya';
  @override
  String get SetAsWallpaper => 'Tetapkan sebagai Wallpaper';
  @override
  String get WallpaperSet => 'Wallpaper Berhasil Ditetapkan';
  @override
  String get FailedToUpload => 'Ups! Upload Gagal, Silakan coba lagi';
  @override
  String get UploadFinished => 'Unggahan Selesai';
  @override
  String get Downloading => 'Mengunduh';
  @override
  String get NoWallpaperSelectedMsg =>
      'Silakan Pilih Wallpaper untuk melanjutkan';
  @override
  String get NoImgSelected => 'Tidak Ada Gambar yang Dipilih';
  @override
  String get Notifications => 'Pemberitahuan';
  @override
  String get StartFollowingMsg => 'mulai mengikutimu';
  @override
  String get PostReactionMsg => 'bereaksi terhadap posting Anda';
  @override
  String get PostCommentMsg => 'mengomentari posting Anda';
  @override
  String get ReplyMsg => 'membalas komentar Anda';
  @override
  String get CommentReactedMsg => 'bereaksi terhadap komentar Anda';
  @override
  String get CannotFindFile => 'Ups! Tidak dapat menemukan file';
  @override
  String get NoFilePicked =>
      'Mencoba mengunggah dan tidak ada file yang diambil';
  @override
  String get SentYouMsg => 'Mengirim Anda pesan';
  @override
  String get Report => 'Laporan';
  @override
  String get Unreport => 'Batalkan laporan';
  @override
  String get ReportDesc => 'Kami menghapus postingan yang memiliki: ';
  @override
  String get ReportReasons =>
      '⚫️ Konten seksual. \n\n⚫️ Konten kekerasan atau menjijikkan. \n\n⚫️ Konten kebencian atau pelecehan. \n\n⚫️ Spam atau menyesatkan.';
  @override
  String get ReportNote =>
      'Kami tidak akan memberi tahu mereka jika Anda mengambil tindakan ini.';
  @override
  String get ReportThanks =>
      'Kami akan memeriksa permintaan Anda, Terima kasih telah membantu meningkatkan komunitas kami';
  @override
  String get Admin => 'Admin';
  @override
  String get ProfanityDetected =>
      'Kata-kata buruk terdeteksi, akun Anda mungkin ditangguhkan!';
  @override
  String get AreYouSure => 'Apakah kamu yakin';
  @override
  String get ConfirmChatDeletion => 'Apakah Anda yakin akan menghapus obrolan';
  @override
  String get ReportedPosts => 'Postingan yang Dilaporkan';
  @override
  String get AllChats => 'Semua Obrolan';
  @override
  String get Users => 'Pengguna';
  @override
  String get TermsService => 'Ketentuan Layanan';
  @override
  String get WaitingToRecord => 'Menunggu untuk merekam';
  @override
  String get AdjustVolume => 'Sesuaikan volume';
  @override
  String get AdjustSpeed => 'Sesuaikan kecepatan';
  @override
  String get Ok => 'Oke';
  @override
  String get ShareExternally => 'Bagikan secara eksternal';
  @override
  String get ShareOnYourWall => 'Bagikan di dinding Anda';
  @override
  String get Error => 'Kesalahan';
  @override
  String get Me => 'Saya';
  @override
  String get OopsCode => 'Ups! kode verifikasi salah';
  @override
  String get OopsNetwork =>
      'Ups! ada yang tidak beres, Silakan periksa jaringan Anda!';
  @override
  String get OopsEmailorPassword => 'Ups! email atau kata sandi salah!';
  @override
  String get OopsNotRegister =>
      'Ups! tidak ada pengguna yang ditemukan, jika tidak mendaftar pertimbangkan untuk mendaftar terlebih dahulu!';
  @override
  String get OopsEmailUsed => 'Ups! email ini sudah digunakan oleh akun lain!';
  @override
  String get OopsPhoneFormat => 'Ups! format telepon tidak valid';
  @override
  String get NoAudioRecorded => 'Tidak ada audio yang ditambahkan';
  @override
  String get OopsWrong => 'Ups! Ada yang tidak beres';
  @override
  String get Optional => 'Opsional';
  @override
  String get Sending => 'Mengirim...';
  @override
  String get NotificationPermission => 'Izin pemberitahuan dijamin';
  @override
  String get Theme => 'Tema';
  @override
  String get DarkMode => 'Mode Gelap';
  @override
  String get Others => 'Yang lain';
  @override
  String get PeopleWhoReacted => 'Orang yang bereaksi';
  @override
  String get BadWords =>
      'Kata-kata buruk terdeteksi, akun Anda mungkin ditangguhkan!';
  @override
  String get PickFile => 'Pilih berkas';
  @override
  String get Loading => 'Memuat...';
  @override
  String get PleaseAddAudio => 'Tolong tambahkan audionya';
  @override
  String get PeopleYouMayFollow => 'Orang yang mungkin Anda ikuti';
  @override
  String get Unsave => 'Batalkan penyimpanan';
  @override
  String get PostsLikes => 'Suka Postingan';
  @override
  String get CommentsLikes => 'Komentar Suka';
  @override
  String get RepliesLikes => 'Balasan Suka';
  @override
  String get ShareiConfezz => 'Bagikan iConfezz';
  @override
  String get MemberSince => 'Anggota Sejak';
  @override
  String get Comment => 'Komentar';
  @override
  String get UnShare => 'Batalkan Bagikan';
  @override
  String get Liked => 'Menyukai';
  @override
  String get Like => 'Suka';
  @override
  String get SAVED => 'DISELAMATKAN';
  @override
  String get SHARED => 'BERSAMA';
  @override
  String get AppLanguage => 'Bahasa Aplikasi';
  @override
  String get ComingSoon => 'Segera hadir';
  @override
  String get ReportProblem => 'Laporkan Masalah';
  @override
  String get PushNotifications => 'Pemberitahuan Dorong';
  @override
  String get ShareProfile => 'Bagikan Profil';
  @override
  String get DeleteAccount => 'Hapus akun';
  @override
  String get DeleteYourAccount => 'Hapus akun anda';
  @override
  String get Record => 'Catatan';
  @override
  String get Or => 'Atau';
  @override
  String get NoPhotoPicked => 'Tidak Ada Foto yang Dipilih';
  @override
  String get SearchForUsers => 'Cari Pengguna';
  @override
  String get NoResultsFound => 'Tidak ada hasil yang ditemukan';
  @override
  String get TemporaryFilesRemoved => 'File sementara berhasil dihapus';
  @override
  String get FailedToClean => 'Gagal membersihkan file sementara';
  @override
  String get exit => 'KELUAR';
  @override
  String get CommunityGuidelines => 'Pedoman Komunitas';
  @override
  String get FAQ => 'Tanya Jawab';
  @override
  String get Paste => 'Paste';
  @override
  String get DoYouWantToPasteThisCode => 'Do you want to paste this code?';
  @override
  String get PasteCode => 'Paste Code';
}

// Path: <root>
class _StringsIt implements _StringsEn {
  /// You can call this constructor and build your own translation instance of this locale.
  /// Constructing via the enum [AppLocale.build] is preferred.
  _StringsIt.build();

  /// Access flat map
  @override
  dynamic operator [](String key) => _flatMap[key];

  // Internal flat map initialized lazily
  @override
  late final Map<String, dynamic> _flatMap = _buildFlatMap();

  @override
  late final _StringsIt _root = this; // ignore: unused_field

  // Translations
  @override
  String get AppName => 'iConfezz';
  @override
  String get AboutApp =>
      'iConfezz è una community dedicata ad ascoltare la voce degli altri.';
  @override
  String get AppVersion => '2.0.0';
  @override
  String get UnknownError => 'Qualcosa è andato storto';
  @override
  String get Next => 'Prossimo';
  @override
  String get EmailUser => 'Sei un utente email, accedi qui!';
  @override
  String get Email => 'E-mail';
  @override
  String get bio => 'Biografia';
  @override
  String get city => 'Città';
  @override
  String get dob => 'Data di nascita';
  @override
  String get Password => 'Parola d\'ordine';
  @override
  String get Login => 'Login';
  @override
  String get PhoneNumber => 'Numero di telefono';
  @override
  String get PhoneVerification => 'Verifica del numero di telefono';
  @override
  String get EnterCode => 'Inserisci il codice inviato a ';
  @override
  String get DidntRecieveCode => 'Non hai ricevuto il codice?';
  @override
  String get InvalidSMSCode => 'Il codice di verifica SMS non è valido';
  @override
  String get RESEND => 'RIPENDERE';
  @override
  String get VERIFY => 'VERIFICARE';
  @override
  String get POSTS => 'POST';
  @override
  String get FOLLOWERS => 'Seguaci';
  @override
  String get FOLLOWINGS => 'Seguenti';
  @override
  String get Follow => 'Seguire';
  @override
  String get Following => 'Seguente';
  @override
  String get Register => 'Registrati';
  @override
  String get Username => 'Nome utente';
  @override
  String get FirstName => 'Nome di battesimo';
  @override
  String get LastName => 'Cognome';
  @override
  String get FullName => 'Nome e cognome';
  @override
  String get Status => 'Stato';
  @override
  String get AddValidUsername => 'Aggiungi un nome utente valido';
  @override
  String get AddValidFirstName => 'Aggiungi un nome valido';
  @override
  String get AddValidLastName => 'Aggiungi un cognome valido';
  @override
  String get SelectGender => 'Per favore seleziona il tuo genere.';
  @override
  String get Male => 'Maschio';
  @override
  String get Female => 'Femmina';
  @override
  String get Other => 'Altro';
  @override
  String get AcceptTerms => 'Accetto tutti i Termini e condizioni';
  @override
  String get Save => 'Salva';
  @override
  String get Search => 'Ricerca';
  @override
  String get Searching => 'Cercando...';
  @override
  String get SelectPhoneCode => 'Seleziona un codice telefono';
  @override
  String get InvalidEmail => 'E-mail non valido';
  @override
  String get ShortPassword => 'La password deve contenere almeno 8 caratteri';
  @override
  String get UsernameTaken => 'Il nome utente è già stato preso';
  @override
  String get InvalidPhone =>
      'Si prega di riempire correttamente tutte le celle';
  @override
  String get Message => 'Messaggio';
  @override
  String get LoginFirst => 'Oops, per favore accedi prima';
  @override
  String get START => 'INIZIARE';
  @override
  String get Wallpapers => 'Sfondi';
  @override
  String get Profile => 'Profilo';
  @override
  String get NoInternet => 'Nessun accesso ad internet';
  @override
  String get Settings => 'Impostazioni';
  @override
  String get Account => 'Account';
  @override
  String get EditProfile => 'Modifica Profilo';
  @override
  String get OnlineStatus => 'Stato online';
  @override
  String get OnlineDescription =>
      'Chiunque può vedere quando sei online l\'ultima volta';
  @override
  String get Logout => 'Disconnettersi';
  @override
  String get DirectMsgs => 'Messaggi di chat';
  @override
  String get DirectMsgsDescr => 'Ricevi notifiche chat';
  @override
  String get GroupsMsgs => 'Messaggi di gruppo';
  @override
  String get GroupsMsgsDesc => 'Ricevi tutte le notifiche dei gruppi';
  @override
  String get AddGroupName => 'Per favore! Aggiungi il nome del gruppo';
  @override
  String get About => 'chi siamo';
  @override
  String get RateUs => 'Votaci';
  @override
  String get EULA => 'Accordo EULA';
  @override
  String get PrivacyPolicy => 'Politica sulla riservatezza';
  @override
  String get Feed => 'Alimentare';
  @override
  String get Post => 'Inviare';
  @override
  String get Gallery => 'Galleria';
  @override
  String get Stories => 'Storie';
  @override
  String get MyPosts => 'I miei messaggi';
  @override
  String get Favorites => 'Preferiti';
  @override
  String get Announcement => 'Annuncio';
  @override
  String get WhatOnMind => 'Cosa condividere?';
  @override
  String get Likes => 'Piace';
  @override
  String get Comments => 'Commenti';
  @override
  String get CreatePost => 'Crea messaggio';
  @override
  String get Share => 'Condividere';
  @override
  String get Edit => 'Modificare';
  @override
  String get Delete => 'Eliminare';
  @override
  String get Copy => 'Copia';
  @override
  String get Copied => 'Copiato';
  @override
  String get ReadMore => '...Leggi di più';
  @override
  String get SuccessPostPublish => 'Il post è stato pubblicato con successo';
  @override
  String get SuccessPostEdited => 'Il post è stato modificato con successo';
  @override
  String get TypeComment => 'Digita un commento...';
  @override
  String get TypeReply => 'Digita una risposta...';
  @override
  String get NotAllowedToPublish =>
      'Scusa! Non sei più autorizzato a pubblicare';
  @override
  String get NotAllowedToComment => 'Scusa! Non sei autorizzato a commentare';
  @override
  String get PostRemoveConfirm => 'Sei sicuro di voler eliminare questo post?';
  @override
  String get CommentRemoveConfirm =>
      'Sei sicuro di voler eliminare questo commento?';
  @override
  String get AddSomeContent => 'Aggiungi alcuni contenuti prima di pubblicare';
  @override
  String get Reply => 'Rispondere';
  @override
  String get Replies => 'Risposte';
  @override
  String get RepliedToComment => 'risposto al tuo commento';
  @override
  String get Chats => 'Chat';
  @override
  String get OnlineUsers => 'UTENTI ONLINE';
  @override
  String get RecentChats => 'CHAT RECENTI';
  @override
  String get RemoveConversation => 'Rimuovi conversazione';
  @override
  String get Messaging => 'Messaggio...';
  @override
  String get CannotChatWithUser => 'Scusa! Non puoi chattare con questo utente';
  @override
  String get MsgDeleteConfirm => 'Sei sicuro di voler eliminare il messaggio?';
  @override
  String get NoChatFound => 'Nessuna chat recente ancora, inizia a chattare!';
  @override
  String get Online => 'In linea';
  @override
  String get Typing => 'Digitando...';
  @override
  String get Image => 'Immagine';
  @override
  String get Voice => 'Voce';
  @override
  String get Video => 'Video';
  @override
  String get Emoji => 'Emoticon';
  @override
  String get GIF => 'GIF';
  @override
  String get Sticker => 'Etichetta';
  @override
  String get Groups => 'Gruppi';
  @override
  String get CreateGroup => 'Crea un gruppo';
  @override
  String get EditGroup => 'Modifica il tuo gruppo';
  @override
  String get Members => 'Membri';
  @override
  String get PhotosLibrary => 'Libreria Foto';
  @override
  String get TakePicture => 'Fare una foto';
  @override
  String get VideosLibrary => 'Libreria video';
  @override
  String get RecordVideo => 'Registrare video';
  @override
  String get Cancel => 'Annulla';
  @override
  String get SlideToCancel => 'Scorri per annullare';
  @override
  String get On => 'SU';
  @override
  String get Off => 'Spento';
  @override
  String get Group => 'Gruppo';
  @override
  String get LeaveGroup => 'Lascia il gruppo';
  @override
  String get GroupName => 'Nome del gruppo';
  @override
  String get Join => 'Giuntura';
  @override
  String get Joined => 'Iscritto';
  @override
  String get Public => 'Pubblico';
  @override
  String get Private => 'Privato';
  @override
  String get GroupType => 'Tipo di gruppo';
  @override
  String get RemoveMember => 'Rimuovi membro';
  @override
  String get AddMembers => 'Aggiungi membri';
  @override
  String get Block => 'Bloccare';
  @override
  String get Unblock => 'Sbloccare';
  @override
  String get Ban => 'Bandire';
  @override
  String get Unban => 'Annullato';
  @override
  String get Banned => 'Vietato';
  @override
  String get Newest => 'Il più recente';
  @override
  String get Trending => 'Di tendenza';
  @override
  String get MostDownloaded => 'Il più scaricato';
  @override
  String get Categories => 'Categorie';
  @override
  String get AddWallpaper => 'Aggiungi sfondo';
  @override
  String get WallpaperName => 'Nome carta da parati';
  @override
  String get Category => 'Categoria';
  @override
  String get Upload => 'Caricamento';
  @override
  String get Home => 'Casa';
  @override
  String get Lock => 'Serratura';
  @override
  String get Both => 'Tutti e due';
  @override
  String get SetAsWallpaper => 'Imposta come sfondo';
  @override
  String get WallpaperSet => 'Impostazione di sfondi con successo';
  @override
  String get FailedToUpload =>
      'Oops! Caricamento non riuscito, per favore riprova';
  @override
  String get UploadFinished => 'Caricamento terminato';
  @override
  String get Downloading => 'Download';
  @override
  String get NoWallpaperSelectedMsg => 'Seleziona lo sfondo per continuare';
  @override
  String get NoImgSelected => 'Nessuna immagine selezionata';
  @override
  String get Notifications => 'Notifiche';
  @override
  String get StartFollowingMsg => 'iniziato a seguirti';
  @override
  String get PostReactionMsg => 'ha reagito al tuo post';
  @override
  String get PostCommentMsg => 'ha commentato il tuo post';
  @override
  String get ReplyMsg => 'risposto al tuo commento';
  @override
  String get CommentReactedMsg => 'ha reagito al tuo commento';
  @override
  String get CannotFindFile => 'Oops! Impossibile trovare il file';
  @override
  String get NoFilePicked =>
      'Tentativo di caricamento e nessun file selezionato';
  @override
  String get SentYouMsg => 'Ti ho mandato un messaggio';
  @override
  String get Report => 'Rapporto';
  @override
  String get Unreport => 'Non denunciare';
  @override
  String get ReportDesc => 'Rimuoviamo il post che ha: ';
  @override
  String get ReportReasons =>
      '⚫️ Contenuti a sfondo sessuale. \n\n⚫️ Contenuti violenti o ripugnanti. \n\n⚫️ Contenuti odiosi o offensivi. \n\n⚫️ Spam o fuorvianti.';
  @override
  String get ReportNote => 'Non li faremo sapere se intraprendi questa azione.';
  @override
  String get ReportThanks =>
      'Controlleremo la tua richiesta, grazie per aver contribuito a migliorare la nostra comunità';
  @override
  String get Admin => 'Amministratore';
  @override
  String get ProfanityDetected =>
      'Sono state rilevate parolacce, il tuo account potrebbe essere sospeso!';
  @override
  String get AreYouSure => 'Sei sicuro di';
  @override
  String get ConfirmChatDeletion => 'Sei sicuro di eliminare la chat';
  @override
  String get ReportedPosts => 'Post segnalati';
  @override
  String get AllChats => 'Tutte le chat';
  @override
  String get Users => 'Utenti';
  @override
  String get TermsService => 'Termini di servizio';
  @override
  String get WaitingToRecord => 'In attesa di registrare';
  @override
  String get AdjustVolume => 'Regola volume';
  @override
  String get AdjustSpeed => 'Regola velocità';
  @override
  String get Ok => 'Ok';
  @override
  String get ShareExternally => 'Condividi esternamente';
  @override
  String get ShareOnYourWall => 'Condividi sulla tua bacheca';
  @override
  String get Error => 'Errore';
  @override
  String get Me => 'Me';
  @override
  String get OopsCode => 'Oops! il codice di verifica è sbagliato';
  @override
  String get OopsNetwork =>
      'Oops! Qualcosa è andato storto, per favore controlla la tua rete!';
  @override
  String get OopsEmailorPassword =>
      'Oops! L\'e-mail o la password non sono corrette!';
  @override
  String get OopsNotRegister =>
      'Oops! nessun utente trovato, in caso contrario registrati considera prima la registrazione!';
  @override
  String get OopsEmailUsed =>
      'Oops! Questa email è già utilizzata da un altro account!';
  @override
  String get OopsPhoneFormat => 'Oops! Il formato del telefono non è valido';
  @override
  String get NoAudioRecorded => 'Nessun audio aggiunto';
  @override
  String get OopsWrong => 'Oops! Qualcosa è andato storto';
  @override
  String get Optional => 'Opzionale';
  @override
  String get Sending => 'Invio...';
  @override
  String get NotificationPermission => 'È garantito il permesso di notifica';
  @override
  String get Theme => 'Tema';
  @override
  String get DarkMode => 'Modalità scura';
  @override
  String get Others => 'Altri';
  @override
  String get PeopleWhoReacted => 'Persone che hanno reagito';
  @override
  String get BadWords =>
      'Sono state rilevate parolacce, il tuo account potrebbe essere sospeso!';
  @override
  String get PickFile => 'Scegli file';
  @override
  String get Loading => 'Caricamento in corso...';
  @override
  String get PleaseAddAudio => 'Aggiungi audio';
  @override
  String get PeopleYouMayFollow => 'Persone che puoi seguire';
  @override
  String get Unsave => 'Non salvare';
  @override
  String get PostsLikes => 'Pubblica Mi piace';
  @override
  String get CommentsLikes => 'Commenti Mi piace';
  @override
  String get RepliesLikes => 'Risposte Mi piace';
  @override
  String get ShareiConfezz => 'Condividi iConfezz';
  @override
  String get MemberSince => 'Membro da';
  @override
  String get Comment => 'Commento';
  @override
  String get UnShare => 'Annulla condivisione';
  @override
  String get Liked => 'È piaciuto';
  @override
  String get Like => 'Piace';
  @override
  String get SAVED => 'SALVATO';
  @override
  String get SHARED => 'CONDIVISA';
  @override
  String get AppLanguage => 'Lingua dell\'app';
  @override
  String get ComingSoon => 'Prossimamente';
  @override
  String get ReportProblem => 'Segnala un problema';
  @override
  String get PushNotifications => 'Le notifiche push';
  @override
  String get ShareProfile => 'Condividi profilo';
  @override
  String get DeleteAccount => 'Eliminare l\'account';
  @override
  String get DeleteYourAccount => 'Cancella il tuo account';
  @override
  String get Record => 'Disco';
  @override
  String get Or => 'O';
  @override
  String get NoPhotoPicked => 'Nessuna foto selezionata';
  @override
  String get SearchForUsers => 'Cerca utenti';
  @override
  String get NoResultsFound => 'Nessun risultato trovato';
  @override
  String get TemporaryFilesRemoved => 'File temporanei rimossi con successo';
  @override
  String get FailedToClean => 'Impossibile pulire i file temporanei';
  @override
  String get exit => 'Uscita';
  @override
  String get CommunityGuidelines => 'Linee guida comunitarie';
  @override
  String get FAQ => 'FAQ';
  @override
  String get Paste => 'Paste';
  @override
  String get DoYouWantToPasteThisCode => 'Do you want to paste this code?';
  @override
  String get PasteCode => 'Paste Code';
}

// Path: <root>
class _StringsJa implements _StringsEn {
  /// You can call this constructor and build your own translation instance of this locale.
  /// Constructing via the enum [AppLocale.build] is preferred.
  _StringsJa.build();

  /// Access flat map
  @override
  dynamic operator [](String key) => _flatMap[key];

  // Internal flat map initialized lazily
  @override
  late final Map<String, dynamic> _flatMap = _buildFlatMap();

  @override
  late final _StringsJa _root = this; // ignore: unused_field

  // Translations
  @override
  String get AppName => 'iConfezz';
  @override
  String get AboutApp => 'iConfezzは他の人の声を聞くことに専念しているコミュニティです。';
  @override
  String get AppVersion => '2.0.0';
  @override
  String get UnknownError => '何かがうまくいかなかった';
  @override
  String get Next => '次';
  @override
  String get EmailUser => 'あなたはユーザーにメールを送信しますか,ここにログインしてください！';
  @override
  String get Email => 'Eメール';
  @override
  String get bio => 'バイオグラフィー';
  @override
  String get city => '街';
  @override
  String get dob => '生年月日';
  @override
  String get Password => 'パスワード';
  @override
  String get Login => 'ログイン';
  @override
  String get PhoneNumber => '電話番号';
  @override
  String get PhoneVerification => '電話番号の確認';
  @override
  String get EnterCode => 'に送信されたコードを入力してください';
  @override
  String get DidntRecieveCode => 'コードを受け取りませんでしたか？';
  @override
  String get InvalidSMSCode => 'SMS確認コードが無効です';
  @override
  String get RESEND => '再送';
  @override
  String get VERIFY => '確認';
  @override
  String get POSTS => 'POSTS';
  @override
  String get FOLLOWERS => 'フォロワー';
  @override
  String get FOLLOWINGS => '以下';
  @override
  String get Follow => '従う';
  @override
  String get Following => '続く';
  @override
  String get Register => '登録';
  @override
  String get Username => 'ユーザー名';
  @override
  String get FirstName => 'ファーストネーム';
  @override
  String get LastName => '苗字';
  @override
  String get FullName => 'フルネーム';
  @override
  String get Status => '状態';
  @override
  String get AddValidUsername => '有効なユーザー名を追加してください';
  @override
  String get AddValidFirstName => '有効な名を追加してください';
  @override
  String get AddValidLastName => '有効な姓を追加してください';
  @override
  String get SelectGender => 'あなたの性別を選択してください。';
  @override
  String get Male => '男';
  @override
  String get Female => '女性';
  @override
  String get Other => '他の';
  @override
  String get AcceptTerms => '私はすべての利用規約に同意します';
  @override
  String get Save => '保存する';
  @override
  String get Search => '探す';
  @override
  String get Searching => '検索中...';
  @override
  String get SelectPhoneCode => '電話コードを選択してください';
  @override
  String get InvalidEmail => '無効なメール';
  @override
  String get ShortPassword => 'パスワードには少なくとも8文字が含まれている必要があります';
  @override
  String get UsernameTaken => 'ユーザー名は既に使われています';
  @override
  String get InvalidPhone => 'すべてのセルを適切に埋めてください';
  @override
  String get Message => 'メッセージ';
  @override
  String get LoginFirst => 'おっと,最初にログインしてください';
  @override
  String get START => '始めましょう';
  @override
  String get Wallpapers => '壁紙';
  @override
  String get Profile => 'プロフィール';
  @override
  String get NoInternet => 'インターネットアクセスなし';
  @override
  String get Settings => '設定';
  @override
  String get Account => 'アカウント';
  @override
  String get EditProfile => 'プロファイル編集';
  @override
  String get OnlineStatus => 'オンラインステータス';
  @override
  String get OnlineDescription => 'あなたが最後にオンラインになったときは誰でも見ることができます';
  @override
  String get Logout => 'ログアウト';
  @override
  String get DirectMsgs => 'チャットメッセージ';
  @override
  String get DirectMsgsDescr => 'チャット通知を受信する';
  @override
  String get GroupsMsgs => 'グループメッセージ';
  @override
  String get GroupsMsgsDesc => 'すべてのグループ通知を受け取る';
  @override
  String get AddGroupName => 'お願い！グループ名を追加してください';
  @override
  String get About => '私たちに関しては';
  @override
  String get RateUs => '私たちを評価してください';
  @override
  String get EULA => 'EULA契約';
  @override
  String get PrivacyPolicy => 'プライバシーポリシー';
  @override
  String get Feed => '餌';
  @override
  String get Post => '役職';
  @override
  String get Gallery => 'ギャラリー';
  @override
  String get Stories => 'ストーリー';
  @override
  String get MyPosts => '私の投稿';
  @override
  String get Favorites => 'お気に入り';
  @override
  String get Announcement => '発表';
  @override
  String get WhatOnMind => '何を共有しますか？';
  @override
  String get Likes => 'いいね';
  @override
  String get Comments => 'コメント';
  @override
  String get CreatePost => '投稿を作成';
  @override
  String get Share => 'シェア';
  @override
  String get Edit => '編集';
  @override
  String get Delete => '消去';
  @override
  String get Copy => 'コピー';
  @override
  String get Copied => 'コピー';
  @override
  String get ReadMore => '...続きを読む';
  @override
  String get SuccessPostPublish => '投稿は正常に公開されました';
  @override
  String get SuccessPostEdited => '投稿は正常に編集されました';
  @override
  String get TypeComment => 'コメントを入力してください...';
  @override
  String get TypeReply => '返信を入力してください...';
  @override
  String get NotAllowedToPublish => '申し訳ありませんが,もう公開することはできません';
  @override
  String get NotAllowedToComment => 'ごめんなさい！コメントは許されません';
  @override
  String get PostRemoveConfirm => 'この投稿を削除してもよろしいですか？';
  @override
  String get CommentRemoveConfirm => 'このコメントを削除してもよろしいですか？';
  @override
  String get AddSomeContent => '投稿する前にコンテンツを追加してください';
  @override
  String get Reply => '返事';
  @override
  String get Replies => '返信';
  @override
  String get RepliedToComment => 'あなたのコメントに返信しました';
  @override
  String get Chats => 'チャット';
  @override
  String get OnlineUsers => 'オンラインユーザー';
  @override
  String get RecentChats => '最近のチャット';
  @override
  String get RemoveConversation => '会話を削除';
  @override
  String get Messaging => 'メッセージ...';
  @override
  String get CannotChatWithUser => '申し訳ありませんが,このユーザーとチャットすることはできません';
  @override
  String get MsgDeleteConfirm => 'メッセージを削除してもよろしいですか？';
  @override
  String get NoChatFound => '最近のチャットはまだありません。チャットを開始してください！';
  @override
  String get Online => 'オンライン';
  @override
  String get Typing => 'タイピング...';
  @override
  String get Image => '画像';
  @override
  String get Voice => '声';
  @override
  String get Video => 'ビデオ';
  @override
  String get Emoji => '絵文字';
  @override
  String get GIF => 'GIF';
  @override
  String get Sticker => 'ステッカー';
  @override
  String get Groups => 'グループ';
  @override
  String get CreateGroup => 'グループを作成する';
  @override
  String get EditGroup => 'グループを編集';
  @override
  String get Members => 'メンバー';
  @override
  String get PhotosLibrary => '写真ライブラリ';
  @override
  String get TakePicture => '写真を撮る';
  @override
  String get VideosLibrary => 'ビデオライブラリ';
  @override
  String get RecordVideo => '録画映像';
  @override
  String get Cancel => 'キャンセル';
  @override
  String get SlideToCancel => 'スライドしてキャンセル';
  @override
  String get On => 'の上';
  @override
  String get Off => 'オフ';
  @override
  String get Group => 'グループ';
  @override
  String get LeaveGroup => 'グループを離れる';
  @override
  String get GroupName => 'グループ名';
  @override
  String get Join => '加入';
  @override
  String get Joined => '参加';
  @override
  String get Public => '公衆';
  @override
  String get Private => 'プライベート';
  @override
  String get GroupType => 'グループタイプ';
  @override
  String get RemoveMember => 'メンバーを削除';
  @override
  String get AddMembers => 'メンバーを追加';
  @override
  String get Block => 'ブロック';
  @override
  String get Unblock => 'ブロック解除';
  @override
  String get Ban => '禁止';
  @override
  String get Unban => '禁止解除';
  @override
  String get Banned => '禁止された';
  @override
  String get Newest => '最新';
  @override
  String get Trending => 'トレンド';
  @override
  String get MostDownloaded => '最もダウンロードされた';
  @override
  String get Categories => 'カテゴリ';
  @override
  String get AddWallpaper => '壁紙を追加';
  @override
  String get WallpaperName => '壁紙名';
  @override
  String get Category => 'カテゴリー';
  @override
  String get Upload => 'アップロード';
  @override
  String get Home => '家';
  @override
  String get Lock => 'ロック';
  @override
  String get Both => '両方';
  @override
  String get SetAsWallpaper => '壁紙として設定';
  @override
  String get WallpaperSet => '壁紙セットに成功しました';
  @override
  String get FailedToUpload => 'おっと！アップロードに失敗しました。もう一度やり直してください';
  @override
  String get UploadFinished => 'アップロードが完了しました';
  @override
  String get Downloading => 'ダウンロード';
  @override
  String get NoWallpaperSelectedMsg => '続行するには壁紙を選択してください';
  @override
  String get NoImgSelected => '画像が選択されていません';
  @override
  String get Notifications => '通知';
  @override
  String get StartFollowingMsg => 'あなたをフォロー始めました';
  @override
  String get PostReactionMsg => 'あなたの投稿に反応しました';
  @override
  String get PostCommentMsg => 'あなたの投稿にコメントしました';
  @override
  String get ReplyMsg => 'あなたのコメントに返信しました';
  @override
  String get CommentReactedMsg => 'あなたのコメントに反応しました';
  @override
  String get CannotFindFile => 'おっと！ファイルが見つかりません';
  @override
  String get NoFilePicked => 'アップロードしようとしましたが,ファイルが選択されていません';
  @override
  String get SentYouMsg => 'メッセージを送信しました';
  @override
  String get Report => '報告する';
  @override
  String get Unreport => '報告しない';
  @override
  String get ReportDesc => '次のような投稿を削除します：';
  @override
  String get ReportReasons =>
      '⚫️性的なコンテンツ。\n\n⚫️暴力的または反発的なコンテンツ。\n\n⚫️憎悪的または虐待的なコンテンツ。\n\n⚫️スパムまたは誤解を招くコンテンツ。';
  @override
  String get ReportNote => 'あなたがこの行動をとった場合,私たちは彼らに知らせません。';
  @override
  String get ReportThanks => '私たちはあなたの要求をチェックします,私たちのコミュニティを改善するのを手伝ってくれてありがとう';
  @override
  String get Admin => '管理者';
  @override
  String get ProfanityDetected => '不適切な言葉が検出された場合,アカウントが停止される可能性があります！';
  @override
  String get AreYouSure => 'よろしいですか';
  @override
  String get ConfirmChatDeletion => 'チャットを削除してもよろしいですか';
  @override
  String get ReportedPosts => '報告された投稿';
  @override
  String get AllChats => 'すべてのチャット';
  @override
  String get Users => 'ユーザー';
  @override
  String get TermsService => '利用規約';
  @override
  String get WaitingToRecord => '録音待ち';
  @override
  String get AdjustVolume => '音量調節';
  @override
  String get AdjustSpeed => '速度を調整する';
  @override
  String get Ok => 'Ok';
  @override
  String get ShareExternally => '外部共有';
  @override
  String get ShareOnYourWall => 'あなたの壁で共有する';
  @override
  String get Error => 'エラー';
  @override
  String get Me => '自分';
  @override
  String get OopsCode => 'おっと！veficationコードが間違っています';
  @override
  String get OopsNetwork => 'おっと！何か問題が発生しました。ネットワークを確認してください！';
  @override
  String get OopsEmailorPassword => 'おっと！メールアドレスまたはパスワードが正しくありません！';
  @override
  String get OopsNotRegister =>
      'おっと！ユーザーが見つかりません。登録しない場合は,最初にサインアップすることを検討してください！';
  @override
  String get OopsEmailUsed => 'おっと！このメールはすでに別のアカウントで使用されています！';
  @override
  String get OopsPhoneFormat => 'おっと！電話のフォーマットが無効です';
  @override
  String get NoAudioRecorded => '音声が追加されていません';
  @override
  String get OopsWrong => 'おっと！何かが間違っていた';
  @override
  String get Optional => 'オプション';
  @override
  String get Sending => '送信... ';
  @override
  String get NotificationPermission => '通知許可が保証されます';
  @override
  String get Theme => 'テーマ';
  @override
  String get DarkMode => 'ダークモード';
  @override
  String get Others => 'その他';
  @override
  String get PeopleWhoReacted => '反応した人';
  @override
  String get BadWords => '不適切な言葉が検出された場合,アカウントが停止される可能性があります！';
  @override
  String get PickFile => 'ファイルを選択';
  @override
  String get Loading => '読み込み中...';
  @override
  String get PleaseAddAudio => '音声を追加してください';
  @override
  String get PeopleYouMayFollow => 'あなたがフォローするかもしれない人々';
  @override
  String get Unsave => '保存解除';
  @override
  String get PostsLikes => 'いいね！';
  @override
  String get CommentsLikes => 'コメントが好き';
  @override
  String get RepliesLikes => 'いいね';
  @override
  String get ShareiConfezz => 'iConfezzを共有する';
  @override
  String get MemberSince => '以来のメンバー';
  @override
  String get Comment => 'コメント';
  @override
  String get UnShare => '共有解除';
  @override
  String get Liked => 'いいね';
  @override
  String get Like => '好き';
  @override
  String get SAVED => '保存済み';
  @override
  String get SHARED => '共有';
  @override
  String get AppLanguage => 'アプリ言語';
  @override
  String get ComingSoon => '近日公開';
  @override
  String get ReportProblem => '問題を報告します';
  @override
  String get PushNotifications => 'プッシュ通知';
  @override
  String get ShareProfile => 'プロフィールを共有する';
  @override
  String get DeleteAccount => 'アカウントを削除する';
  @override
  String get DeleteYourAccount => 'アカウントを削除';
  @override
  String get Record => '記録';
  @override
  String get Or => 'または';
  @override
  String get NoPhotoPicked => '写真が選択されていません';
  @override
  String get SearchForUsers => 'ユーザーの検索';
  @override
  String get NoResultsFound => '結果が見つかりません';
  @override
  String get TemporaryFilesRemoved => '一時ファイルは正常に削除されました';
  @override
  String get FailedToClean => '一時ファイルのクリーンアップに失敗しました';
  @override
  String get exit => '出口';
  @override
  String get CommunityGuidelines => 'コミュニティガイドライン';
  @override
  String get FAQ => 'よくある質問';
  @override
  String get Paste => 'Paste';
  @override
  String get DoYouWantToPasteThisCode => 'Do you want to paste this code?';
  @override
  String get PasteCode => 'Paste Code';
}

// Path: <root>
class _StringsKo implements _StringsEn {
  /// You can call this constructor and build your own translation instance of this locale.
  /// Constructing via the enum [AppLocale.build] is preferred.
  _StringsKo.build();

  /// Access flat map
  @override
  dynamic operator [](String key) => _flatMap[key];

  // Internal flat map initialized lazily
  @override
  late final Map<String, dynamic> _flatMap = _buildFlatMap();

  @override
  late final _StringsKo _root = this; // ignore: unused_field

  // Translations
  @override
  String get AppName => '아이컨페즈';
  @override
  String get AboutApp => 'iConfezz는 다른 사람들의 목소리를 들을 수 있는 커뮤니티입니다.';
  @override
  String get AppVersion => '2.0.0';
  @override
  String get UnknownError => '뭔가 잘못됐다';
  @override
  String get Next => '다음';
  @override
  String get EmailUser => '이메일 사용자입니까, 여기에 로그인하십시오!';
  @override
  String get Email => '이메일';
  @override
  String get bio => '전기';
  @override
  String get city => '도시';
  @override
  String get dob => '생일';
  @override
  String get Password => '비밀번호';
  @override
  String get Login => '로그인';
  @override
  String get PhoneNumber => '전화 번호';
  @override
  String get PhoneVerification => '전화번호 인증';
  @override
  String get EnterCode => '로 전송된 코드를 입력하십시오';
  @override
  String get DidntRecieveCode => '코드를 받지 못하셨나요?';
  @override
  String get InvalidSMSCode => 'SMS 인증 코드가 유효하지 않습니다';
  @override
  String get RESEND => '재전송';
  @override
  String get VERIFY => '확인';
  @override
  String get POSTS => '포스트';
  @override
  String get FOLLOWERS => '팔로워';
  @override
  String get FOLLOWINGS => '다음';
  @override
  String get Follow => '따르다';
  @override
  String get Following => '수행원';
  @override
  String get Register => '등록하다';
  @override
  String get Username => '사용자 이름';
  @override
  String get FirstName => '이름';
  @override
  String get LastName => '성';
  @override
  String get FullName => '이름';
  @override
  String get Status => '상태';
  @override
  String get AddValidUsername => '유효한 사용자 이름 추가';
  @override
  String get AddValidFirstName => '유효한 이름 추가';
  @override
  String get AddValidLastName => '유효한 성을 추가하십시오';
  @override
  String get SelectGender => '당신의 성별을 선택 해주세요.';
  @override
  String get Male => '남성';
  @override
  String get Female => '여자';
  @override
  String get Other => '다른';
  @override
  String get AcceptTerms => '모든 이용약관에 동의합니다';
  @override
  String get Save => '구하다';
  @override
  String get Search => '검색';
  @override
  String get Searching => '수색...';
  @override
  String get SelectPhoneCode => '전화번호 선택';
  @override
  String get InvalidEmail => '잘못된 이메일';
  @override
  String get ShortPassword => '비밀번호는 8자 이상이어야 합니다';
  @override
  String get UsernameTaken => '이미 사용중인 이름입니다';
  @override
  String get InvalidPhone => '모든 셀을 제대로 채워주세요';
  @override
  String get Message => '메시지';
  @override
  String get LoginFirst => '아, 먼저 로그인하십시오';
  @override
  String get START => '시작하다';
  @override
  String get Wallpapers => '배경 화면';
  @override
  String get Profile => '프로필';
  @override
  String get NoInternet => '인터넷에 액세스할 수 없음';
  @override
  String get Settings => '설정';
  @override
  String get Account => '계정';
  @override
  String get EditProfile => '프로필 편집';
  @override
  String get OnlineStatus => '온라인 상태';
  @override
  String get OnlineDescription => '당신이 언제 마지막 온라인인지 누구나 볼 수 있습니다';
  @override
  String get Logout => '로그 아웃';
  @override
  String get DirectMsgs => '채팅 메시지';
  @override
  String get DirectMsgsDescr => '채팅 알림 받기';
  @override
  String get GroupsMsgs => '그룹 메시지';
  @override
  String get GroupsMsgsDesc => '모든 그룹 알림 수신';
  @override
  String get AddGroupName => '제발! 그룹 이름 추가';
  @override
  String get About => '우리에 대해';
  @override
  String get RateUs => '우리 평가';
  @override
  String get EULA => 'EULA 계약';
  @override
  String get PrivacyPolicy => '개인 정보 정책';
  @override
  String get Feed => '먹이다';
  @override
  String get Post => '게시하다';
  @override
  String get Gallery => '갤러리';
  @override
  String get Stories => '이야기';
  @override
  String get MyPosts => '내 게시물';
  @override
  String get Favorites => '즐겨찾기';
  @override
  String get Announcement => '발표';
  @override
  String get WhatOnMind => '무엇을 공유할 것인가?';
  @override
  String get Likes => '좋아요';
  @override
  String get Comments => '코멘트';
  @override
  String get CreatePost => '게시물 작성';
  @override
  String get Share => '공유하다';
  @override
  String get Edit => '편집하다';
  @override
  String get Delete => '삭제';
  @override
  String get Copy => '복사';
  @override
  String get Copied => '복사';
  @override
  String get ReadMore => '...자세히 보기';
  @override
  String get SuccessPostPublish => '게시물이 성공적으로 게시되었습니다';
  @override
  String get SuccessPostEdited => '게시물이 성공적으로 편집되었습니다';
  @override
  String get TypeComment => '댓글을 입력하세요...';
  @override
  String get TypeReply => '답장을 입력하세요...';
  @override
  String get NotAllowedToPublish => '죄송합니다! 더 이상 게시할 수 없습니다.';
  @override
  String get NotAllowedToComment => '죄송합니다! 댓글을 달 수 없습니다.';
  @override
  String get PostRemoveConfirm => '이 게시물을 삭제하시겠습니까?';
  @override
  String get CommentRemoveConfirm => '이 댓글을 삭제하시겠습니까?';
  @override
  String get AddSomeContent => '게시하기 전에 일부 콘텐츠를 추가하십시오';
  @override
  String get Reply => '회신하다';
  @override
  String get Replies => '답장';
  @override
  String get RepliedToComment => '댓글에 답장';
  @override
  String get Chats => '채팅';
  @override
  String get OnlineUsers => '온라인 사용자';
  @override
  String get RecentChats => '최근 채팅';
  @override
  String get RemoveConversation => '대화 제거';
  @override
  String get Messaging => '메시지...';
  @override
  String get CannotChatWithUser => '죄송합니다! 이 사용자와 채팅할 수 없습니다';
  @override
  String get MsgDeleteConfirm => '정말 메시지를 삭제하시겠습니까?';
  @override
  String get NoChatFound => '아직 최근 채팅이 없습니다. 채팅을 시작하세요!';
  @override
  String get Online => '온라인';
  @override
  String get Typing => '타자...';
  @override
  String get Image => '영상';
  @override
  String get Voice => '목소리';
  @override
  String get Video => '동영상';
  @override
  String get Emoji => '이모티콘';
  @override
  String get GIF => 'GIF';
  @override
  String get Sticker => '상표';
  @override
  String get Groups => '여러 떼';
  @override
  String get CreateGroup => '그룹 만들기';
  @override
  String get EditGroup => '그룹 편집';
  @override
  String get Members => '회원';
  @override
  String get PhotosLibrary => '사진 라이브러리';
  @override
  String get TakePicture => '사진을 촬영';
  @override
  String get VideosLibrary => '비디오 라이브러리';
  @override
  String get RecordVideo => '비디오 녹화';
  @override
  String get Cancel => '취소';
  @override
  String get SlideToCancel => '슬라이드하여 취소';
  @override
  String get On => '에';
  @override
  String get Off => '끄다';
  @override
  String get Group => '그룹';
  @override
  String get LeaveGroup => '그룹을 떠나다';
  @override
  String get GroupName => '그룹 이름';
  @override
  String get Join => '가입하다';
  @override
  String get Joined => '가입';
  @override
  String get Public => '공공의';
  @override
  String get Private => '사적인';
  @override
  String get GroupType => '그룹 유형';
  @override
  String get RemoveMember => '멤버 제거';
  @override
  String get AddMembers => '구성원 추가';
  @override
  String get Block => '차단하다';
  @override
  String get Unblock => '차단 해제';
  @override
  String get Ban => '반';
  @override
  String get Unban => '해제';
  @override
  String get Banned => '금지';
  @override
  String get Newest => '최신';
  @override
  String get Trending => '트렌딩';
  @override
  String get MostDownloaded => '가장 많이 다운로드됨';
  @override
  String get Categories => '카테고리';
  @override
  String get AddWallpaper => '배경 화면 추가';
  @override
  String get WallpaperName => '배경 화면 이름';
  @override
  String get Category => '범주';
  @override
  String get Upload => '업로드';
  @override
  String get Home => '집';
  @override
  String get Lock => '잠그다';
  @override
  String get Both => '둘 다';
  @override
  String get SetAsWallpaper => '배경화면으로 설정';
  @override
  String get WallpaperSet => '배경 화면이 성공적으로 설정되었습니다';
  @override
  String get FailedToUpload => '죄송합니다! 업로드에 실패했습니다. 다시 시도하십시오.';
  @override
  String get UploadFinished => '업로드 완료';
  @override
  String get Downloading => '다운로드';
  @override
  String get NoWallpaperSelectedMsg => '계속하려면 배경 화면을 선택하십시오';
  @override
  String get NoImgSelected => '선택한 이미지가 없습니다';
  @override
  String get Notifications => '알림';
  @override
  String get StartFollowingMsg => '당신을 팔로우하기 시작했습니다';
  @override
  String get PostReactionMsg => '당신의 게시물에 반응했습니다';
  @override
  String get PostCommentMsg => '당신의 게시물에 댓글을 달았습니다';
  @override
  String get ReplyMsg => '댓글에 답장';
  @override
  String get CommentReactedMsg => '당신의 의견에 반응했습니다';
  @override
  String get CannotFindFile => '죄송합니다! 파일을 찾을 수 없습니다';
  @override
  String get NoFilePicked => '업로드하려고 하는데 파일이 선택되지 않았습니다';
  @override
  String get SentYouMsg => '메시지를 보냈습니다';
  @override
  String get Report => '보고서';
  @override
  String get Unreport => '신고 취소';
  @override
  String get ReportDesc => '다음이 있는 게시물을 제거합니다. ';
  @override
  String get ReportReasons =>
      '⚫️ 성적인 콘텐츠. \n\n⚫️ 폭력적이거나 혐오스러운 콘텐츠. \n\n⚫️ 증오 또는 모욕적인 콘텐츠. \n\n⚫️ 스팸 또는 오해의 소지가 있는 콘텐츠.';
  @override
  String get ReportNote => '당신이 이 조치를 취하면 우리는 그들에게 알리지 않을 것입니다.';
  @override
  String get ReportThanks => '요청을 확인하겠습니다. 커뮤니티 개선에 도움을 주셔서 감사합니다.';
  @override
  String get Admin => '관리자';
  @override
  String get ProfanityDetected => '나쁜 단어가 감지되면 계정이 정지될 수 있습니다!';
  @override
  String get AreYouSure => '확실히';
  @override
  String get ConfirmChatDeletion => '채팅을 삭제하시겠습니까?';
  @override
  String get ReportedPosts => '신고된 게시물';
  @override
  String get AllChats => '모든 채팅';
  @override
  String get Users => '사용자';
  @override
  String get TermsService => '서비스 약관';
  @override
  String get WaitingToRecord => '녹화 대기 중';
  @override
  String get AdjustVolume => '볼륨 조정';
  @override
  String get AdjustSpeed => '속도 조정';
  @override
  String get Ok => '확인';
  @override
  String get ShareExternally => '외부로 공유';
  @override
  String get ShareOnYourWall => '벽에 공유';
  @override
  String get Error => '오류';
  @override
  String get Me => '나';
  @override
  String get OopsCode => '죄송합니다! 인증 코드가 잘못되었습니다';
  @override
  String get OopsNetwork => '죄송합니다! 문제가 발생했습니다. 네트워크를 확인하십시오!';
  @override
  String get OopsEmailorPassword => '죄송합니다! 이메일 또는 비밀번호가 올바르지 않습니다!';
  @override
  String get OopsNotRegister => '죄송합니다! 사용자가 없습니다. 등록하지 않은 경우 먼저 등록을 고려하십시오!';
  @override
  String get OopsEmailUsed => '죄송합니다! 이 이메일은 이미 다른 계정에서 사용 중입니다!';
  @override
  String get OopsPhoneFormat => '죄송합니다! 전화 형식이 잘못되었습니다';
  @override
  String get NoAudioRecorded => '오디오가 추가되지 않았습니다';
  @override
  String get OopsWrong => '아차! 문제가 발생했습니다';
  @override
  String get Optional => '선택 과목';
  @override
  String get Sending => '보내는 중...';
  @override
  String get NotificationPermission => '알림 권한이 부여됩니다';
  @override
  String get Theme => '주제';
  @override
  String get DarkMode => '다크 모드';
  @override
  String get Others => '기타';
  @override
  String get PeopleWhoReacted => '반응하는 사람들';
  @override
  String get BadWords => '나쁜 단어가 감지되면 계정이 정지될 수 있습니다!';
  @override
  String get PickFile => '파일 선택';
  @override
  String get Loading => '로드 중...';
  @override
  String get PleaseAddAudio => '오디오를 추가하십시오';
  @override
  String get PeopleYouMayFollow => '팔로우할 수 있는 사람들';
  @override
  String get Unsave => '저장 취소';
  @override
  String get PostsLikes => '게시물 좋아요';
  @override
  String get CommentsLikes => '댓글 좋아요';
  @override
  String get RepliesLikes => '좋아요 답글';
  @override
  String get ShareiConfezz => 'iConfezz 공유';
  @override
  String get MemberSince => '회원 가입일';
  @override
  String get Comment => '논평';
  @override
  String get UnShare => '공유 취소';
  @override
  String get Liked => '좋아요';
  @override
  String get Like => '처럼';
  @override
  String get SAVED => '저장됨';
  @override
  String get SHARED => '공유';
  @override
  String get AppLanguage => '앱 언어';
  @override
  String get ComingSoon => '곧';
  @override
  String get ReportProblem => '문제 보고';
  @override
  String get PushNotifications => '푸시 알림';
  @override
  String get ShareProfile => '프로필 공유';
  @override
  String get DeleteAccount => '계정 삭제';
  @override
  String get DeleteYourAccount => '계정 삭제';
  @override
  String get Record => '기록';
  @override
  String get Or => '또는';
  @override
  String get NoPhotoPicked => '선택한 사진이 없습니다';
  @override
  String get SearchForUsers => '사용자 검색';
  @override
  String get NoResultsFound => '검색 결과가 없습니다';
  @override
  String get TemporaryFilesRemoved => '임시 파일 제거 성공';
  @override
  String get FailedToClean => '임시 파일을 정리하지 못했습니다';
  @override
  String get exit => '출구';
  @override
  String get CommunityGuidelines => '커뮤니티 가이드라인';
  @override
  String get FAQ => '자주하는 질문';
  @override
  String get Paste => 'Paste';
  @override
  String get DoYouWantToPasteThisCode => 'Do you want to paste this code?';
  @override
  String get PasteCode => 'Paste Code';
}

// Path: <root>
class _StringsRu implements _StringsEn {
  /// You can call this constructor and build your own translation instance of this locale.
  /// Constructing via the enum [AppLocale.build] is preferred.
  _StringsRu.build();

  /// Access flat map
  @override
  dynamic operator [](String key) => _flatMap[key];

  // Internal flat map initialized lazily
  @override
  late final Map<String, dynamic> _flatMap = _buildFlatMap();

  @override
  late final _StringsRu _root = this; // ignore: unused_field

  // Translations
  @override
  String get AppName => 'АйКонфез';
  @override
  String get AboutApp =>
      'iConfezz — это сообщество, созданное для того, чтобы слышать голоса других';
  @override
  String get AppVersion => '2.0.0';
  @override
  String get UnknownError => 'Что-то пошло не так';
  @override
  String get Next => 'Следующий';
  @override
  String get EmailUser => 'Вы пользователь электронной почты, войдите здесь!';
  @override
  String get Email => 'Эл. адрес';
  @override
  String get bio => 'Биография';
  @override
  String get city => 'Город';
  @override
  String get dob => 'Дата рождения';
  @override
  String get Password => 'Пароль';
  @override
  String get Login => 'Авторизоваться';
  @override
  String get PhoneNumber => 'Телефонный номер';
  @override
  String get PhoneVerification => 'Подтверждение номера телефона';
  @override
  String get EnterCode => 'Введите код, отправленный на ';
  @override
  String get DidntRecieveCode => 'Не получили код?';
  @override
  String get InvalidSMSCode => 'Код подтверждения SMS недействителен';
  @override
  String get RESEND => 'ОТПРАВИТЬ';
  @override
  String get VERIFY => 'ПРОВЕРЯТЬ';
  @override
  String get POSTS => 'ПОСТЫ';
  @override
  String get FOLLOWERS => 'Последователи';
  @override
  String get FOLLOWINGS => 'Следования';
  @override
  String get Follow => 'Следовать';
  @override
  String get Following => 'Следующий';
  @override
  String get Register => 'Регистр';
  @override
  String get Username => 'Имя пользователя';
  @override
  String get FirstName => 'Имя';
  @override
  String get LastName => 'Фамилия';
  @override
  String get FullName => 'ФИО';
  @override
  String get Status => 'Статус';
  @override
  String get AddValidUsername => 'Добавить действительное имя пользователя';
  @override
  String get AddValidFirstName => 'Добавить действительное имя';
  @override
  String get AddValidLastName => 'Добавить действительную фамилию';
  @override
  String get SelectGender => 'Пожалуйста, выберите Ваш пол.';
  @override
  String get Male => 'Мужской';
  @override
  String get Female => 'Женский';
  @override
  String get Other => 'Другой';
  @override
  String get AcceptTerms => 'Я принимаю все условия и положения';
  @override
  String get Save => 'Сохранять';
  @override
  String get Search => 'Поиск';
  @override
  String get Searching => 'В поисках...';
  @override
  String get SelectPhoneCode => 'Выберите код телефона';
  @override
  String get InvalidEmail => 'Неверный адрес электронной почты';
  @override
  String get ShortPassword => 'Пароль должен содержать не менее 8 символов';
  @override
  String get UsernameTaken => 'Имя пользователя уже используется';
  @override
  String get InvalidPhone => 'Пожалуйста, заполните все ячейки правильно';
  @override
  String get Message => 'Сообщение';
  @override
  String get LoginFirst => 'К сожалению, сначала войдите в систему';
  @override
  String get START => 'НАЧАТЬ';
  @override
  String get Wallpapers => 'Обои';
  @override
  String get Profile => 'Профиль';
  @override
  String get NoInternet => 'Нет доступа в Интернет';
  @override
  String get Settings => 'Настройки';
  @override
  String get Account => 'Счет';
  @override
  String get EditProfile => 'Редактировать профиль';
  @override
  String get OnlineStatus => 'Онлайн-статус';
  @override
  String get OnlineDescription =>
      'Все могут видеть, когда вы в последний раз были в сети';
  @override
  String get Logout => 'Выйти';
  @override
  String get DirectMsgs => 'Сообщения в чате';
  @override
  String get DirectMsgsDescr => 'Получать уведомления чата';
  @override
  String get GroupsMsgs => 'Групповые сообщения';
  @override
  String get GroupsMsgsDesc => 'Получать все уведомления групп';
  @override
  String get AddGroupName => 'Пожалуйста! Добавьте название группы';
  @override
  String get About => 'о нас';
  @override
  String get RateUs => 'Оцените нас';
  @override
  String get EULA => 'Соглашение с конечным пользователем';
  @override
  String get PrivacyPolicy => 'Политика конфиденциальности';
  @override
  String get Feed => 'Подача';
  @override
  String get Post => 'Сообщение';
  @override
  String get Gallery => 'Галерея';
  @override
  String get Stories => 'Рассказы';
  @override
  String get MyPosts => 'Мои сообщения';
  @override
  String get Favorites => 'Избранное';
  @override
  String get Announcement => 'Объявление';
  @override
  String get WhatOnMind => 'Чем поделиться?';
  @override
  String get Likes => 'Нравится';
  @override
  String get Comments => 'Комментарии';
  @override
  String get CreatePost => 'Создать пост';
  @override
  String get Share => 'Делиться';
  @override
  String get Edit => 'Редактировать';
  @override
  String get Delete => 'Удалить';
  @override
  String get Copy => 'Копировать';
  @override
  String get Copied => 'Скопировано';
  @override
  String get ReadMore => '...Прочитайте больше';
  @override
  String get SuccessPostPublish => 'Пост успешно опубликован';
  @override
  String get SuccessPostEdited => 'Пост успешно отредактирован';
  @override
  String get TypeComment => 'Введите комментарий...';
  @override
  String get TypeReply => 'Введите ответ...';
  @override
  String get NotAllowedToPublish =>
      'Извините! Вам больше не разрешено публиковать';
  @override
  String get NotAllowedToComment => 'Извините! Вы не можете комментировать';
  @override
  String get PostRemoveConfirm => 'Вы уверены, что хотите удалить эту запись?';
  @override
  String get CommentRemoveConfirm =>
      'Вы уверенны, что хотите удалить этот комментарий?';
  @override
  String get AddSomeContent => 'Пожалуйста, добавьте контент перед публикацией';
  @override
  String get Reply => 'Отвечать';
  @override
  String get Replies => 'Ответы';
  @override
  String get RepliedToComment => 'ответил на ваш комментарий';
  @override
  String get Chats => 'Чаты';
  @override
  String get OnlineUsers => 'ОНЛАЙН-ПОЛЬЗОВАТЕЛИ';
  @override
  String get RecentChats => 'ПОСЛЕДНИЕ ЧАТЫ';
  @override
  String get RemoveConversation => 'Удалить беседу';
  @override
  String get Messaging => 'Сообщение...';
  @override
  String get CannotChatWithUser =>
      'Извините! Вы не можете общаться с этим пользователем';
  @override
  String get MsgDeleteConfirm => 'Вы уверены, что хотите удалить сообщение?';
  @override
  String get NoChatFound => 'Чата пока нет, начните общаться!';
  @override
  String get Online => 'В сети';
  @override
  String get Typing => 'Ввод...';
  @override
  String get Image => 'Изображение';
  @override
  String get Voice => 'Голос';
  @override
  String get Video => 'Видео';
  @override
  String get Emoji => 'Эмодзи';
  @override
  String get GIF => 'ГИФ';
  @override
  String get Sticker => 'Наклейка';
  @override
  String get Groups => 'Группы';
  @override
  String get CreateGroup => 'Создать группу';
  @override
  String get EditGroup => 'Редактировать свою группу';
  @override
  String get Members => 'Члены';
  @override
  String get PhotosLibrary => 'Библиотека фотографий';
  @override
  String get TakePicture => 'Сделать фотографию';
  @override
  String get VideosLibrary => 'Видеотека';
  @override
  String get RecordVideo => 'Запись видео';
  @override
  String get Cancel => 'Отмена';
  @override
  String get SlideToCancel => 'Слайд для отмены';
  @override
  String get On => 'На';
  @override
  String get Off => 'Выключенный';
  @override
  String get Group => 'Группа';
  @override
  String get LeaveGroup => 'Покинуть группу';
  @override
  String get GroupName => 'Название группы';
  @override
  String get Join => 'Присоединиться';
  @override
  String get Joined => 'Присоединился';
  @override
  String get Public => 'Общественный';
  @override
  String get Private => 'Частный';
  @override
  String get GroupType => 'Тип группы';
  @override
  String get RemoveMember => 'Удалить участника';
  @override
  String get AddMembers => 'Добавить участников';
  @override
  String get Block => 'Блокировать';
  @override
  String get Unblock => 'Разблокировать';
  @override
  String get Ban => 'Запрет';
  @override
  String get Unban => 'Разбан';
  @override
  String get Banned => 'Запрещено';
  @override
  String get Newest => 'Новейший';
  @override
  String get Trending => 'В тренде';
  @override
  String get MostDownloaded => 'Самые скачиваемые';
  @override
  String get Categories => 'Категории';
  @override
  String get AddWallpaper => 'Добавить обои';
  @override
  String get WallpaperName => 'Имя обоев';
  @override
  String get Category => 'Категория';
  @override
  String get Upload => 'Загрузить';
  @override
  String get Home => 'Дом';
  @override
  String get Lock => 'Замок';
  @override
  String get Both => 'Оба';
  @override
  String get SetAsWallpaper => 'Установить как обои рабочего стола';
  @override
  String get WallpaperSet => 'Обои установлены успешно';
  @override
  String get FailedToUpload => 'Ой! Ошибка загрузки. Повторите попытку';
  @override
  String get UploadFinished => 'Загрузка завершена';
  @override
  String get Downloading => 'Скачивание';
  @override
  String get NoWallpaperSelectedMsg =>
      'Пожалуйста, выберите обои, чтобы продолжить';
  @override
  String get NoImgSelected => 'Изображение не выбрано';
  @override
  String get Notifications => 'Уведомления';
  @override
  String get StartFollowingMsg => 'начал следить за тобой';
  @override
  String get PostReactionMsg => 'отреагировал на ваш пост';
  @override
  String get PostCommentMsg => 'прокомментировал ваш пост';
  @override
  String get ReplyMsg => 'ответил на ваш комментарий';
  @override
  String get CommentReactedMsg => 'отреагировал на ваш комментарий';
  @override
  String get CannotFindFile => 'Ой! Не удается найти файл';
  @override
  String get NoFilePicked => 'Попытка загрузки, но файл не выбран';
  @override
  String get SentYouMsg => 'Отправил вам сообщение';
  @override
  String get Report => 'Отчет';
  @override
  String get Unreport => 'Не сообщать';
  @override
  String get ReportDesc => 'Удаляем пост, в котором есть: ';
  @override
  String get ReportReasons =>
      '⚫️ Контент сексуального характера. \n\n⚫️ Жестокий или отталкивающий контент. \n\n⚫️ Ненавистный или оскорбительный контент. \n\n⚫️ Спам или вводящий в заблуждение.';
  @override
  String get ReportNote =>
      'Мы не сообщим им, если вы предпримете это действие.';
  @override
  String get ReportThanks =>
      'Мы проверим ваш запрос, спасибо за помощь в улучшении нашего сообщества';
  @override
  String get Admin => 'Админ';
  @override
  String get ProfanityDetected =>
      'Обнаружены нецензурные слова, ваша учетная запись может быть заблокирована!';
  @override
  String get AreYouSure => 'Вы уверены';
  @override
  String get ConfirmChatDeletion => 'Вы уверены, что хотите удалить чат';
  @override
  String get ReportedPosts => 'Отмеченные сообщения';
  @override
  String get AllChats => 'Все чаты';
  @override
  String get Users => 'Пользователи';
  @override
  String get TermsService => 'Условия обслуживания';
  @override
  String get WaitingToRecord => 'В ожидании записи';
  @override
  String get AdjustVolume => 'Регулировать громкость';
  @override
  String get AdjustSpeed => 'Регулировать скорость';
  @override
  String get Ok => 'В порядке';
  @override
  String get ShareExternally => 'Поделиться снаружи';
  @override
  String get ShareOnYourWall => 'Поделиться на стене';
  @override
  String get Error => 'Ошибка';
  @override
  String get Me => 'Мне';
  @override
  String get OopsCode => 'К сожалению, неверный код подтверждения';
  @override
  String get OopsNetwork =>
      'К сожалению, что-то пошло не так, пожалуйста, проверьте сеть!';
  @override
  String get OopsEmailorPassword => 'Ой! Электронная почта или пароль неверны!';
  @override
  String get OopsNotRegister =>
      'К сожалению, пользователь не найден, если вы не зарегистрируетесь, сначала зарегистрируйтесь!';
  @override
  String get OopsEmailUsed =>
      'К сожалению, этот адрес электронной почты уже используется другим аккаунтом!';
  @override
  String get OopsPhoneFormat => 'К сожалению, формат телефона недействителен';
  @override
  String get NoAudioRecorded => 'Аудио не добавлено';
  @override
  String get OopsWrong => 'Упс! Что-то пошло не так';
  @override
  String get Optional => 'Необязательный';
  @override
  String get Sending => 'Отправка...';
  @override
  String get NotificationPermission =>
      'Разрешение на уведомление гарантировано';
  @override
  String get Theme => 'Тема';
  @override
  String get DarkMode => 'Темный режим';
  @override
  String get Others => 'Другие';
  @override
  String get PeopleWhoReacted => 'Люди, которые отреагировали';
  @override
  String get BadWords =>
      'Обнаружены нецензурные слова, ваша учетная запись может быть заблокирована!';
  @override
  String get PickFile => 'Выбрать файл';
  @override
  String get Loading => 'Загружается...';
  @override
  String get PleaseAddAudio => 'Пожалуйста, добавьте звук';
  @override
  String get PeopleYouMayFollow => 'Люди, на которых вы можете подписаться';
  @override
  String get Unsave => 'Отменить сохранение';
  @override
  String get PostsLikes => 'Посты лайки';
  @override
  String get CommentsLikes => 'Комментарии лайки';
  @override
  String get RepliesLikes => 'Ответы лайки';
  @override
  String get ShareiConfezz => 'Поделиться iConfezz';
  @override
  String get MemberSince => 'Член с тех пор';
  @override
  String get Comment => 'Комментарий';
  @override
  String get UnShare => 'Не делиться';
  @override
  String get Liked => 'Понравилось';
  @override
  String get Like => 'Нравиться';
  @override
  String get SAVED => 'СОХРАНЕНО';
  @override
  String get SHARED => 'ОБЩИЙ';
  @override
  String get AppLanguage => 'Язык приложения';
  @override
  String get ComingSoon => 'Вскоре';
  @override
  String get ReportProblem => 'Сообщить о проблеме';
  @override
  String get PushNotifications => 'Всплывающие уведомления';
  @override
  String get ShareProfile => 'Поделиться профилем';
  @override
  String get DeleteAccount => 'Удалить аккаунт';
  @override
  String get DeleteYourAccount => 'Удалить учетную запись';
  @override
  String get Record => 'Записывать';
  @override
  String get Or => 'Или';
  @override
  String get NoPhotoPicked => 'Фото не выбрано';
  @override
  String get SearchForUsers => 'Поиск пользователей';
  @override
  String get NoResultsFound => 'Результаты не найдены';
  @override
  String get TemporaryFilesRemoved => 'Временные файлы успешно удалены';
  @override
  String get FailedToClean => 'Не удалось очистить временные файлы';
  @override
  String get exit => 'выход';
  @override
  String get CommunityGuidelines => 'Принципы сообщества';
  @override
  String get FAQ => 'ЧАСТО ЗАДАВАЕМЫЕ ВОПРОСЫ';
  @override
  String get Paste => 'Paste';
  @override
  String get DoYouWantToPasteThisCode => 'Do you want to paste this code?';
  @override
  String get PasteCode => 'Paste Code';
}

// Path: <root>
class _StringsTr implements _StringsEn {
  /// You can call this constructor and build your own translation instance of this locale.
  /// Constructing via the enum [AppLocale.build] is preferred.
  _StringsTr.build();

  /// Access flat map
  @override
  dynamic operator [](String key) => _flatMap[key];

  // Internal flat map initialized lazily
  @override
  late final Map<String, dynamic> _flatMap = _buildFlatMap();

  @override
  late final _StringsTr _root = this; // ignore: unused_field

  // Translations
  @override
  String get AppName => 'iConfezz';
  @override
  String get AboutApp =>
      'iConfezz, başkalarının sesini duymaya adanmış bir topluluktur.';
  @override
  String get AppVersion => '2.0.0';
  @override
  String get UnknownError => 'Bir şeyler yanlış gitti';
  @override
  String get Next => 'Sonraki';
  @override
  String get EmailUser => 'E-posta Kullanıcısı mısınız, Buradan giriş yapın!';
  @override
  String get Email => 'E-posta';
  @override
  String get bio => 'Biyografi';
  @override
  String get city => 'Şehir';
  @override
  String get dob => 'Doğum tarihi';
  @override
  String get Password => 'Parola';
  @override
  String get Login => 'Giriş yapmak';
  @override
  String get PhoneNumber => 'Telefon numarası';
  @override
  String get PhoneVerification => 'Telefon Numarası Doğrulama';
  @override
  String get EnterCode => 'Gönderilen kodu girin';
  @override
  String get DidntRecieveCode => 'Kodu almadınız mı?';
  @override
  String get InvalidSMSCode => 'SMS Doğrulama Kodu Geçersiz';
  @override
  String get RESEND => 'YENİDEN GÖNDER';
  @override
  String get VERIFY => 'DOĞRULAYIN';
  @override
  String get POSTS => 'GÖNDERİLER';
  @override
  String get FOLLOWERS => 'Takipçiler';
  @override
  String get FOLLOWINGS => 'Takipler';
  @override
  String get Follow => 'Takip etmek';
  @override
  String get Following => 'Takip etmek';
  @override
  String get Register => 'Kayıt olmak';
  @override
  String get Username => 'Kullanıcı adı';
  @override
  String get FirstName => 'İlk adı';
  @override
  String get LastName => 'Soyadı';
  @override
  String get FullName => 'Ad Soyad';
  @override
  String get Status => 'Durum';
  @override
  String get AddValidUsername => 'Geçerli bir kullanıcı adı ekleyin';
  @override
  String get AddValidFirstName => 'Geçerli bir ad ekleyin';
  @override
  String get AddValidLastName => 'Geçerli bir soyadı ekleyin';
  @override
  String get SelectGender => 'Lütfen cinsiyetinizi seçin.';
  @override
  String get Male => 'Erkek';
  @override
  String get Female => 'Dişi';
  @override
  String get Other => 'Diğer';
  @override
  String get AcceptTerms => 'Tüm Hüküm ve Koşulları Kabul Ediyorum';
  @override
  String get Save => 'Kaydetmek';
  @override
  String get Search => 'Aramak';
  @override
  String get Searching => 'Aranıyor...';
  @override
  String get SelectPhoneCode => 'Bir telefon kodu seçin';
  @override
  String get InvalidEmail => 'Geçersiz e-posta';
  @override
  String get ShortPassword => 'Şifre En Az 8 Karakter İçermelidir';
  @override
  String get UsernameTaken => 'Kullanıcı adı zaten alınmış';
  @override
  String get InvalidPhone => 'Lütfen tüm hücreleri uygun şekilde doldurun';
  @override
  String get Message => 'İleti';
  @override
  String get LoginFirst => 'Oops, Lütfen önce giriş yapın';
  @override
  String get START => 'BAŞLAMAK';
  @override
  String get Wallpapers => 'Duvar kağıtları';
  @override
  String get Profile => 'Profil';
  @override
  String get NoInternet => 'İnternet erişimi yok';
  @override
  String get Settings => 'Ayarlar';
  @override
  String get Account => 'Hesap';
  @override
  String get EditProfile => 'Profili Düzenle';
  @override
  String get OnlineStatus => 'Çevrimiçi durum';
  @override
  String get OnlineDescription =>
      'En son ne zaman çevrimiçi olduğunuzu herkes görebilir';
  @override
  String get Logout => 'Çıkış Yap';
  @override
  String get DirectMsgs => 'Sohbet Mesajları';
  @override
  String get DirectMsgsDescr => 'Sohbet bildirimlerini al';
  @override
  String get GroupsMsgs => 'Grup Mesajları';
  @override
  String get GroupsMsgsDesc => 'Tüm Grupların Bildirimlerini Al';
  @override
  String get AddGroupName => 'Lütfen! Grup Adı Ekle';
  @override
  String get About => 'Hakkımızda';
  @override
  String get RateUs => 'Bizi değerlendirin';
  @override
  String get EULA => 'EULA Anlaşması';
  @override
  String get PrivacyPolicy => 'Gizlilik Politikası';
  @override
  String get Feed => 'Beslemek';
  @override
  String get Post => 'Postalamak';
  @override
  String get Gallery => 'Galeri';
  @override
  String get Stories => 'Hikayeler';
  @override
  String get MyPosts => 'Gönderilerim';
  @override
  String get Favorites => 'Favoriler';
  @override
  String get Announcement => 'Duyuru';
  @override
  String get WhatOnMind => 'Neler Paylaşılır?';
  @override
  String get Likes => 'Seviyor';
  @override
  String get Comments => 'Yorumlar';
  @override
  String get CreatePost => 'Posta Oluştur';
  @override
  String get Share => 'Paylaşmak';
  @override
  String get Edit => 'Düzenlemek';
  @override
  String get Delete => 'Silmek';
  @override
  String get Copy => 'Kopyala';
  @override
  String get Copied => 'Kopyalandı';
  @override
  String get ReadMore => '...Daha fazla oku';
  @override
  String get SuccessPostPublish => 'Yazı Başarıyla Yayınlandı';
  @override
  String get SuccessPostEdited => 'Gönderi Başarıyla Düzenlendi';
  @override
  String get TypeComment => 'Bir yorum yazın...';
  @override
  String get TypeReply => 'Bir yanıt yazın...';
  @override
  String get NotAllowedToPublish => 'Üzgünüm! Artık yayınlama izniniz yok';
  @override
  String get NotAllowedToComment => 'Üzgünüm! Yorum yapmanıza izin verilmiyor';
  @override
  String get PostRemoveConfirm =>
      'Bu gönderiyi silmek istediğinizden emin misiniz?';
  @override
  String get CommentRemoveConfirm =>
      'Bu yorumu silmek istediğinizden emin misiniz?';
  @override
  String get AddSomeContent => 'Lütfen göndermeden önce biraz içerik ekleyin';
  @override
  String get Reply => 'Cevap vermek';
  @override
  String get Replies => 'Yanıtlar';
  @override
  String get RepliedToComment => 'yorumunuzu yanıtladım';
  @override
  String get Chats => 'Sohbetler';
  @override
  String get OnlineUsers => 'ÇEVRİMİÇİ KULLANICILAR';
  @override
  String get RecentChats => 'SON SOHBETLER';
  @override
  String get RemoveConversation => 'Konuşmayı Kaldır';
  @override
  String get Messaging => 'İleti...';
  @override
  String get CannotChatWithUser =>
      'Üzgünüm! Bu kullanıcıyla sohbet edemezsiniz';
  @override
  String get MsgDeleteConfirm => 'Mesajı silmek istediğinizden emin misiniz?';
  @override
  String get NoChatFound => 'Henüz Son Sohbet yok, sohbete başla!';
  @override
  String get Online => 'Çevrimiçi';
  @override
  String get Typing => 'Yazıyor...';
  @override
  String get Image => 'Resim';
  @override
  String get Voice => 'Ses';
  @override
  String get Video => 'Video';
  @override
  String get Emoji => 'Emoji';
  @override
  String get GIF => 'GIF';
  @override
  String get Sticker => 'Etiket';
  @override
  String get Groups => 'Gruplar';
  @override
  String get CreateGroup => 'Grup Oluştur';
  @override
  String get EditGroup => 'Grubunuzu düzenleyin';
  @override
  String get Members => 'Üyeler';
  @override
  String get PhotosLibrary => 'Fotoğraf Kitaplığı';
  @override
  String get TakePicture => 'Fotoğraf çek';
  @override
  String get VideosLibrary => 'Video Kitaplığı';
  @override
  String get RecordVideo => 'Video kaydetmek';
  @override
  String get Cancel => 'İptal etmek';
  @override
  String get SlideToCancel => 'İptal etmek için kaydırın';
  @override
  String get On => 'Açık';
  @override
  String get Off => 'Kapalı';
  @override
  String get Group => 'Grup';
  @override
  String get LeaveGroup => 'Gruptan ayrıl';
  @override
  String get GroupName => 'Grup ismi';
  @override
  String get Join => 'Katılmak';
  @override
  String get Joined => 'katıldı';
  @override
  String get Public => 'Halk';
  @override
  String get Private => 'Özel';
  @override
  String get GroupType => 'Grup Türü';
  @override
  String get RemoveMember => 'Üyeyi kaldır';
  @override
  String get AddMembers => 'Üye ekle';
  @override
  String get Block => 'Engellemek';
  @override
  String get Unblock => 'Engeli kaldırmak';
  @override
  String get Ban => 'Yasaklamak';
  @override
  String get Unban => 'Yasağı kaldır';
  @override
  String get Banned => 'Yasaklandı';
  @override
  String get Newest => 'En yeni';
  @override
  String get Trending => 'Trend';
  @override
  String get MostDownloaded => 'En Çok İndirilenler';
  @override
  String get Categories => 'Kategoriler';
  @override
  String get AddWallpaper => 'Duvar Kağıdı Ekle';
  @override
  String get WallpaperName => 'Duvar Kağıdı Adı';
  @override
  String get Category => 'Kategori';
  @override
  String get Upload => 'Yüklemek';
  @override
  String get Home => 'Ev';
  @override
  String get Lock => 'Kilit';
  @override
  String get Both => 'İkisi birden';
  @override
  String get SetAsWallpaper => 'Duvar kağıdı olarak ayarlamak';
  @override
  String get WallpaperSet => 'Duvar Kağıdı Başarıyla Ayarlandı';
  @override
  String get FailedToUpload => 'Hata! Yükleme Başarısız, Lütfen tekrar deneyin';
  @override
  String get UploadFinished => 'Yükleme Tamamlandı';
  @override
  String get Downloading => 'İndirme';
  @override
  String get NoWallpaperSelectedMsg =>
      'Devam etmek için lütfen Duvar Kağıdını Seçin';
  @override
  String get NoImgSelected => 'Görüntü Seçilmedi';
  @override
  String get Notifications => 'Bildirimler';
  @override
  String get StartFollowingMsg => 'seni takip etmeye başladım';
  @override
  String get PostReactionMsg => 'yazınıza tepki verdim';
  @override
  String get PostCommentMsg => 'gönderinize yorum yaptı';
  @override
  String get ReplyMsg => 'yorumunuzu yanıtladım';
  @override
  String get CommentReactedMsg => 'yorumunuza tepki verdim';
  @override
  String get CannotFindFile => 'Hata! Dosya bulunamıyor';
  @override
  String get NoFilePicked => 'Yüklemeye çalışılıyor ve hiçbir dosya seçilmiyor';
  @override
  String get SentYouMsg => 'Size mesaj gönderdim';
  @override
  String get Report => 'Rapor';
  @override
  String get Unreport => 'Raporu iptal et';
  @override
  String get ReportDesc => 'Şu olan gönderiyi kaldırıyoruz: ';
  @override
  String get ReportReasons =>
      '⚫️ Cinsel içerik. \n\n⚫️ Şiddet içeren veya tiksindirici içerik. \n\n⚫️ Nefret dolu veya taciz edici içerik. \n\n⚫️ Spam veya yanıltıcı.';
  @override
  String get ReportNote => 'Bu eylemi yaparsan onlara haber vermeyeceğiz.';
  @override
  String get ReportThanks =>
      'İsteğinizi kontrol edeceğiz, Topluluğumuzu geliştirmeye yardımcı olduğunuz için teşekkürler';
  @override
  String get Admin => 'Yönetici';
  @override
  String get ProfanityDetected =>
      'Kötü sözler tespit edildi, hesabınız askıya alınabilir!';
  @override
  String get AreYouSure => 'Emin misin';
  @override
  String get ConfirmChatDeletion =>
      'Sohbeti silmek istediğinizden emin misiniz';
  @override
  String get ReportedPosts => 'Raporlanan Yazılar';
  @override
  String get AllChats => 'Tüm Sohbetler';
  @override
  String get Users => 'Kullanıcılar';
  @override
  String get TermsService => 'Kullanım Şartları';
  @override
  String get WaitingToRecord => 'Kayıt bekleniyor';
  @override
  String get AdjustVolume => 'Sesi ayarla';
  @override
  String get AdjustSpeed => 'Hızı ayarla';
  @override
  String get Ok => 'Tamam';
  @override
  String get ShareExternally => 'Harici olarak paylaş';
  @override
  String get ShareOnYourWall => 'Duvarında paylaş';
  @override
  String get Error => 'Hata';
  @override
  String get Me => 'Ben mi';
  @override
  String get OopsCode => 'Hata! Vefication kodu yanlış';
  @override
  String get OopsNetwork =>
      'Hata! bir şeyler ters gitti, Lütfen ağınızı kontrol edin!';
  @override
  String get OopsEmailorPassword => 'Hata! e-posta veya şifre doğru değil!';
  @override
  String get OopsNotRegister =>
      'Hata! kullanıcı bulunamadı, eğer kaydolmadıysanız önce kaydolmayı düşünün!';
  @override
  String get OopsEmailUsed =>
      'Hata! bu e-posta zaten başka bir hesap tarafından kullanılıyor!';
  @override
  String get OopsPhoneFormat => 'Hata! telefon biçimi geçersiz';
  @override
  String get NoAudioRecorded => 'Ses eklenmedi';
  @override
  String get OopsWrong => 'Hoop! Birşeyler yanlış gitti';
  @override
  String get Optional => 'İsteğe bağlı';
  @override
  String get Sending => 'Gönderiliyor...';
  @override
  String get NotificationPermission => 'Bildirim izni garantilidir';
  @override
  String get Theme => 'Tema';
  @override
  String get DarkMode => 'Karanlık Mod';
  @override
  String get Others => 'Diğerleri';
  @override
  String get PeopleWhoReacted => 'Tepki verenler';
  @override
  String get BadWords =>
      'Kötü sözler tespit edildi, hesabınız askıya alınabilir!';
  @override
  String get PickFile => 'Dosya seç';
  @override
  String get Loading => 'Yükleniyor...';
  @override
  String get PleaseAddAudio => 'Lütfen ses ekleyin';
  @override
  String get PeopleYouMayFollow => 'Takip edebileceğiniz kişiler';
  @override
  String get Unsave => 'Kaydetmeyi iptal et';
  @override
  String get PostsLikes => 'Gönderiler Beğenileri';
  @override
  String get CommentsLikes => 'Yorum Beğenileri';
  @override
  String get RepliesLikes => 'Yanıt Beğenileri';
  @override
  String get ShareiConfezz => 'iConfezz\'i paylaşın';
  @override
  String get MemberSince => 'Den beri üye';
  @override
  String get Comment => 'Yorum';
  @override
  String get UnShare => 'Paylaşmayı Kaldır';
  @override
  String get Liked => 'Beğendim';
  @override
  String get Like => 'Beğenmek';
  @override
  String get SAVED => 'KAYDEDİLDİ';
  @override
  String get SHARED => 'PAYLAŞILMIŞ';
  @override
  String get AppLanguage => 'Uygulama Dili';
  @override
  String get ComingSoon => 'Yakında gelecek';
  @override
  String get ReportProblem => 'Problemi şikayet et';
  @override
  String get PushNotifications => 'Push bildirimleri';
  @override
  String get ShareProfile => 'Profili Paylaş';
  @override
  String get DeleteAccount => 'Hesabı sil';
  @override
  String get DeleteYourAccount => 'Hesabını sil';
  @override
  String get Record => 'Kayıt';
  @override
  String get Or => 'Veya';
  @override
  String get NoPhotoPicked => 'Fotoğraf Seçilmedi';
  @override
  String get SearchForUsers => 'Kullanıcıları Ara';
  @override
  String get NoResultsFound => 'Sonuç bulunamadı';
  @override
  String get TemporaryFilesRemoved => 'Geçici dosyalar başarıyla kaldırıldı';
  @override
  String get FailedToClean => 'Geçici dosyalar temizlenemedi';
  @override
  String get exit => 'çıkış';
  @override
  String get CommunityGuidelines => 'Topluluk Rehberleri';
  @override
  String get FAQ => 'SSS';
  @override
  String get Paste => 'Paste';
  @override
  String get DoYouWantToPasteThisCode => 'Do you want to paste this code?';
  @override
  String get PasteCode => 'Paste Code';
}

// Path: <root>
class _StringsZh implements _StringsEn {
  /// You can call this constructor and build your own translation instance of this locale.
  /// Constructing via the enum [AppLocale.build] is preferred.
  _StringsZh.build();

  /// Access flat map
  @override
  dynamic operator [](String key) => _flatMap[key];

  // Internal flat map initialized lazily
  @override
  late final Map<String, dynamic> _flatMap = _buildFlatMap();

  @override
  late final _StringsZh _root = this; // ignore: unused_field

  // Translations
  @override
  String get AppName => 'iConfezz';
  @override
  String get AboutApp => 'iConfezz 是一个致力于倾听他人声音的社区。';
  @override
  String get AppVersion => '2.0.0';
  @override
  String get UnknownError => '出问题了';
  @override
  String get Next => '下一个';
  @override
  String get EmailUser => '您是电子邮件用户吗,请在此处登录！';
  @override
  String get Email => '电子邮件';
  @override
  String get bio => '传';
  @override
  String get city => '城市';
  @override
  String get dob => '出生日期';
  @override
  String get Password => '密码';
  @override
  String get Login => '登录';
  @override
  String get PhoneNumber => '电话号码';
  @override
  String get PhoneVerification => '电话号码验证';
  @override
  String get EnterCode => '输入发送到的代码';
  @override
  String get DidntRecieveCode => '没有收到验证码？';
  @override
  String get InvalidSMSCode => '短信验证码无效';
  @override
  String get RESEND => '重发';
  @override
  String get VERIFY => '核实';
  @override
  String get POSTS => '帖子';
  @override
  String get FOLLOWERS => '追随者';
  @override
  String get FOLLOWINGS => '以下';
  @override
  String get Follow => '跟随';
  @override
  String get Following => '下列的';
  @override
  String get Register => '登记';
  @override
  String get Username => '用户名';
  @override
  String get FirstName => '名';
  @override
  String get LastName => '姓';
  @override
  String get FullName => '全名';
  @override
  String get Status => '地位';
  @override
  String get AddValidUsername => '添加一个有效的用户名';
  @override
  String get AddValidFirstName => '添加一个有效的名字';
  @override
  String get AddValidLastName => '添加一个有效的姓氏';
  @override
  String get SelectGender => '请选择您的性别。';
  @override
  String get Male => '男性';
  @override
  String get Female => '女性';
  @override
  String get Other => '其他';
  @override
  String get AcceptTerms => '我接受所有条款和条件';
  @override
  String get Save => '节省';
  @override
  String get Search => '搜索';
  @override
  String get Searching => '正在搜索...';
  @override
  String get SelectPhoneCode => '选择电话号码';
  @override
  String get InvalidEmail => '不合规电邮';
  @override
  String get ShortPassword => '密码必须包含至少 8 个字符';
  @override
  String get UsernameTaken => '用户名已被占用';
  @override
  String get InvalidPhone => '请正确填写所有单元格';
  @override
  String get Message => '信息';
  @override
  String get LoginFirst => '糟糕,请先登录';
  @override
  String get START => '开始';
  @override
  String get Wallpapers => '壁纸';
  @override
  String get Profile => '轮廓';
  @override
  String get NoInternet => '不能访问网络';
  @override
  String get Settings => '设置';
  @override
  String get Account => '帐户';
  @override
  String get EditProfile => '编辑个人资料';
  @override
  String get OnlineStatus => '在线状态';
  @override
  String get OnlineDescription => '任何人都可以看到您上次在线的时间';
  @override
  String get Logout => '登出';
  @override
  String get DirectMsgs => '聊天消息';
  @override
  String get DirectMsgsDescr => '接收聊天通知';
  @override
  String get GroupsMsgs => '群组消息';
  @override
  String get GroupsMsgsDesc => '接收所有组通知';
  @override
  String get AddGroupName => '请！添加组名';
  @override
  String get About => '关于我们';
  @override
  String get RateUs => '评价我们';
  @override
  String get EULA => '最终用户许可协议';
  @override
  String get PrivacyPolicy => '隐私政策';
  @override
  String get Feed => '喂养';
  @override
  String get Post => '邮政';
  @override
  String get Gallery => '画廊';
  @override
  String get Stories => '故事';
  @override
  String get MyPosts => '我的帖子';
  @override
  String get Favorites => '最爱';
  @override
  String get Announcement => '公告';
  @override
  String get WhatOnMind => '要分享什么？';
  @override
  String get Likes => '喜欢';
  @override
  String get Comments => '评论';
  @override
  String get CreatePost => '创建帖子';
  @override
  String get Share => '分享';
  @override
  String get Edit => '编辑';
  @override
  String get Delete => '删除';
  @override
  String get Copy => '复制';
  @override
  String get Copied => '复制';
  @override
  String get ReadMore => '...阅读更多';
  @override
  String get SuccessPostPublish => '帖子已成功发布';
  @override
  String get SuccessPostEdited => '帖子已成功编辑';
  @override
  String get TypeComment => '输入评论...';
  @override
  String get TypeReply => '输入回复...';
  @override
  String get NotAllowedToPublish => '对不起！你不能再发布了';
  @override
  String get NotAllowedToComment => '对不起！你不能评论';
  @override
  String get PostRemoveConfirm => '你确定你要删除这个帖子？';
  @override
  String get CommentRemoveConfirm => '你确定要删除此评论？';
  @override
  String get AddSomeContent => '请在发帖前添加一些内容';
  @override
  String get Reply => '回复';
  @override
  String get Replies => '回复';
  @override
  String get RepliedToComment => '回复你的评论';
  @override
  String get Chats => '聊天';
  @override
  String get OnlineUsers => '在线用户';
  @override
  String get RecentChats => '最近的聊天';
  @override
  String get RemoveConversation => '删除对话';
  @override
  String get Messaging => '信息...';
  @override
  String get CannotChatWithUser => '对不起！你不能和这个用户聊天';
  @override
  String get MsgDeleteConfirm => '您确定要删除该消息吗？';
  @override
  String get NoChatFound => '还没有最近聊天,开始聊天吧！';
  @override
  String get Online => '在线的';
  @override
  String get Typing => '正在打字...';
  @override
  String get Image => '图片';
  @override
  String get Voice => '嗓音';
  @override
  String get Video => '视频';
  @override
  String get Emoji => '表情符号';
  @override
  String get GIF => 'GIF';
  @override
  String get Sticker => '贴纸';
  @override
  String get Groups => '团体';
  @override
  String get CreateGroup => '创建一个组';
  @override
  String get EditGroup => '编辑你的组';
  @override
  String get Members => '成员';
  @override
  String get PhotosLibrary => '照片库';
  @override
  String get TakePicture => '拍照片';
  @override
  String get VideosLibrary => '视频库';
  @override
  String get RecordVideo => '录视频';
  @override
  String get Cancel => '取消';
  @override
  String get SlideToCancel => '滑动取消';
  @override
  String get On => '在';
  @override
  String get Off => '离开';
  @override
  String get Group => '团体';
  @override
  String get LeaveGroup => '离开组';
  @override
  String get GroupName => '组的名字';
  @override
  String get Join => '加入';
  @override
  String get Joined => '加入';
  @override
  String get Public => '上市';
  @override
  String get Private => '私人的';
  @override
  String get GroupType => '组类型';
  @override
  String get RemoveMember => '删除成员';
  @override
  String get AddMembers => '添加成员';
  @override
  String get Block => '堵塞';
  @override
  String get Unblock => '解除封锁';
  @override
  String get Ban => '禁止';
  @override
  String get Unban => '解禁';
  @override
  String get Banned => '禁止';
  @override
  String get Newest => '最新';
  @override
  String get Trending => '趋势';
  @override
  String get MostDownloaded => '下载次数最多';
  @override
  String get Categories => '类别';
  @override
  String get AddWallpaper => '添加壁纸';
  @override
  String get WallpaperName => '壁纸名称';
  @override
  String get Category => '类别';
  @override
  String get Upload => '上传';
  @override
  String get Home => '家';
  @override
  String get Lock => '锁';
  @override
  String get Both => '两个都';
  @override
  String get SetAsWallpaper => '设置为墙纸';
  @override
  String get WallpaperSet => '壁纸设置成功';
  @override
  String get FailedToUpload => '糟糕！上传失败,请重试';
  @override
  String get UploadFinished => '上传完成';
  @override
  String get Downloading => '下载';
  @override
  String get NoWallpaperSelectedMsg => '请选择壁纸继续';
  @override
  String get NoImgSelected => '未选择图像';
  @override
  String get Notifications => '通知';
  @override
  String get StartFollowingMsg => '开始关注您';
  @override
  String get PostReactionMsg => '对你的帖子做出反应';
  @override
  String get PostCommentMsg => '评论了你的帖子';
  @override
  String get ReplyMsg => '回复你的评论';
  @override
  String get CommentReactedMsg => '对您的评论作出反应';
  @override
  String get CannotFindFile => '糟糕！找不到文件';
  @override
  String get NoFilePicked => '尝试上传但没有选择文件';
  @override
  String get SentYouMsg => '给你发消息';
  @override
  String get Report => '报告';
  @override
  String get Unreport => '取消报告';
  @override
  String get ReportDesc => '我们删除具有以下内容的帖子：';
  @override
  String get ReportReasons =>
      '⚫️ 色情内容。\n\n⚫️ 暴力或令人反感的内容。\n\n⚫️ 仇恨或辱骂内容。\n\n⚫️ 垃圾邮件或误导性内容。';
  @override
  String get ReportNote => '如果您采取此行动,我们不会让他们知道。';
  @override
  String get ReportThanks => '我们将检查您的请求,感谢您帮助改善我们的社区';
  @override
  String get Admin => '行政';
  @override
  String get ProfanityDetected => '检测到坏话,您的帐号可能会被暂停！';
  @override
  String get AreYouSure => '你确定';
  @override
  String get ConfirmChatDeletion => '确定要删除聊天';
  @override
  String get ReportedPosts => '报告的帖子';
  @override
  String get AllChats => '所有聊天';
  @override
  String get Users => '用户';
  @override
  String get TermsService => '服务条款';
  @override
  String get WaitingToRecord => '等待记录';
  @override
  String get AdjustVolume => '调整音量';
  @override
  String get AdjustSpeed => '调整速度';
  @override
  String get Ok => '行';
  @override
  String get ShareExternally => '对外分享';
  @override
  String get ShareOnYourWall => '在你的墙上分享';
  @override
  String get Error => '错误';
  @override
  String get Me => '我';
  @override
  String get OopsCode => '糟糕！验证码错误';
  @override
  String get OopsNetwork => '糟糕！出了点问题,请检查您的网络！';
  @override
  String get OopsEmailorPassword => '糟糕！电子邮件或密码不正确！';
  @override
  String get OopsNotRegister => '哎呀！没有找到用户,如果没有注册考虑先注册！';
  @override
  String get OopsEmailUsed => '糟糕！此电子邮件已被另一个帐户使用！';
  @override
  String get OopsPhoneFormat => '糟糕！电话格式无效';
  @override
  String get NoAudioRecorded => '未添加音频';
  @override
  String get OopsWrong => '哎呀！出事了';
  @override
  String get Optional => '选修的';
  @override
  String get Sending => '发送...';
  @override
  String get NotificationPermission => '保证通知许可';
  @override
  String get Theme => '主题';
  @override
  String get DarkMode => '黑暗模式';
  @override
  String get Others => '其他';
  @override
  String get PeopleWhoReacted => '做出反应的人';
  @override
  String get BadWords => '检测到坏话,您的帐号可能会被暂停！';
  @override
  String get PickFile => '选择文件';
  @override
  String get Loading => '加载中...';
  @override
  String get PleaseAddAudio => '请添加音频';
  @override
  String get PeopleYouMayFollow => '你可能关注的人';
  @override
  String get Unsave => '不保存';
  @override
  String get PostsLikes => '帖子喜欢';
  @override
  String get CommentsLikes => '评论喜欢';
  @override
  String get RepliesLikes => '回复喜欢';
  @override
  String get ShareiConfezz => '分享 iConfezz';
  @override
  String get MemberSince => '成员以来';
  @override
  String get Comment => '评论';
  @override
  String get UnShare => '取消共享';
  @override
  String get Liked => '喜欢';
  @override
  String get Like => '像';
  @override
  String get SAVED => '已保存';
  @override
  String get SHARED => '共享';
  @override
  String get AppLanguage => '应用程序语言';
  @override
  String get ComingSoon => '快来了';
  @override
  String get ReportProblem => '报告一个问题';
  @override
  String get PushNotifications => '推送通知';
  @override
  String get ShareProfile => '分享个人资料';
  @override
  String get DeleteAccount => '删除帐户';
  @override
  String get DeleteYourAccount => '删除您的帐户';
  @override
  String get Record => '记录';
  @override
  String get Or => '或者';
  @override
  String get NoPhotoPicked => '没有选择照片';
  @override
  String get SearchForUsers => '搜索用户';
  @override
  String get NoResultsFound => '未找到结果';
  @override
  String get TemporaryFilesRemoved => '成功删除临时文件';
  @override
  String get FailedToClean => '清理临时文件失败';
  @override
  String get exit => '出口';
  @override
  String get CommunityGuidelines => '社区准则';
  @override
  String get FAQ => '常问问题';
  @override
  String get Paste => 'Paste';
  @override
  String get DoYouWantToPasteThisCode => 'Do you want to paste this code?';
  @override
  String get PasteCode => 'Paste Code';
}

/// Flat map(s) containing all translations.
/// Only for edge cases! For simple maps, use the map function of this library.

extension on _StringsEn {
  Map<String, dynamic> _buildFlatMap() {
    return <String, dynamic>{
      'AppName': 'iConfezz',
      'AboutApp': 'iConfezz is a community dedicated to hear voice of others.',
      'AppVersion': '2.0.0',
      'UnknownError': 'Oops, Something went Wrong!',
      'Next': 'Next',
      'EmailUser': 'Are you Email User, Login here!',
      'Email': 'Email',
      'bio': 'Biography',
      'city': 'City',
      'dob': 'Date of birth',
      'Password': 'Password',
      'Login': 'Login',
      'PhoneNumber': 'Phone Number',
      'PhoneVerification': 'Phone Number Verification',
      'EnterCode': 'Enter the code sent to ',
      'DidntRecieveCode': 'Didn\'t receive the code? ',
      'InvalidSMSCode': 'The SMS Verification Code is Invalid',
      'RESEND': 'RESEND',
      'VERIFY': 'VERIFY',
      'POSTS': 'POSTS',
      'FOLLOWERS': 'Followers',
      'FOLLOWINGS': 'Followings',
      'Follow': 'Follow',
      'Following': 'Following',
      'Register': 'Register',
      'Username': 'Username',
      'FirstName': 'First Name',
      'LastName': 'Last Name',
      'FullName': 'Full Name',
      'Status': 'Status',
      'AddValidUsername': 'Add a valid username',
      'AddValidFirstName': 'Add a valid first name',
      'AddValidLastName': 'Add a valid last name',
      'SelectGender': 'Please select your gender.',
      'Male': 'Male',
      'Female': 'Female',
      'Other': 'Other',
      'AcceptTerms': 'I Accept All Terms & Conditions',
      'Save': 'Save',
      'Search': 'Search',
      'Searching': 'Searching...',
      'SelectPhoneCode': 'Select a phone code',
      'InvalidEmail': 'Invalid Email',
      'ShortPassword': 'Password Must Contain At Least 8 characters',
      'UsernameTaken': 'Username is Already taken',
      'InvalidPhone': 'Please fill up all the cells properly',
      'Message': 'Message',
      'LoginFirst': 'Oops, Please login first',
      'START': 'GET STARTED',
      'Wallpapers': 'Wallpapers',
      'Profile': 'Profile',
      'NoInternet': 'No Internet Access',
      'Settings': 'Settings',
      'Account': 'Account',
      'EditProfile': 'Edit Profile',
      'OnlineStatus': 'Online Status',
      'OnlineDescription': 'Anyone can see when you\'re last Online',
      'Logout': 'Logout',
      'DirectMsgs': 'Chat Messages',
      'DirectMsgsDescr': 'Recieve chat notifications',
      'GroupsMsgs': 'Groups Messages',
      'GroupsMsgsDesc': 'Recieve all Groups Notifications',
      'AddGroupName': 'Please! Add Group Name',
      'About': 'About Us',
      'RateUs': 'Rate Us',
      'EULA': 'EULA Agreement',
      'PrivacyPolicy': 'Privacy Policy',
      'Feed': 'Feed',
      'Post': 'Post',
      'Gallery': 'Gallery',
      'Stories': 'Stories',
      'MyPosts': 'My Posts',
      'Favorites': 'Favorites',
      'Announcement': 'Announcement',
      'WhatOnMind': 'What to Share?',
      'Likes': 'Likes',
      'Comments': 'Comments',
      'CreatePost': 'Create Post',
      'Share': 'Share',
      'Edit': 'Edit',
      'Delete': 'Delete',
      'Copy': 'Copy',
      'Copied': 'Copied',
      'ReadMore': '...Read More',
      'SuccessPostPublish': 'Post has been published Successfully',
      'SuccessPostEdited': 'Post has been edited Successfully',
      'TypeComment': 'Type a comment...',
      'TypeReply': 'Type a reply...',
      'NotAllowedToPublish': 'Sorry! You are not allowed to publish anymore',
      'NotAllowedToComment': 'Sorry! You are not allowed to comment',
      'PostRemoveConfirm': 'Are you sure you want to delete this post?',
      'CommentRemoveConfirm': 'Are you sure you want to delete this comment?',
      'AddSomeContent': 'Please add some content before posting',
      'Reply': 'Reply',
      'Replies': 'Replies',
      'RepliedToComment': 'replied to your comment',
      'Chats': 'Chats',
      'OnlineUsers': 'ONLINE USERS',
      'RecentChats': 'RECENT CHATS',
      'RemoveConversation': 'Remove Conversation',
      'Messaging': 'Message...',
      'CannotChatWithUser': 'Sorry! You can\'t chat wih this user',
      'MsgDeleteConfirm': 'Are you sure you want to delete the message?',
      'NoChatFound': 'No Recents chat yet, start chatting!',
      'Online': 'Online',
      'Typing': 'Typing...',
      'Image': 'Image',
      'Voice': 'Voice',
      'Video': 'Video',
      'Emoji': 'Emoji',
      'GIF': 'GIF',
      'Sticker': 'Sticker',
      'Groups': 'Groups',
      'CreateGroup': 'Create a Group',
      'EditGroup': 'Edit your Group',
      'Members': 'Members',
      'PhotosLibrary': 'Photos Library',
      'TakePicture': 'Take Picture',
      'VideosLibrary': 'Videos Library',
      'RecordVideo': 'Record Video',
      'Cancel': 'Cancel',
      'SlideToCancel': 'Slide to cancel',
      'On': 'On',
      'Off': 'Off',
      'Group': 'Group',
      'LeaveGroup': 'Leave Group',
      'GroupName': 'Group Name',
      'Join': 'Join',
      'Joined': 'Joined',
      'Public': 'Public',
      'Private': 'Private',
      'GroupType': 'Group Type',
      'RemoveMember': 'Remove member',
      'AddMembers': 'Add Members',
      'Block': 'Block',
      'Unblock': 'UnBlock',
      'Ban': 'Ban',
      'Unban': 'Unban',
      'Banned': 'Banned',
      'Newest': 'Newest',
      'Trending': 'Trending',
      'MostDownloaded': 'Most Downloaded',
      'Categories': 'Categories',
      'AddWallpaper': 'Add Wallpaper',
      'WallpaperName': 'Wallpaper Name',
      'Category': 'Category',
      'Upload': 'Upload',
      'Home': 'Home',
      'Lock': 'Lock',
      'Both': 'Both',
      'SetAsWallpaper': 'Set as Wallpaper',
      'WallpaperSet': 'Wallpaper Set Successfully',
      'FailedToUpload': 'Oops! Upload Failed, Please try again',
      'UploadFinished': 'Upload Finished',
      'Downloading': 'Downloading',
      'NoWallpaperSelectedMsg': 'Please Select Wallpaper to continue',
      'NoImgSelected': 'No Image Selected',
      'Notifications': 'Notifications',
      'StartFollowingMsg': 'started following you',
      'PostReactionMsg': 'reacted to your post',
      'PostCommentMsg': 'commented your post',
      'ReplyMsg': 'replied to your comment',
      'CommentReactedMsg': 'reacted to your comment',
      'CannotFindFile': 'Oops! Cannot find the file',
      'NoFilePicked': 'Trying to upload and no file is picked',
      'SentYouMsg': 'Sent you message',
      'Report': 'Report',
      'Unreport': 'Unreport',
      'ReportDesc': 'We remove post that has: ',
      'ReportReasons':
          '⚫️ Sexual content. \n\n⚫️ Violent or repulsive content. \n\n⚫️ Hateful or abusive content. \n\n⚫️ Spam or misleading.',
      'ReportNote': 'We wont let them know if you take this action.',
      'ReportThanks':
          'We will check your request, Thanks for helping improve our community',
      'Admin': 'Admin',
      'ProfanityDetected':
          'Bad words detected, your account may get suspended!',
      'AreYouSure': 'Are you sure to',
      'ConfirmChatDeletion': 'Are you sure to delete chat',
      'ReportedPosts': 'Reported Posts',
      'AllChats': 'All Chats',
      'Users': 'Users',
      'TermsService': 'Terms of Service',
      'WaitingToRecord': 'Waiting to record',
      'AdjustVolume': 'Adjust volume',
      'AdjustSpeed': 'Adjust speed',
      'Ok': 'Ok',
      'ShareExternally': 'Share externally',
      'ShareOnYourWall': 'Share on your wall',
      'Error': 'Error',
      'Me': 'Me',
      'OopsCode': 'Oops! vefication code is wrong',
      'OopsNetwork': 'Oops! something went wrong, Please check your network!',
      'OopsEmailorPassword': 'Oops! email or password is not correct!',
      'OopsNotRegister':
          'Oops! no user found, if not register consider sign up first!',
      'OopsEmailUsed': 'Oops! this email is already used by another account!',
      'OopsPhoneFormat': 'Oops! phone format is invalid',
      'NoAudioRecorded': 'No audio added',
      'OopsWrong': 'Oops, something went wrong',
      'Optional': 'Optional',
      'Sending': 'Sending... ',
      'NotificationPermission': 'Notification permission is garanted',
      'Theme': 'Theme',
      'DarkMode': 'Dark Mode',
      'Others': 'Others',
      'PeopleWhoReacted': 'People who reacted',
      'BadWords': 'Bad words detected, your account may get suspended!',
      'PickFile': 'Pick file',
      'Loading': 'Loading...',
      'PleaseAddAudio': 'Please add audio',
      'PeopleYouMayFollow': 'People you may follow',
      'Unsave': 'Unsave',
      'PostsLikes': 'Posts Likes',
      'CommentsLikes': 'Comments Likes',
      'RepliesLikes': 'Replies Likes',
      'ShareiConfezz': 'Share iConfezz',
      'MemberSince': 'Member since',
      'Comment': 'Comment',
      'UnShare': 'UnShare',
      'Liked': 'Liked',
      'Like': 'Like',
      'SAVED': 'SAVED',
      'SHARED': 'SHARED',
      'AppLanguage': 'App Language',
      'ComingSoon': 'Coming Soon',
      'ReportProblem': 'Report a Problem',
      'PushNotifications': 'Push Notifications',
      'ShareProfile': 'Share Profile',
      'DeleteAccount': 'Delete Account',
      'DeleteYourAccount': 'Delete Your Account',
      'Record': 'Record',
      'Or': 'Or',
      'NoPhotoPicked': 'No Photo Picked',
      'SearchForUsers': 'Search for Users',
      'NoResultsFound': 'No results found',
      'TemporaryFilesRemoved': 'Temporary files removed with success',
      'FailedToClean': 'Failed to clean temporary files',
      'exit': 'exit',
      'CommunityGuidelines': 'Community Guidelines',
      'FAQ': 'FAQ',
      'Paste': 'Paste',
      'DoYouWantToPasteThisCode': 'Do you want to paste this code?',
      'PasteCode': 'Paste Code',
    };
  }
}

extension on _StringsAr {
  Map<String, dynamic> _buildFlatMap() {
    return <String, dynamic>{
      'AppName': 'iConfezz',
      'AboutApp': 'iConfezz هو مجتمع مخصص لسماع صوت الآخرين.',
      'AppVersion': '2.0.0',
      'UnknownError': 'عفوا, حدث خطأ ما!',
      'Next': 'التالي',
      'EmailUser': 'هل أنت مستخدم البريد الإلكتروني ، تسجيل الدخول هنا!',
      'Email': 'البريد الإلكتروني',
      'bio': 'السيرة الذاتية',
      'city': 'المدينة',
      'dob': 'تاريخ الميلاد',
      'Password': 'كلمة المرور',
      'Login': 'تسجيل الدخول',
      'PhoneNumber': 'رقم الهاتف',
      'PhoneVerification': 'التحقق من رقم الهاتف',
      'EnterCode': 'أدخل الرمز المرسل إلى ',
      'DidntRecieveCode': 'لم تتلق الرمز؟ ',
      'InvalidSMSCode': 'رمز التحقق غير صالح',
      'RESEND': 'إعادة إرسال',
      'VERIFY': 'تحقق',
      'POSTS': 'المشاركات',
      'FOLLOWERS': 'المتابعون',
      'FOLLOWINGS': 'اتابع',
      'Follow': 'اتبع',
      'Following': 'متابع',
      'Register': 'تسجيل',
      'Username': 'اسم المستخدم',
      'FirstName': 'الاسم الأول',
      'LastName': 'اسم العائلة',
      'FullName': 'الاسم الكامل',
      'Status': 'الحالة',
      'AddValidUsername': 'إضافة اسم مستخدم صحيح',
      'AddValidFirstName': 'إضافة اسم أول صحيح',
      'AddValidLastName': 'إضافة اسم العائلة صحيح',
      'SelectGender': 'يرجى تحديد جنسك',
      'Male': 'ذكر',
      'Female': 'أنثى',
      'Other': 'أخرى',
      'AcceptTerms': 'أنا أقبل جميع الشروط والأحكام',
      'Save': 'حفظ',
      'Search': 'بحث',
      'Searching': 'البحث...',
      'SelectPhoneCode': 'حدد رمز الهاتف',
      'InvalidEmail': 'بريد إلكتروني غير صحيح',
      'ShortPassword': 'يجب أن تحتوي كلمة المرور على 8 أحرف على الأقل',
      'UsernameTaken': 'اسم المستخدم مأخوذ بالفعل',
      'InvalidPhone': 'رقم هاتف غير صحيح',
      'Message': 'رسالة',
      'LoginFirst': 'عفوا ، يرجى الدخول أولا',
      'START': 'ابدأ',
      'Wallpapers': 'خلفيات',
      'Profile': 'الملف الشخصي',
      'NoInternet': 'لا يوجد اتصال بالإنترنت',
      'Settings': 'الإعدادات',
      'Account': 'الحساب',
      'EditProfile': 'تعديل الملف الشخصي',
      'OnlineStatus': 'حالة الاتصال',
      'OnlineDescription': 'يمكن لأي شخص أن يرى عندما كنت آخر على الانترنت',
      'Logout': 'الخروج',
      'DirectMsgs': 'رسائل الدردشة',
      'DirectMsgsDescr': 'تلقي إشعارات الدردشة',
      'GroupsMsgs': 'رسائل المجموعات',
      'GroupsMsgsDesc': 'تلقي جميع إخطارات المجموعات',
      'AddGroupName': 'من فضلك! إضافة اسم المجموعة',
      'About': 'معلومات عنا',
      'RateUs': 'قيمنا',
      'EULA': 'اتفاقية EULA',
      'PrivacyPolicy': 'سياسة الخصوصية',
      'Feed': 'تغذية',
      'Post': 'بوست',
      'Gallery': 'معرض',
      'Stories': 'قصص',
      'MyPosts': 'مشاركاتي',
      'Favorites': 'المفضلة',
      'Announcement': 'الإعلان',
      'WhatOnMind': 'ماذا الذي تريد مشاركته',
      'Likes': 'إعجاب',
      'Comments': 'تعليقات',
      'CreatePost': 'إنشاء مشاركة',
      'Share': 'مشاركة',
      'Edit': 'تحرير',
      'Delete': 'حذف',
      'Copy': 'نسخ',
      'Copied': 'نسخ',
      'ReadMore': '...اقرأ المزيد',
      'SuccessPostPublish': 'تم نشر المشاركة بنجاح',
      'SuccessPostEdited': 'تم تحرير المشاركة بنجاح',
      'TypeComment': 'اكتب تعليق...',
      'TypeReply': 'اكتب الرد...',
      'NotAllowedToPublish': 'آسف! لا يسمح لك بنشر بعد الآن',
      'NotAllowedToComment': 'آسف! لا يسمح لك تعليق',
      'PostRemoveConfirm': 'هل أنت متأكد من أنك تريد حذف هذا المنشور ؟ ',
      'CommentRemoveConfirm': 'هل أنت متأكد من أنك تريد حذف هذا التعليق ؟ ',
      'AddSomeContent': 'يرجى إضافة بعض المحتوى قبل النشر',
      'Reply': 'الرد',
      'Replies': 'الردود',
      'RepliedToComment': 'رد على تعليقك',
      'Chats': 'الدردشات',
      'OnlineUsers': 'مستخدمون متصلون',
      'RecentChats': 'الدردشات الأخيرة',
      'RemoveConversation': 'إزالة المحادثة',
      'Messaging': 'رسالة...',
      'CannotChatWithUser': 'آسف! لا يمكنك الدردشة مع المستخدم',
      'MsgDeleteConfirm': 'هل أنت متأكد من أنك تريد حذف الرسالة ؟ ',
      'NoChatFound': 'لا توجد دردشة حديثة بعد ، ابدأ الدردشة!',
      'Online': 'متصل',
      'Typing': 'الكتابة...',
      'Image': 'صورة',
      'Voice': 'صوت',
      'Video': 'فيديو',
      'Emoji': 'ايموجي',
      'GIF': 'GIF',
      'Sticker': 'ملصق',
      'Groups': 'المجموعات',
      'CreateGroup': 'إنشاء مجموعة',
      'EditGroup': 'تحرير مجموعتك',
      'Members': 'أعضاء',
      'PhotosLibrary': 'مكتبة الصور',
      'TakePicture': 'التقاط صورة',
      'VideosLibrary': 'مكتبة الفيديو',
      'RecordVideo': 'تسجيل الفيديو',
      'Cancel': 'إلغاء',
      'SlideToCancel': 'Slide to cancel',
      'On': 'على',
      'Off': 'إيقاف',
      'Group': 'المجموعة',
      'LeaveGroup': 'مغادرة المجموعة',
      'GroupName': 'اسم المجموعة',
      'Join': 'انضم',
      'Joined': 'منضم',
      'Public': 'عامة',
      'Private': 'خاصة',
      'GroupType': 'نوع المجموعة',
      'RemoveMember': 'حذف الأعضاء',
      'AddMembers': 'إضافة أعضاء',
      'Block': 'حظر',
      'Unblock': 'إلغاء الحظر',
      'Ban': 'بان',
      'Unban': 'Unban',
      'Banned': 'المحظورة',
      'Newest': 'الأحدث',
      'Trending': 'شائع',
      'MostDownloaded': 'الأكثر تنزيلا',
      'Categories': 'الفئات',
      'AddWallpaper': 'إضافة خلفية',
      'WallpaperName': 'Wallpapername',
      'Category': 'الفئة',
      'Upload': 'تحميل',
      'Home': 'الصفحة الرئيسية',
      'Lock': 'قفل',
      'Both': 'كلاهما',
      'SetAsWallpaper': 'تعيين كخلفية',
      'WallpaperSet': 'خلفية تعيين بنجاح',
      'FailedToUpload': 'عفوا! فشل التحميل ، يرجى المحاولة مرة أخرى',
      'UploadFinished': 'Uploadfinished',
      'Downloading': 'التنزيل',
      'NoWallpaperSelectedMsg': 'الرجاء تحديد خلفية للمتابعة',
      'NoImgSelected': 'لا توجد صورة محددة',
      'Notifications': 'الإشعارات',
      'StartFollowingMsg': 'بدأ في متابعتك',
      'PostReactionMsg': 'اعجب بمشاركتك',
      'PostCommentMsg': 'علق علي مشاركتك',
      'ReplyMsg': 'رد على تعليقك',
      'CommentReactedMsg': 'اعجب بتعليقك',
      'CannotFindFile': 'عفوا! لا يمكن العثور على الملف',
      'NoFilePicked': 'محاولة تحميل ويتم اختيار أي ملف',
      'SentYouMsg': 'أرسل لك رسالة',
      'Report': 'ابلاغ',
      'Unreport': 'عدم ابلاغ',
      'ReportDesc': 'نحن نزيل اي مشاركة تحتوي على: ',
      'ReportReasons':
          ' المحتوى الجنسي. \n\n ⚫ كبيرة العنيفة أو مثيرة للاشمئزاز المحتوى. \n\n ⚫ كبيرة البغيضة أو المحتوى المسيء. \n \n Spam️ البريد المزعج أو مضللة.',
      'ReportNote': 'لن نعلمهم إذا اتخذت هذا الإجراء.',
      'ReportThanks': 'سوف نتحقق من طلبك ، شكرا للمساعدة في تحسين مجتمعنا',
      'Admin': 'المشرف',
      'ProfanityDetected': 'الكلمات السيئة المكتشفة ، قد يتم تعليق حسابك!',
      'AreYouSure': 'هل أنت متأكد من ',
      'ConfirmChatDeletion': 'هل أنت متأكد من حذف الدردشة',
      'ReportedPosts': 'المشاركات المبلغ عنها',
      'AllChats': 'جميع الدردشات',
      'Users': 'المستخدمون',
      'TermsService': 'الشروط والأحكام',
      'WaitingToRecord': 'انتظار للتسجيل',
      'AdjustVolume': 'ضبط مستوى الصوت',
      'AdjustSpeed': 'ضبط السرعة',
      'Ok': 'موافق',
      'ShareExternally': 'شارك خارجيا',
      'ShareOnYourWall': 'شارك على الحائط الخاص بك',
      'Error': 'خطأ',
      'Me': 'أنا',
      'OopsCode': 'عفوا! رمز تحقق خاطئ',
      'OopsNetwork': 'عفوا! حدث خطأ ما ، يرجى التحقق من شبكتك!',
      'OopsEmailorPassword':
          'عفوا! البريد الإلكتروني أو كلمة المرور غير صحيحة!',
      'OopsNotRegister':
          'عفوا! لم يتم العثور على المستخدم, إن لم يكن مسجل يرجي الاشتراك أولا!',
      'OopsEmailUsed':
          'عفوا! يستخدم هذا البريد الإلكتروني بالفعل من قبل حساب آخر!',
      'OopsPhoneFormat': 'عفوا! شكل الهاتف غير صالح',
      'NoAudioRecorded': 'لم يضاف اي تسجيل صوتي',
      'OopsWrong': 'عفوا, حدث خطأ',
      'Optional': 'اختياري',
      'Sending': 'إرسال... ',
      'NotificationPermission': 'تم السماح بالاشعارات',
      'Theme': 'الشكل',
      'DarkMode': 'الوضع الليلي',
      'Others': 'اخري',
      'PeopleWhoReacted': 'الناس الذين تفاعلوا',
      'BadWords': 'تم اكتشاف الكلمات السيئة ، قد يتم تعليق حسابك!',
      'PickFile': 'اختر ملف',
      'Loading': 'تحميل...',
      'PleaseAddAudio': 'يرجى إضافة الصوت',
      'PeopleYouMayFollow': 'أشخاص يمكن ان تتابعهم',
      'Unsave': 'الغاء الحفظ',
      'PostsLikes': 'اعجابات المنشورات',
      'CommentsLikes': 'اعجابات التعليقات',
      'RepliesLikes': 'اعجابات الردود',
      'ShareiConfezz': ' iConfezz مشاركة',
      'MemberSince': 'مشترك منذ',
      'Comment': 'تعليق',
      'UnShare': 'الغاء مشاركة',
      'Liked': 'اعجبني',
      'Like': 'اعجاب',
      'SAVED': 'المحفوظ',
      'SHARED': 'المشارك',
      'AppLanguage': 'لغة التطبيق',
      'ComingSoon': 'قادم قريبا',
      'ReportProblem': 'ابلاغ عن مشكله',
      'PushNotifications': 'إشعارات لحظية',
      'ShareProfile': 'مشاركة الملف الشخصي',
      'DeleteAccount': 'حذف الحساب',
      'DeleteYourAccount': 'حذف حسابك',
      'Record': 'تسجيل',
      'Or': 'أو',
      'NoPhotoPicked': 'لم يتم اختيار أي صورة',
      'SearchForUsers': 'بحث عن مستخدمين',
      'NoResultsFound': 'لم يتم العثور على نتائج',
      'TemporaryFilesRemoved': 'تمت إزالة الملفات المؤقتة بنجاح',
      'FailedToClean': 'فشل إزالة الملفات المؤقتة',
      'exit': 'الخروج',
      'CommunityGuidelines': 'إرشادات المجتمع',
      'FAQ': 'التعليمات',
      'Paste': 'لصق',
      'DoYouWantToPasteThisCode': 'هل تريد لصق هذا الرمز؟',
      'PasteCode': 'لصق هذا الرمز',
    };
  }
}

extension on _StringsDe {
  Map<String, dynamic> _buildFlatMap() {
    return <String, dynamic>{
      'AppName': 'iConfezz',
      'AboutApp':
          'iConfezz ist eine Community, die sich dafür einsetzt, die Stimme anderer zu hören.',
      'AppVersion': '2.0.0',
      'UnknownError': 'Etwas ist schief gelaufen',
      'Next': 'Nächste',
      'EmailUser': 'Sind Sie E-Mail-Benutzer, melden Sie sich hier an!',
      'Email': 'Email',
      'bio': 'Biografie',
      'city': 'Stadt',
      'dob': 'Geburtsdatum',
      'Password': 'Passwort',
      'Login': 'Anmeldung',
      'PhoneNumber': 'Telefonnummer',
      'PhoneVerification': 'Verifizierung der Telefonnummer',
      'EnterCode': 'Geben Sie den gesendeten Code ein an ',
      'DidntRecieveCode': 'Sie haben den Code nicht erhalten?',
      'InvalidSMSCode': 'Der SMS-Bestätigungscode ist ungültig',
      'RESEND': 'ERNEUT SENDEN',
      'VERIFY': 'VERIFIZIEREN',
      'POSTS': 'POSTEN',
      'FOLLOWERS': 'Follower',
      'FOLLOWINGS': 'Folgen',
      'Follow': 'Folgen',
      'Following': 'folgend',
      'Register': 'Registrieren',
      'Username': 'Nutzername',
      'FirstName': 'Vorname',
      'LastName': 'Familienname, Nachname',
      'FullName': 'Vollständiger Name',
      'Status': 'Status',
      'AddValidUsername': 'Gültigen Benutzernamen hinzufügen',
      'AddValidFirstName': 'Gültigen Vornamen hinzufügen',
      'AddValidLastName': 'Gültigen Nachnamen hinzufügen',
      'SelectGender': 'Bitte wählen Sie Ihr Geschlecht.',
      'Male': 'Männlich',
      'Female': 'Weiblich',
      'Other': 'Sonstiges',
      'AcceptTerms': 'Ich akzeptiere alle Geschäftsbedingungen',
      'Save': 'Speichern',
      'Search': 'Suche',
      'Searching': 'Suche...',
      'SelectPhoneCode': 'Wählen Sie einen Telefoncode',
      'InvalidEmail': 'Ungültige E-Mail',
      'ShortPassword': 'Passwort muss mindestens 8 Zeichen enthalten',
      'UsernameTaken': 'Benutzername ist bereits vergeben',
      'InvalidPhone': 'Bitte füllen Sie alle Zellen richtig aus',
      'Message': 'Nachricht',
      'LoginFirst': 'Hoppla, bitte melden Sie sich zuerst an',
      'START': 'LOSLEGEN',
      'Wallpapers': 'Hintergründe',
      'Profile': 'Profil',
      'NoInternet': 'Kein Internetzugang',
      'Settings': 'Einstellungen',
      'Account': 'Konto',
      'EditProfile': 'Profil bearbeiten',
      'OnlineStatus': 'Online Status',
      'OnlineDescription': 'Jeder kann sehen, wann du zuletzt online warst',
      'Logout': 'Ausloggen',
      'DirectMsgs': 'Chat-Nachrichten',
      'DirectMsgsDescr': 'Chat-Benachrichtigungen erhalten',
      'GroupsMsgs': 'Gruppennachrichten',
      'GroupsMsgsDesc': 'Alle Gruppenbenachrichtigungen erhalten',
      'AddGroupName': 'Bitte! Gruppennamen hinzufügen',
      'About': 'über uns',
      'RateUs': 'Bewerten Sie uns',
      'EULA': 'EULA-Vereinbarung',
      'PrivacyPolicy': 'Datenschutz-Bestimmungen',
      'Feed': 'Füttern',
      'Post': 'Post',
      'Gallery': 'Galerie',
      'Stories': 'Geschichten',
      'MyPosts': 'Meine Posts',
      'Favorites': 'Favoriten',
      'Announcement': 'Bekanntmachung',
      'WhatOnMind': 'Was teilen?',
      'Likes': 'Likes',
      'Comments': 'Bemerkungen',
      'CreatePost': 'Beitrag erstellen',
      'Share': 'Teilen',
      'Edit': 'Bearbeiten',
      'Delete': 'Löschen',
      'Copy': 'Kopieren',
      'Copied': 'Kopiert',
      'ReadMore': '...Weiterlesen',
      'SuccessPostPublish': 'Beitrag wurde erfolgreich veröffentlicht',
      'SuccessPostEdited': 'Beitrag wurde erfolgreich bearbeitet',
      'TypeComment': 'Geben Sie einen Kommentar ein...',
      'TypeReply': 'Geben Sie eine Antwort ein...',
      'NotAllowedToPublish':
          'Entschuldigung! Sie dürfen nicht mehr veröffentlichen',
      'NotAllowedToComment': 'Entschuldigung! Sie dürfen nicht kommentieren',
      'PostRemoveConfirm':
          'Sind Sie sicher, dass Sie diesen Beitrag löschen möchten?',
      'CommentRemoveConfirm':
          'Sind Sie sicher, dass Sie diesen Kommentar löschen möchten?',
      'AddSomeContent': 'Bitte fügen Sie vor dem Posten Inhalte hinzu',
      'Reply': 'Antwort',
      'Replies': 'Antworten',
      'RepliedToComment': 'hat auf deinen Kommentar geantwortet',
      'Chats': 'Chats',
      'OnlineUsers': 'ONLINE BENUTZER',
      'RecentChats': 'LETZTE CHATS',
      'RemoveConversation': 'Gespräch entfernen',
      'Messaging': 'Nachricht...',
      'CannotChatWithUser':
          'Entschuldigung! Sie können mit diesem Benutzer nicht chatten',
      'MsgDeleteConfirm':
          'Sind Sie sicher, dass Sie die Nachricht löschen möchten?',
      'NoChatFound':
          'Noch kein Chat mit den neuesten Nachrichten, beginnen Sie mit dem Chat!',
      'Online': 'Online',
      'Typing': 'Schreiben...',
      'Image': 'Bild',
      'Voice': 'Stimme',
      'Video': 'Video',
      'Emoji': 'Emoji',
      'GIF': 'GIF',
      'Sticker': 'Aufkleber',
      'Groups': 'Gruppen',
      'CreateGroup': 'Erstellen Sie eine Gruppe',
      'EditGroup': 'Gruppe bearbeiten',
      'Members': 'Mitglieder',
      'PhotosLibrary': 'Fotobibliothek',
      'TakePicture': 'Ein Bild machen',
      'VideosLibrary': 'Videobibliothek',
      'RecordVideo': 'Ein Video aufnehmen',
      'Cancel': 'Absagen',
      'SlideToCancel': 'Zum Abbrechen schieben',
      'On': 'Auf',
      'Off': 'Aus',
      'Group': 'Gruppe',
      'LeaveGroup': 'Gruppe verlassen',
      'GroupName': 'Gruppenname',
      'Join': 'Beitreten',
      'Joined': 'Trat bei',
      'Public': 'Öffentlich',
      'Private': 'Privat',
      'GroupType': 'Gruppentyp',
      'RemoveMember': 'Mitglied entfernen',
      'AddMembers': 'Fügen Sie Mitglieder hinzu',
      'Block': 'Block',
      'Unblock': 'Blockierung aufheben',
      'Ban': 'Verbot',
      'Unban': 'Bann aufheben',
      'Banned': 'Verboten',
      'Newest': 'Neueste',
      'Trending': 'Trend',
      'MostDownloaded': 'Am meisten heruntergeladen',
      'Categories': 'Kategorien',
      'AddWallpaper': 'Hintergrundbild hinzufügen',
      'WallpaperName': 'Hintergrundname',
      'Category': 'Kategorie',
      'Upload': 'Hochladen',
      'Home': 'Heim',
      'Lock': 'Sperren',
      'Both': 'Beide',
      'SetAsWallpaper': 'Als Hintergrundbild festlegen',
      'WallpaperSet': 'Wallpaper erfolgreich eingestellt',
      'FailedToUpload':
          'Hoppla! Hochladen fehlgeschlagen, bitte versuchen Sie es erneut',
      'UploadFinished': 'Hochladen abgeschlossen',
      'Downloading': 'Wird heruntergeladen',
      'NoWallpaperSelectedMsg':
          'Bitte Hintergrundbild auswählen, um fortzufahren',
      'NoImgSelected': 'Kein Bild ausgewählt',
      'Notifications': 'Benachrichtigungen',
      'StartFollowingMsg': 'Begann dir zu folgen',
      'PostReactionMsg': 'auf deinen Beitrag reagiert',
      'PostCommentMsg': 'hat deinen Beitrag kommentiert',
      'ReplyMsg': 'hat auf deinen Kommentar geantwortet',
      'CommentReactedMsg': 'auf Ihren Kommentar reagiert',
      'CannotFindFile': 'Hoppla! Kann die Datei nicht finden',
      'NoFilePicked':
          'Versuche hochzuladen und es wurde keine Datei ausgewählt',
      'SentYouMsg': 'Ich habe Ihnen eine Nachricht gesendet',
      'Report': 'Prüfbericht',
      'Unreport': 'Nicht melden',
      'ReportDesc': 'Wir entfernen Beiträge mit: ',
      'ReportReasons':
          '⚫️ Sexuelle Inhalte. \n\n⚫️ Gewalttätige oder abstoßende Inhalte. \n\n⚫️ Hasserfüllte oder beleidigende Inhalte. \n\n⚫️ Spam oder irreführend.',
      'ReportNote':
          'Wir werden es ihnen nicht mitteilen, wenn Sie diese Maßnahme ergreifen.',
      'ReportThanks':
          'Wir werden Ihre Anfrage prüfen. Vielen Dank für Ihre Hilfe bei der Verbesserung unserer Community',
      'Admin': 'Administrator',
      'ProfanityDetected':
          'Schlimme Wörter entdeckt, Ihr Konto wird möglicherweise gesperrt!',
      'AreYouSure': 'Sind Sie sicher',
      'ConfirmChatDeletion':
          'Sind Sie sicher, dass Sie den Chat löschen möchten',
      'ReportedPosts': 'Gemeldete Beiträge',
      'AllChats': 'Alle Chats',
      'Users': 'Benutzer',
      'TermsService': 'Nutzungsbedingungen',
      'WaitingToRecord': 'Auf Aufnahme warten',
      'AdjustVolume': 'Lautstärke anpassen',
      'AdjustSpeed': 'Geschwindigkeit anpassen',
      'Ok': 'In Ordnung',
      'ShareExternally': 'Extern teilen',
      'ShareOnYourWall': 'Teil es auf deiner Pinnwand',
      'Error': 'Fehler',
      'Me': 'Mir',
      'OopsCode': 'Ups! Bestätigungscode ist falsch',
      'OopsNetwork':
          'Hoppla! etwas ist schief gelaufen, bitte überprüfen Sie Ihr Netzwerk!',
      'OopsEmailorPassword': 'Ups! E-Mail oder Passwort ist nicht korrekt!',
      'OopsNotRegister':
          'Hoppla! kein Benutzer gefunden, wenn nicht, erwägen Sie, sich zuerst anzumelden!',
      'OopsEmailUsed':
          'Hoppla! Diese E-Mail wird bereits von einem anderen Konto verwendet!',
      'OopsPhoneFormat': 'Hoppla! Telefonformat ist ungültig',
      'NoAudioRecorded': 'Kein Audio hinzugefügt',
      'OopsWrong': 'Ups! Irgendwas lief schief',
      'Optional': 'Optional',
      'Sending': 'Senden...',
      'NotificationPermission': 'Benachrichtigungserlaubnis ist gewährleistet',
      'Theme': 'Thema',
      'DarkMode': 'Dunkler Modus',
      'Others': 'Andere',
      'PeopleWhoReacted': 'Menschen, die reagiert haben',
      'BadWords':
          'Schlimme Wörter entdeckt, Ihr Konto wird möglicherweise gesperrt!',
      'PickFile': 'Datei auswählen',
      'Loading': 'Wird geladen...',
      'PleaseAddAudio': 'Bitte Audio hinzufügen',
      'PeopleYouMayFollow': 'Menschen, denen Sie folgen können',
      'Unsave': 'Nicht speichern',
      'PostsLikes': 'Gefällt mir für Beiträge',
      'CommentsLikes': 'Gefällt mir Kommentare',
      'RepliesLikes': 'Antworten Likes',
      'ShareiConfezz': 'iConfezz teilen',
      'MemberSince': 'Mitglied seit',
      'Comment': 'Kommentar',
      'UnShare': 'Freigabe aufheben',
      'Liked': 'Gefallen',
      'Like': 'Wie',
      'SAVED': 'GERETTET',
      'SHARED': 'GETEILT',
      'AppLanguage': 'App-Sprache',
      'ComingSoon': 'Kommt bald',
      'ReportProblem': 'Ein Problem melden',
      'PushNotifications': 'Mitteilungen',
      'ShareProfile': 'Profil teilen',
      'DeleteAccount': 'Konto löschen',
      'DeleteYourAccount': 'Lösche deinen Account',
      'Record': 'Aufzeichnung',
      'Or': 'Oder',
      'NoPhotoPicked': 'Kein Foto ausgewählt',
      'SearchForUsers': 'Benutzer suchen',
      'NoResultsFound': 'Keine Ergebnisse gefunden',
      'TemporaryFilesRemoved': 'Temporäre Dateien erfolgreich entfernt',
      'FailedToClean': 'Fehler beim Bereinigen temporärer Dateien',
      'exit': 'Ausfahrt',
      'CommunityGuidelines': 'Community-Richtlinien',
      'FAQ': 'FAQ',
      'Paste': 'Paste',
      'DoYouWantToPasteThisCode': 'Do you want to paste this code?',
      'PasteCode': 'Paste Code',
    };
  }
}

extension on _StringsEs {
  Map<String, dynamic> _buildFlatMap() {
    return <String, dynamic>{
      'AppName': 'iConfezz',
      'AboutApp':
          'iConfezz es una comunidad dedicada a escuchar la voz de los demás.',
      'AppVersion': '2.0.0',
      'UnknownError': 'Algo salió mal',
      'Next': 'Próximo',
      'EmailUser':
          '¿Eres un usuario de correo electrónico? ¡Inicia sesión aquí!',
      'Email': 'Correo electrónico',
      'bio': 'Biografía',
      'city': 'Ciudad',
      'dob': 'Fecha de cumpleaños',
      'Password': 'Clave',
      'Login': 'Acceso',
      'PhoneNumber': 'Número de teléfono',
      'PhoneVerification': 'Verificación del número de teléfono',
      'EnterCode': 'Ingrese el código enviado a ',
      'DidntRecieveCode': '¿No recibiste el código?',
      'InvalidSMSCode': 'El código de verificación de SMS no es válido',
      'RESEND': 'REENVIAR',
      'VERIFY': 'VERIFICAR',
      'POSTS': 'PUBLICACIONES',
      'FOLLOWERS': 'Seguidores',
      'FOLLOWINGS': 'Seguimientos',
      'Follow': 'Seguir',
      'Following': 'Siguiente',
      'Register': 'Registro',
      'Username': 'Nombre de usuario',
      'FirstName': 'Primer nombre',
      'LastName': 'Apellido',
      'FullName': 'Nombre completo',
      'Status': 'Estado',
      'AddValidUsername': 'Agregar un nombre de usuario válido',
      'AddValidFirstName': 'Añadir un nombre válido',
      'AddValidLastName': 'Añadir un apellido válido',
      'SelectGender': 'Por favor seleccione su género.',
      'Male': 'Masculino',
      'Female': 'Femenino',
      'Other': 'Otro',
      'AcceptTerms': 'Acepto todos los términos y condiciones',
      'Save': 'Guardar',
      'Search': 'Búsqueda',
      'Searching': 'Buscando...',
      'SelectPhoneCode': 'Seleccione un código de teléfono',
      'InvalidEmail': 'Email inválido',
      'ShortPassword': 'La contraseña debe contener al menos 8 caracteres',
      'UsernameTaken': 'Este nombre de usuario ya está tomado',
      'InvalidPhone': 'Por favor llene todas las celdas correctamente',
      'Message': 'Mensaje',
      'LoginFirst': 'Vaya, inicie sesión primero',
      'START': 'EMPEZAR',
      'Wallpapers': 'Fondos de pantalla',
      'Profile': 'Perfil',
      'NoInternet': 'Sin acceso a Internet',
      'Settings': 'Ajustes',
      'Account': 'Cuenta',
      'EditProfile': 'Editar perfil',
      'OnlineStatus': 'Estado en línea',
      'OnlineDescription':
          'Cualquiera puede ver cuándo estuvo en línea por última vez',
      'Logout': 'Cerrar sesión',
      'DirectMsgs': 'Mensajes de chat',
      'DirectMsgsDescr': 'Recibir notificaciones de chat',
      'GroupsMsgs': 'Mensajes de Grupos',
      'GroupsMsgsDesc': 'Recibir Notificaciones de todos los Grupos',
      'AddGroupName': '¡Por favor! Agregue el nombre del grupo',
      'About': 'Sobre nosotros',
      'RateUs': 'Nos califica',
      'EULA': 'Acuerdo CLUF',
      'PrivacyPolicy': 'Política de privacidad',
      'Feed': 'Alimentación',
      'Post': 'Correo',
      'Gallery': 'Galería',
      'Stories': 'Cuentos',
      'MyPosts': 'Mis publicaciones',
      'Favorites': 'Favoritos',
      'Announcement': 'Anuncio',
      'WhatOnMind': '¿Qué compartir?',
      'Likes': 'Gustos',
      'Comments': 'Comentarios',
      'CreatePost': 'Crear publicación',
      'Share': 'Cuota',
      'Edit': 'Editar',
      'Delete': 'Borrar',
      'Copy': 'Dupdo',
      'Copied': 'Copiado',
      'ReadMore': '...Lee mas',
      'SuccessPostPublish': 'La publicación ha sido publicada con éxito',
      'SuccessPostEdited': 'La publicación ha sido editada con éxito',
      'TypeComment': 'Escribe un comentario...',
      'TypeReply': 'Escribe una respuesta...',
      'NotAllowedToPublish': '¡Lo siento! Ya no tienes permiso para publicar',
      'NotAllowedToComment': '¡Lo siento! No tienes permitido comentar',
      'PostRemoveConfirm':
          '¿Estás seguro de que quieres eliminar esta publicación?',
      'CommentRemoveConfirm':
          '¿Está seguro de que desea eliminar este comentario?',
      'AddSomeContent': 'Agregue contenido antes de publicar',
      'Reply': 'Respuesta',
      'Replies': 'Respuestas',
      'RepliedToComment': 'respondí a tu comentario',
      'Chats': 'Chats',
      'OnlineUsers': 'USUARIOS EN LÍNEA',
      'RecentChats': 'CHATS RECIENTES',
      'RemoveConversation': 'Eliminar conversación',
      'Messaging': 'Mensaje...',
      'CannotChatWithUser': '¡Lo siento! No puedes chatear con este usuario',
      'MsgDeleteConfirm': '¿Está seguro de que desea eliminar el mensaje?',
      'NoChatFound': 'Todavía no hay chat de Recientes, ¡empieza a chatear!',
      'Online': 'En línea',
      'Typing': 'Mecanografía...',
      'Image': 'Imagen',
      'Voice': 'Voz',
      'Video': 'Video',
      'Emoji': 'Emoji',
      'GIF': 'GIF',
      'Sticker': 'Pegatina',
      'Groups': 'Grupos',
      'CreateGroup': 'Crear un grupo',
      'EditGroup': 'Edita tu Grupo',
      'Members': 'Miembros',
      'PhotosLibrary': 'Biblioteca de Fotos',
      'TakePicture': 'Tomar la foto',
      'VideosLibrary': 'Biblioteca de videos',
      'RecordVideo': 'Grabar video',
      'Cancel': 'Cancelar',
      'SlideToCancel': 'Deslizar para cancelar',
      'On': 'Sobre',
      'Off': 'Apagado',
      'Group': 'Grupo',
      'LeaveGroup': 'Abandonar grupo',
      'GroupName': 'Nombre del grupo',
      'Join': 'Entrar',
      'Joined': 'Unido',
      'Public': 'Público',
      'Private': 'Privado',
      'GroupType': 'Tipo de grupo',
      'RemoveMember': 'Eliminar miembro',
      'AddMembers': 'Añadir miembros',
      'Block': 'Cuadra',
      'Unblock': 'Desatascar',
      'Ban': 'Prohibición',
      'Unban': 'Desbloquear',
      'Banned': 'Prohibido',
      'Newest': 'El más nuevo',
      'Trending': 'Tendencias',
      'MostDownloaded': 'Más descargados',
      'Categories': 'Categorías',
      'AddWallpaper': 'Agregar fondo de pantalla',
      'WallpaperName': 'Nombre del fondo de pantalla',
      'Category': 'Categoría',
      'Upload': 'Subir',
      'Home': 'Casa',
      'Lock': 'Cerrar con llave',
      'Both': 'Ambas cosas',
      'SetAsWallpaper': 'Establecer como fondo de pantalla',
      'WallpaperSet': 'Fondo de pantalla establecido con éxito',
      'FailedToUpload': '¡Vaya! Carga fallida, inténtalo de nuevo',
      'UploadFinished': 'Carga Finalizada',
      'Downloading': 'Descargando',
      'NoWallpaperSelectedMsg':
          'Seleccione el fondo de pantalla para continuar',
      'NoImgSelected': 'Ninguna imagen seleccionada',
      'Notifications': 'Notificaciones',
      'StartFollowingMsg': 'empecé a seguirte',
      'PostReactionMsg': 'reaccioné a tu publicación',
      'PostCommentMsg': 'comenté tu publicación',
      'ReplyMsg': 'respondí a tu comentario',
      'CommentReactedMsg': 'reaccioné a tu comentario',
      'CannotFindFile': '¡Vaya! No se puede encontrar el archivo',
      'NoFilePicked': 'Intentando cargar y no se selecciona ningún archivo',
      'SentYouMsg': 'Te envié un mensaje',
      'Report': 'Reporte',
      'Unreport': 'No denunciar',
      'ReportDesc': 'Eliminamos la publicación que tiene: ',
      'ReportReasons':
          '⚫️ Contenido sexual. \n\n⚫️ Contenido violento o repulsivo. \n\n⚫️ Contenido odioso o abusivo. \n\n⚫️ Spam o engañoso.',
      'ReportNote': 'No les haremos saber si tomas esta acción.',
      'ReportThanks':
          'Revisaremos su solicitud, gracias por ayudar a mejorar nuestra comunidad',
      'Admin': 'Administración',
      'ProfanityDetected':
          '¡Se detectaron malas palabras, su cuenta puede ser suspendida!',
      'AreYouSure': '¿Estás seguro de?',
      'ConfirmChatDeletion': '¿Estás seguro de eliminar el chat?',
      'ReportedPosts': 'Publicaciones reportadas',
      'AllChats': 'Todos los chats',
      'Users': 'Usuarios',
      'TermsService': 'Términos de servicio',
      'WaitingToRecord': 'Esperando para grabar',
      'AdjustVolume': 'Ajusta el volúmen',
      'AdjustSpeed': 'Ajustar velocidad',
      'Ok': 'De acuerdo',
      'ShareExternally': 'Compartir externamente',
      'ShareOnYourWall': 'Compartir en tu muro',
      'Error': 'Error',
      'Me': 'Me',
      'OopsCode': '¡Ups! El código de verificación es incorrecto',
      'OopsNetwork': '¡Vaya! Algo salió mal. ¡Por favor revise su red!',
      'OopsEmailorPassword':
          '¡Vaya! ¡El correo electrónico o la contraseña no son correctos!',
      'OopsNotRegister':
          '¡Vaya! No se encontró ningún usuario, si no se registra, ¡considere registrarse primero!',
      'OopsEmailUsed': '¡Vaya! ¡Otra cuenta ya usa este correo electrónico!',
      'OopsPhoneFormat': '¡Vaya! El formato del teléfono no es válido',
      'NoAudioRecorded': 'Sin audio agregado',
      'OopsWrong': 'Huy! Algo salió mal',
      'Optional': 'Opcional',
      'Sending': 'Enviando... ',
      'NotificationPermission': 'El permiso de notificación está garantizado',
      'Theme': 'Temática',
      'DarkMode': 'Modo oscuro',
      'Others': 'Otros',
      'PeopleWhoReacted': 'Gente que reaccionó',
      'BadWords':
          '¡Se detectaron malas palabras, su cuenta puede ser suspendida!',
      'PickFile': 'Seleccionar archivo',
      'Loading': 'Cargando...',
      'PleaseAddAudio': 'Por favor agregue audio',
      'PeopleYouMayFollow': 'Personas a las que puedes seguir',
      'Unsave': 'No guardar',
      'PostsLikes': 'Me gusta de las publicaciones',
      'CommentsLikes': 'Me gusta comentarios',
      'RepliesLikes': 'Me gusta de respuestas',
      'ShareiConfezz': 'Compartir iConfezz',
      'MemberSince': 'Miembro desde',
      'Comment': 'Comentario',
      'UnShare': 'Dejar de compartir',
      'Liked': 'Gustó',
      'Like': 'Me gusta',
      'SAVED': 'SALVADO',
      'SHARED': 'COMPARTIDO',
      'AppLanguage': 'Idioma de la aplicación',
      'ComingSoon': 'Próximamente',
      'ReportProblem': 'Informar de un problema',
      'PushNotifications': 'Notificaciones push',
      'ShareProfile': 'Compartir perfil',
      'DeleteAccount': 'Borrar cuenta',
      'DeleteYourAccount': 'Eliminar su cuenta',
      'Record': 'Registro',
      'Or': 'O',
      'NoPhotoPicked': 'Sin foto seleccionada',
      'SearchForUsers': 'Buscar Usuarios',
      'NoResultsFound': 'No se han encontrado resultados',
      'TemporaryFilesRemoved': 'Archivos temporales eliminados con éxito',
      'FailedToClean': 'Error al limpiar archivos temporales',
      'exit': 'salida',
      'CommunityGuidelines': 'Principios de la Comunidad',
      'FAQ': 'PREGUNTAS MÁS FRECUENTES',
      'Paste': 'Paste',
      'DoYouWantToPasteThisCode': 'Do you want to paste this code?',
      'PasteCode': 'Paste Code',
    };
  }
}

extension on _StringsFa {
  Map<String, dynamic> _buildFlatMap() {
    return <String, dynamic>{
      'AppName': 'iConfezz',
      'AboutApp':
          'iConfezz انجمنی است که برای شنیدن صدای دیگران اختصاص داده شده است.',
      'AppVersion': '2.0.0',
      'UnknownError': 'چیزی اشتباه شد',
      'Next': 'بعد',
      'EmailUser': 'آیا شما کاربر ایمیل هستید, اینجا وارد شوید!',
      'Email': 'پست الکترونیک',
      'bio': 'زندگینامه',
      'city': 'شهر',
      'dob': 'تاریخ تولد',
      'Password': 'کلمه عبور',
      'Login': 'وارد شدن',
      'PhoneNumber': 'شماره تلفن',
      'PhoneVerification': 'تأیید شماره تلفن',
      'EnterCode': 'کد ارسال شده را وارد کنید',
      'DidntRecieveCode': 'کد را دریافت نکردید؟',
      'InvalidSMSCode': 'کد تایید پیامک نامعتبر است',
      'RESEND': 'ارسال مجدد',
      'VERIFY': 'تأیید',
      'POSTS': 'نوشته ها',
      'FOLLOWERS': 'پیروان',
      'FOLLOWINGS': 'پیروان',
      'Follow': 'دنبال کردن',
      'Following': 'ذیل',
      'Register': 'ثبت نام',
      'Username': 'نام کاربری',
      'FirstName': 'نام کوچک',
      'LastName': 'نام خانوادگی',
      'FullName': 'نام و نام خانوادگی',
      'Status': 'وضعیت',
      'AddValidUsername': 'یک نام کاربری معتبر اضافه کنید',
      'AddValidFirstName': 'یک نام معتبر اضافه کنید',
      'AddValidLastName': 'یک نام خانوادگی معتبر اضافه کنید',
      'SelectGender': 'لطفا جنسیت خود را انتخاب کنید.',
      'Male': 'نر',
      'Female': 'مونث',
      'Other': 'دیگر',
      'AcceptTerms': 'من همه شرایط و ضوابط را می پذیرم',
      'Save': 'صرفه جویی',
      'Search': 'جستجو کردن',
      'Searching': 'جستجوکردن...',
      'SelectPhoneCode': 'انتخاب یک کد تلفن',
      'InvalidEmail': 'ایمیل نامعتبر',
      'ShortPassword': 'رمز عبور باید حداقل 8 کاراکتر داشته باشد',
      'UsernameTaken': 'نام کاربری قبلاً گرفته شده است',
      'InvalidPhone': 'لطفاً تمام سلول ها را به درستی پر کنید',
      'Message': 'پیام',
      'LoginFirst': 'اوه, لطفا ابتدا وارد شوید',
      'START': 'شروع کنید',
      'Wallpapers': 'تصاویر پس زمینه',
      'Profile': 'مشخصات',
      'NoInternet': 'بدون دسترسی به اینترنت',
      'Settings': 'تنظیمات',
      'Account': 'حساب',
      'EditProfile': 'ویرایش نمایه',
      'OnlineStatus': 'وضعیت آنلاین',
      'OnlineDescription':
          'همه می توانند ببینند آخرین بار چه زمانی آنلاین هستید',
      'Logout': 'خروج',
      'DirectMsgs': 'پیام های چت',
      'DirectMsgsDescr': 'دریافت اعلان های چت',
      'GroupsMsgs': 'پیام های گروهی',
      'GroupsMsgsDesc': 'دریافت اعلان های همه گروه ها',
      'AddGroupName': 'لطفا! نام گروه را اضافه کنید',
      'About': 'درباره ما',
      'RateUs': 'به ما رای دهید',
      'EULA': 'توافقنامه EULA',
      'PrivacyPolicy': 'سیاست حفظ حریم خصوصی',
      'Feed': 'خوراک',
      'Post': 'پست',
      'Gallery': 'آلبوم عکس',
      'Stories': 'داستان ها',
      'MyPosts': 'پست های من',
      'Favorites': 'موارد دلخواه',
      'Announcement': 'اطلاعیه',
      'WhatOnMind': 'چه چیزی را به اشتراک بگذاریم؟',
      'Likes': 'دوست دارد',
      'Comments': 'نظرات',
      'CreatePost': 'ایجاد پست',
      'Share': 'اشتراک گذاری',
      'Edit': 'ویرایش',
      'Delete': 'حذف',
      'Copy': 'کپی 🀄',
      'Copied': 'کپی شده',
      'ReadMore': '...ادامه مطلب',
      'SuccessPostPublish': 'پست با موفقیت منتشر شد',
      'SuccessPostEdited': 'پست با موفقیت ویرایش شد',
      'TypeComment': 'یک نظر تایپ کنید...',
      'TypeReply': 'پاسخ را تایپ کنید...',
      'NotAllowedToPublish': 'با عرض پوزش! شما دیگر اجازه انتشار ندارید',
      'NotAllowedToComment': 'با عرض پوزش! شما مجاز به نظر دادن نیستید',
      'PostRemoveConfirm': 'آیا مطمئن هستید که می خواهید این پست را حذف کنید؟',
      'CommentRemoveConfirm':
          'آیا مطمئن هستید که می خواهید این نظر را حذف کنید؟',
      'AddSomeContent': 'لطفا قبل از ارسال مقداری محتوا اضافه کنید',
      'Reply': 'پاسخ',
      'Replies': 'پاسخ ها',
      'RepliedToComment': 'به نظر شما پاسخ دادم',
      'Chats': 'چت ها',
      'OnlineUsers': 'کاربران آنلاین',
      'RecentChats': 'چت های اخیر',
      'RemoveConversation': 'حذف مکالمه',
      'Messaging': 'پیام...',
      'CannotChatWithUser': 'متأسفیم! شما نمی توانید با این کاربر چت کنید',
      'MsgDeleteConfirm': 'آیا مطمئن هستید که می خواهید پیام را حذف کنید؟',
      'NoChatFound': 'هنوز گفتگوی اخیر وجود ندارد, چت را شروع کنید!',
      'Online': 'برخط',
      'Typing': 'تایپ کردن...',
      'Image': 'تصویر',
      'Voice': 'صدا',
      'Video': 'ویدئو',
      'Emoji': 'اموجی',
      'GIF': 'GIF',
      'Sticker': 'استیکر',
      'Groups': 'گروه ها',
      'CreateGroup': 'ایجاد یک گروه',
      'EditGroup': 'گروه خود را ویرایش کنید',
      'Members': 'اعضا',
      'PhotosLibrary': 'کتابخانه عکس',
      'TakePicture': 'عکس بگیر',
      'VideosLibrary': 'کتابخانه فیلم',
      'RecordVideo': 'ضبط تصویر',
      'Cancel': 'لغو',
      'SlideToCancel': 'برای لغو اسلاید کنید',
      'On': 'بر',
      'Off': 'خاموش',
      'Group': 'گروه',
      'LeaveGroup': 'گروه را ترک کن',
      'GroupName': 'اسم گروه',
      'Join': 'پیوستن',
      'Joined': 'پیوستن',
      'Public': 'عمومی',
      'Private': 'خصوصی',
      'GroupType': 'نوع گروه',
      'RemoveMember': 'حذف عضو',
      'AddMembers': 'افزودن اعضا',
      'Block': 'مسدود کردن',
      'Unblock': 'لغو انسداد',
      'Ban': 'ممنوع کردن',
      'Unban': 'لغو ممنوع',
      'Banned': 'ممنوع',
      'Newest': 'جدیدترین',
      'Trending': 'روند',
      'MostDownloaded': 'بیشترین میزان دانلود شده',
      'Categories': 'دسته بندی ها',
      'AddWallpaper': 'افزودن تصویر زمینه',
      'WallpaperName': 'نام کاغذ دیواری',
      'Category': 'دسته بندی',
      'Upload': 'بارگذاری',
      'Home': 'خانه',
      'Lock': 'قفل کردن',
      'Both': 'هر دو',
      'SetAsWallpaper': 'تنظیم به عنوان تصویر زمینه',
      'WallpaperSet': 'کاغذ دیواری با موفقیت تنظیم شد',
      'FailedToUpload': 'اوه! آپلود انجام نشد, لطفا دوباره امتحان کنید',
      'UploadFinished': 'آپلود به پایان رسید',
      'Downloading': 'دانلود',
      'NoWallpaperSelectedMsg': 'لطفا برای ادامه تصویر زمینه را انتخاب کنید',
      'NoImgSelected': 'هیچ تصویری انتخاب نشده است',
      'Notifications': 'اطلاعیه',
      'StartFollowingMsg': 'شروع به تعقیب تو کرد',
      'PostReactionMsg': 'به پست شما واکنش نشان داد',
      'PostCommentMsg': 'پست خود را کامنت کرد',
      'ReplyMsg': 'به نظر شما پاسخ دادم',
      'CommentReactedMsg': 'به نظر شما واکنش نشان داد',
      'CannotFindFile': 'اوه! فایل را نمی توان یافت',
      'NoFilePicked': 'در حال تلاش برای آپلود و هیچ فایلی انتخاب نشده است',
      'SentYouMsg': 'پیام برای شما ارسال شد',
      'Report': 'گزارش',
      'Unreport': 'عدم گزارش',
      'ReportDesc': 'ما پستی را حذف می کنیم که دارای: ',
      'ReportReasons':
          '⚫️ محتوای جنسی. \n\n⚫️ محتوای خشونت آمیز یا زننده. \n\n⚫️ محتوای نفرت انگیز یا توهین آمیز. \n\n⚫️ هرزنامه یا گمراه کننده.',
      'ReportNote': 'اگر شما این اقدام را انجام دهید به آنها اطلاع نمی دهیم.',
      'ReportThanks':
          'ما درخواست شما را بررسی خواهیم کرد, برای کمک به بهبود جامعه ما متشکریم',
      'Admin': 'مدیر',
      'ProfanityDetected':
          'کلمات بد شناسایی شد, حساب شما ممکن است به حالت تعلیق درآید!',
      'AreYouSure': 'آیا مطمئنی',
      'ConfirmChatDeletion': 'آیا مطمئن هستید که چت را حذف می کنید',
      'ReportedPosts': 'پست های گزارش شده',
      'AllChats': 'همه چت ها',
      'Users': 'کاربران',
      'TermsService': 'شرایط استفاده از خدمات',
      'WaitingToRecord': 'در انتظار ضبط',
      'AdjustVolume': 'تنظیم صدا',
      'AdjustSpeed': 'تنظیم سرعت',
      'Ok': 'خوب',
      'ShareExternally': 'به اشتراک گذاری خارجی',
      'ShareOnYourWall': 'روی دیوار خود به اشتراک بگذارید',
      'Error': 'خطا',
      'Me': 'من',
      'OopsCode': 'اوه! کد تایید اشتباه است',
      'OopsNetwork': 'اوه! مشکلی پیش آمد, لطفاً شبکه خود را بررسی کنید!',
      'OopsEmailorPassword': 'اوه! ایمیل یا رمز عبور صحیح نیست!',
      'OopsNotRegister':
          'اوه! هیچ کاربری پیدا نشد, اگر ثبت نام نکردید ابتدا ثبت نام کنید!',
      'OopsEmailUsed': 'اوه! این ایمیل قبلا توسط حساب دیگری استفاده شده است!',
      'OopsPhoneFormat': 'اوه! قالب تلفن نامعتبر است',
      'NoAudioRecorded': 'صوتی اضافه نشده است',
      'OopsWrong': 'اوه, مشکلی پیش آمد',
      'Optional': 'اختیاری',
      'Sending': 'در حال ارسال... ',
      'NotificationPermission': 'اجازه اطلاع رسانی تضمین شده است',
      'Theme': 'موضوع',
      'DarkMode': 'حالت تاریک',
      'Others': 'دیگران',
      'PeopleWhoReacted': 'افرادی که واکنش نشان دادند',
      'BadWords': 'کلمات بد شناسایی شد, حساب شما ممکن است به حالت تعلیق درآید!',
      'PickFile': 'انتخاب فایل',
      'Loading': 'بارگذاری...',
      'PleaseAddAudio': 'لطفاً صدا را اضافه کنید',
      'PeopleYouMayFollow': 'افرادی که ممکن است آنها را دنبال کنید',
      'Unsave': 'غیر ذخیره',
      'PostsLikes': 'پسند پست ها',
      'CommentsLikes': 'نظرات لایک',
      'RepliesLikes': 'پاسخ ها لایک',
      'ShareiConfezz': 'iConfezz را به اشتراک بگذارید',
      'MemberSince': 'عضو از زمان',
      'Comment': 'اظهار نظر',
      'UnShare': 'لغو اشتراک',
      'Liked': 'دوست داشت',
      'Like': 'پسندیدن',
      'SAVED': 'ذخیره',
      'SHARED': 'به اشتراک گذاشته شده',
      'AppLanguage': 'زبان برنامه',
      'ComingSoon': 'به زودی',
      'ReportProblem': 'گزارش یک مشکل',
      'PushNotifications': 'اعلان های فشاری',
      'ShareProfile': 'اشتراک گذاری نمایه',
      'DeleteAccount': 'حذف حساب کاربری',
      'DeleteYourAccount': 'حذف حساب خود',
      'Record': 'رکورد',
      'Or': 'یا',
      'NoPhotoPicked': 'عکسی انتخاب نشده است',
      'SearchForUsers': 'جستجوی کاربران',
      'NoResultsFound': 'نتیجه ای پیدا نشد',
      'TemporaryFilesRemoved': 'فایل های موقت با موفقیت حذف شدند',
      'FailedToClean': 'فایل های موقت پاک نشد',
      'exit': 'خروج',
      'CommunityGuidelines': 'دستورالعمل های جامعه',
      'FAQ': 'سؤالات متداول',
      'Paste': 'Paste',
      'DoYouWantToPasteThisCode': 'Do you want to paste this code?',
      'PasteCode': 'Paste Code',
    };
  }
}

extension on _StringsFr {
  Map<String, dynamic> _buildFlatMap() {
    return <String, dynamic>{
      'AppName': 'iConfezz',
      'AboutApp':
          'iConfezz est une communauté dédiée à entendre la voix des autres.',
      'AppVersion': '2.0.0',
      'UnknownError': 'Quelque chose s\'est mal passé',
      'Next': 'Suivant',
      'EmailUser':
          'Êtes-vous un utilisateur de messagerie, connectez-vous ici !',
      'Email': 'E-mail',
      'bio': 'Biographie',
      'city': 'Ville',
      'dob': 'Date de naissance',
      'Password': 'Mot de passe',
      'Login': 'Connexion',
      'PhoneNumber': 'Numéro de téléphone',
      'PhoneVerification': 'Vérification du numéro de téléphone',
      'EnterCode': 'Entrez le code envoyé à',
      'DidntRecieveCode': 'Vous n\'avez pas reçu le code ?',
      'InvalidSMSCode': 'Le code de vérification par SMS est invalide',
      'RESEND': 'RENVOYER',
      'VERIFY': 'VÉRIFIER',
      'POSTS': 'DES POSTES',
      'FOLLOWERS': 'Suiveurs',
      'FOLLOWINGS': 'Suivis',
      'Follow': 'Suivre',
      'Following': 'Suivant',
      'Register': 'S\'inscrire',
      'Username': 'Nom d\'utilisateur',
      'FirstName': 'Prénom',
      'LastName': 'Nom de famille',
      'FullName': 'Nom complet',
      'Status': 'Statut',
      'AddValidUsername': 'Ajouter un nom d\'utilisateur valide',
      'AddValidFirstName': 'Ajouter un prénom valide',
      'AddValidLastName': 'Ajouter un nom de famille valide',
      'SelectGender': 'S\'il vous plait selectionnez votre genre.',
      'Male': 'Homme',
      'Female': 'Femelle',
      'Other': 'Autre',
      'AcceptTerms': 'J\'accepte tous les termes et conditions',
      'Save': 'Sauver',
      'Search': 'Recherche',
      'Searching': 'Recherche...',
      'SelectPhoneCode': 'Sélectionner un code téléphone',
      'InvalidEmail': 'Email invalide',
      'ShortPassword': 'Le mot de passe doit contenir au moins 8 caractères',
      'UsernameTaken': 'Nom d\'utilisateur déjà pris',
      'InvalidPhone': 'Veuillez remplir correctement toutes les cellules',
      'Message': 'Message',
      'LoginFirst': 'Oups, veuillez d\'abord vous connecter',
      'START': 'COMMENCER',
      'Wallpapers': 'Fonds d\'écran',
      'Profile': 'Profil',
      'NoInternet': 'Pas d\'accès Internet',
      'Settings': 'Réglages',
      'Account': 'Compte',
      'EditProfile': 'Editer le profil',
      'OnlineStatus': 'Statut en ligne',
      'OnlineDescription':
          'Tout le monde peut voir quand vous êtes en ligne pour la dernière fois',
      'Logout': 'Se déconnecter',
      'DirectMsgs': 'Chat Messages',
      'DirectMsgsDescr': 'Recevoir des notifications de chat',
      'GroupsMsgs': ' Messages de groupes ',
      'GroupsMsgsDesc': 'Recevoir toutes les notifications de groupes',
      'AddGroupName': 'S\'il vous plaît ! Ajoutez le nom du groupe',
      'About': 'à propos de nous',
      'RateUs': 'Évaluez nous',
      'EULA': 'Accord CLUF',
      'PrivacyPolicy': 'Politique de confidentialité',
      'Feed': 'Nourrir',
      'Post': 'Poster',
      'Gallery': 'Galerie',
      'Stories': 'Histoires',
      'MyPosts': 'Mes publications',
      'Favorites': 'Favoris',
      'Announcement': 'Annonce',
      'WhatOnMind': 'Quoi partager ?',
      'Likes': 'Aime',
      'Comments': 'Commentaires',
      'CreatePost': 'Créer un article',
      'Share': 'Partager',
      'Edit': 'Modifier',
      'Delete': 'Supprimer',
      'Copy': 'Copie',
      'Copied': 'Copié',
      'ReadMore': '...Lire la suite',
      'SuccessPostPublish': 'Le message a été publié avec succès',
      'SuccessPostEdited': 'Le message a été modifié avec succès',
      'TypeComment': 'Tapez un commentaire...',
      'TypeReply': 'Tapez une réponse...',
      'NotAllowedToPublish': 'Désolé ! Vous n\'êtes plus autorisé à publier',
      'NotAllowedToComment': 'Désolé ! Vous n\'êtes pas autorisé à commenter',
      'PostRemoveConfirm': 'Es-tu sur de vouloir supprimer cette annonce?',
      'CommentRemoveConfirm':
          'Êtes-vous sûr de vouloir supprimer ce commentaire?',
      'AddSomeContent': 'Veuillez ajouter du contenu avant de publier',
      'Reply': 'Réponse',
      'Replies': 'Réponses',
      'RepliedToComment': 'répondu à votre commentaire',
      'Chats': 'Chats',
      'OnlineUsers': 'UTILISATEURS EN LIGNE',
      'RecentChats': 'CHATS RÉCENTES',
      'RemoveConversation': 'Supprimer la conversation',
      'Messaging': 'Message...',
      'CannotChatWithUser':
          'Désolé ! Vous ne pouvez pas discuter avec cet utilisateur',
      'MsgDeleteConfirm': 'Êtes-vous sûr de vouloir supprimer le message ?',
      'NoChatFound': 'Aucun chat récent pour le moment, commencez à chatter !',
      'Online': 'En ligne',
      'Typing': 'Dactylographie...',
      'Image': 'Image',
      'Voice': 'Voix',
      'Video': 'Vidéo',
      'Emoji': 'Émoji',
      'GIF': 'GIF',
      'Sticker': 'Autocollant',
      'Groups': 'Groupes',
      'CreateGroup': 'Créer un groupe',
      'EditGroup': 'Modifier votre groupe',
      'Members': 'Membres',
      'PhotosLibrary': 'Photothèque',
      'TakePicture': 'Prendre une photo',
      'VideosLibrary': 'Vidéothèque',
      'RecordVideo': 'Enregistrer une vidéo',
      'Cancel': 'Annuler',
      'SlideToCancel': 'Glisser pour annuler',
      'On': 'Sur',
      'Off': 'Désactivé',
      'Group': 'Grouper',
      'LeaveGroup': 'Quitter le groupe',
      'GroupName': 'Nom de groupe',
      'Join': 'Rejoindre',
      'Joined': 'Inscrit',
      'Public': 'Public',
      'Private': 'Privé',
      'GroupType': 'Type de groupe',
      'RemoveMember': 'Supprimer le membre',
      'AddMembers': 'Ajouter des membres',
      'Block': 'Bloc',
      'Unblock': 'Débloquer',
      'Ban': 'Interdire',
      'Unban': 'Unban',
      'Banned': 'Banni',
      'Newest': 'Le plus récent',
      'Trending': 'Tendance',
      'MostDownloaded': 'Le plus téléchargé',
      'Categories': 'Catégories',
      'AddWallpaper': 'Ajouter un fond d\'écran',
      'WallpaperName': 'Nom du fond d\'écran',
      'Category': 'Catégorie',
      'Upload': 'Télécharger',
      'Home': 'Domicile',
      'Lock': 'Fermer à clé',
      'Both': 'Tous les deux',
      'SetAsWallpaper': 'Définir en tant que fond d\'écran',
      'WallpaperSet': 'Fond d\'écran défini avec succès',
      'FailedToUpload': 'Oups ! Le téléchargement a échoué, veuillez réessayer',
      'UploadFinished': 'Téléchargement terminé',
      'Downloading': 'Téléchargement',
      'NoWallpaperSelectedMsg':
          'Veuillez sélectionner le fond d\'écran pour continuer',
      'NoImgSelected': 'Aucune image sélectionnée',
      'Notifications': 'Notifications',
      'StartFollowingMsg': 'commencé à te suivre',
      'PostReactionMsg': 'a réagi à votre message',
      'PostCommentMsg': 'a commenté votre message',
      'ReplyMsg': 'répondu à votre commentaire',
      'CommentReactedMsg': 'réagi à votre commentaire',
      'CannotFindFile': 'Oups ! Impossible de trouver le fichier',
      'NoFilePicked':
          'Tentative de téléchargement et aucun fichier n\'est sélectionné',
      'SentYouMsg': 'Je vous ai envoyé un message',
      'Report': 'Signaler',
      'Unreport': 'Annuler le signalement',
      'ReportDesc': 'Nous supprimons le message qui contient : ',
      'ReportReasons':
          '⚫️ Contenu sexuel. \n\n⚫️ Contenu violent ou répulsif. \n\n⚫️ Contenu haineux ou abusif. \n\n⚫️ Spam ou trompeur.',
      'ReportNote':
          'Nous ne leur ferons pas savoir si vous prenez cette mesure.',
      'ReportThanks':
          'Nous allons vérifier votre demande, merci d\'aider à améliorer notre communauté',
      'Admin': 'Administrateur',
      'ProfanityDetected':
          'Mots grossiers détectés, votre compte peut être suspendu !',
      'AreYouSure': 'Êtes-vous sûr de',
      'ConfirmChatDeletion': 'Etes-vous sûr de supprimer le chat',
      'ReportedPosts': 'Messages signalés',
      'AllChats': 'Tous les chats',
      'Users': 'Utilisateurs',
      'TermsService': 'Conditions d\'utilisation',
      'WaitingToRecord': 'En attente d\'enregistrement',
      'AdjustVolume': 'Régler le volume',
      'AdjustSpeed': 'Régler la vitesse',
      'Ok': 'D\'accord',
      'ShareExternally': 'Partager en externe',
      'ShareOnYourWall': 'Partager sur votre mur',
      'Error': 'Erreur',
      'Me': 'Moi',
      'OopsCode': 'Oups ! Le code de vérification est erroné',
      'OopsNetwork':
          'Oups ! quelque chose s\'est mal passé, veuillez vérifier votre réseau !',
      'OopsEmailorPassword':
          'Oups ! l\'e-mail ou le mot de passe n\'est pas correct !',
      'OopsNotRegister':
          'Oups ! Aucun utilisateur trouvé, sinon inscrivez-vous d\'abord !',
      'OopsEmailUsed':
          'Oups ! cet e-mail est déjà utilisé par un autre compte !',
      'OopsPhoneFormat': 'Oups ! le format du téléphone n\'est pas valide',
      'NoAudioRecorded': 'Aucun audio ajouté',
      'OopsWrong': 'Oups, quelque chose s\'est mal passé',
      'Optional': 'Optionnel',
      'Sending': 'Envoi en cours... ',
      'NotificationPermission': 'L\'autorisation de notification est garantie',
      'Theme': 'Thème',
      'DarkMode': 'Mode sombre',
      'Others': 'Autres',
      'PeopleWhoReacted': 'Des gens qui ont réagi',
      'BadWords': 'Mots grossiers détectés, votre compte peut être suspendu !',
      'PickFile': 'Choisir un fichier',
      'Loading': 'Chargement...',
      'PleaseAddAudio': 'Veuillez ajouter un son',
      'PeopleYouMayFollow': 'Les gens que vous pouvez suivre',
      'Unsave': 'Désenregistrer',
      'PostsLikes': 'Messages J\'aime',
      'CommentsLikes': 'Commentaires J\'aime',
      'RepliesLikes': 'Réponds J\'aime',
      'ShareiConfezz': 'Partager iConfezz',
      'MemberSince': 'Membre depuis',
      'Comment': 'Commenter',
      'UnShare': 'Annuler le partage',
      'Liked': 'Aimé',
      'Like': 'Aimer',
      'SAVED': 'ENREGISTRÉ',
      'SHARED': 'PARTAGÉ',
      'AppLanguage': 'Langue de l\'application',
      'ComingSoon': 'À venir',
      'ReportProblem': 'Signaler un problème',
      'PushNotifications': 'Notifications push',
      'ShareProfile': 'Partager le profil',
      'DeleteAccount': 'Supprimer le compte',
      'DeleteYourAccount': 'Supprimer votre compte',
      'Record': 'Record',
      'Or': 'Ou alors',
      'NoPhotoPicked': 'Aucune photo choisie',
      'SearchForUsers': 'Rechercher des utilisateurs',
      'NoResultsFound': 'Aucun résultat trouvé',
      'TemporaryFilesRemoved': 'Fichiers temporaires supprimés avec succès',
      'FailedToClean': 'Échec du nettoyage des fichiers temporaires',
      'exit': 'sortie',
      'CommunityGuidelines': 'Règles de la communauté',
      'FAQ': 'FAQ',
      'Paste': 'Paste',
      'DoYouWantToPasteThisCode': 'Do you want to paste this code?',
      'PasteCode': 'Paste Code',
    };
  }
}

extension on _StringsHi {
  Map<String, dynamic> _buildFlatMap() {
    return <String, dynamic>{
      'AppName': 'आईकॉन्फ़ेज़',
      'AboutApp':
          'iConfez एक ऐसा समुदाय है जो दूसरों की आवाज़ सुनने के लिए समर्पित है।',
      'AppVersion': '2.0.0',
      'UnknownError': 'कुछ गलत हो गया',
      'Next': 'अगला',
      'EmailUser': 'क्या आप ईमेल उपयोगकर्ता हैं, यहां लॉग इन करें!',
      'Email': 'ईमेल',
      'bio': 'जीवनी',
      'city': 'शहर',
      'dob': 'जन्म की तारीख',
      'Password': 'पासवर्ड',
      'Login': 'लॉग इन करें',
      'PhoneNumber': 'फ़ोन नंबर',
      'PhoneVerification': 'फ़ोन नंबर सत्यापन',
      'EnterCode': 'को भेजा गया कोड दर्ज करें',
      'DidntRecieveCode': 'कोड प्राप्त नहीं हुआ?',
      'InvalidSMSCode': 'एसएमएस सत्यापन कोड अमान्य है',
      'RESEND': 'फिर से भेजें',
      'VERIFY': 'सत्यापित करना',
      'POSTS': 'पोस्ट',
      'FOLLOWERS': 'अनुयायियों',
      'FOLLOWINGS': 'निम्नलिखित',
      'Follow': 'अनुसरण',
      'Following': 'अगले',
      'Register': 'पंजीकरण करवाना',
      'Username': 'उपयोगकर्ता नाम',
      'FirstName': 'पहला नाम',
      'LastName': 'उपनाम',
      'FullName': 'पूरा नाम',
      'Status': 'स्थिति',
      'AddValidUsername': 'एक वैध उपयोगकर्ता नाम जोड़ें',
      'AddValidFirstName': 'एक मान्य प्रथम नाम जोड़ें',
      'AddValidLastName': 'एक मान्य अंतिम नाम जोड़ें',
      'SelectGender': 'कृपया अपना लिंग चुनें।',
      'Male': 'नर',
      'Female': 'महिला',
      'Other': 'अन्य',
      'AcceptTerms': 'मैं सभी नियम और शर्तें स्वीकार करता हूं',
      'Save': 'बचाना',
      'Search': 'खोज',
      'Searching': 'खोज कर...',
      'SelectPhoneCode': 'फ़ोन कोड चुनें',
      'InvalidEmail': 'अमान्य ईमेल',
      'ShortPassword': 'पासवर्ड में कम से कम 8 अक्षर होने चाहिए',
      'UsernameTaken': 'उपयोगकर्ता का नाम पहले से लिया है',
      'InvalidPhone': 'कृपया सभी कक्षों को ठीक से भरें',
      'Message': 'संदेश',
      'LoginFirst': 'ओह, कृपया पहले लॉगिन करें',
      'START': 'शुरू हो जाओ',
      'Wallpapers': 'वॉलपेपर',
      'Profile': 'प्रोफ़ाइल',
      'NoInternet': 'इन्टरनेट उपलब्ध नहीँ है',
      'Settings': 'समायोजन',
      'Account': 'खाता',
      'EditProfile': 'प्रोफ़ाइल संपादित करें',
      'OnlineStatus': 'ऑनलाइन स्थिति',
      'OnlineDescription': 'कोई भी देख सकता है कि आप आखिरी बार कब ऑनलाइन थे',
      'Logout': 'लॉग आउट',
      'DirectMsgs': 'चैट संदेश',
      'DirectMsgsDescr': 'चैट सूचनाएं प्राप्त करें',
      'GroupsMsgs': 'समूह संदेश',
      'GroupsMsgsDesc': 'सभी समूह सूचनाएं प्राप्त करें',
      'AddGroupName': 'कृपया! समूह का नाम जोड़ें',
      'About': 'हमारे बारे में',
      'RateUs': 'हमें रेटिंग दें',
      'EULA': 'ईयूएलए समझौता',
      'PrivacyPolicy': 'गोपनीयता नीति',
      'Feed': 'चारा',
      'Post': 'डाक',
      'Gallery': 'गेलरी',
      'Stories': 'कहानियों',
      'MyPosts': 'मेरी पोस्ट',
      'Favorites': 'पसंदीदा',
      'Announcement': 'घोषणा',
      'WhatOnMind': 'क्या साझा करें?',
      'Likes': 'को यह पसंद है',
      'Comments': 'टिप्पणियाँ',
      'CreatePost': 'पोस्ट बनाएं',
      'Share': 'साझा करना',
      'Edit': 'संपादन करना',
      'Delete': 'मिटाना',
      'Copy': 'प्रतिलिपि',
      'Copied': 'प्रतिलिपि',
      'ReadMore': '...अधिक पढ़ें',
      'SuccessPostPublish': 'पोस्ट सफलतापूर्वक प्रकाशित किया गया है',
      'SuccessPostEdited': 'पोस्ट सफलतापूर्वक संपादित किया गया है',
      'TypeComment': 'टिप्पणी लिखें...',
      'TypeReply': 'जवाब लिखें...',
      'NotAllowedToPublish':
          'क्षमा करें! आपको अब और प्रकाशित करने की अनुमति नहीं है',
      'NotAllowedToComment': 'क्षमा करें! आपको टिप्पणी करने की अनुमति नहीं है',
      'PostRemoveConfirm': 'क्या आप वाकई इस पोस्ट को हटाना चाहते हैं?',
      'CommentRemoveConfirm':
          'क्या आप इस कमेंट को मिटाने के बारे में पक्के हैं?',
      'AddSomeContent': 'कृपया पोस्ट करने से पहले कुछ सामग्री जोड़ें',
      'Reply': 'जवाब',
      'Replies': 'जवाब',
      'RepliedToComment': 'आपकी टिप्पणी का जवाब दिया',
      'Chats': 'चैट',
      'OnlineUsers': 'ऑनलाइन उपयोगकर्ता',
      'RecentChats': 'हालिया चैट',
      'RemoveConversation': 'बातचीत हटाएं',
      'Messaging': 'संदेश...',
      'CannotChatWithUser':
          'क्षमा करें! आप इस उपयोगकर्ता के साथ चैट नहीं कर सकते',
      'MsgDeleteConfirm': 'क्या आप वाकई संदेश हटाना चाहते हैं?',
      'NoChatFound': 'अभी तक कोई हाल की चैट नहीं है, चैट करना शुरू करें!',
      'Online': 'ऑनलाइन',
      'Typing': 'टाइपिंग...',
      'Image': 'छवि',
      'Voice': 'आवाज़',
      'Video': 'वीडियो',
      'Emoji': 'इमोजी',
      'GIF': 'जीआईएफ',
      'Sticker': 'स्टिकर',
      'Groups': 'समूह',
      'CreateGroup': 'एक समूह बनाएं',
      'EditGroup': 'अपना समूह संपादित करें',
      'Members': 'सदस्य',
      'PhotosLibrary': 'फोटो लाइब्रेरी',
      'TakePicture': 'तस्वीर ले लो',
      'VideosLibrary': 'वीडियो लाइब्रेरी',
      'RecordVideo': 'वीडियो रिकॉर्ड करो',
      'Cancel': 'रद्द करना',
      'SlideToCancel': 'रद्द करने के लिए स्लाइड',
      'On': 'पर',
      'Off': 'बंद',
      'Group': 'समूह',
      'LeaveGroup': 'समूह छोड़ दें',
      'GroupName': 'समूह का नाम',
      'Join': 'जोड़ना',
      'Joined': 'में शामिल हो गए',
      'Public': 'जनता',
      'Private': 'निजी',
      'GroupType': 'समूह प्रकार',
      'RemoveMember': 'सदस्य निकालें',
      'AddMembers': 'सदस्य जोड़ें',
      'Block': 'अवरोध पैदा करना',
      'Unblock': 'अनब्लॉक करें',
      'Ban': 'प्रतिबंध',
      'Unban': 'अनबन',
      'Banned': 'प्रतिबंधित',
      'Newest': 'नवीनतम',
      'Trending': 'रुझान',
      'MostDownloaded': 'सबसे ज्यादा डाउनलोड किया जाने वाला',
      'Categories': 'श्रेणियाँ',
      'AddWallpaper': 'वॉलपेपर जोड़ें',
      'WallpaperName': 'वॉलपेपर का नाम',
      'Category': 'श्रेणी',
      'Upload': 'डालना',
      'Home': 'घर',
      'Lock': 'ताला',
      'Both': 'दोनों',
      'SetAsWallpaper': 'वॉलपेपर के रूप में सेट',
      'WallpaperSet': 'वॉलपेपर सफलतापूर्वक सेट',
      'FailedToUpload': 'उफ़! अपलोड विफल, कृपया पुनः प्रयास करें',
      'UploadFinished': 'अपलोड समाप्त',
      'Downloading': 'डाउनलोडिंग',
      'NoWallpaperSelectedMsg': 'कृपया जारी रखने के लिए वॉलपेपर चुनें',
      'NoImgSelected': 'कोई छवि चयनित नहीं',
      'Notifications': 'सूचनाएं',
      'StartFollowingMsg': 'ने आपको फॉलो करना शुरू किया',
      'PostReactionMsg': 'आपकी पोस्ट पर प्रतिक्रिया दी',
      'PostCommentMsg': 'आपकी पोस्ट पर टिप्पणी की',
      'ReplyMsg': 'आपकी टिप्पणी का जवाब दिया',
      'CommentReactedMsg': 'आपकी टिप्पणी पर प्रतिक्रिया दी',
      'CannotFindFile': 'ओह! फ़ाइल नहीं ढूँढ सकता',
      'NoFilePicked':
          'अपलोड करने का प्रयास किया जा रहा है और कोई फ़ाइल नहीं चुनी गई',
      'SentYouMsg': 'आपको संदेश भेजा',
      'Report': 'शिकायत करना',
      'Unreport': 'अनरिपोर्ट',
      'ReportDesc': 'हम उस पोस्ट को हटा देते हैं जिसमें:',
      'ReportReasons':
          '⚫️ यौन सामग्री। \n\n⚫️ हिंसक या प्रतिकूल सामग्री। \n\n⚫️ घृणित या अपमानजनक सामग्री। \n\n⚫️ स्पैम या भ्रामक।',
      'ReportNote':
          'यदि आप यह कार्रवाई करते हैं तो हम उन्हें इसकी सूचना नहीं देंगे।',
      'ReportThanks':
          'हम आपके अनुरोध की जांच करेंगे, हमारे समुदाय को बेहतर बनाने में मदद करने के लिए धन्यवाद',
      'Admin': 'व्यवस्थापक',
      'ProfanityDetected':
          'गलत शब्दों का पता चला है, आपका खाता निलंबित हो सकता है!',
      'AreYouSure': 'क्या आपको यकीन है',
      'ConfirmChatDeletion': 'क्या आप निश्चित रूप से चैट हटाना चाहते हैं',
      'ReportedPosts': 'रिपोर्ट की गई पोस्ट',
      'AllChats': 'सभी चैट',
      'Users': 'उपयोगकर्ता',
      'TermsService': 'सेवा की शर्तें',
      'WaitingToRecord': 'रिकॉर्ड करने की प्रतीक्षा कर रहा है',
      'AdjustVolume': 'वॉल्यूम समायोजित करें',
      'AdjustSpeed': 'गति समायोजित करें',
      'Ok': 'ठीक है',
      'ShareExternally': 'बाहरी रूप से साझा करें',
      'ShareOnYourWall': 'अपनी वॉल पर शेयर करें',
      'Error': 'त्रुटि',
      'Me': 'मैं',
      'OopsCode': 'उफ़! सत्यापन कोड गलत है',
      'OopsNetwork': 'ओह! कुछ गलत हो गया, कृपया अपना नेटवर्क जांचें!',
      'OopsEmailorPassword': 'उफ़! ईमेल या पासवर्ड सही नहीं है!',
      'OopsNotRegister':
          'उफ़! कोई उपयोगकर्ता नहीं मिला, अगर रजिस्टर नहीं है तो पहले साइन अप करने पर विचार करें!',
      'OopsEmailUsed':
          'ओह! यह ईमेल पहले से ही किसी अन्य खाते द्वारा उपयोग किया जा रहा है!',
      'OopsPhoneFormat': 'उफ़! फ़ोन प्रारूप अमान्य है',
      'NoAudioRecorded': 'कोई ऑडियो नहीं जोड़ा गया',
      'OopsWrong': 'ओह! कुछ गलत हो गया है',
      'Optional': 'वैकल्पिक',
      'Sending': 'भेजना... ',
      'NotificationPermission': 'अधिसूचना अनुमति की गारंटी है',
      'Theme': 'थीम',
      'DarkMode': 'डार्क मोड',
      'Others': 'अन्य',
      'PeopleWhoReacted': 'प्रतिक्रिया देने वाले लोग',
      'BadWords': 'गलत शब्दों का पता चला है, आपका खाता निलंबित हो सकता है!',
      'PickFile': 'फ़ाइल चुनें',
      'Loading': 'लोड हो रहा है...',
      'PleaseAddAudio': 'कृपया ऑडियो जोड़ें',
      'PeopleYouMayFollow': 'वे लोग जिनका आप अनुसरण कर सकते हैं',
      'Unsave': 'अनसेव',
      'PostsLikes': 'पोस्ट लाइक',
      'CommentsLikes': 'टिप्पणी पसंद',
      'RepliesLikes': 'जवाब पसंद है',
      'ShareiConfezz': 'शेयर आईकॉन्फेज़',
      'MemberSince': 'से सदस्ये',
      'Comment': 'टिप्पणी',
      'UnShare': 'अनशेयर',
      'Liked': 'पसंद किया',
      'Like': 'पसंद करना',
      'SAVED': 'बचाया',
      'SHARED': 'साझा',
      'AppLanguage': 'ऐप भाषा',
      'ComingSoon': 'जल्द आ रहा है',
      'ReportProblem': 'एक समस्या का आख्या',
      'PushNotifications': 'सूचनाएं धक्का',
      'ShareProfile': 'प्रोफ़ाइल साझा करें',
      'DeleteAccount': 'खाता हटा दो',
      'DeleteYourAccount': 'अपने खाते को नष्ट करो',
      'Record': 'अभिलेख',
      'Or': 'या',
      'NoPhotoPicked': 'कोई फोटो नहीं उठाया',
      'SearchForUsers': 'उपयोगकर्ताओं के लिए खोजें',
      'NoResultsFound': 'कोई परिणाम नहीं मिला',
      'TemporaryFilesRemoved': 'अस्थायी फ़ाइलें सफलतापूर्वक हटाई गईं',
      'FailedToClean': 'अस्थायी फ़ाइलों को साफ करने में विफल',
      'exit': 'बाहर निकलना',
      'CommunityGuidelines': 'समुदाय दिशानिर्देश',
      'FAQ': 'सामान्य प्रश्न',
      'Paste': 'Paste',
      'DoYouWantToPasteThisCode': 'Do you want to paste this code?',
      'PasteCode': 'Paste Code',
    };
  }
}

extension on _StringsId {
  Map<String, dynamic> _buildFlatMap() {
    return <String, dynamic>{
      'AppName': 'iConfezz',
      'AboutApp':
          'iConfezz adalah komunitas yang didedikasikan untuk mendengarkan suara orang lain.',
      'AppVersion': '2.0.0',
      'UnknownError': 'Ada yang salah',
      'Next': 'Berikutnya',
      'EmailUser': 'Apakah Anda Pengguna Email, Login di sini!',
      'Email': 'Surel',
      'bio': 'Biografi',
      'city': 'Kota',
      'dob': 'Tanggal lahir',
      'Password': 'Kata sandi',
      'Login': 'Gabung',
      'PhoneNumber': 'Nomor telepon',
      'PhoneVerification': 'Verifikasi Nomor Telepon',
      'EnterCode': 'Masukkan kode yang dikirim ke ',
      'DidntRecieveCode': 'Tidak menerima kode?',
      'InvalidSMSCode': 'Kode Verifikasi SMS Tidak Valid',
      'RESEND': 'KIRIM ULANG',
      'VERIFY': 'MEMERIKSA',
      'POSTS': 'POSTINGAN',
      'FOLLOWERS': 'Pengikut',
      'FOLLOWINGS': 'Berikut',
      'Follow': 'Mengikuti',
      'Following': 'Mengikuti',
      'Register': 'Daftar',
      'Username': 'Nama belakang',
      'FirstName': 'Nama depan',
      'LastName': 'Nama keluarga',
      'FullName': 'Nama lengkap',
      'Status': 'Status',
      'AddValidUsername': 'Tambahkan nama pengguna yang valid',
      'AddValidFirstName': 'Tambahkan nama depan yang valid',
      'AddValidLastName': 'Tambahkan nama belakang yang valid',
      'SelectGender': 'Silakan pilih jenis kelamin Anda.',
      'Male': 'Pria',
      'Female': 'Perempuan',
      'Other': 'Lainnya',
      'AcceptTerms': 'Saya Menerima Semua Syarat & Ketentuan',
      'Save': 'Menyimpan',
      'Search': 'Mencari',
      'Searching': 'Mencari...',
      'SelectPhoneCode': 'Pilih kode telepon',
      'InvalidEmail': 'Email tidak valid',
      'ShortPassword': 'Kata Sandi Harus Berisi Setidaknya 8 karakter',
      'UsernameTaken': 'Nama Pengguna Sudah Diambil',
      'InvalidPhone': 'Silakan isi semua sel dengan benar',
      'Message': 'Pesan',
      'LoginFirst': 'Ups, Silahkan login dulu',
      'START': 'MEMULAI',
      'Wallpapers': 'Wallpaper',
      'Profile': 'Profil',
      'NoInternet': 'Tidak ada akses internet',
      'Settings': 'Pengaturan',
      'Account': 'Akun',
      'EditProfile': 'Sunting profil',
      'OnlineStatus': 'Status Daring',
      'OnlineDescription': 'Siapa pun dapat melihat kapan Anda terakhir Online',
      'Logout': 'Keluar',
      'DirectMsgs': 'Pesan Obrolan',
      'DirectMsgsDescr': 'Terima notifikasi obrolan',
      'GroupsMsgs': 'Pesan Grup',
      'GroupsMsgsDesc': 'Terima semua Pemberitahuan Grup',
      'AddGroupName': 'Silahkan! Tambahkan Nama Grup',
      'About': 'tentang kami',
      'RateUs': 'Nilai Kami',
      'EULA': 'Perjanjian EULA',
      'PrivacyPolicy': 'Kebijakan pribadi',
      'Feed': 'Memberi makan',
      'Post': 'Pos',
      'Gallery': 'Galeri',
      'Stories': 'Cerita',
      'MyPosts': 'Postingan Saya',
      'Favorites': 'Favorit',
      'Announcement': 'Pengumuman',
      'WhatOnMind': 'Apa yang Harus Dibagikan?',
      'Likes': 'Suka',
      'Comments': 'Komentar',
      'CreatePost': 'Buat Postingan',
      'Share': 'Membagikan',
      'Edit': 'Mengedit',
      'Delete': 'Menghapus',
      'Copy': 'Salinan',
      'Copied': 'Disalin',
      'ReadMore': '...Baca selengkapnya',
      'SuccessPostPublish': 'Posting telah berhasil diterbitkan',
      'SuccessPostEdited': 'Posting telah berhasil diedit',
      'TypeComment': 'Ketik komentar...',
      'TypeReply': 'Ketik balasan...',
      'NotAllowedToPublish':
          'Maaf! Anda tidak diperbolehkan untuk mempublikasikan lagi',
      'NotAllowedToComment': 'Maaf! Anda tidak diperbolehkan berkomentar',
      'PostRemoveConfirm': 'Apakah Anda yakin ingin menghapus postingan ini?',
      'CommentRemoveConfirm': 'Apakah Anda yakin ingin menghapus komentar ini?',
      'AddSomeContent': 'Tolong tambahkan beberapa konten sebelum memposting',
      'Reply': 'Membalas',
      'Replies': 'Balasan',
      'RepliedToComment': 'membalas komentar Anda',
      'Chats': 'Obrolan',
      'OnlineUsers': 'PENGGUNA ONLINE',
      'RecentChats': 'CHAT TERBARU',
      'RemoveConversation': 'Hapus Percakapan',
      'Messaging': 'Pesan...',
      'CannotChatWithUser':
          'Maaf! Anda tidak dapat mengobrol dengan pengguna ini',
      'MsgDeleteConfirm': 'Apakah Anda yakin ingin menghapus pesan itu?',
      'NoChatFound': 'Belum ada obrolan Terbaru, mulailah mengobrol!',
      'Online': 'On line',
      'Typing': 'Mengetik...',
      'Image': 'Gambar',
      'Voice': 'Suara',
      'Video': 'Video',
      'Emoji': 'Emoji',
      'GIF': 'GIF',
      'Sticker': 'Stiker',
      'Groups': 'Grup',
      'CreateGroup': 'Buat Grup',
      'EditGroup': 'Edit Grup Anda',
      'Members': 'Anggota',
      'PhotosLibrary': 'Perpustakaan Foto',
      'TakePicture': 'Mengambil gambar',
      'VideosLibrary': 'Perpustakaan Video',
      'RecordVideo': 'Merekam video',
      'Cancel': 'Membatalkan',
      'SlideToCancel': 'Geser untuk membatalkan',
      'On': 'Pada',
      'Off': 'Mati',
      'Group': 'Kelompok',
      'LeaveGroup': 'Meninggalkan grup',
      'GroupName': 'Nama grup',
      'Join': 'Bergabung',
      'Joined': 'Bergabung',
      'Public': 'Publik',
      'Private': 'Pribadi',
      'GroupType': 'Jenis Grup',
      'RemoveMember': 'Hapus anggota',
      'AddMembers': 'Tambah Anggota',
      'Block': 'Memblokir',
      'Unblock': 'Buka Blokir',
      'Ban': 'Melarang',
      'Unban': 'Batalkan larangan',
      'Banned': 'Dilarang',
      'Newest': 'Terbaru',
      'Trending': 'Tren',
      'MostDownloaded': 'Paling banyak di download',
      'Categories': 'Kategori',
      'AddWallpaper': 'Tambahkan Wallpaper',
      'WallpaperName': 'Nama Wallpaper',
      'Category': 'Kategori',
      'Upload': 'Mengunggah',
      'Home': 'Rumah',
      'Lock': 'Kunci',
      'Both': 'Keduanya',
      'SetAsWallpaper': 'Tetapkan sebagai Wallpaper',
      'WallpaperSet': 'Wallpaper Berhasil Ditetapkan',
      'FailedToUpload': 'Ups! Upload Gagal, Silakan coba lagi',
      'UploadFinished': 'Unggahan Selesai',
      'Downloading': 'Mengunduh',
      'NoWallpaperSelectedMsg': 'Silakan Pilih Wallpaper untuk melanjutkan',
      'NoImgSelected': 'Tidak Ada Gambar yang Dipilih',
      'Notifications': 'Pemberitahuan',
      'StartFollowingMsg': 'mulai mengikutimu',
      'PostReactionMsg': 'bereaksi terhadap posting Anda',
      'PostCommentMsg': 'mengomentari posting Anda',
      'ReplyMsg': 'membalas komentar Anda',
      'CommentReactedMsg': 'bereaksi terhadap komentar Anda',
      'CannotFindFile': 'Ups! Tidak dapat menemukan file',
      'NoFilePicked': 'Mencoba mengunggah dan tidak ada file yang diambil',
      'SentYouMsg': 'Mengirim Anda pesan',
      'Report': 'Laporan',
      'Unreport': 'Batalkan laporan',
      'ReportDesc': 'Kami menghapus postingan yang memiliki: ',
      'ReportReasons':
          '⚫️ Konten seksual. \n\n⚫️ Konten kekerasan atau menjijikkan. \n\n⚫️ Konten kebencian atau pelecehan. \n\n⚫️ Spam atau menyesatkan.',
      'ReportNote':
          'Kami tidak akan memberi tahu mereka jika Anda mengambil tindakan ini.',
      'ReportThanks':
          'Kami akan memeriksa permintaan Anda, Terima kasih telah membantu meningkatkan komunitas kami',
      'Admin': 'Admin',
      'ProfanityDetected':
          'Kata-kata buruk terdeteksi, akun Anda mungkin ditangguhkan!',
      'AreYouSure': 'Apakah kamu yakin',
      'ConfirmChatDeletion': 'Apakah Anda yakin akan menghapus obrolan',
      'ReportedPosts': 'Postingan yang Dilaporkan',
      'AllChats': 'Semua Obrolan',
      'Users': 'Pengguna',
      'TermsService': 'Ketentuan Layanan',
      'WaitingToRecord': 'Menunggu untuk merekam',
      'AdjustVolume': 'Sesuaikan volume',
      'AdjustSpeed': 'Sesuaikan kecepatan',
      'Ok': 'Oke',
      'ShareExternally': 'Bagikan secara eksternal',
      'ShareOnYourWall': 'Bagikan di dinding Anda',
      'Error': 'Kesalahan',
      'Me': 'Saya',
      'OopsCode': 'Ups! kode verifikasi salah',
      'OopsNetwork':
          'Ups! ada yang tidak beres, Silakan periksa jaringan Anda!',
      'OopsEmailorPassword': 'Ups! email atau kata sandi salah!',
      'OopsNotRegister':
          'Ups! tidak ada pengguna yang ditemukan, jika tidak mendaftar pertimbangkan untuk mendaftar terlebih dahulu!',
      'OopsEmailUsed': 'Ups! email ini sudah digunakan oleh akun lain!',
      'OopsPhoneFormat': 'Ups! format telepon tidak valid',
      'NoAudioRecorded': 'Tidak ada audio yang ditambahkan',
      'OopsWrong': 'Ups! Ada yang tidak beres',
      'Optional': 'Opsional',
      'Sending': 'Mengirim...',
      'NotificationPermission': 'Izin pemberitahuan dijamin',
      'Theme': 'Tema',
      'DarkMode': 'Mode Gelap',
      'Others': 'Yang lain',
      'PeopleWhoReacted': 'Orang yang bereaksi',
      'BadWords': 'Kata-kata buruk terdeteksi, akun Anda mungkin ditangguhkan!',
      'PickFile': 'Pilih berkas',
      'Loading': 'Memuat...',
      'PleaseAddAudio': 'Tolong tambahkan audionya',
      'PeopleYouMayFollow': 'Orang yang mungkin Anda ikuti',
      'Unsave': 'Batalkan penyimpanan',
      'PostsLikes': 'Suka Postingan',
      'CommentsLikes': 'Komentar Suka',
      'RepliesLikes': 'Balasan Suka',
      'ShareiConfezz': 'Bagikan iConfezz',
      'MemberSince': 'Anggota Sejak',
      'Comment': 'Komentar',
      'UnShare': 'Batalkan Bagikan',
      'Liked': 'Menyukai',
      'Like': 'Suka',
      'SAVED': 'DISELAMATKAN',
      'SHARED': 'BERSAMA',
      'AppLanguage': 'Bahasa Aplikasi',
      'ComingSoon': 'Segera hadir',
      'ReportProblem': 'Laporkan Masalah',
      'PushNotifications': 'Pemberitahuan Dorong',
      'ShareProfile': 'Bagikan Profil',
      'DeleteAccount': 'Hapus akun',
      'DeleteYourAccount': 'Hapus akun anda',
      'Record': 'Catatan',
      'Or': 'Atau',
      'NoPhotoPicked': 'Tidak Ada Foto yang Dipilih',
      'SearchForUsers': 'Cari Pengguna',
      'NoResultsFound': 'Tidak ada hasil yang ditemukan',
      'TemporaryFilesRemoved': 'File sementara berhasil dihapus',
      'FailedToClean': 'Gagal membersihkan file sementara',
      'exit': 'KELUAR',
      'CommunityGuidelines': 'Pedoman Komunitas',
      'FAQ': 'Tanya Jawab',
      'Paste': 'Paste',
      'DoYouWantToPasteThisCode': 'Do you want to paste this code?',
      'PasteCode': 'Paste Code',
    };
  }
}

extension on _StringsIt {
  Map<String, dynamic> _buildFlatMap() {
    return <String, dynamic>{
      'AppName': 'iConfezz',
      'AboutApp':
          'iConfezz è una community dedicata ad ascoltare la voce degli altri.',
      'AppVersion': '2.0.0',
      'UnknownError': 'Qualcosa è andato storto',
      'Next': 'Prossimo',
      'EmailUser': 'Sei un utente email, accedi qui!',
      'Email': 'E-mail',
      'bio': 'Biografia',
      'city': 'Città',
      'dob': 'Data di nascita',
      'Password': 'Parola d\'ordine',
      'Login': 'Login',
      'PhoneNumber': 'Numero di telefono',
      'PhoneVerification': 'Verifica del numero di telefono',
      'EnterCode': 'Inserisci il codice inviato a ',
      'DidntRecieveCode': 'Non hai ricevuto il codice?',
      'InvalidSMSCode': 'Il codice di verifica SMS non è valido',
      'RESEND': 'RIPENDERE',
      'VERIFY': 'VERIFICARE',
      'POSTS': 'POST',
      'FOLLOWERS': 'Seguaci',
      'FOLLOWINGS': 'Seguenti',
      'Follow': 'Seguire',
      'Following': 'Seguente',
      'Register': 'Registrati',
      'Username': 'Nome utente',
      'FirstName': 'Nome di battesimo',
      'LastName': 'Cognome',
      'FullName': 'Nome e cognome',
      'Status': 'Stato',
      'AddValidUsername': 'Aggiungi un nome utente valido',
      'AddValidFirstName': 'Aggiungi un nome valido',
      'AddValidLastName': 'Aggiungi un cognome valido',
      'SelectGender': 'Per favore seleziona il tuo genere.',
      'Male': 'Maschio',
      'Female': 'Femmina',
      'Other': 'Altro',
      'AcceptTerms': 'Accetto tutti i Termini e condizioni',
      'Save': 'Salva',
      'Search': 'Ricerca',
      'Searching': 'Cercando...',
      'SelectPhoneCode': 'Seleziona un codice telefono',
      'InvalidEmail': 'E-mail non valido',
      'ShortPassword': 'La password deve contenere almeno 8 caratteri',
      'UsernameTaken': 'Il nome utente è già stato preso',
      'InvalidPhone': 'Si prega di riempire correttamente tutte le celle',
      'Message': 'Messaggio',
      'LoginFirst': 'Oops, per favore accedi prima',
      'START': 'INIZIARE',
      'Wallpapers': 'Sfondi',
      'Profile': 'Profilo',
      'NoInternet': 'Nessun accesso ad internet',
      'Settings': 'Impostazioni',
      'Account': 'Account',
      'EditProfile': 'Modifica Profilo',
      'OnlineStatus': 'Stato online',
      'OnlineDescription':
          'Chiunque può vedere quando sei online l\'ultima volta',
      'Logout': 'Disconnettersi',
      'DirectMsgs': 'Messaggi di chat',
      'DirectMsgsDescr': 'Ricevi notifiche chat',
      'GroupsMsgs': 'Messaggi di gruppo',
      'GroupsMsgsDesc': 'Ricevi tutte le notifiche dei gruppi',
      'AddGroupName': 'Per favore! Aggiungi il nome del gruppo',
      'About': 'chi siamo',
      'RateUs': 'Votaci',
      'EULA': 'Accordo EULA',
      'PrivacyPolicy': 'Politica sulla riservatezza',
      'Feed': 'Alimentare',
      'Post': 'Inviare',
      'Gallery': 'Galleria',
      'Stories': 'Storie',
      'MyPosts': 'I miei messaggi',
      'Favorites': 'Preferiti',
      'Announcement': 'Annuncio',
      'WhatOnMind': 'Cosa condividere?',
      'Likes': 'Piace',
      'Comments': 'Commenti',
      'CreatePost': 'Crea messaggio',
      'Share': 'Condividere',
      'Edit': 'Modificare',
      'Delete': 'Eliminare',
      'Copy': 'Copia',
      'Copied': 'Copiato',
      'ReadMore': '...Leggi di più',
      'SuccessPostPublish': 'Il post è stato pubblicato con successo',
      'SuccessPostEdited': 'Il post è stato modificato con successo',
      'TypeComment': 'Digita un commento...',
      'TypeReply': 'Digita una risposta...',
      'NotAllowedToPublish': 'Scusa! Non sei più autorizzato a pubblicare',
      'NotAllowedToComment': 'Scusa! Non sei autorizzato a commentare',
      'PostRemoveConfirm': 'Sei sicuro di voler eliminare questo post?',
      'CommentRemoveConfirm': 'Sei sicuro di voler eliminare questo commento?',
      'AddSomeContent': 'Aggiungi alcuni contenuti prima di pubblicare',
      'Reply': 'Rispondere',
      'Replies': 'Risposte',
      'RepliedToComment': 'risposto al tuo commento',
      'Chats': 'Chat',
      'OnlineUsers': 'UTENTI ONLINE',
      'RecentChats': 'CHAT RECENTI',
      'RemoveConversation': 'Rimuovi conversazione',
      'Messaging': 'Messaggio...',
      'CannotChatWithUser': 'Scusa! Non puoi chattare con questo utente',
      'MsgDeleteConfirm': 'Sei sicuro di voler eliminare il messaggio?',
      'NoChatFound': 'Nessuna chat recente ancora, inizia a chattare!',
      'Online': 'In linea',
      'Typing': 'Digitando...',
      'Image': 'Immagine',
      'Voice': 'Voce',
      'Video': 'Video',
      'Emoji': 'Emoticon',
      'GIF': 'GIF',
      'Sticker': 'Etichetta',
      'Groups': 'Gruppi',
      'CreateGroup': 'Crea un gruppo',
      'EditGroup': 'Modifica il tuo gruppo',
      'Members': 'Membri',
      'PhotosLibrary': 'Libreria Foto',
      'TakePicture': 'Fare una foto',
      'VideosLibrary': 'Libreria video',
      'RecordVideo': 'Registrare video',
      'Cancel': 'Annulla',
      'SlideToCancel': 'Scorri per annullare',
      'On': 'SU',
      'Off': 'Spento',
      'Group': 'Gruppo',
      'LeaveGroup': 'Lascia il gruppo',
      'GroupName': 'Nome del gruppo',
      'Join': 'Giuntura',
      'Joined': 'Iscritto',
      'Public': 'Pubblico',
      'Private': 'Privato',
      'GroupType': 'Tipo di gruppo',
      'RemoveMember': 'Rimuovi membro',
      'AddMembers': 'Aggiungi membri',
      'Block': 'Bloccare',
      'Unblock': 'Sbloccare',
      'Ban': 'Bandire',
      'Unban': 'Annullato',
      'Banned': 'Vietato',
      'Newest': 'Il più recente',
      'Trending': 'Di tendenza',
      'MostDownloaded': 'Il più scaricato',
      'Categories': 'Categorie',
      'AddWallpaper': 'Aggiungi sfondo',
      'WallpaperName': 'Nome carta da parati',
      'Category': 'Categoria',
      'Upload': 'Caricamento',
      'Home': 'Casa',
      'Lock': 'Serratura',
      'Both': 'Tutti e due',
      'SetAsWallpaper': 'Imposta come sfondo',
      'WallpaperSet': 'Impostazione di sfondi con successo',
      'FailedToUpload': 'Oops! Caricamento non riuscito, per favore riprova',
      'UploadFinished': 'Caricamento terminato',
      'Downloading': 'Download',
      'NoWallpaperSelectedMsg': 'Seleziona lo sfondo per continuare',
      'NoImgSelected': 'Nessuna immagine selezionata',
      'Notifications': 'Notifiche',
      'StartFollowingMsg': 'iniziato a seguirti',
      'PostReactionMsg': 'ha reagito al tuo post',
      'PostCommentMsg': 'ha commentato il tuo post',
      'ReplyMsg': 'risposto al tuo commento',
      'CommentReactedMsg': 'ha reagito al tuo commento',
      'CannotFindFile': 'Oops! Impossibile trovare il file',
      'NoFilePicked': 'Tentativo di caricamento e nessun file selezionato',
      'SentYouMsg': 'Ti ho mandato un messaggio',
      'Report': 'Rapporto',
      'Unreport': 'Non denunciare',
      'ReportDesc': 'Rimuoviamo il post che ha: ',
      'ReportReasons':
          '⚫️ Contenuti a sfondo sessuale. \n\n⚫️ Contenuti violenti o ripugnanti. \n\n⚫️ Contenuti odiosi o offensivi. \n\n⚫️ Spam o fuorvianti.',
      'ReportNote': 'Non li faremo sapere se intraprendi questa azione.',
      'ReportThanks':
          'Controlleremo la tua richiesta, grazie per aver contribuito a migliorare la nostra comunità',
      'Admin': 'Amministratore',
      'ProfanityDetected':
          'Sono state rilevate parolacce, il tuo account potrebbe essere sospeso!',
      'AreYouSure': 'Sei sicuro di',
      'ConfirmChatDeletion': 'Sei sicuro di eliminare la chat',
      'ReportedPosts': 'Post segnalati',
      'AllChats': 'Tutte le chat',
      'Users': 'Utenti',
      'TermsService': 'Termini di servizio',
      'WaitingToRecord': 'In attesa di registrare',
      'AdjustVolume': 'Regola volume',
      'AdjustSpeed': 'Regola velocità',
      'Ok': 'Ok',
      'ShareExternally': 'Condividi esternamente',
      'ShareOnYourWall': 'Condividi sulla tua bacheca',
      'Error': 'Errore',
      'Me': 'Me',
      'OopsCode': 'Oops! il codice di verifica è sbagliato',
      'OopsNetwork':
          'Oops! Qualcosa è andato storto, per favore controlla la tua rete!',
      'OopsEmailorPassword': 'Oops! L\'e-mail o la password non sono corrette!',
      'OopsNotRegister':
          'Oops! nessun utente trovato, in caso contrario registrati considera prima la registrazione!',
      'OopsEmailUsed':
          'Oops! Questa email è già utilizzata da un altro account!',
      'OopsPhoneFormat': 'Oops! Il formato del telefono non è valido',
      'NoAudioRecorded': 'Nessun audio aggiunto',
      'OopsWrong': 'Oops! Qualcosa è andato storto',
      'Optional': 'Opzionale',
      'Sending': 'Invio...',
      'NotificationPermission': 'È garantito il permesso di notifica',
      'Theme': 'Tema',
      'DarkMode': 'Modalità scura',
      'Others': 'Altri',
      'PeopleWhoReacted': 'Persone che hanno reagito',
      'BadWords':
          'Sono state rilevate parolacce, il tuo account potrebbe essere sospeso!',
      'PickFile': 'Scegli file',
      'Loading': 'Caricamento in corso...',
      'PleaseAddAudio': 'Aggiungi audio',
      'PeopleYouMayFollow': 'Persone che puoi seguire',
      'Unsave': 'Non salvare',
      'PostsLikes': 'Pubblica Mi piace',
      'CommentsLikes': 'Commenti Mi piace',
      'RepliesLikes': 'Risposte Mi piace',
      'ShareiConfezz': 'Condividi iConfezz',
      'MemberSince': 'Membro da',
      'Comment': 'Commento',
      'UnShare': 'Annulla condivisione',
      'Liked': 'È piaciuto',
      'Like': 'Piace',
      'SAVED': 'SALVATO',
      'SHARED': 'CONDIVISA',
      'AppLanguage': 'Lingua dell\'app',
      'ComingSoon': 'Prossimamente',
      'ReportProblem': 'Segnala un problema',
      'PushNotifications': 'Le notifiche push',
      'ShareProfile': 'Condividi profilo',
      'DeleteAccount': 'Eliminare l\'account',
      'DeleteYourAccount': 'Cancella il tuo account',
      'Record': 'Disco',
      'Or': 'O',
      'NoPhotoPicked': 'Nessuna foto selezionata',
      'SearchForUsers': 'Cerca utenti',
      'NoResultsFound': 'Nessun risultato trovato',
      'TemporaryFilesRemoved': 'File temporanei rimossi con successo',
      'FailedToClean': 'Impossibile pulire i file temporanei',
      'exit': 'Uscita',
      'CommunityGuidelines': 'Linee guida comunitarie',
      'FAQ': 'FAQ',
      'Paste': 'Paste',
      'DoYouWantToPasteThisCode': 'Do you want to paste this code?',
      'PasteCode': 'Paste Code',
    };
  }
}

extension on _StringsJa {
  Map<String, dynamic> _buildFlatMap() {
    return <String, dynamic>{
      'AppName': 'iConfezz',
      'AboutApp': 'iConfezzは他の人の声を聞くことに専念しているコミュニティです。',
      'AppVersion': '2.0.0',
      'UnknownError': '何かがうまくいかなかった',
      'Next': '次',
      'EmailUser': 'あなたはユーザーにメールを送信しますか,ここにログインしてください！',
      'Email': 'Eメール',
      'bio': 'バイオグラフィー',
      'city': '街',
      'dob': '生年月日',
      'Password': 'パスワード',
      'Login': 'ログイン',
      'PhoneNumber': '電話番号',
      'PhoneVerification': '電話番号の確認',
      'EnterCode': 'に送信されたコードを入力してください',
      'DidntRecieveCode': 'コードを受け取りませんでしたか？',
      'InvalidSMSCode': 'SMS確認コードが無効です',
      'RESEND': '再送',
      'VERIFY': '確認',
      'POSTS': 'POSTS',
      'FOLLOWERS': 'フォロワー',
      'FOLLOWINGS': '以下',
      'Follow': '従う',
      'Following': '続く',
      'Register': '登録',
      'Username': 'ユーザー名',
      'FirstName': 'ファーストネーム',
      'LastName': '苗字',
      'FullName': 'フルネーム',
      'Status': '状態',
      'AddValidUsername': '有効なユーザー名を追加してください',
      'AddValidFirstName': '有効な名を追加してください',
      'AddValidLastName': '有効な姓を追加してください',
      'SelectGender': 'あなたの性別を選択してください。',
      'Male': '男',
      'Female': '女性',
      'Other': '他の',
      'AcceptTerms': '私はすべての利用規約に同意します',
      'Save': '保存する',
      'Search': '探す',
      'Searching': '検索中...',
      'SelectPhoneCode': '電話コードを選択してください',
      'InvalidEmail': '無効なメール',
      'ShortPassword': 'パスワードには少なくとも8文字が含まれている必要があります',
      'UsernameTaken': 'ユーザー名は既に使われています',
      'InvalidPhone': 'すべてのセルを適切に埋めてください',
      'Message': 'メッセージ',
      'LoginFirst': 'おっと,最初にログインしてください',
      'START': '始めましょう',
      'Wallpapers': '壁紙',
      'Profile': 'プロフィール',
      'NoInternet': 'インターネットアクセスなし',
      'Settings': '設定',
      'Account': 'アカウント',
      'EditProfile': 'プロファイル編集',
      'OnlineStatus': 'オンラインステータス',
      'OnlineDescription': 'あなたが最後にオンラインになったときは誰でも見ることができます',
      'Logout': 'ログアウト',
      'DirectMsgs': 'チャットメッセージ',
      'DirectMsgsDescr': 'チャット通知を受信する',
      'GroupsMsgs': 'グループメッセージ',
      'GroupsMsgsDesc': 'すべてのグループ通知を受け取る',
      'AddGroupName': 'お願い！グループ名を追加してください',
      'About': '私たちに関しては',
      'RateUs': '私たちを評価してください',
      'EULA': 'EULA契約',
      'PrivacyPolicy': 'プライバシーポリシー',
      'Feed': '餌',
      'Post': '役職',
      'Gallery': 'ギャラリー',
      'Stories': 'ストーリー',
      'MyPosts': '私の投稿',
      'Favorites': 'お気に入り',
      'Announcement': '発表',
      'WhatOnMind': '何を共有しますか？',
      'Likes': 'いいね',
      'Comments': 'コメント',
      'CreatePost': '投稿を作成',
      'Share': 'シェア',
      'Edit': '編集',
      'Delete': '消去',
      'Copy': 'コピー',
      'Copied': 'コピー',
      'ReadMore': '...続きを読む',
      'SuccessPostPublish': '投稿は正常に公開されました',
      'SuccessPostEdited': '投稿は正常に編集されました',
      'TypeComment': 'コメントを入力してください...',
      'TypeReply': '返信を入力してください...',
      'NotAllowedToPublish': '申し訳ありませんが,もう公開することはできません',
      'NotAllowedToComment': 'ごめんなさい！コメントは許されません',
      'PostRemoveConfirm': 'この投稿を削除してもよろしいですか？',
      'CommentRemoveConfirm': 'このコメントを削除してもよろしいですか？',
      'AddSomeContent': '投稿する前にコンテンツを追加してください',
      'Reply': '返事',
      'Replies': '返信',
      'RepliedToComment': 'あなたのコメントに返信しました',
      'Chats': 'チャット',
      'OnlineUsers': 'オンラインユーザー',
      'RecentChats': '最近のチャット',
      'RemoveConversation': '会話を削除',
      'Messaging': 'メッセージ...',
      'CannotChatWithUser': '申し訳ありませんが,このユーザーとチャットすることはできません',
      'MsgDeleteConfirm': 'メッセージを削除してもよろしいですか？',
      'NoChatFound': '最近のチャットはまだありません。チャットを開始してください！',
      'Online': 'オンライン',
      'Typing': 'タイピング...',
      'Image': '画像',
      'Voice': '声',
      'Video': 'ビデオ',
      'Emoji': '絵文字',
      'GIF': 'GIF',
      'Sticker': 'ステッカー',
      'Groups': 'グループ',
      'CreateGroup': 'グループを作成する',
      'EditGroup': 'グループを編集',
      'Members': 'メンバー',
      'PhotosLibrary': '写真ライブラリ',
      'TakePicture': '写真を撮る',
      'VideosLibrary': 'ビデオライブラリ',
      'RecordVideo': '録画映像',
      'Cancel': 'キャンセル',
      'SlideToCancel': 'スライドしてキャンセル',
      'On': 'の上',
      'Off': 'オフ',
      'Group': 'グループ',
      'LeaveGroup': 'グループを離れる',
      'GroupName': 'グループ名',
      'Join': '加入',
      'Joined': '参加',
      'Public': '公衆',
      'Private': 'プライベート',
      'GroupType': 'グループタイプ',
      'RemoveMember': 'メンバーを削除',
      'AddMembers': 'メンバーを追加',
      'Block': 'ブロック',
      'Unblock': 'ブロック解除',
      'Ban': '禁止',
      'Unban': '禁止解除',
      'Banned': '禁止された',
      'Newest': '最新',
      'Trending': 'トレンド',
      'MostDownloaded': '最もダウンロードされた',
      'Categories': 'カテゴリ',
      'AddWallpaper': '壁紙を追加',
      'WallpaperName': '壁紙名',
      'Category': 'カテゴリー',
      'Upload': 'アップロード',
      'Home': '家',
      'Lock': 'ロック',
      'Both': '両方',
      'SetAsWallpaper': '壁紙として設定',
      'WallpaperSet': '壁紙セットに成功しました',
      'FailedToUpload': 'おっと！アップロードに失敗しました。もう一度やり直してください',
      'UploadFinished': 'アップロードが完了しました',
      'Downloading': 'ダウンロード',
      'NoWallpaperSelectedMsg': '続行するには壁紙を選択してください',
      'NoImgSelected': '画像が選択されていません',
      'Notifications': '通知',
      'StartFollowingMsg': 'あなたをフォロー始めました',
      'PostReactionMsg': 'あなたの投稿に反応しました',
      'PostCommentMsg': 'あなたの投稿にコメントしました',
      'ReplyMsg': 'あなたのコメントに返信しました',
      'CommentReactedMsg': 'あなたのコメントに反応しました',
      'CannotFindFile': 'おっと！ファイルが見つかりません',
      'NoFilePicked': 'アップロードしようとしましたが,ファイルが選択されていません',
      'SentYouMsg': 'メッセージを送信しました',
      'Report': '報告する',
      'Unreport': '報告しない',
      'ReportDesc': '次のような投稿を削除します：',
      'ReportReasons':
          '⚫️性的なコンテンツ。\n\n⚫️暴力的または反発的なコンテンツ。\n\n⚫️憎悪的または虐待的なコンテンツ。\n\n⚫️スパムまたは誤解を招くコンテンツ。',
      'ReportNote': 'あなたがこの行動をとった場合,私たちは彼らに知らせません。',
      'ReportThanks': '私たちはあなたの要求をチェックします,私たちのコミュニティを改善するのを手伝ってくれてありがとう',
      'Admin': '管理者',
      'ProfanityDetected': '不適切な言葉が検出された場合,アカウントが停止される可能性があります！',
      'AreYouSure': 'よろしいですか',
      'ConfirmChatDeletion': 'チャットを削除してもよろしいですか',
      'ReportedPosts': '報告された投稿',
      'AllChats': 'すべてのチャット',
      'Users': 'ユーザー',
      'TermsService': '利用規約',
      'WaitingToRecord': '録音待ち',
      'AdjustVolume': '音量調節',
      'AdjustSpeed': '速度を調整する',
      'Ok': 'Ok',
      'ShareExternally': '外部共有',
      'ShareOnYourWall': 'あなたの壁で共有する',
      'Error': 'エラー',
      'Me': '自分',
      'OopsCode': 'おっと！veficationコードが間違っています',
      'OopsNetwork': 'おっと！何か問題が発生しました。ネットワークを確認してください！',
      'OopsEmailorPassword': 'おっと！メールアドレスまたはパスワードが正しくありません！',
      'OopsNotRegister': 'おっと！ユーザーが見つかりません。登録しない場合は,最初にサインアップすることを検討してください！',
      'OopsEmailUsed': 'おっと！このメールはすでに別のアカウントで使用されています！',
      'OopsPhoneFormat': 'おっと！電話のフォーマットが無効です',
      'NoAudioRecorded': '音声が追加されていません',
      'OopsWrong': 'おっと！何かが間違っていた',
      'Optional': 'オプション',
      'Sending': '送信... ',
      'NotificationPermission': '通知許可が保証されます',
      'Theme': 'テーマ',
      'DarkMode': 'ダークモード',
      'Others': 'その他',
      'PeopleWhoReacted': '反応した人',
      'BadWords': '不適切な言葉が検出された場合,アカウントが停止される可能性があります！',
      'PickFile': 'ファイルを選択',
      'Loading': '読み込み中...',
      'PleaseAddAudio': '音声を追加してください',
      'PeopleYouMayFollow': 'あなたがフォローするかもしれない人々',
      'Unsave': '保存解除',
      'PostsLikes': 'いいね！',
      'CommentsLikes': 'コメントが好き',
      'RepliesLikes': 'いいね',
      'ShareiConfezz': 'iConfezzを共有する',
      'MemberSince': '以来のメンバー',
      'Comment': 'コメント',
      'UnShare': '共有解除',
      'Liked': 'いいね',
      'Like': '好き',
      'SAVED': '保存済み',
      'SHARED': '共有',
      'AppLanguage': 'アプリ言語',
      'ComingSoon': '近日公開',
      'ReportProblem': '問題を報告します',
      'PushNotifications': 'プッシュ通知',
      'ShareProfile': 'プロフィールを共有する',
      'DeleteAccount': 'アカウントを削除する',
      'DeleteYourAccount': 'アカウントを削除',
      'Record': '記録',
      'Or': 'または',
      'NoPhotoPicked': '写真が選択されていません',
      'SearchForUsers': 'ユーザーの検索',
      'NoResultsFound': '結果が見つかりません',
      'TemporaryFilesRemoved': '一時ファイルは正常に削除されました',
      'FailedToClean': '一時ファイルのクリーンアップに失敗しました',
      'exit': '出口',
      'CommunityGuidelines': 'コミュニティガイドライン',
      'FAQ': 'よくある質問',
      'Paste': 'Paste',
      'DoYouWantToPasteThisCode': 'Do you want to paste this code?',
      'PasteCode': 'Paste Code',
    };
  }
}

extension on _StringsKo {
  Map<String, dynamic> _buildFlatMap() {
    return <String, dynamic>{
      'AppName': '아이컨페즈',
      'AboutApp': 'iConfezz는 다른 사람들의 목소리를 들을 수 있는 커뮤니티입니다.',
      'AppVersion': '2.0.0',
      'UnknownError': '뭔가 잘못됐다',
      'Next': '다음',
      'EmailUser': '이메일 사용자입니까, 여기에 로그인하십시오!',
      'Email': '이메일',
      'bio': '전기',
      'city': '도시',
      'dob': '생일',
      'Password': '비밀번호',
      'Login': '로그인',
      'PhoneNumber': '전화 번호',
      'PhoneVerification': '전화번호 인증',
      'EnterCode': '로 전송된 코드를 입력하십시오',
      'DidntRecieveCode': '코드를 받지 못하셨나요?',
      'InvalidSMSCode': 'SMS 인증 코드가 유효하지 않습니다',
      'RESEND': '재전송',
      'VERIFY': '확인',
      'POSTS': '포스트',
      'FOLLOWERS': '팔로워',
      'FOLLOWINGS': '다음',
      'Follow': '따르다',
      'Following': '수행원',
      'Register': '등록하다',
      'Username': '사용자 이름',
      'FirstName': '이름',
      'LastName': '성',
      'FullName': '이름',
      'Status': '상태',
      'AddValidUsername': '유효한 사용자 이름 추가',
      'AddValidFirstName': '유효한 이름 추가',
      'AddValidLastName': '유효한 성을 추가하십시오',
      'SelectGender': '당신의 성별을 선택 해주세요.',
      'Male': '남성',
      'Female': '여자',
      'Other': '다른',
      'AcceptTerms': '모든 이용약관에 동의합니다',
      'Save': '구하다',
      'Search': '검색',
      'Searching': '수색...',
      'SelectPhoneCode': '전화번호 선택',
      'InvalidEmail': '잘못된 이메일',
      'ShortPassword': '비밀번호는 8자 이상이어야 합니다',
      'UsernameTaken': '이미 사용중인 이름입니다',
      'InvalidPhone': '모든 셀을 제대로 채워주세요',
      'Message': '메시지',
      'LoginFirst': '아, 먼저 로그인하십시오',
      'START': '시작하다',
      'Wallpapers': '배경 화면',
      'Profile': '프로필',
      'NoInternet': '인터넷에 액세스할 수 없음',
      'Settings': '설정',
      'Account': '계정',
      'EditProfile': '프로필 편집',
      'OnlineStatus': '온라인 상태',
      'OnlineDescription': '당신이 언제 마지막 온라인인지 누구나 볼 수 있습니다',
      'Logout': '로그 아웃',
      'DirectMsgs': '채팅 메시지',
      'DirectMsgsDescr': '채팅 알림 받기',
      'GroupsMsgs': '그룹 메시지',
      'GroupsMsgsDesc': '모든 그룹 알림 수신',
      'AddGroupName': '제발! 그룹 이름 추가',
      'About': '우리에 대해',
      'RateUs': '우리 평가',
      'EULA': 'EULA 계약',
      'PrivacyPolicy': '개인 정보 정책',
      'Feed': '먹이다',
      'Post': '게시하다',
      'Gallery': '갤러리',
      'Stories': '이야기',
      'MyPosts': '내 게시물',
      'Favorites': '즐겨찾기',
      'Announcement': '발표',
      'WhatOnMind': '무엇을 공유할 것인가?',
      'Likes': '좋아요',
      'Comments': '코멘트',
      'CreatePost': '게시물 작성',
      'Share': '공유하다',
      'Edit': '편집하다',
      'Delete': '삭제',
      'Copy': '복사',
      'Copied': '복사',
      'ReadMore': '...자세히 보기',
      'SuccessPostPublish': '게시물이 성공적으로 게시되었습니다',
      'SuccessPostEdited': '게시물이 성공적으로 편집되었습니다',
      'TypeComment': '댓글을 입력하세요...',
      'TypeReply': '답장을 입력하세요...',
      'NotAllowedToPublish': '죄송합니다! 더 이상 게시할 수 없습니다.',
      'NotAllowedToComment': '죄송합니다! 댓글을 달 수 없습니다.',
      'PostRemoveConfirm': '이 게시물을 삭제하시겠습니까?',
      'CommentRemoveConfirm': '이 댓글을 삭제하시겠습니까?',
      'AddSomeContent': '게시하기 전에 일부 콘텐츠를 추가하십시오',
      'Reply': '회신하다',
      'Replies': '답장',
      'RepliedToComment': '댓글에 답장',
      'Chats': '채팅',
      'OnlineUsers': '온라인 사용자',
      'RecentChats': '최근 채팅',
      'RemoveConversation': '대화 제거',
      'Messaging': '메시지...',
      'CannotChatWithUser': '죄송합니다! 이 사용자와 채팅할 수 없습니다',
      'MsgDeleteConfirm': '정말 메시지를 삭제하시겠습니까?',
      'NoChatFound': '아직 최근 채팅이 없습니다. 채팅을 시작하세요!',
      'Online': '온라인',
      'Typing': '타자...',
      'Image': '영상',
      'Voice': '목소리',
      'Video': '동영상',
      'Emoji': '이모티콘',
      'GIF': 'GIF',
      'Sticker': '상표',
      'Groups': '여러 떼',
      'CreateGroup': '그룹 만들기',
      'EditGroup': '그룹 편집',
      'Members': '회원',
      'PhotosLibrary': '사진 라이브러리',
      'TakePicture': '사진을 촬영',
      'VideosLibrary': '비디오 라이브러리',
      'RecordVideo': '비디오 녹화',
      'Cancel': '취소',
      'SlideToCancel': '슬라이드하여 취소',
      'On': '에',
      'Off': '끄다',
      'Group': '그룹',
      'LeaveGroup': '그룹을 떠나다',
      'GroupName': '그룹 이름',
      'Join': '가입하다',
      'Joined': '가입',
      'Public': '공공의',
      'Private': '사적인',
      'GroupType': '그룹 유형',
      'RemoveMember': '멤버 제거',
      'AddMembers': '구성원 추가',
      'Block': '차단하다',
      'Unblock': '차단 해제',
      'Ban': '반',
      'Unban': '해제',
      'Banned': '금지',
      'Newest': '최신',
      'Trending': '트렌딩',
      'MostDownloaded': '가장 많이 다운로드됨',
      'Categories': '카테고리',
      'AddWallpaper': '배경 화면 추가',
      'WallpaperName': '배경 화면 이름',
      'Category': '범주',
      'Upload': '업로드',
      'Home': '집',
      'Lock': '잠그다',
      'Both': '둘 다',
      'SetAsWallpaper': '배경화면으로 설정',
      'WallpaperSet': '배경 화면이 성공적으로 설정되었습니다',
      'FailedToUpload': '죄송합니다! 업로드에 실패했습니다. 다시 시도하십시오.',
      'UploadFinished': '업로드 완료',
      'Downloading': '다운로드',
      'NoWallpaperSelectedMsg': '계속하려면 배경 화면을 선택하십시오',
      'NoImgSelected': '선택한 이미지가 없습니다',
      'Notifications': '알림',
      'StartFollowingMsg': '당신을 팔로우하기 시작했습니다',
      'PostReactionMsg': '당신의 게시물에 반응했습니다',
      'PostCommentMsg': '당신의 게시물에 댓글을 달았습니다',
      'ReplyMsg': '댓글에 답장',
      'CommentReactedMsg': '당신의 의견에 반응했습니다',
      'CannotFindFile': '죄송합니다! 파일을 찾을 수 없습니다',
      'NoFilePicked': '업로드하려고 하는데 파일이 선택되지 않았습니다',
      'SentYouMsg': '메시지를 보냈습니다',
      'Report': '보고서',
      'Unreport': '신고 취소',
      'ReportDesc': '다음이 있는 게시물을 제거합니다. ',
      'ReportReasons':
          '⚫️ 성적인 콘텐츠. \n\n⚫️ 폭력적이거나 혐오스러운 콘텐츠. \n\n⚫️ 증오 또는 모욕적인 콘텐츠. \n\n⚫️ 스팸 또는 오해의 소지가 있는 콘텐츠.',
      'ReportNote': '당신이 이 조치를 취하면 우리는 그들에게 알리지 않을 것입니다.',
      'ReportThanks': '요청을 확인하겠습니다. 커뮤니티 개선에 도움을 주셔서 감사합니다.',
      'Admin': '관리자',
      'ProfanityDetected': '나쁜 단어가 감지되면 계정이 정지될 수 있습니다!',
      'AreYouSure': '확실히',
      'ConfirmChatDeletion': '채팅을 삭제하시겠습니까?',
      'ReportedPosts': '신고된 게시물',
      'AllChats': '모든 채팅',
      'Users': '사용자',
      'TermsService': '서비스 약관',
      'WaitingToRecord': '녹화 대기 중',
      'AdjustVolume': '볼륨 조정',
      'AdjustSpeed': '속도 조정',
      'Ok': '확인',
      'ShareExternally': '외부로 공유',
      'ShareOnYourWall': '벽에 공유',
      'Error': '오류',
      'Me': '나',
      'OopsCode': '죄송합니다! 인증 코드가 잘못되었습니다',
      'OopsNetwork': '죄송합니다! 문제가 발생했습니다. 네트워크를 확인하십시오!',
      'OopsEmailorPassword': '죄송합니다! 이메일 또는 비밀번호가 올바르지 않습니다!',
      'OopsNotRegister': '죄송합니다! 사용자가 없습니다. 등록하지 않은 경우 먼저 등록을 고려하십시오!',
      'OopsEmailUsed': '죄송합니다! 이 이메일은 이미 다른 계정에서 사용 중입니다!',
      'OopsPhoneFormat': '죄송합니다! 전화 형식이 잘못되었습니다',
      'NoAudioRecorded': '오디오가 추가되지 않았습니다',
      'OopsWrong': '아차! 문제가 발생했습니다',
      'Optional': '선택 과목',
      'Sending': '보내는 중...',
      'NotificationPermission': '알림 권한이 부여됩니다',
      'Theme': '주제',
      'DarkMode': '다크 모드',
      'Others': '기타',
      'PeopleWhoReacted': '반응하는 사람들',
      'BadWords': '나쁜 단어가 감지되면 계정이 정지될 수 있습니다!',
      'PickFile': '파일 선택',
      'Loading': '로드 중...',
      'PleaseAddAudio': '오디오를 추가하십시오',
      'PeopleYouMayFollow': '팔로우할 수 있는 사람들',
      'Unsave': '저장 취소',
      'PostsLikes': '게시물 좋아요',
      'CommentsLikes': '댓글 좋아요',
      'RepliesLikes': '좋아요 답글',
      'ShareiConfezz': 'iConfezz 공유',
      'MemberSince': '회원 가입일',
      'Comment': '논평',
      'UnShare': '공유 취소',
      'Liked': '좋아요',
      'Like': '처럼',
      'SAVED': '저장됨',
      'SHARED': '공유',
      'AppLanguage': '앱 언어',
      'ComingSoon': '곧',
      'ReportProblem': '문제 보고',
      'PushNotifications': '푸시 알림',
      'ShareProfile': '프로필 공유',
      'DeleteAccount': '계정 삭제',
      'DeleteYourAccount': '계정 삭제',
      'Record': '기록',
      'Or': '또는',
      'NoPhotoPicked': '선택한 사진이 없습니다',
      'SearchForUsers': '사용자 검색',
      'NoResultsFound': '검색 결과가 없습니다',
      'TemporaryFilesRemoved': '임시 파일 제거 성공',
      'FailedToClean': '임시 파일을 정리하지 못했습니다',
      'exit': '출구',
      'CommunityGuidelines': '커뮤니티 가이드라인',
      'FAQ': '자주하는 질문',
      'Paste': 'Paste',
      'DoYouWantToPasteThisCode': 'Do you want to paste this code?',
      'PasteCode': 'Paste Code',
    };
  }
}

extension on _StringsRu {
  Map<String, dynamic> _buildFlatMap() {
    return <String, dynamic>{
      'AppName': 'АйКонфез',
      'AboutApp':
          'iConfezz — это сообщество, созданное для того, чтобы слышать голоса других',
      'AppVersion': '2.0.0',
      'UnknownError': 'Что-то пошло не так',
      'Next': 'Следующий',
      'EmailUser': 'Вы пользователь электронной почты, войдите здесь!',
      'Email': 'Эл. адрес',
      'bio': 'Биография',
      'city': 'Город',
      'dob': 'Дата рождения',
      'Password': 'Пароль',
      'Login': 'Авторизоваться',
      'PhoneNumber': 'Телефонный номер',
      'PhoneVerification': 'Подтверждение номера телефона',
      'EnterCode': 'Введите код, отправленный на ',
      'DidntRecieveCode': 'Не получили код?',
      'InvalidSMSCode': 'Код подтверждения SMS недействителен',
      'RESEND': 'ОТПРАВИТЬ',
      'VERIFY': 'ПРОВЕРЯТЬ',
      'POSTS': 'ПОСТЫ',
      'FOLLOWERS': 'Последователи',
      'FOLLOWINGS': 'Следования',
      'Follow': 'Следовать',
      'Following': 'Следующий',
      'Register': 'Регистр',
      'Username': 'Имя пользователя',
      'FirstName': 'Имя',
      'LastName': 'Фамилия',
      'FullName': 'ФИО',
      'Status': 'Статус',
      'AddValidUsername': 'Добавить действительное имя пользователя',
      'AddValidFirstName': 'Добавить действительное имя',
      'AddValidLastName': 'Добавить действительную фамилию',
      'SelectGender': 'Пожалуйста, выберите Ваш пол.',
      'Male': 'Мужской',
      'Female': 'Женский',
      'Other': 'Другой',
      'AcceptTerms': 'Я принимаю все условия и положения',
      'Save': 'Сохранять',
      'Search': 'Поиск',
      'Searching': 'В поисках...',
      'SelectPhoneCode': 'Выберите код телефона',
      'InvalidEmail': 'Неверный адрес электронной почты',
      'ShortPassword': 'Пароль должен содержать не менее 8 символов',
      'UsernameTaken': 'Имя пользователя уже используется',
      'InvalidPhone': 'Пожалуйста, заполните все ячейки правильно',
      'Message': 'Сообщение',
      'LoginFirst': 'К сожалению, сначала войдите в систему',
      'START': 'НАЧАТЬ',
      'Wallpapers': 'Обои',
      'Profile': 'Профиль',
      'NoInternet': 'Нет доступа в Интернет',
      'Settings': 'Настройки',
      'Account': 'Счет',
      'EditProfile': 'Редактировать профиль',
      'OnlineStatus': 'Онлайн-статус',
      'OnlineDescription':
          'Все могут видеть, когда вы в последний раз были в сети',
      'Logout': 'Выйти',
      'DirectMsgs': 'Сообщения в чате',
      'DirectMsgsDescr': 'Получать уведомления чата',
      'GroupsMsgs': 'Групповые сообщения',
      'GroupsMsgsDesc': 'Получать все уведомления групп',
      'AddGroupName': 'Пожалуйста! Добавьте название группы',
      'About': 'о нас',
      'RateUs': 'Оцените нас',
      'EULA': 'Соглашение с конечным пользователем',
      'PrivacyPolicy': 'Политика конфиденциальности',
      'Feed': 'Подача',
      'Post': 'Сообщение',
      'Gallery': 'Галерея',
      'Stories': 'Рассказы',
      'MyPosts': 'Мои сообщения',
      'Favorites': 'Избранное',
      'Announcement': 'Объявление',
      'WhatOnMind': 'Чем поделиться?',
      'Likes': 'Нравится',
      'Comments': 'Комментарии',
      'CreatePost': 'Создать пост',
      'Share': 'Делиться',
      'Edit': 'Редактировать',
      'Delete': 'Удалить',
      'Copy': 'Копировать',
      'Copied': 'Скопировано',
      'ReadMore': '...Прочитайте больше',
      'SuccessPostPublish': 'Пост успешно опубликован',
      'SuccessPostEdited': 'Пост успешно отредактирован',
      'TypeComment': 'Введите комментарий...',
      'TypeReply': 'Введите ответ...',
      'NotAllowedToPublish': 'Извините! Вам больше не разрешено публиковать',
      'NotAllowedToComment': 'Извините! Вы не можете комментировать',
      'PostRemoveConfirm': 'Вы уверены, что хотите удалить эту запись?',
      'CommentRemoveConfirm':
          'Вы уверенны, что хотите удалить этот комментарий?',
      'AddSomeContent': 'Пожалуйста, добавьте контент перед публикацией',
      'Reply': 'Отвечать',
      'Replies': 'Ответы',
      'RepliedToComment': 'ответил на ваш комментарий',
      'Chats': 'Чаты',
      'OnlineUsers': 'ОНЛАЙН-ПОЛЬЗОВАТЕЛИ',
      'RecentChats': 'ПОСЛЕДНИЕ ЧАТЫ',
      'RemoveConversation': 'Удалить беседу',
      'Messaging': 'Сообщение...',
      'CannotChatWithUser':
          'Извините! Вы не можете общаться с этим пользователем',
      'MsgDeleteConfirm': 'Вы уверены, что хотите удалить сообщение?',
      'NoChatFound': 'Чата пока нет, начните общаться!',
      'Online': 'В сети',
      'Typing': 'Ввод...',
      'Image': 'Изображение',
      'Voice': 'Голос',
      'Video': 'Видео',
      'Emoji': 'Эмодзи',
      'GIF': 'ГИФ',
      'Sticker': 'Наклейка',
      'Groups': 'Группы',
      'CreateGroup': 'Создать группу',
      'EditGroup': 'Редактировать свою группу',
      'Members': 'Члены',
      'PhotosLibrary': 'Библиотека фотографий',
      'TakePicture': 'Сделать фотографию',
      'VideosLibrary': 'Видеотека',
      'RecordVideo': 'Запись видео',
      'Cancel': 'Отмена',
      'SlideToCancel': 'Слайд для отмены',
      'On': 'На',
      'Off': 'Выключенный',
      'Group': 'Группа',
      'LeaveGroup': 'Покинуть группу',
      'GroupName': 'Название группы',
      'Join': 'Присоединиться',
      'Joined': 'Присоединился',
      'Public': 'Общественный',
      'Private': 'Частный',
      'GroupType': 'Тип группы',
      'RemoveMember': 'Удалить участника',
      'AddMembers': 'Добавить участников',
      'Block': 'Блокировать',
      'Unblock': 'Разблокировать',
      'Ban': 'Запрет',
      'Unban': 'Разбан',
      'Banned': 'Запрещено',
      'Newest': 'Новейший',
      'Trending': 'В тренде',
      'MostDownloaded': 'Самые скачиваемые',
      'Categories': 'Категории',
      'AddWallpaper': 'Добавить обои',
      'WallpaperName': 'Имя обоев',
      'Category': 'Категория',
      'Upload': 'Загрузить',
      'Home': 'Дом',
      'Lock': 'Замок',
      'Both': 'Оба',
      'SetAsWallpaper': 'Установить как обои рабочего стола',
      'WallpaperSet': 'Обои установлены успешно',
      'FailedToUpload': 'Ой! Ошибка загрузки. Повторите попытку',
      'UploadFinished': 'Загрузка завершена',
      'Downloading': 'Скачивание',
      'NoWallpaperSelectedMsg': 'Пожалуйста, выберите обои, чтобы продолжить',
      'NoImgSelected': 'Изображение не выбрано',
      'Notifications': 'Уведомления',
      'StartFollowingMsg': 'начал следить за тобой',
      'PostReactionMsg': 'отреагировал на ваш пост',
      'PostCommentMsg': 'прокомментировал ваш пост',
      'ReplyMsg': 'ответил на ваш комментарий',
      'CommentReactedMsg': 'отреагировал на ваш комментарий',
      'CannotFindFile': 'Ой! Не удается найти файл',
      'NoFilePicked': 'Попытка загрузки, но файл не выбран',
      'SentYouMsg': 'Отправил вам сообщение',
      'Report': 'Отчет',
      'Unreport': 'Не сообщать',
      'ReportDesc': 'Удаляем пост, в котором есть: ',
      'ReportReasons':
          '⚫️ Контент сексуального характера. \n\n⚫️ Жестокий или отталкивающий контент. \n\n⚫️ Ненавистный или оскорбительный контент. \n\n⚫️ Спам или вводящий в заблуждение.',
      'ReportNote': 'Мы не сообщим им, если вы предпримете это действие.',
      'ReportThanks':
          'Мы проверим ваш запрос, спасибо за помощь в улучшении нашего сообщества',
      'Admin': 'Админ',
      'ProfanityDetected':
          'Обнаружены нецензурные слова, ваша учетная запись может быть заблокирована!',
      'AreYouSure': 'Вы уверены',
      'ConfirmChatDeletion': 'Вы уверены, что хотите удалить чат',
      'ReportedPosts': 'Отмеченные сообщения',
      'AllChats': 'Все чаты',
      'Users': 'Пользователи',
      'TermsService': 'Условия обслуживания',
      'WaitingToRecord': 'В ожидании записи',
      'AdjustVolume': 'Регулировать громкость',
      'AdjustSpeed': 'Регулировать скорость',
      'Ok': 'В порядке',
      'ShareExternally': 'Поделиться снаружи',
      'ShareOnYourWall': 'Поделиться на стене',
      'Error': 'Ошибка',
      'Me': 'Мне',
      'OopsCode': 'К сожалению, неверный код подтверждения',
      'OopsNetwork':
          'К сожалению, что-то пошло не так, пожалуйста, проверьте сеть!',
      'OopsEmailorPassword': 'Ой! Электронная почта или пароль неверны!',
      'OopsNotRegister':
          'К сожалению, пользователь не найден, если вы не зарегистрируетесь, сначала зарегистрируйтесь!',
      'OopsEmailUsed':
          'К сожалению, этот адрес электронной почты уже используется другим аккаунтом!',
      'OopsPhoneFormat': 'К сожалению, формат телефона недействителен',
      'NoAudioRecorded': 'Аудио не добавлено',
      'OopsWrong': 'Упс! Что-то пошло не так',
      'Optional': 'Необязательный',
      'Sending': 'Отправка...',
      'NotificationPermission': 'Разрешение на уведомление гарантировано',
      'Theme': 'Тема',
      'DarkMode': 'Темный режим',
      'Others': 'Другие',
      'PeopleWhoReacted': 'Люди, которые отреагировали',
      'BadWords':
          'Обнаружены нецензурные слова, ваша учетная запись может быть заблокирована!',
      'PickFile': 'Выбрать файл',
      'Loading': 'Загружается...',
      'PleaseAddAudio': 'Пожалуйста, добавьте звук',
      'PeopleYouMayFollow': 'Люди, на которых вы можете подписаться',
      'Unsave': 'Отменить сохранение',
      'PostsLikes': 'Посты лайки',
      'CommentsLikes': 'Комментарии лайки',
      'RepliesLikes': 'Ответы лайки',
      'ShareiConfezz': 'Поделиться iConfezz',
      'MemberSince': 'Член с тех пор',
      'Comment': 'Комментарий',
      'UnShare': 'Не делиться',
      'Liked': 'Понравилось',
      'Like': 'Нравиться',
      'SAVED': 'СОХРАНЕНО',
      'SHARED': 'ОБЩИЙ',
      'AppLanguage': 'Язык приложения',
      'ComingSoon': 'Вскоре',
      'ReportProblem': 'Сообщить о проблеме',
      'PushNotifications': 'Всплывающие уведомления',
      'ShareProfile': 'Поделиться профилем',
      'DeleteAccount': 'Удалить аккаунт',
      'DeleteYourAccount': 'Удалить учетную запись',
      'Record': 'Записывать',
      'Or': 'Или',
      'NoPhotoPicked': 'Фото не выбрано',
      'SearchForUsers': 'Поиск пользователей',
      'NoResultsFound': 'Результаты не найдены',
      'TemporaryFilesRemoved': 'Временные файлы успешно удалены',
      'FailedToClean': 'Не удалось очистить временные файлы',
      'exit': 'выход',
      'CommunityGuidelines': 'Принципы сообщества',
      'FAQ': 'ЧАСТО ЗАДАВАЕМЫЕ ВОПРОСЫ',
      'Paste': 'Paste',
      'DoYouWantToPasteThisCode': 'Do you want to paste this code?',
      'PasteCode': 'Paste Code',
    };
  }
}

extension on _StringsTr {
  Map<String, dynamic> _buildFlatMap() {
    return <String, dynamic>{
      'AppName': 'iConfezz',
      'AboutApp':
          'iConfezz, başkalarının sesini duymaya adanmış bir topluluktur.',
      'AppVersion': '2.0.0',
      'UnknownError': 'Bir şeyler yanlış gitti',
      'Next': 'Sonraki',
      'EmailUser': 'E-posta Kullanıcısı mısınız, Buradan giriş yapın!',
      'Email': 'E-posta',
      'bio': 'Biyografi',
      'city': 'Şehir',
      'dob': 'Doğum tarihi',
      'Password': 'Parola',
      'Login': 'Giriş yapmak',
      'PhoneNumber': 'Telefon numarası',
      'PhoneVerification': 'Telefon Numarası Doğrulama',
      'EnterCode': 'Gönderilen kodu girin',
      'DidntRecieveCode': 'Kodu almadınız mı?',
      'InvalidSMSCode': 'SMS Doğrulama Kodu Geçersiz',
      'RESEND': 'YENİDEN GÖNDER',
      'VERIFY': 'DOĞRULAYIN',
      'POSTS': 'GÖNDERİLER',
      'FOLLOWERS': 'Takipçiler',
      'FOLLOWINGS': 'Takipler',
      'Follow': 'Takip etmek',
      'Following': 'Takip etmek',
      'Register': 'Kayıt olmak',
      'Username': 'Kullanıcı adı',
      'FirstName': 'İlk adı',
      'LastName': 'Soyadı',
      'FullName': 'Ad Soyad',
      'Status': 'Durum',
      'AddValidUsername': 'Geçerli bir kullanıcı adı ekleyin',
      'AddValidFirstName': 'Geçerli bir ad ekleyin',
      'AddValidLastName': 'Geçerli bir soyadı ekleyin',
      'SelectGender': 'Lütfen cinsiyetinizi seçin.',
      'Male': 'Erkek',
      'Female': 'Dişi',
      'Other': 'Diğer',
      'AcceptTerms': 'Tüm Hüküm ve Koşulları Kabul Ediyorum',
      'Save': 'Kaydetmek',
      'Search': 'Aramak',
      'Searching': 'Aranıyor...',
      'SelectPhoneCode': 'Bir telefon kodu seçin',
      'InvalidEmail': 'Geçersiz e-posta',
      'ShortPassword': 'Şifre En Az 8 Karakter İçermelidir',
      'UsernameTaken': 'Kullanıcı adı zaten alınmış',
      'InvalidPhone': 'Lütfen tüm hücreleri uygun şekilde doldurun',
      'Message': 'İleti',
      'LoginFirst': 'Oops, Lütfen önce giriş yapın',
      'START': 'BAŞLAMAK',
      'Wallpapers': 'Duvar kağıtları',
      'Profile': 'Profil',
      'NoInternet': 'İnternet erişimi yok',
      'Settings': 'Ayarlar',
      'Account': 'Hesap',
      'EditProfile': 'Profili Düzenle',
      'OnlineStatus': 'Çevrimiçi durum',
      'OnlineDescription':
          'En son ne zaman çevrimiçi olduğunuzu herkes görebilir',
      'Logout': 'Çıkış Yap',
      'DirectMsgs': 'Sohbet Mesajları',
      'DirectMsgsDescr': 'Sohbet bildirimlerini al',
      'GroupsMsgs': 'Grup Mesajları',
      'GroupsMsgsDesc': 'Tüm Grupların Bildirimlerini Al',
      'AddGroupName': 'Lütfen! Grup Adı Ekle',
      'About': 'Hakkımızda',
      'RateUs': 'Bizi değerlendirin',
      'EULA': 'EULA Anlaşması',
      'PrivacyPolicy': 'Gizlilik Politikası',
      'Feed': 'Beslemek',
      'Post': 'Postalamak',
      'Gallery': 'Galeri',
      'Stories': 'Hikayeler',
      'MyPosts': 'Gönderilerim',
      'Favorites': 'Favoriler',
      'Announcement': 'Duyuru',
      'WhatOnMind': 'Neler Paylaşılır?',
      'Likes': 'Seviyor',
      'Comments': 'Yorumlar',
      'CreatePost': 'Posta Oluştur',
      'Share': 'Paylaşmak',
      'Edit': 'Düzenlemek',
      'Delete': 'Silmek',
      'Copy': 'Kopyala',
      'Copied': 'Kopyalandı',
      'ReadMore': '...Daha fazla oku',
      'SuccessPostPublish': 'Yazı Başarıyla Yayınlandı',
      'SuccessPostEdited': 'Gönderi Başarıyla Düzenlendi',
      'TypeComment': 'Bir yorum yazın...',
      'TypeReply': 'Bir yanıt yazın...',
      'NotAllowedToPublish': 'Üzgünüm! Artık yayınlama izniniz yok',
      'NotAllowedToComment': 'Üzgünüm! Yorum yapmanıza izin verilmiyor',
      'PostRemoveConfirm': 'Bu gönderiyi silmek istediğinizden emin misiniz?',
      'CommentRemoveConfirm': 'Bu yorumu silmek istediğinizden emin misiniz?',
      'AddSomeContent': 'Lütfen göndermeden önce biraz içerik ekleyin',
      'Reply': 'Cevap vermek',
      'Replies': 'Yanıtlar',
      'RepliedToComment': 'yorumunuzu yanıtladım',
      'Chats': 'Sohbetler',
      'OnlineUsers': 'ÇEVRİMİÇİ KULLANICILAR',
      'RecentChats': 'SON SOHBETLER',
      'RemoveConversation': 'Konuşmayı Kaldır',
      'Messaging': 'İleti...',
      'CannotChatWithUser': 'Üzgünüm! Bu kullanıcıyla sohbet edemezsiniz',
      'MsgDeleteConfirm': 'Mesajı silmek istediğinizden emin misiniz?',
      'NoChatFound': 'Henüz Son Sohbet yok, sohbete başla!',
      'Online': 'Çevrimiçi',
      'Typing': 'Yazıyor...',
      'Image': 'Resim',
      'Voice': 'Ses',
      'Video': 'Video',
      'Emoji': 'Emoji',
      'GIF': 'GIF',
      'Sticker': 'Etiket',
      'Groups': 'Gruplar',
      'CreateGroup': 'Grup Oluştur',
      'EditGroup': 'Grubunuzu düzenleyin',
      'Members': 'Üyeler',
      'PhotosLibrary': 'Fotoğraf Kitaplığı',
      'TakePicture': 'Fotoğraf çek',
      'VideosLibrary': 'Video Kitaplığı',
      'RecordVideo': 'Video kaydetmek',
      'Cancel': 'İptal etmek',
      'SlideToCancel': 'İptal etmek için kaydırın',
      'On': 'Açık',
      'Off': 'Kapalı',
      'Group': 'Grup',
      'LeaveGroup': 'Gruptan ayrıl',
      'GroupName': 'Grup ismi',
      'Join': 'Katılmak',
      'Joined': 'katıldı',
      'Public': 'Halk',
      'Private': 'Özel',
      'GroupType': 'Grup Türü',
      'RemoveMember': 'Üyeyi kaldır',
      'AddMembers': 'Üye ekle',
      'Block': 'Engellemek',
      'Unblock': 'Engeli kaldırmak',
      'Ban': 'Yasaklamak',
      'Unban': 'Yasağı kaldır',
      'Banned': 'Yasaklandı',
      'Newest': 'En yeni',
      'Trending': 'Trend',
      'MostDownloaded': 'En Çok İndirilenler',
      'Categories': 'Kategoriler',
      'AddWallpaper': 'Duvar Kağıdı Ekle',
      'WallpaperName': 'Duvar Kağıdı Adı',
      'Category': 'Kategori',
      'Upload': 'Yüklemek',
      'Home': 'Ev',
      'Lock': 'Kilit',
      'Both': 'İkisi birden',
      'SetAsWallpaper': 'Duvar kağıdı olarak ayarlamak',
      'WallpaperSet': 'Duvar Kağıdı Başarıyla Ayarlandı',
      'FailedToUpload': 'Hata! Yükleme Başarısız, Lütfen tekrar deneyin',
      'UploadFinished': 'Yükleme Tamamlandı',
      'Downloading': 'İndirme',
      'NoWallpaperSelectedMsg': 'Devam etmek için lütfen Duvar Kağıdını Seçin',
      'NoImgSelected': 'Görüntü Seçilmedi',
      'Notifications': 'Bildirimler',
      'StartFollowingMsg': 'seni takip etmeye başladım',
      'PostReactionMsg': 'yazınıza tepki verdim',
      'PostCommentMsg': 'gönderinize yorum yaptı',
      'ReplyMsg': 'yorumunuzu yanıtladım',
      'CommentReactedMsg': 'yorumunuza tepki verdim',
      'CannotFindFile': 'Hata! Dosya bulunamıyor',
      'NoFilePicked': 'Yüklemeye çalışılıyor ve hiçbir dosya seçilmiyor',
      'SentYouMsg': 'Size mesaj gönderdim',
      'Report': 'Rapor',
      'Unreport': 'Raporu iptal et',
      'ReportDesc': 'Şu olan gönderiyi kaldırıyoruz: ',
      'ReportReasons':
          '⚫️ Cinsel içerik. \n\n⚫️ Şiddet içeren veya tiksindirici içerik. \n\n⚫️ Nefret dolu veya taciz edici içerik. \n\n⚫️ Spam veya yanıltıcı.',
      'ReportNote': 'Bu eylemi yaparsan onlara haber vermeyeceğiz.',
      'ReportThanks':
          'İsteğinizi kontrol edeceğiz, Topluluğumuzu geliştirmeye yardımcı olduğunuz için teşekkürler',
      'Admin': 'Yönetici',
      'ProfanityDetected':
          'Kötü sözler tespit edildi, hesabınız askıya alınabilir!',
      'AreYouSure': 'Emin misin',
      'ConfirmChatDeletion': 'Sohbeti silmek istediğinizden emin misiniz',
      'ReportedPosts': 'Raporlanan Yazılar',
      'AllChats': 'Tüm Sohbetler',
      'Users': 'Kullanıcılar',
      'TermsService': 'Kullanım Şartları',
      'WaitingToRecord': 'Kayıt bekleniyor',
      'AdjustVolume': 'Sesi ayarla',
      'AdjustSpeed': 'Hızı ayarla',
      'Ok': 'Tamam',
      'ShareExternally': 'Harici olarak paylaş',
      'ShareOnYourWall': 'Duvarında paylaş',
      'Error': 'Hata',
      'Me': 'Ben mi',
      'OopsCode': 'Hata! Vefication kodu yanlış',
      'OopsNetwork':
          'Hata! bir şeyler ters gitti, Lütfen ağınızı kontrol edin!',
      'OopsEmailorPassword': 'Hata! e-posta veya şifre doğru değil!',
      'OopsNotRegister':
          'Hata! kullanıcı bulunamadı, eğer kaydolmadıysanız önce kaydolmayı düşünün!',
      'OopsEmailUsed':
          'Hata! bu e-posta zaten başka bir hesap tarafından kullanılıyor!',
      'OopsPhoneFormat': 'Hata! telefon biçimi geçersiz',
      'NoAudioRecorded': 'Ses eklenmedi',
      'OopsWrong': 'Hoop! Birşeyler yanlış gitti',
      'Optional': 'İsteğe bağlı',
      'Sending': 'Gönderiliyor...',
      'NotificationPermission': 'Bildirim izni garantilidir',
      'Theme': 'Tema',
      'DarkMode': 'Karanlık Mod',
      'Others': 'Diğerleri',
      'PeopleWhoReacted': 'Tepki verenler',
      'BadWords': 'Kötü sözler tespit edildi, hesabınız askıya alınabilir!',
      'PickFile': 'Dosya seç',
      'Loading': 'Yükleniyor...',
      'PleaseAddAudio': 'Lütfen ses ekleyin',
      'PeopleYouMayFollow': 'Takip edebileceğiniz kişiler',
      'Unsave': 'Kaydetmeyi iptal et',
      'PostsLikes': 'Gönderiler Beğenileri',
      'CommentsLikes': 'Yorum Beğenileri',
      'RepliesLikes': 'Yanıt Beğenileri',
      'ShareiConfezz': 'iConfezz\'i paylaşın',
      'MemberSince': 'Den beri üye',
      'Comment': 'Yorum',
      'UnShare': 'Paylaşmayı Kaldır',
      'Liked': 'Beğendim',
      'Like': 'Beğenmek',
      'SAVED': 'KAYDEDİLDİ',
      'SHARED': 'PAYLAŞILMIŞ',
      'AppLanguage': 'Uygulama Dili',
      'ComingSoon': 'Yakında gelecek',
      'ReportProblem': 'Problemi şikayet et',
      'PushNotifications': 'Push bildirimleri',
      'ShareProfile': 'Profili Paylaş',
      'DeleteAccount': 'Hesabı sil',
      'DeleteYourAccount': 'Hesabını sil',
      'Record': 'Kayıt',
      'Or': 'Veya',
      'NoPhotoPicked': 'Fotoğraf Seçilmedi',
      'SearchForUsers': 'Kullanıcıları Ara',
      'NoResultsFound': 'Sonuç bulunamadı',
      'TemporaryFilesRemoved': 'Geçici dosyalar başarıyla kaldırıldı',
      'FailedToClean': 'Geçici dosyalar temizlenemedi',
      'exit': 'çıkış',
      'CommunityGuidelines': 'Topluluk Rehberleri',
      'FAQ': 'SSS',
      'Paste': 'Paste',
      'DoYouWantToPasteThisCode': 'Do you want to paste this code?',
      'PasteCode': 'Paste Code',
    };
  }
}

extension on _StringsZh {
  Map<String, dynamic> _buildFlatMap() {
    return <String, dynamic>{
      'AppName': 'iConfezz',
      'AboutApp': 'iConfezz 是一个致力于倾听他人声音的社区。',
      'AppVersion': '2.0.0',
      'UnknownError': '出问题了',
      'Next': '下一个',
      'EmailUser': '您是电子邮件用户吗,请在此处登录！',
      'Email': '电子邮件',
      'bio': '传',
      'city': '城市',
      'dob': '出生日期',
      'Password': '密码',
      'Login': '登录',
      'PhoneNumber': '电话号码',
      'PhoneVerification': '电话号码验证',
      'EnterCode': '输入发送到的代码',
      'DidntRecieveCode': '没有收到验证码？',
      'InvalidSMSCode': '短信验证码无效',
      'RESEND': '重发',
      'VERIFY': '核实',
      'POSTS': '帖子',
      'FOLLOWERS': '追随者',
      'FOLLOWINGS': '以下',
      'Follow': '跟随',
      'Following': '下列的',
      'Register': '登记',
      'Username': '用户名',
      'FirstName': '名',
      'LastName': '姓',
      'FullName': '全名',
      'Status': '地位',
      'AddValidUsername': '添加一个有效的用户名',
      'AddValidFirstName': '添加一个有效的名字',
      'AddValidLastName': '添加一个有效的姓氏',
      'SelectGender': '请选择您的性别。',
      'Male': '男性',
      'Female': '女性',
      'Other': '其他',
      'AcceptTerms': '我接受所有条款和条件',
      'Save': '节省',
      'Search': '搜索',
      'Searching': '正在搜索...',
      'SelectPhoneCode': '选择电话号码',
      'InvalidEmail': '不合规电邮',
      'ShortPassword': '密码必须包含至少 8 个字符',
      'UsernameTaken': '用户名已被占用',
      'InvalidPhone': '请正确填写所有单元格',
      'Message': '信息',
      'LoginFirst': '糟糕,请先登录',
      'START': '开始',
      'Wallpapers': '壁纸',
      'Profile': '轮廓',
      'NoInternet': '不能访问网络',
      'Settings': '设置',
      'Account': '帐户',
      'EditProfile': '编辑个人资料',
      'OnlineStatus': '在线状态',
      'OnlineDescription': '任何人都可以看到您上次在线的时间',
      'Logout': '登出',
      'DirectMsgs': '聊天消息',
      'DirectMsgsDescr': '接收聊天通知',
      'GroupsMsgs': '群组消息',
      'GroupsMsgsDesc': '接收所有组通知',
      'AddGroupName': '请！添加组名',
      'About': '关于我们',
      'RateUs': '评价我们',
      'EULA': '最终用户许可协议',
      'PrivacyPolicy': '隐私政策',
      'Feed': '喂养',
      'Post': '邮政',
      'Gallery': '画廊',
      'Stories': '故事',
      'MyPosts': '我的帖子',
      'Favorites': '最爱',
      'Announcement': '公告',
      'WhatOnMind': '要分享什么？',
      'Likes': '喜欢',
      'Comments': '评论',
      'CreatePost': '创建帖子',
      'Share': '分享',
      'Edit': '编辑',
      'Delete': '删除',
      'Copy': '复制',
      'Copied': '复制',
      'ReadMore': '...阅读更多',
      'SuccessPostPublish': '帖子已成功发布',
      'SuccessPostEdited': '帖子已成功编辑',
      'TypeComment': '输入评论...',
      'TypeReply': '输入回复...',
      'NotAllowedToPublish': '对不起！你不能再发布了',
      'NotAllowedToComment': '对不起！你不能评论',
      'PostRemoveConfirm': '你确定你要删除这个帖子？',
      'CommentRemoveConfirm': '你确定要删除此评论？',
      'AddSomeContent': '请在发帖前添加一些内容',
      'Reply': '回复',
      'Replies': '回复',
      'RepliedToComment': '回复你的评论',
      'Chats': '聊天',
      'OnlineUsers': '在线用户',
      'RecentChats': '最近的聊天',
      'RemoveConversation': '删除对话',
      'Messaging': '信息...',
      'CannotChatWithUser': '对不起！你不能和这个用户聊天',
      'MsgDeleteConfirm': '您确定要删除该消息吗？',
      'NoChatFound': '还没有最近聊天,开始聊天吧！',
      'Online': '在线的',
      'Typing': '正在打字...',
      'Image': '图片',
      'Voice': '嗓音',
      'Video': '视频',
      'Emoji': '表情符号',
      'GIF': 'GIF',
      'Sticker': '贴纸',
      'Groups': '团体',
      'CreateGroup': '创建一个组',
      'EditGroup': '编辑你的组',
      'Members': '成员',
      'PhotosLibrary': '照片库',
      'TakePicture': '拍照片',
      'VideosLibrary': '视频库',
      'RecordVideo': '录视频',
      'Cancel': '取消',
      'SlideToCancel': '滑动取消',
      'On': '在',
      'Off': '离开',
      'Group': '团体',
      'LeaveGroup': '离开组',
      'GroupName': '组的名字',
      'Join': '加入',
      'Joined': '加入',
      'Public': '上市',
      'Private': '私人的',
      'GroupType': '组类型',
      'RemoveMember': '删除成员',
      'AddMembers': '添加成员',
      'Block': '堵塞',
      'Unblock': '解除封锁',
      'Ban': '禁止',
      'Unban': '解禁',
      'Banned': '禁止',
      'Newest': '最新',
      'Trending': '趋势',
      'MostDownloaded': '下载次数最多',
      'Categories': '类别',
      'AddWallpaper': '添加壁纸',
      'WallpaperName': '壁纸名称',
      'Category': '类别',
      'Upload': '上传',
      'Home': '家',
      'Lock': '锁',
      'Both': '两个都',
      'SetAsWallpaper': '设置为墙纸',
      'WallpaperSet': '壁纸设置成功',
      'FailedToUpload': '糟糕！上传失败,请重试',
      'UploadFinished': '上传完成',
      'Downloading': '下载',
      'NoWallpaperSelectedMsg': '请选择壁纸继续',
      'NoImgSelected': '未选择图像',
      'Notifications': '通知',
      'StartFollowingMsg': '开始关注您',
      'PostReactionMsg': '对你的帖子做出反应',
      'PostCommentMsg': '评论了你的帖子',
      'ReplyMsg': '回复你的评论',
      'CommentReactedMsg': '对您的评论作出反应',
      'CannotFindFile': '糟糕！找不到文件',
      'NoFilePicked': '尝试上传但没有选择文件',
      'SentYouMsg': '给你发消息',
      'Report': '报告',
      'Unreport': '取消报告',
      'ReportDesc': '我们删除具有以下内容的帖子：',
      'ReportReasons':
          '⚫️ 色情内容。\n\n⚫️ 暴力或令人反感的内容。\n\n⚫️ 仇恨或辱骂内容。\n\n⚫️ 垃圾邮件或误导性内容。',
      'ReportNote': '如果您采取此行动,我们不会让他们知道。',
      'ReportThanks': '我们将检查您的请求,感谢您帮助改善我们的社区',
      'Admin': '行政',
      'ProfanityDetected': '检测到坏话,您的帐号可能会被暂停！',
      'AreYouSure': '你确定',
      'ConfirmChatDeletion': '确定要删除聊天',
      'ReportedPosts': '报告的帖子',
      'AllChats': '所有聊天',
      'Users': '用户',
      'TermsService': '服务条款',
      'WaitingToRecord': '等待记录',
      'AdjustVolume': '调整音量',
      'AdjustSpeed': '调整速度',
      'Ok': '行',
      'ShareExternally': '对外分享',
      'ShareOnYourWall': '在你的墙上分享',
      'Error': '错误',
      'Me': '我',
      'OopsCode': '糟糕！验证码错误',
      'OopsNetwork': '糟糕！出了点问题,请检查您的网络！',
      'OopsEmailorPassword': '糟糕！电子邮件或密码不正确！',
      'OopsNotRegister': '哎呀！没有找到用户,如果没有注册考虑先注册！',
      'OopsEmailUsed': '糟糕！此电子邮件已被另一个帐户使用！',
      'OopsPhoneFormat': '糟糕！电话格式无效',
      'NoAudioRecorded': '未添加音频',
      'OopsWrong': '哎呀！出事了',
      'Optional': '选修的',
      'Sending': '发送...',
      'NotificationPermission': '保证通知许可',
      'Theme': '主题',
      'DarkMode': '黑暗模式',
      'Others': '其他',
      'PeopleWhoReacted': '做出反应的人',
      'BadWords': '检测到坏话,您的帐号可能会被暂停！',
      'PickFile': '选择文件',
      'Loading': '加载中...',
      'PleaseAddAudio': '请添加音频',
      'PeopleYouMayFollow': '你可能关注的人',
      'Unsave': '不保存',
      'PostsLikes': '帖子喜欢',
      'CommentsLikes': '评论喜欢',
      'RepliesLikes': '回复喜欢',
      'ShareiConfezz': '分享 iConfezz',
      'MemberSince': '成员以来',
      'Comment': '评论',
      'UnShare': '取消共享',
      'Liked': '喜欢',
      'Like': '像',
      'SAVED': '已保存',
      'SHARED': '共享',
      'AppLanguage': '应用程序语言',
      'ComingSoon': '快来了',
      'ReportProblem': '报告一个问题',
      'PushNotifications': '推送通知',
      'ShareProfile': '分享个人资料',
      'DeleteAccount': '删除帐户',
      'DeleteYourAccount': '删除您的帐户',
      'Record': '记录',
      'Or': '或者',
      'NoPhotoPicked': '没有选择照片',
      'SearchForUsers': '搜索用户',
      'NoResultsFound': '未找到结果',
      'TemporaryFilesRemoved': '成功删除临时文件',
      'FailedToClean': '清理临时文件失败',
      'exit': '出口',
      'CommunityGuidelines': '社区准则',
      'FAQ': '常问问题',
      'Paste': 'Paste',
      'DoYouWantToPasteThisCode': 'Do you want to paste this code?',
      'PasteCode': 'Paste Code',
    };
  }
}
