// ignore_for_file: always_use_package_imports

//import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/cupertino.dart';

import '../imports.dart';

// Future<void> showConfirmDialog(
//   BuildContext context, {
//   String? title,
//   String desc = '',
//   VoidCallback? onConfirm,
// }) async {
//   await AwesomeDialog(
//     context: context,
//     dialogType: DialogType.NO_HEADER,
//     title: title ?? t.AreYouSure,
//     buttonsTextStyle: TextStyles.textStyleRegularBold(context),
//     desc: desc,
//     btnOk: AppButton(t.Ok, onTap: onConfirm),
//     btnCancel: AppButton(
//       t.Cancel,
//       onTap: () => Navigator.of(context).pop(),
//     ),
//   ).show();
// }

Future<void> showConfirmDialog(
  BuildContext context, {
  String? title,
  String desc = '',
  VoidCallback? onConfirm,
}) async {
  showCupertinoDialog(
    context: context,
    builder: (_) => Theme(
      data: appPrefs.isDarkMode() ? ThemeData.dark() : ThemeData.light(),
      child: CupertinoAlertDialog(
        title: AppTextSmallBold(
          title ?? t.AreYouSure,
          color: Theme.of(context).iconTheme.color,
        ),
        content: AppTextSmall(desc),
        actions: [
          // Close the dialog
          // You can use the CupertinoDialogAction widget instead
          CupertinoButton(
            child: AppTextRegularBold(
              t.Cancel,
              color: Theme.of(context).primaryColor,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          CupertinoButton(
            onPressed: onConfirm,
            child: AppTextRegularBold(
              t.Ok,
              color: Theme.of(context).primaryColor,
            ),
          )
        ],
      ),
    ),
  );
}
