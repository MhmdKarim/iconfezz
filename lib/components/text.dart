// ignore_for_file: always_use_package_imports

import '../imports.dart';

class AppTextTitleBold extends StatelessWidget {
  final String data;
  final double? fontSize;
  final Color? color;
  final int? maxLines;
  final TextAlign? textAlign;
  final FontWeight? fontWeight;
  final TextOverflow? overflow;
  final TextDecoration? decoration;
  const AppTextTitleBold(
    this.data, {
    Key? key,
    this.fontSize,
    this.color,
    this.maxLines,
    this.textAlign,
    this.fontWeight,
    this.overflow,
    this.decoration,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      data,
      style: TextStyles.textStyleTitleBold(
        context,
        color: color,
        fontWeight: fontWeight,
        decoration: decoration,
      ),
      maxLines: maxLines,
      textAlign: textAlign,
      overflow: overflow,
    );
  }
}

class AppTextTitle extends StatelessWidget {
  final String data;
  final double? fontSize;
  final Color? color;
  final int? maxLines;
  final TextAlign? textAlign;
  final FontWeight? fontWeight;
  final TextOverflow? overflow;
  final TextDecoration? decoration;
  const AppTextTitle(
    this.data, {
    Key? key,
    this.fontSize,
    this.color,
    this.maxLines,
    this.textAlign,
    this.fontWeight,
    this.overflow,
    this.decoration,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      data,
      style: TextStyles.textStyleTitle(
        context,
        color: color,
        fontWeight: fontWeight,
        decoration: decoration,
      ),
      maxLines: maxLines,
      textAlign: textAlign,
      overflow: overflow,
    );
  }
}

class AppTextSubTitleBold extends StatelessWidget {
  final String data;
  final double? fontSize;
  final Color? color;
  final int? maxLines;
  final TextAlign? textAlign;
  final FontWeight? fontWeight;
  final TextOverflow? overflow;
  final TextDecoration? decoration;
  const AppTextSubTitleBold(
    this.data, {
    Key? key,
    this.fontSize,
    this.color,
    this.maxLines,
    this.textAlign,
    this.fontWeight,
    this.overflow,
    this.decoration,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      data,
      style: TextStyles.textStyleSubTitleBold(
        context,
        color: color,
        fontWeight: fontWeight,
        decoration: decoration,
      ),
      maxLines: maxLines,
      textAlign: textAlign,
      overflow: overflow,
    );
  }
}

class AppTextSubTitle extends StatelessWidget {
  final String data;
  final double? fontSize;
  final Color? color;
  final int? maxLines;
  final TextAlign? textAlign;
  final FontWeight? fontWeight;
  final TextOverflow? overflow;
  final TextDecoration? decoration;
  const AppTextSubTitle(
    this.data, {
    Key? key,
    this.fontSize,
    this.color,
    this.maxLines,
    this.textAlign,
    this.fontWeight,
    this.overflow,
    this.decoration,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      data,
      style: TextStyles.textStyleSubTitle(
        context,
        color: color,
        fontWeight: fontWeight,
        decoration: decoration,
      ),
      maxLines: maxLines,
      textAlign: textAlign,
      overflow: overflow,
    );
  }
}

class AppTextRegularBold extends StatelessWidget {
  final String data;
  final double? fontSize;
  final Color? color;
  final int? maxLines;
  final TextAlign? textAlign;
  final FontWeight? fontWeight;
  final TextOverflow? overflow;
  final TextDecoration? decoration;
  const AppTextRegularBold(
    this.data, {
    Key? key,
    this.fontSize,
    this.color,
    this.maxLines,
    this.textAlign,
    this.fontWeight,
    this.overflow,
    this.decoration,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      data,
      style: TextStyles.textStyleRegularBold(
        context,
        color: color,
        fontWeight: fontWeight,
        decoration: decoration,
      ),
      maxLines: maxLines,
      textAlign: textAlign,
      overflow: overflow,
    );
  }
}

class AppTextRegular extends StatelessWidget {
  final String data;
  final double? fontSize;
  final Color? color;
  final int? maxLines;
  final TextAlign? textAlign;
  final FontWeight? fontWeight;
  final TextOverflow? overflow;
  final TextDecoration? decoration;
  const AppTextRegular(
    this.data, {
    Key? key,
    this.fontSize,
    this.color,
    this.maxLines,
    this.textAlign,
    this.fontWeight,
    this.overflow,
    this.decoration,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      data,
      style: TextStyles.textStyleRegular(
        context,
        color: color,
        fontWeight: fontWeight,
        decoration: decoration,
      ),
      maxLines: maxLines,
      textAlign: textAlign,
      overflow: overflow,
    );
  }
}

class AppTextSmallBold extends StatelessWidget {
  final String data;
  final double? fontSize;
  final Color? color;
  final int? maxLines;
  final TextAlign? textAlign;
  final FontWeight? fontWeight;
  final TextOverflow? overflow;
  final TextDecoration? decoration;
  const AppTextSmallBold(
    this.data, {
    Key? key,
    this.fontSize,
    this.color,
    this.maxLines,
    this.textAlign,
    this.fontWeight,
    this.overflow,
    this.decoration,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      data,
      style: TextStyles.textStyleSmallBold(
        context,
        color: color,
        fontWeight: fontWeight,
        decoration: decoration,
      ),
      maxLines: maxLines,
      textAlign: textAlign,
      overflow: overflow,
    );
  }
}

class AppTextSmall extends StatelessWidget {
  final String data;
  final double? fontSize;
  final Color? color;
  final int? maxLines;
  final TextAlign? textAlign;
  final FontWeight? fontWeight;
  final TextOverflow? overflow;
  final TextDecoration? decoration;
  const AppTextSmall(
    this.data, {
    Key? key,
    this.fontSize,
    this.color,
    this.maxLines,
    this.textAlign,
    this.fontWeight,
    this.overflow,
    this.decoration,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      data,
      style: TextStyles.textStyleSmall(
        context,
        color: color,
        fontWeight: fontWeight,
        decoration: decoration,
      ),
      maxLines: maxLines,
      textAlign: textAlign,
      overflow: overflow,
    );
  }
}
