// ignore_for_file: always_use_package_imports

import 'package:flutter/material.dart';

import 'text.dart';

class Appbar extends StatelessWidget implements PreferredSizeWidget {
  final Widget? title;
  final List<Widget>? actions;
  final String? titleStr;
  final bool centerTitle;
  final Widget? leading;
  final Color? backgroundColor;

  const Appbar({
    Key? key,
    this.title,
    this.titleStr,
    this.actions,
    this.centerTitle = true,
    this.leading,
    this.backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return AppBar(
      iconTheme: IconThemeData(
        color: theme.primaryColor,
      ),
      backgroundColor: theme.canvasColor,
      elevation: 0.5,
      // toolbarHeight: 230,
      leading: leading,
      //  ?? IconButton(onPressed: () {}, icon: Icon(Icons.arrow_back_ios_new)),

      // flexibleSpace: ClipPath(
      //   clipper: Customshape(),
      //   child: Container(
      //     height: 300,
      //     width: MediaQuery.of(context).size.width,
      //     color: Colors.red,
      //   ),
      // ),
      centerTitle: true,
      title: AppTextTitleBold(
        titleStr ?? '',
        color: theme.primaryColor,
      ),
      actions: actions,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}

// class Customshape extends CustomClipper<Path> {
//   @override
//   Path getClip(Size size) {
//     double height = size.height;
//     double width = size.width;
//     var path = Path();
//     path.lineTo(0, height - 50);
//     path.quadraticBezierTo(width / 2, height, width, height - 50);
//     path.lineTo(width, 0);
//     path.close();
//     return path;
//   }

//   @override
//   bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
//     // TODO: implement shouldReclip
//     return true;
//   }
// }
