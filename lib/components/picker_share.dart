// import 'package:flutter/cupertino.dart';
// import 'package:flutter/foundation.dart';
// // import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
// import 'package:share/share.dart';

// import '../imports.dart';
// import '../modules/feeds/data/posts.dart';
// import '../modules/feeds/models/post.dart';

// Future pickShareType(BuildContext context, Post post, String id) =>
//     showMaterialModalBottomSheet(
//       context: context,
//       builder: (_) => SharePickerWidget(post: post),
//     );

// class SharePickerWidget extends StatelessWidget {
//   final Post? post;
//   final BuildContext? context;
//   final String? id;
//   const SharePickerWidget({
//     Key? key,
//     this.post,
//     this.context,
//     this.id,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       decoration: BoxDecoration(
//           borderRadius: BorderRadius.only(
//         topLeft: Radius.circular(50),
//         topRight: Radius.circular(50),
//       )),
//       padding: EdgeInsets.fromLTRB(16, 24, 16, 24),
//       child: Column(
//         mainAxisSize: MainAxisSize.min,
//         children: [
//           _AttachmentBtn(
//             text: t.ShareExternally,
//             icon: Icons.share_rounded,
//             onPressed: () {
//               // print('shareeeeeeeee');
//               // print(post!.audioFile!.path);
//               _onShareExternally(context);
//             },
//           ),
//           _AttachmentBtn(
//               icon: Icons.share,
//               text: t.ShareOnYourWall,
//               onPressed: () {
//                 PostsRepository.sharePostOnWall(post!);
//               }),
//         ],
//       ),
//     );
//   }

//   // Future _onShareOnYourTimeline() async {
//   //   final List<String?> sharedByList = post!.sharedBy;
//   //   sharedByList.add(id);
//   //   post!.copyWith(sharedBy: sharedByList);
//   // }

//   Future _onShareExternally(BuildContext context) async {
//     // A builder is used to retrieve the context immediately
//     // surrounding the ElevatedButton.
//     //
//     // The context's `findRenderObject` returns the first
//     // RenderObject in its descendent tree when it's not
//     // a RenderObjectWidget. The ElevatedButton's RenderObject
//     // has its position and size after it's built.
//     // final RenderBox box = context.findRenderObject() as RenderBox;

//     if (post != null) {
//       await Share.share(
//         post!.audioFile!.path,
//         subject: '',
//       );
//     } else {
//       await Share.share(
//         '',
//         subject: '',
//         // sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size
//       );
//     }
//   }
// }

// class _AttachmentBtn extends StatelessWidget {
//   final IconData icon;
//   final VoidCallback onPressed;
//   final String text;
//   const _AttachmentBtn({
//     Key? key,
//     required this.icon,
//     required this.onPressed,
//     required this.text,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return MaterialButton(
//       padding: EdgeInsets.fromLTRB(0, 8, 8, 0),
//       onPressed: onPressed,
//       child: Row(
//         children: <Widget>[
//           Icon(icon),
//           Padding(
//             padding: EdgeInsets.only(left: 32),
//             child: AppTextSubTitle(
//               text,
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
