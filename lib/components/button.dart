// ignore_for_file: always_use_package_imports

import 'package:flutter/material.dart';

import 'text.dart';

class AppButton extends StatelessWidget {
  final String text;
  final Color? color;
  final Color? backgroundColor;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final bool? isLoading;
  final VoidCallback? onTap;
  final EdgeInsets contentPadding;
  final double width;
  final Color borderColor;
  //final Color? textColor;
  final double borderRadius;
  const AppButton(
    this.text, {
    Key? key,
    this.color,
    this.backgroundColor,
    this.onTap,
    this.isLoading = false,
    this.prefixIcon,
    this.suffixIcon,
    this.contentPadding =
        const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
    this.width = 300,
    this.borderColor = Colors.transparent,
    //this.textColor ,
    this.borderRadius = 10,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      padding: contentPadding,
      height: 60,
      width: isLoading! ? 150 : width,
      child: ElevatedButton(
        onPressed:
            onTap == null ? null : () => isLoading! ? null : onTap?.call(),
        style: ElevatedButton.styleFrom(
          elevation: 1,
          primary: backgroundColor ?? theme.primaryColor,
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: borderColor,
            ),
            borderRadius: BorderRadius.circular(borderRadius),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (isLoading!)
              SizedBox.fromSize(
                size: Size.fromRadius(12),
                child: CircularProgressIndicator(
                  color: theme.iconTheme.color,
                  valueColor: AlwaysStoppedAnimation(
                    theme.iconTheme.color,
                  ),
                ),
              )
            else ...[
              if (prefixIcon != null) ...[prefixIcon!, SizedBox(width: 10)],
              Flexible(
                child: AppTextSubTitleBold(
                  text,
                  color: theme.iconTheme.color,
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              if (suffixIcon != null) suffixIcon!,
            ],
          ],
        ),
      ),
    );
  }
}
