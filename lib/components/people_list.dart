// ignore_for_file: always_use_package_imports

import '../imports.dart';
import 'appbar_icon.dart';
import 'user_list.dart';

class PeopleListPage extends StatelessWidget {
  final List<String> iDs;
  final String title;

  const PeopleListPage(
    this.iDs,
    this.title, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final theme = Theme.of(context);
    return Scaffold(
      appBar: Appbar(
        titleStr: title,
        actions: const [
          AppBarIcon(Icons.group_sharp),
        ],
      ),
      body: UsersList(usersId: iDs),
    );
  }
}
