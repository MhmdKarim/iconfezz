import 'package:flutter/material.dart';

class AppBarIcon extends StatelessWidget {
  final IconData iconData;
  const AppBarIcon(this.iconData);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Icon(
        iconData,
        size: 25,
        color: theme.iconTheme.color,
      ),
    );
  }
}
