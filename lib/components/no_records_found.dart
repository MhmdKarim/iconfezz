// ignore_for_file: always_use_package_imports

import 'package:flutter/material.dart';

import 'text.dart';

class NoRecordsFound extends StatefulWidget {
  const NoRecordsFound({Key? key}) : super(key: key);

  @override
  State<NoRecordsFound> createState() => _NoRecordsFoundState();
}

class _NoRecordsFoundState extends State<NoRecordsFound> {
  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height * 2 / 3;
    final double width = MediaQuery.of(context).size.width;
    return SizedBox(
      height: height,
      width: width,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Icon(
              Icons.arrow_downward,
              size: 60,
            ),
            SizedBox(
              height: 20,
            ),
            AppTextSubTitleBold(
              'Nothing Found.\n\n Pull down to refresh',
              textAlign: TextAlign.center,
            ), //mkmkmk
          ],
        ),
      ),
    );
  }
}
