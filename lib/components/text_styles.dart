// ignore_for_file: always_use_package_imports

import 'package:google_fonts/google_fonts.dart';
import '../imports.dart';

// const appFont = GoogleFonts.robotoCondensed;
const appFont = GoogleFonts.barlowCondensed;

// ignore: avoid_classes_with_only_static_members
class TextStyles {
  static TextStyle textStyleTitleBold(
    BuildContext context, {
    Color? color,
    FontWeight? fontWeight,
    TextDecoration? decoration,
  }) {
    return appFont(
      fontSize: Theme.of(context).textTheme.headline6!.fontSize,
      fontWeight: FontWeight.bold,
      color: color,
      decoration: decoration,
    );
  }

  static TextStyle textStyleTitle(
    BuildContext context, {
    Color? color,
    FontWeight? fontWeight,
    TextDecoration? decoration,
  }) {
    return appFont(
      fontSize: Theme.of(context).textTheme.headline6!.fontSize,
      color: color,
      fontWeight: fontWeight,
      decoration: decoration,
    );
  }

  static TextStyle textStyleSubTitleBold(
    BuildContext context, {
    Color? color,
    FontWeight? fontWeight,
    TextDecoration? decoration,
  }) {
    return appFont(
      fontSize: Theme.of(context).textTheme.subtitle1!.fontSize,
      fontWeight: FontWeight.bold,
      color: color,
      decoration: decoration,
    );
  }

  static TextStyle textStyleSubTitle(
    BuildContext context, {
    Color? color,
    FontWeight? fontWeight,
    TextDecoration? decoration,
  }) {
    return appFont(
      fontSize: Theme.of(context).textTheme.subtitle1!.fontSize,
      color: color,
      fontWeight: fontWeight,
      decoration: decoration,
    );
  }

  static TextStyle textStyleRegularBold(
    BuildContext context, {
    Color? color,
    FontWeight? fontWeight,
    TextDecoration? decoration,
  }) {
    return appFont(
      fontSize: Theme.of(context).textTheme.bodyText2!.fontSize,
      fontWeight: FontWeight.bold,
      color: color,
      decoration: decoration,
    );
  }

  static TextStyle textStyleRegular(
    BuildContext context, {
    Color? color,
    FontWeight? fontWeight,
    TextDecoration? decoration,
  }) {
    return appFont(
      fontSize: Theme.of(context).textTheme.bodyText2!.fontSize,
      color: color,
      fontWeight: fontWeight,
      decoration: decoration,
    );
  }

  static TextStyle textStyleSmallBold(
    BuildContext context, {
    Color? color,
    FontWeight? fontWeight,
    TextDecoration? decoration,
  }) {
    return appFont(
      fontSize: Theme.of(context).textTheme.caption!.fontSize,
      color: color,
      fontWeight: FontWeight.bold,
      decoration: decoration,
    );
  }

  static TextStyle textStyleSmall(
    BuildContext context, {
    Color? color,
    FontWeight? fontWeight,
    TextDecoration? decoration,
  }) {
    return appFont(
      fontSize: Theme.of(context).textTheme.caption!.fontSize,
      color: color,
      fontWeight: fontWeight,
      decoration: decoration,
    );
  }
}
