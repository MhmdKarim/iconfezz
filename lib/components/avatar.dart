// ignore_for_file: always_use_package_imports

import 'package:badges/badges.dart';

import '../imports.dart';

class AvatarWidget extends StatelessWidget {
  final String photo;
  final double radius;
  final bool? showBadge;
  final bool isLoading;
  final VoidCallback? onTap;
  final String? errorAsset;
  final Color? badgeColor;
  final Widget? badgeContent;

  const AvatarWidget(
    this.photo, {
    Key? key,
    this.radius = 70,
    this.showBadge,
    this.isLoading = false,
    this.onTap,
    this.errorAsset,
    this.badgeColor,
    this.badgeContent,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Badge(
      showBadge: showBadge ?? false,
      badgeColor: Colors.green,
      padding: EdgeInsets.all(0),
      badgeContent: badgeContent ??
          CircleAvatar(
            backgroundColor: badgeColor ?? Colors.green,
            radius: radius * 0.15,
          ),
      position: BadgePosition(bottom: 0, end: 0),
      child: ClipOval(
        child: Container(
          width: radius,
          height: radius,
          color: context.theme.primaryColor,
          child: GestureDetector(
            onTap: onTap,
            child: AppImage(
              ImageModel(photo, height: radius, width: radius),
              errorAsset: errorAsset ?? Assets.images.avatar.path,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}
