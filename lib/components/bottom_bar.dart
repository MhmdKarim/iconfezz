// ignore_for_file: always_use_package_imports

import 'package:flutter/services.dart';

import '../imports.dart';
import '../modules/auth/pages/profile/index.dart';
import '../modules/feeds/pages/posts/feed.dart';
import '../modules/notifications/pages/index.dart';
import '../modules/settings/settings.dart';
import 'confirm_dialog.dart';

class BottomBar extends StatefulWidget {
  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  int _selectedPage = 0;
  List<Widget> pageList = <Widget>[];

  @override
  void initState() {
    pageList.add(FeedPage());
    pageList.add(NotificationScreen());
    pageList.add(ProfilePage());
    // pageList.add(GroupsListPage());
    pageList.add(SettingsPage());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return WillPopScope(
      onWillPop: () async {
        throw showConfirmDialog(
          context,
          title: '${t.AreYouSure} ${t.exit}',
          onConfirm: () {
            Navigator.of(context).pop();
            SystemNavigator.pop();
          },
        );
      },
      child: Scaffold(
        body: IndexedStack(
          index: _selectedPage,
          children: pageList,
        ),
        bottomNavigationBar: BottomNavigationBar(
          elevation: 2,
          selectedLabelStyle:
              appFont(fontSize: theme.textTheme.caption!.fontSize),
          type: BottomNavigationBarType.fixed,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: t.Home,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.notifications),
              label: t.Notifications,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: t.Profile,
            ),
            // BottomNavigationBarItem(
            //   icon: Icon(Icons.group),
            //   label: 'Groups',
            // ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              label: t.Settings,
            ),
          ],
          currentIndex: _selectedPage,
          selectedItemColor: theme.primaryColor,
          // backgroundColor: theme.primaryColor,
          onTap: _onItemTapped,
        ), // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedPage = index;
    });
  }
}
