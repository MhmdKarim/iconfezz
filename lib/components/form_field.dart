// ignore_for_file: always_use_package_imports

import 'package:flutter/services.dart';
import '../../../../imports.dart';

class AppTextFormField extends StatelessWidget {
  final String? label;
  final TextEditingController? controller;
  final String? initialValue;
  final bool isObscure;
  final IconData? icon;
  final Widget? trailing;
  final String? Function(String?)? validator;
  final ValueChanged<String?>? onSave;
  final TextInputType? keyboardType;
  final double borderRadius;
  final int? maxLength;
  final String? helperText;
  final String? errorText;
  final double? labelSize;
  final List<TextInputFormatter>? formatters;
  final EdgeInsets? margin;
  final Function(String)? onChanged;
  final int? maxLines;
  final double? padding;

  const AppTextFormField({
    Key? key,
    this.label,
    this.controller,
    this.initialValue,
    this.isObscure = false,
    this.icon,
    this.trailing,
    this.validator,
    this.onSave,
    this.keyboardType,
    this.borderRadius = 10,
    this.maxLength,
    this.helperText,
    this.errorText,
    this.labelSize,
    this.formatters,
    this.margin,
    this.onChanged,
    this.maxLines,
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Container(
      padding: margin ?? const EdgeInsets.all(8),
      constraints: BoxConstraints(
        maxWidth: 400,
      ),
      child: Theme(
        data: ThemeData(
          textSelectionTheme: TextSelectionThemeData(
            cursorColor: theme.primaryColor,
            selectionColor: theme.primaryColor,
            selectionHandleColor: theme.primaryColor,
          ),
        ),

        // Theme.of(context).copyWith(
        //   textSelectionTheme: TextSelectionThemeData(
        //     selectionColor: theme.primaryColor,
        //     selectionHandleColor: theme.primaryColor,
        //     // cursorColor: theme.primaryColor,
        //   ),
        // ),
        child: TextFormField(
          maxLines: maxLines ?? 1,
          cursorColor: theme.primaryColor,
          initialValue: controller == null ? initialValue : null,
          controller: controller,
          autofocus: true,
          style: theme.textTheme.subtitle1,
          obscureText: isObscure,
          validator: validator,
          keyboardType: keyboardType,
          maxLength: maxLength,
          onFieldSubmitted: onSave,
          onSaved: onSave,
          inputFormatters: formatters,
          cursorHeight: 25,
          // enableInteractiveSelection: false,
          decoration: InputDecoration(
            counterStyle: TextStyle(fontSize: 12, color: Colors.grey),
            filled: true,
            errorStyle: theme.textTheme.caption!.copyWith(color: Colors.red),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(borderRadius),
              borderSide: BorderSide(color: theme.primaryColor),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(borderRadius),
              borderSide: BorderSide(color: theme.primaryColor),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(borderRadius),
              borderSide: BorderSide(color: theme.primaryColor),
            ),
            contentPadding:
                const EdgeInsets.symmetric(horizontal: 20, vertical: 12),
            labelText: label,
            labelStyle:
                TextStyles.textStyleRegular(context, color: theme.primaryColor),
            helperText: helperText,
            errorText: errorText,
            prefixIcon: icon == null
                ? null
                : Padding(
                    padding: EdgeInsets.only(left: 4, bottom: padding ?? 0),
                    child: Icon(
                      icon,
                      color: theme.primaryColor,
                    ),
                  ),
            suffixIcon: trailing == null
                ? null
                : Padding(padding: EdgeInsets.only(right: 12), child: trailing),
          ),
          onChanged: onChanged,
        ),
      ),
    );
  }
}
