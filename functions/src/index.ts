import * as admin from 'firebase-admin'

admin.initializeApp()
admin.firestore().settings({ ignoreUndefinedProperties: true })

import { handleChatMsg } from './chat_msg'
import { handleGroupMsg } from './group_msg'
import { editProfile } from './profile_edit'
import { banUser } from './ban_user'
import { onEmailSignup } from './on_signup'

export { handleChatMsg, handleGroupMsg, editProfile, banUser, onEmailSignup }
